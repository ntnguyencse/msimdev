//----------------------------------------------------
// Filename     : main2.cpp
// DATE         : 17 Nov 2019
// Description  : main 
//----------------------------------------------------
// Rotated preview scenario
//----------------------------------------------------
// MST0: Camera write (rotation)
// MST3: Display read (raster-scan)
//
// MST5: Background   (DMA) optional
//----------------------------------------------------
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <iostream>

#include "CMST.h"
#include "CSLV.h"
#include "CMMU.h"
#include "CBUS.h"

// struct timespec {
//	time_t	tv_sec;		/* seconds */
//	long	tv_nsec;	/* nanoseconds */
// };

int main() {

	struct timespec Time_SimStart;
	struct timespec Time_SimFinish;
	
	clock_gettime(CLOCK_MONOTONIC, &Time_SimStart);


	int64_t nCycle = 1;
	
	//------------------------------------
	// 1. Generate and initialize
	//------------------------------------
	CPMST	cpMST0	= new CMST("MST0");
	CPMST	cpMST1	= new CMST("MST1");
	CPMST	cpMST2	= new CMST("MST2");
	CPMST	cpMST3	= new CMST("MST3");

	CPMST	cpMST4	= new CMST("MST4");		// Background traffic. No system MMU 
	CPMST	cpMST5	= new CMST("MST5");		// Background DMA traffic. No system MMU

	CPMMU	cpMMU0	= new CMMU("MMU0");
	CPMMU	cpMMU1	= new CMMU("MMU1");
	CPMMU	cpMMU2	= new CMMU("MMU2");
	CPMMU	cpMMU3	= new CMMU("MMU3");
	CPBUS	cpBUS	= new CBUS("BUS", 6);;
	CPSLV	cpSLV	= new CSLV("SLV");

	//------------------------------------
	// 2. Link TRx topology
	//------------------------------------
	// MST0 and MMU0 
	cpMST0->cpTx_AR->SetPair(cpMMU0->cpRx_AR);
	cpMST0->cpTx_AW->SetPair(cpMMU0->cpRx_AW);
	cpMST0->cpTx_W->SetPair(cpMMU0->cpRx_W);
	cpMMU0->cpTx_R->SetPair(cpMST0->cpRx_R);
	cpMMU0->cpTx_B->SetPair(cpMST0->cpRx_B);

	// MST1 and MMU1 
	cpMST1->cpTx_AR->SetPair(cpMMU1->cpRx_AR);
	cpMST1->cpTx_AW->SetPair(cpMMU1->cpRx_AW);
	cpMST1->cpTx_W->SetPair(cpMMU1->cpRx_W);
	cpMMU1->cpTx_R->SetPair(cpMST1->cpRx_R);
	cpMMU1->cpTx_B->SetPair(cpMST1->cpRx_B);

	// MST2 and MMU2 
	cpMST2->cpTx_AR->SetPair(cpMMU2->cpRx_AR);
	cpMST2->cpTx_AW->SetPair(cpMMU2->cpRx_AW);
	cpMST2->cpTx_W->SetPair(cpMMU2->cpRx_W);
	cpMMU2->cpTx_R->SetPair(cpMST2->cpRx_R);
	cpMMU2->cpTx_B->SetPair(cpMST2->cpRx_B);

	// MST3 and MMU3 
	cpMST3->cpTx_AR->SetPair(cpMMU3->cpRx_AR);
	cpMST3->cpTx_AW->SetPair(cpMMU3->cpRx_AW);
	cpMST3->cpTx_W->SetPair(cpMMU3->cpRx_W);
	cpMMU3->cpTx_R->SetPair(cpMST3->cpRx_R);
	cpMMU3->cpTx_B->SetPair(cpMST3->cpRx_B);

	// MMU0 and BUS
	cpMMU0->cpTx_AR->SetPair(cpBUS->cpRx_AR[0]);
	cpMMU0->cpTx_AW->SetPair(cpBUS->cpRx_AW[0]);
	cpMMU0->cpTx_W->SetPair(cpBUS->cpRx_W[0]);
	cpBUS->cpTx_R[0]->SetPair(cpMMU0->cpRx_R);
	cpBUS->cpTx_B[0]->SetPair(cpMMU0->cpRx_B);
	
	// MMU1 and BUS 
	cpMMU1->cpTx_AR->SetPair(cpBUS->cpRx_AR[1]);
	cpMMU1->cpTx_AW->SetPair(cpBUS->cpRx_AW[1]);
	cpMMU1->cpTx_W->SetPair(cpBUS->cpRx_W[1]);
	cpBUS->cpTx_R[1]->SetPair(cpMMU1->cpRx_R);
	cpBUS->cpTx_B[1]->SetPair(cpMMU1->cpRx_B);
	
	// MMU2 and BUS 
	cpMMU2->cpTx_AR->SetPair(cpBUS->cpRx_AR[2]);
	cpMMU2->cpTx_AW->SetPair(cpBUS->cpRx_AW[2]);
	cpMMU2->cpTx_W->SetPair(cpBUS->cpRx_W[2]);
	cpBUS->cpTx_R[2]->SetPair(cpMMU2->cpRx_R);
	cpBUS->cpTx_B[2]->SetPair(cpMMU2->cpRx_B);
	
	// MMU3 and BUS 
	cpMMU3->cpTx_AR->SetPair(cpBUS->cpRx_AR[3]);
	cpMMU3->cpTx_AW->SetPair(cpBUS->cpRx_AW[3]);
	cpMMU3->cpTx_W->SetPair(cpBUS->cpRx_W[3]);
	cpBUS->cpTx_R[3]->SetPair(cpMMU3->cpRx_R);
	cpBUS->cpTx_B[3]->SetPair(cpMMU3->cpRx_B);

	
	// MST4 and BUS 
	cpMST4->cpTx_AR->SetPair(cpBUS->cpRx_AR[4]);
	cpMST4->cpTx_AW->SetPair(cpBUS->cpRx_AW[4]);
	cpMST4->cpTx_W->SetPair(cpBUS->cpRx_W[4]);
	cpBUS->cpTx_R[4]->SetPair(cpMST4->cpRx_R);
	cpBUS->cpTx_B[4]->SetPair(cpMST4->cpRx_B);
	
	// MST5 and BUS 
	cpMST5->cpTx_AR->SetPair(cpBUS->cpRx_AR[5]);
	cpMST5->cpTx_AW->SetPair(cpBUS->cpRx_AW[5]);
	cpMST5->cpTx_W->SetPair(cpBUS->cpRx_W[5]);
	cpBUS->cpTx_R[5]->SetPair(cpMST5->cpRx_R);
	cpBUS->cpTx_B[5]->SetPair(cpMST5->cpRx_B);
	
	
	// BUS and SLV 
	cpBUS->cpTx_AR->SetPair(cpSLV->cpRx_AR);
	cpBUS->cpTx_AW->SetPair(cpSLV->cpRx_AW);
	cpBUS->cpTx_W->SetPair(cpSLV->cpRx_W);
	cpSLV->cpTx_R->SetPair(cpBUS->cpRx_R);
	cpSLV->cpTx_B->SetPair(cpBUS->cpRx_B);


	// Debug
	cpMST0->CheckLink();
	cpMST1->CheckLink();
	cpMST2->CheckLink();
	cpMST3->CheckLink();
	cpMST4->CheckLink();
	cpMST5->CheckLink();
	cpMMU0->CheckLink();
	cpMMU1->CheckLink();
	cpMMU2->CheckLink();
	cpMMU3->CheckLink();
	cpBUS->CheckLink();
	cpSLV->CheckLink();


	//------------------------------
	// Set total Ax NUM 
	//------------------------------
	int NUM = IMG_HORIZONTAL_SIZE*IMG_VERTICAL_SIZE*BYTE_PER_PIXEL/MAX_TRANS_SIZE;

	// Number of transactions
	cpMST0->Set_nAR_GEN_NUM(0);
	cpMST0->Set_nAW_GEN_NUM(NUM);

	cpMST1->Set_nAR_GEN_NUM(0);
	cpMST1->Set_nAW_GEN_NUM(0);

	cpMST2->Set_nAR_GEN_NUM(0);
	cpMST2->Set_nAW_GEN_NUM(0);

	cpMST3->Set_nAR_GEN_NUM(NUM);
	cpMST3->Set_nAW_GEN_NUM(0);

	cpMST4->Set_nAR_GEN_NUM(0);
	cpMST4->Set_nAW_GEN_NUM(0);

	#ifdef DMA_ENABLE
	cpMST5->Set_nAR_GEN_NUM(NUM);
	cpMST5->Set_nAW_GEN_NUM(NUM);
	#else
	cpMST5->Set_nAR_GEN_NUM(0);
	cpMST5->Set_nAW_GEN_NUM(0);
	#endif


	//------------------------------
	// Set image scaling
	//------------------------------
	cpMST0->Set_ScalingFactor(1);
	cpMST1->Set_ScalingFactor(1);
	cpMST2->Set_ScalingFactor(1);
	cpMST3->Set_ScalingFactor(1);
	cpMST4->Set_ScalingFactor(1);
	cpMST5->Set_ScalingFactor(1);


	//------------------------------
	// Set start address MST
	//------------------------------
	cpMST0->Set_nAR_START_ADDR(0x10000000);
	cpMST0->Set_nAW_START_ADDR(0x20000000);

	cpMST1->Set_nAR_START_ADDR(0x30000000);
	cpMST1->Set_nAW_START_ADDR(0x40000000);

	cpMST2->Set_nAR_START_ADDR(0x50000000);
	cpMST2->Set_nAW_START_ADDR(0x60000000);

	cpMST3->Set_nAR_START_ADDR(0x70000000);
	cpMST3->Set_nAW_START_ADDR(0x80000000);

	cpMST4->Set_nAR_START_ADDR(0x90000000);
	cpMST4->Set_nAW_START_ADDR(0xA0000000);

	cpMST5->Set_nAR_START_ADDR(0xB0000000);
	cpMST5->Set_nAW_START_ADDR(0xC0000000);



	//------------------------------
	// Set start address MMU
	//------------------------------
	cpMMU0->Set_nAR_START_ADDR(0x10000000);
	cpMMU0->Set_nAW_START_ADDR(0x20000000);

	cpMMU1->Set_nAR_START_ADDR(0x30000000);
	cpMMU1->Set_nAW_START_ADDR(0x40000000);

	cpMMU2->Set_nAR_START_ADDR(0x50000000);
	cpMMU2->Set_nAW_START_ADDR(0x60000000);

	cpMMU3->Set_nAR_START_ADDR(0x70000000);
	cpMMU3->Set_nAW_START_ADDR(0x80000000);


	//------------------------------
	// Set operation 
	//------------------------------
	// RASTER_SCAN, ROTATION, RANDOM
	//------------------------------
	cpMST0->Set_AR_Operation("RASTER_SCAN");
	cpMST0->Set_AW_Operation("ROTATION");
	
	cpMST1->Set_AR_Operation("RASTER_SCAN");
	cpMST1->Set_AW_Operation("RASTER_SCAN");
	
	cpMST2->Set_AR_Operation("RASTER_SCAN");
	cpMST2->Set_AW_Operation("RASTER_SCAN");
	
	cpMST3->Set_AR_Operation("RASTER_SCAN");
	cpMST3->Set_AW_Operation("RASTER_SCAN");

	cpMST4->Set_AR_Operation("RANDOM");		// Background
	cpMST4->Set_AW_Operation("RANDOM");

	cpMST5->Set_AR_Operation("RASTER_SCAN");	// Background DMA
	cpMST5->Set_AW_Operation("RASTER_SCAN");


	//------------------------------
	// Set Ax issue interval cycles 
	//------------------------------
	cpMST0->Set_AR_ISSUE_MIN_INTERVAL(4);
	cpMST0->Set_AW_ISSUE_MIN_INTERVAL(4);

	cpMST1->Set_AR_ISSUE_MIN_INTERVAL(4);
	cpMST1->Set_AW_ISSUE_MIN_INTERVAL(4);

	cpMST2->Set_AR_ISSUE_MIN_INTERVAL(4);
	cpMST2->Set_AW_ISSUE_MIN_INTERVAL(4);

	cpMST3->Set_AR_ISSUE_MIN_INTERVAL(4);
	cpMST3->Set_AW_ISSUE_MIN_INTERVAL(4);

	cpMST4->Set_AR_ISSUE_MIN_INTERVAL(128);		// Background
	cpMST4->Set_AW_ISSUE_MIN_INTERVAL(128);

	cpMST5->Set_AR_ISSUE_MIN_INTERVAL(128);		// Background DMA. Cycles
	cpMST5->Set_AW_ISSUE_MIN_INTERVAL(128);


	//------------------------------
	// Set AddrMap
	//------------------------------
	string cAR_AddrMap;
	string cAW_AddrMap;
	
	#ifdef AR_LIAM
	cAR_AddrMap = "LIAM";
	#elif AR_BFAM
	cAR_AddrMap = "BFAM";
	#elif AR_TILE
	cAR_AddrMap = "TILE";
	#endif
	
	#ifdef AW_LIAM
	cAW_AddrMap = "LIAM";
	#elif AW_BFAM
	cAW_AddrMap = "BFAM";
	#elif AW_TILE
	cAW_AddrMap = "TILE";
	#endif



	// Simulate
	while (1) {

		//---------------------------
		// 3. Reset
		//---------------------------
		if (nCycle == 1) {
			cpMST0->Reset();
			cpMST1->Reset();
			cpMST2->Reset();
			cpMST3->Reset();
			cpMST4->Reset();
			cpMST5->Reset();
			cpMMU0->Reset();
			cpMMU1->Reset();
			cpMMU2->Reset();
			cpMMU3->Reset();
			cpBUS->Reset();
			cpSLV->Reset();

			//------------------------------------
			// Master geneates transactions (Debug)
			//------------------------------------
			// #ifdef AR_ADDR_GEN_TEST
			// cpMST0->LoadTransfer_AR_Test(nCycle);
			// cpMST1->LoadTransfer_AR_Test(nCycle);
			// cpMST2->LoadTransfer_AR_Test(nCycle);
			// cpMST3->LoadTransfer_AR_Test(nCycle);
			// #endif

			// #ifdef AW_ADDR_GEN_TEST
			// cpMST0->LoadTransfer_AW_Test(nCycle);
			// cpMST1->LoadTransfer_AW_Test(nCycle);
			// cpMST2->LoadTransfer_AW_Test(nCycle);
			// cpMST3->LoadTransfer_AW_Test(nCycle);
			// #endif
		};

		//---------------------------------------------
		// 4. Start simulation
		//---------------------------------------------

		// Master geneates transactions

		cpMST0->LoadTransfer_AR(nCycle, cAR_AddrMap, "RASTER_SCAN");
		cpMST0->LoadTransfer_AW(nCycle, cAW_AddrMap, "ROTATION");
		
		cpMST1->LoadTransfer_AR(nCycle, cAR_AddrMap, "RASTER_SCAN");
		cpMST1->LoadTransfer_AW(nCycle, cAW_AddrMap, "RASTER_SCAN");

		cpMST2->LoadTransfer_AR(nCycle, cAR_AddrMap, "RASTER_SCAN");
		cpMST2->LoadTransfer_AW(nCycle, cAW_AddrMap, "RASTER_SCAN");
		
		cpMST3->LoadTransfer_AR(nCycle, cAR_AddrMap, "RASTER_SCAN");
		cpMST3->LoadTransfer_AW(nCycle, cAW_AddrMap, "RASTER_SCAN");

		cpMST4->LoadTransfer_AR(nCycle, cAR_AddrMap, "RANDOM");		// Background
		cpMST4->LoadTransfer_AW(nCycle, cAW_AddrMap, "RANDOM");

		cpMST5->LoadTransfer_AR(nCycle, "LIAM",      "RASTER_SCAN");	// Background
		cpMST5->LoadTransfer_AW(nCycle, "LIAM",      "RASTER_SCAN");

		//---------------------------------
		// Propagate valid
		//---------------------------------

		//---------------------------------
		// AR, AW, W VALID
		//---------------------------------
		cpMST0->Do_AR_fwd(nCycle);
		cpMST0->Do_AW_fwd(nCycle);
		// cpMST0->Do_W_fwd(nCycle);

		cpMST1->Do_AR_fwd(nCycle);
		cpMST1->Do_AW_fwd(nCycle);
		// cpMST1->Do_W_fwd(nCycle);

		cpMST2->Do_AR_fwd(nCycle);
		cpMST2->Do_AW_fwd(nCycle);
		// cpMST2->Do_W_fwd(nCycle);

		cpMST3->Do_AR_fwd(nCycle);
		cpMST3->Do_AW_fwd(nCycle);
		// cpMST3->Do_W_fwd(nCycle);

		cpMST4->Do_AR_fwd(nCycle);
		cpMST4->Do_AW_fwd(nCycle);
		// cpMST4->Do_W_fwd(nCycle);

		cpMST5->Do_AR_fwd(nCycle);
		cpMST5->Do_AW_fwd(nCycle);
		// cpMST5->Do_W_fwd(nCycle);

		//--------------------------------
		#ifdef MMU_OFF	
		cpMMU0->Do_AR_fwd_MMU_OFF(nCycle);
		cpMMU0->Do_AW_fwd_MMU_OFF(nCycle); 
		// cpMMU0->Do_W_fwd_MMU_OFF(nCycle);
		#endif
		#ifdef MMU_ON
		#ifdef RMM_ENABLE 
		cpMMU0->Do_AR_fwd_SI_RMM(nCycle);
		cpMMU0->Do_AR_fwd_MI(nCycle);
		cpMMU0->Do_AW_fwd_RMM(nCycle);      
		// cpMMU0->Do_W_fwd_SI(nCycle);
		// cpMMU0->Do_W_fwd_MI(nCycle);
		#elif AT_ENABLE 
		cpMMU0->Do_AR_fwd_SI_AT(nCycle);
		cpMMU0->Do_AR_fwd_MI(nCycle);
		cpMMU0->Do_AW_fwd_AT(nCycle);      
		// cpMMU0->Do_W_fwd_SI(nCycle);
		// cpMMU0->Do_W_fwd_MI(nCycle);
		#else
		cpMMU0->Do_AR_fwd_SI(nCycle);
		cpMMU0->Do_AR_fwd_MI(nCycle);
		cpMMU0->Do_AW_fwd(nCycle);      
		// cpMMU0->Do_W_fwd_SI(nCycle);
		// cpMMU0->Do_W_fwd_MI(nCycle);
		#endif
		#endif


		#ifdef MMU_OFF	
		cpMMU1->Do_AR_fwd_MMU_OFF(nCycle);
		cpMMU1->Do_AW_fwd_MMU_OFF(nCycle); 
		// cpMMU1->Do_W_fwd_MMU_OFF(nCycle);
		#endif
		#ifdef MMU_ON
		#ifdef RMM_ENABLE 
		cpMMU1->Do_AR_fwd_SI_RMM(nCycle);
		cpMMU1->Do_AR_fwd_MI(nCycle);
		cpMMU1->Do_AW_fwd_RMM(nCycle);      
		// cpMMU1->Do_W_fwd_SI(nCycle);
		// cpMMU1->Do_W_fwd_MI(nCycle);
		#elif AT_ENABLE 
		cpMMU1->Do_AR_fwd_SI_AT(nCycle);
		cpMMU1->Do_AR_fwd_MI(nCycle);
		cpMMU1->Do_AW_fwd_AT(nCycle);      
		// cpMMU1->Do_W_fwd_SI(nCycle);
		// cpMMU1->Do_W_fwd_MI(nCycle);
		#else
		cpMMU1->Do_AR_fwd_SI(nCycle);
		cpMMU1->Do_AR_fwd_MI(nCycle);
		cpMMU1->Do_AW_fwd(nCycle);      
		// cpMMU1->Do_W_fwd_SI(nCycle);
		// cpMMU1->Do_W_fwd_MI(nCycle);
		#endif
		#endif


		#ifdef MMU_OFF	
		cpMMU2->Do_AR_fwd_MMU_OFF(nCycle);
		cpMMU2->Do_AW_fwd_MMU_OFF(nCycle); 
		// cpMMU2->Do_W_fwd_MMU_OFF(nCycle);
		#endif
		#ifdef MMU_ON
		#ifdef RMM_ENABLE 
		cpMMU2->Do_AR_fwd_SI_RMM(nCycle);
		cpMMU2->Do_AR_fwd_MI(nCycle);
		cpMMU2->Do_AW_fwd_RMM(nCycle);      
		// cpMMU2->Do_W_fwd_SI(nCycle);
		// cpMMU2->Do_W_fwd_MI(nCycle);
		#elif AT_ENABLE 
		cpMMU2->Do_AR_fwd_SI_AT(nCycle);
		cpMMU2->Do_AR_fwd_MI(nCycle);
		cpMMU2->Do_AW_fwd_AT(nCycle);      
		// cpMMU2->Do_W_fwd_SI(nCycle);
		// cpMMU2->Do_W_fwd_MI(nCycle);
		#else
		cpMMU2->Do_AR_fwd_SI(nCycle);
		cpMMU2->Do_AR_fwd_MI(nCycle);
		cpMMU2->Do_AW_fwd(nCycle);      
		// cpMMU2->Do_W_fwd_SI(nCycle);
		// cpMMU2->Do_W_fwd_MI(nCycle);
		#endif
		#endif


		#ifdef MMU_OFF	
		cpMMU3->Do_AR_fwd_MMU_OFF(nCycle);
		cpMMU3->Do_AW_fwd_MMU_OFF(nCycle); 
		// cpMMU3->Do_W_fwd_MMU_OFF(nCycle);
		#endif
		#ifdef MMU_ON
		#ifdef RMM_ENABLE 
		cpMMU3->Do_AR_fwd_SI_RMM(nCycle);
		cpMMU3->Do_AR_fwd_MI(nCycle);
		cpMMU3->Do_AW_fwd_RMM(nCycle);      
		// cpMMU3->Do_W_fwd_SI(nCycle);
		// cpMMU3->Do_W_fwd_MI(nCycle);
		#elif AT_ENABLE 
		cpMMU3->Do_AR_fwd_SI_AT(nCycle);
		cpMMU3->Do_AR_fwd_MI(nCycle);
		cpMMU3->Do_AW_fwd_AT(nCycle);      
		// cpMMU3->Do_W_fwd_SI(nCycle);
		// cpMMU3->Do_W_fwd_MI(nCycle);
		#else
		cpMMU3->Do_AR_fwd_SI(nCycle);
		cpMMU3->Do_AR_fwd_MI(nCycle);
		cpMMU3->Do_AW_fwd(nCycle);      
		// cpMMU3->Do_W_fwd_SI(nCycle);
		// cpMMU3->Do_W_fwd_MI(nCycle);
		#endif
		#endif

		//-------------------------------
		cpBUS->Do_AR_fwd(nCycle);
		cpBUS->Do_AW_fwd(nCycle);
		// cpBUS->Do_W_fwd(nCycle);


		//-------------------------------
		#ifdef IDEAL_MEMORY
		cpSLV->Do_AR_fwd(nCycle);
		#endif

		#ifdef MEMORY_CONTROLLER
		cpSLV->Do_AR_fwd_MC_Frontend(nCycle);
		cpSLV->Do_AR_fwd_MC_Backend_Response(nCycle);
		#endif

		#ifdef IDEAL_MEMORY
		cpSLV->Do_AW_fwd(nCycle);
		// cpSLV->Do_W_fwd(nCycle);
		#endif

		#ifdef MEMORY_CONTROLLER
		cpSLV->Do_AW_fwd_MC_Frontend(nCycle);
		// cpSLV->Do_W_fwd_MC_Frontend(nCycle);
		cpSLV->Do_AW_fwd_MC_Backend_Response(nCycle);
		#endif

		#ifdef MEMORY_CONTROLLER
		cpSLV->Do_Ax_fwd_MC_Backend_Request(nCycle);
		#endif


		//---------------------------------
		// R, B VALID
		//---------------------------------
		cpSLV->Do_R_fwd(nCycle);
		cpSLV->Do_B_fwd(nCycle);


		cpBUS->Do_R_fwd(nCycle);
		cpBUS->Do_B_fwd(nCycle);


		//---------------------------
		#ifdef MMU_OFF
		cpMMU0->Do_R_fwd_MMU_OFF(nCycle);
		cpMMU0->Do_B_fwd_MMU_OFF(nCycle);
		#endif
		#ifdef MMU_ON
		#ifdef RMM_ENABLE 
		cpMMU0->Do_R_fwd_RMM(nCycle);
		cpMMU0->Do_B_fwd(nCycle);
		#elif AT_ENABLE 
		cpMMU0->Do_R_fwd_AT(nCycle);
		cpMMU0->Do_B_fwd(nCycle);
		#else
		cpMMU0->Do_R_fwd(nCycle);
		cpMMU0->Do_B_fwd(nCycle);
		#endif
		#endif


		#ifdef MMU_OFF
		cpMMU1->Do_R_fwd_MMU_OFF(nCycle);
		cpMMU1->Do_B_fwd_MMU_OFF(nCycle);
		#endif
		#ifdef MMU_ON
		#ifdef RMM_ENABLE 
		cpMMU1->Do_R_fwd_RMM(nCycle);
		cpMMU1->Do_B_fwd(nCycle);
		#elif AT_ENABLE 
		cpMMU1->Do_R_fwd_AT(nCycle);
		cpMMU1->Do_B_fwd(nCycle);
		#else
		cpMMU1->Do_R_fwd(nCycle);
		cpMMU1->Do_B_fwd(nCycle);
		#endif
		#endif


		#ifdef MMU_OFF
		cpMMU2->Do_R_fwd_MMU_OFF(nCycle);
		cpMMU2->Do_B_fwd_MMU_OFF(nCycle);
		#endif
		#ifdef MMU_ON
		#ifdef RMM_ENABLE 
		cpMMU2->Do_R_fwd_RMM(nCycle);
		cpMMU2->Do_B_fwd(nCycle);
		#elif AT_ENABLE 
		cpMMU2->Do_R_fwd_AT(nCycle);
		cpMMU2->Do_B_fwd(nCycle);
		#else
		cpMMU2->Do_R_fwd(nCycle);
		cpMMU2->Do_B_fwd(nCycle);
		#endif
		#endif


		#ifdef MMU_OFF
		cpMMU3->Do_R_fwd_MMU_OFF(nCycle);
		cpMMU3->Do_B_fwd_MMU_OFF(nCycle);
		#endif
		#ifdef MMU_ON
		#ifdef RMM_ENABLE 
		cpMMU3->Do_R_fwd_RMM(nCycle);
		cpMMU3->Do_B_fwd(nCycle);
		#elif AT_ENABLE 
		cpMMU3->Do_R_fwd_AT(nCycle);
		cpMMU3->Do_B_fwd(nCycle);
		#else
		cpMMU3->Do_R_fwd(nCycle);
		cpMMU3->Do_B_fwd(nCycle);
		#endif
		#endif


		//---------------------------
		cpMST0->Do_R_fwd(nCycle);
		cpMST0->Do_B_fwd(nCycle);

		cpMST1->Do_R_fwd(nCycle);
		cpMST1->Do_B_fwd(nCycle);

		cpMST2->Do_R_fwd(nCycle);
		cpMST2->Do_B_fwd(nCycle);

		cpMST3->Do_R_fwd(nCycle);
		cpMST3->Do_B_fwd(nCycle);

		cpMST4->Do_R_fwd(nCycle);
		cpMST4->Do_B_fwd(nCycle);

		cpMST5->Do_R_fwd(nCycle);
		cpMST5->Do_B_fwd(nCycle);


		//---------------------------------
		// Propagate ready
		//---------------------------------

		//---------------------------------
		// AR, AW, W READY
		//---------------------------------
		cpSLV->Do_AR_bwd(nCycle);
		cpSLV->Do_AW_bwd(nCycle);
		// cpSLV->Do_W_bwd(nCycle);


		cpBUS->Do_AR_bwd(nCycle);
		cpBUS->Do_AW_bwd(nCycle);
		// cpBUS->Do_W_bwd(nCycle);


		#ifdef MMU_OFF
		cpMMU0->Do_AR_bwd_MMU_OFF(nCycle);
		cpMMU0->Do_AW_bwd_MMU_OFF(nCycle);
		// cpMMU0->Do_W_bwd_MMU_OFF(nCycle);
		#endif
		#ifdef MMU_ON
		cpMMU0->Do_AR_bwd(nCycle);
		cpMMU0->Do_AW_bwd(nCycle);
		// cpMMU0->Do_W_bwd(nCycle);
		#endif

		#ifdef MMU_OFF
		cpMMU1->Do_AR_bwd_MMU_OFF(nCycle);
		cpMMU1->Do_AW_bwd_MMU_OFF(nCycle);
		// cpMMU1->Do_W_bwd_MMU_OFF(nCycle);
		#endif
		#ifdef MMU_ON
		cpMMU1->Do_AR_bwd(nCycle);
		cpMMU1->Do_AW_bwd(nCycle);
		// cpMMU1->Do_W_bwd(nCycle);
		#endif

		#ifdef MMU_OFF
		cpMMU2->Do_AR_bwd_MMU_OFF(nCycle);
		cpMMU2->Do_AW_bwd_MMU_OFF(nCycle);
		// cpMMU2->Do_W_bwd_MMU_OFF(nCycle);
		#endif
		#ifdef MMU_ON
		cpMMU2->Do_AR_bwd(nCycle);
		cpMMU2->Do_AW_bwd(nCycle);
		// cpMMU2->Do_W_bwd(nCycle);
		#endif

		#ifdef MMU_OFF
		cpMMU3->Do_AR_bwd_MMU_OFF(nCycle);
		cpMMU3->Do_AW_bwd_MMU_OFF(nCycle);
		// cpMMU3->Do_W_bwd_MMU_OFF(nCycle);
		#endif
		#ifdef MMU_ON
		cpMMU3->Do_AR_bwd(nCycle);
		cpMMU3->Do_AW_bwd(nCycle);
		// cpMMU3->Do_W_bwd(nCycle);
		#endif

		//-----------------------------
		cpMST0->Do_AR_bwd(nCycle);
		cpMST0->Do_AW_bwd(nCycle);
		// cpMST0->Do_W_bwd(nCycle);

		cpMST1->Do_AR_bwd(nCycle);
		cpMST1->Do_AW_bwd(nCycle);
		// cpMST1->Do_W_bwd(nCycle);

		cpMST2->Do_AR_bwd(nCycle);
		cpMST2->Do_AW_bwd(nCycle);
		// cpMST2->Do_W_bwd(nCycle);

		cpMST3->Do_AR_bwd(nCycle);
		cpMST3->Do_AW_bwd(nCycle);
		// cpMST3->Do_W_bwd(nCycle);

		cpMST4->Do_AR_bwd(nCycle);
		cpMST4->Do_AW_bwd(nCycle);
		// cpMST4->Do_W_bwd(nCycle);

		cpMST5->Do_AR_bwd(nCycle);
		cpMST5->Do_AW_bwd(nCycle);
		// cpMST5->Do_W_bwd(nCycle);


		//---------------------------------
		// R, B READY
		//---------------------------------

		cpMST0->Do_R_bwd(nCycle);
		cpMST0->Do_B_bwd(nCycle);

		cpMST1->Do_R_bwd(nCycle);
		cpMST1->Do_B_bwd(nCycle);

		cpMST2->Do_R_bwd(nCycle);
		cpMST2->Do_B_bwd(nCycle);

		cpMST3->Do_R_bwd(nCycle);
		cpMST3->Do_B_bwd(nCycle);

		cpMST4->Do_R_bwd(nCycle);
		cpMST4->Do_B_bwd(nCycle);

		cpMST5->Do_R_bwd(nCycle);
		cpMST5->Do_B_bwd(nCycle);

		//---------------------------
		#ifdef MMU_OFF
		cpMMU0->Do_R_bwd_MMU_OFF(nCycle);
		cpMMU0->Do_B_bwd_MMU_OFF(nCycle);
		#endif
		#ifdef MMU_ON
		cpMMU0->Do_R_bwd(nCycle);
		cpMMU0->Do_B_bwd(nCycle);
		#endif

		#ifdef MMU_OFF
		cpMMU1->Do_R_bwd_MMU_OFF(nCycle);
		cpMMU1->Do_B_bwd_MMU_OFF(nCycle);
		#endif
		#ifdef MMU_ON
		cpMMU1->Do_R_bwd(nCycle);
		cpMMU1->Do_B_bwd(nCycle);
		#endif

		#ifdef MMU_OFF
		cpMMU2->Do_R_bwd_MMU_OFF(nCycle);
		cpMMU2->Do_B_bwd_MMU_OFF(nCycle);
		#endif
		#ifdef MMU_ON
		cpMMU2->Do_R_bwd(nCycle);
		cpMMU2->Do_B_bwd(nCycle);
		#endif

		#ifdef MMU_OFF
		cpMMU3->Do_R_bwd_MMU_OFF(nCycle);
		cpMMU3->Do_B_bwd_MMU_OFF(nCycle);
		#endif
		#ifdef MMU_ON
		cpMMU3->Do_R_bwd(nCycle);
		cpMMU3->Do_B_bwd(nCycle);
		#endif

		//---------------------------
		cpBUS->Do_R_bwd(nCycle);
		cpBUS->Do_B_bwd(nCycle);

		cpSLV->Do_R_bwd(nCycle);
		cpSLV->Do_B_bwd(nCycle);


		//---------------------------------
		// 5. Update state
		//---------------------------------
		cpMST0->UpdateState(nCycle);
		// cpMST1->UpdateState(nCycle);
		// cpMST2->UpdateState(nCycle);
		cpMST3->UpdateState(nCycle);
		// cpMST4->UpdateState(nCycle);
		cpMST5->UpdateState(nCycle);
		cpMMU0->UpdateState(nCycle);
		// cpMMU1->UpdateState(nCycle);
		// cpMMU2->UpdateState(nCycle);
		cpMMU3->UpdateState(nCycle);
		cpBUS->UpdateState(nCycle);
		cpSLV->UpdateState(nCycle);


		//---------------------------------
		// 6. Check simulation finish 
		//---------------------------------
		// main1, main2
		if (cpMST0->IsAWTransFinished() == ERESULT_TYPE_YES and cpMST3->IsARTransFinished() == ERESULT_TYPE_YES) {
		
		// main5
		// if (cpMST0->IsAWTransFinished()  == ERESULT_TYPE_YES and cpMST1->IsARTransFinished()  == ERESULT_TYPE_YES and cpMST2->IsAWTransFinished()  == ERESULT_TYPE_YES and cpMST3->IsARTransFinished()  == ERESULT_TYPE_YES) {
		
		// main6
		// if (cpMST1->IsARTransFinished()  == ERESULT_TYPE_YES and cpMST2->IsAWTransFinished()  == ERESULT_TYPE_YES and cpMST3->IsARTransFinished()  == ERESULT_TYPE_YES) {
		
		// main7
		// if (cpMST1->IsARTransFinished()  == ERESULT_TYPE_YES) {



			//--------------------------------------------------------------
			printf("[Cycle %3ld] Simulation is finished. \n", nCycle);
			printf("---------------------------------------------\n");
			printf("\t Rotated preview scenario \n");
			printf("---------------------------------------------\n");
			printf("\t MST0: \t Camera write (rotation) \n");
			printf("\t MST3: \t Display read (raster-scan) \n");
			#ifdef DMA_ENABLE
			printf("\t MST5: \t Background read, write (raster-scan) \n");
			#endif
			printf("---------------------------------------------\n");
			printf("\t Parameters \n");
			printf("---------------------------------------------\n");
			// printf("\t AR_GEN_NUM		: %d\n", AR_GEN_NUM);
			// printf("\t AW_GEN_NUM		: %d\n", AW_GEN_NUM);
			printf("\t IMG_HORIZONTAL_SIZE		: %d\n", IMG_HORIZONTAL_SIZE);
			printf("\t IMG_VERTICAL_SIZE		: %d\n", IMG_VERTICAL_SIZE);
			printf("\t MAX_MO_COUNT			: %d\n", MAX_MO_COUNT);
			// printf("\t AR_ADDR_INCREMENT		: 0x%x\n", AR_ADDR_INC);
			// printf("\t AW_ADDR_INCREMENT		: 0x%x\n", AW_ADDR_INC);
			
			#ifdef MMU_OFF 
			printf("\t MMU				: OFF \n");
			#endif
			#ifdef MMU_ON
			printf("\t MMU				: ON \n");
			#endif
			
			#ifdef SINGLE_FETCH 
			printf("\t PTW				: Single fetch\n");
			#endif
			#ifdef BLOCK_FETCH 
			printf("\t PTW				: Block fetch\n");
			#endif
			
			#ifdef CONTIGUITY_DISABLE
			printf("\t CONTIGUITY			: DISABLE \n");
			#endif
			
			#ifdef CONTIGUITY_ENABLE
			#ifdef CONTIGUITY_0_PERCENT 
			printf("\t CONTIGUITY			: 0 percent \n");
			#endif
			#ifdef CONTIGUITY_25_PERCENT 
			printf("\t CONTIGUITY			: 25 percent \n");
			#endif
			#ifdef CONTIGUITY_50_PERCENT 
			printf("\t CONTIGUITY			: 50 percent \n");
			#endif
			#ifdef CONTIGUITY_75_PERCENT 
			printf("\t CONTIGUITY			: 75 percent \n");
			#endif
			#ifdef CONTIGUITY_100_PERCENT 
			printf("\t CONTIGUITY			: 100 percent \n");
			#endif
			#endif
			
			#ifdef AR_ADDR_RANDOM
			printf("\t AR RANDOM ADDRESS 		: ON \n");
			#endif
			
			#ifdef AW_ADDR_RANDOM
			printf("\t AW RANDOM ADDRESS 		: ON \n");
			#endif
			
			#ifdef IDEAL_MEMORY
			printf("\t IDEAL MEMORY 		: ON \n");
			#endif
			
			#ifdef MEMORY_CONTROLLER 
			printf("\t MEMORY_CONTROLLER 		: ON \n");
			#endif
			
			// printf("---------------------------------------------\n");
			
			
			//--------------------------------------------------------------
			// FILE out
			//--------------------------------------------------------------
			FILE *fp = NULL;
			
			// Stat
			cpMST0->PrintStat(nCycle, fp);
			// cpMST1->PrintStat(nCycle, fp);
			// cpMST2->PrintStat(nCycle, fp);
			cpMST3->PrintStat(nCycle, fp);
			// cpMST4->PrintStat(nCycle, fp);
			// cpMST5->PrintStat(nCycle, fp);
			#ifdef MMU_ON
			cpMMU0->PrintStat(nCycle, fp);
			// cpMMU1->PrintStat(nCycle, fp);
			// cpMMU2->PrintStat(nCycle, fp);
			cpMMU3->PrintStat(nCycle, fp);

			// Get total avg TLB hit rate
			int Total_nAR_SI = cpMMU0->Get_nAR_SI() + cpMMU1->Get_nAR_SI() + cpMMU2->Get_nAR_SI() + cpMMU3->Get_nAR_SI();
			int Total_nAW_SI = cpMMU0->Get_nAW_SI() + cpMMU1->Get_nAW_SI() + cpMMU2->Get_nAW_SI() + cpMMU3->Get_nAW_SI();
			int Total_nHit_AR_TLB = cpMMU0->Get_nHit_AR_TLB() + cpMMU1->Get_nHit_AR_TLB() + cpMMU2->Get_nHit_AR_TLB() + cpMMU3->Get_nHit_AR_TLB();
			int Total_nHit_AW_TLB = cpMMU0->Get_nHit_AW_TLB() + cpMMU1->Get_nHit_AW_TLB() + cpMMU2->Get_nHit_AW_TLB() + cpMMU3->Get_nHit_AW_TLB();
			
	 		float Avg_TLB_hit_rate = (float) (Total_nHit_AR_TLB + Total_nHit_AW_TLB) / (Total_nAR_SI + Total_nAW_SI) ;   

			// Get total avg TLB reach
			float Avg_TLB_reach = (float) (cpMMU0->GetTLB_reach(nCycle) + cpMMU3->GetTLB_reach(nCycle)) / 2;

			printf("---------------------------------------------\n");
			printf("\t Total Avg TLB hit rate = %1.3f\n", Avg_TLB_hit_rate);
			printf("\t Total Avg TLB reach    = %1.3f\n", Avg_TLB_reach);
			printf("---------------------------------------------\n");
			
			#endif

			cpBUS ->PrintStat(nCycle, fp);
			cpSLV ->PrintStat(nCycle, fp);
			
			#ifdef FILE_OUT
			fclose(fp);
			#endif
			
			
			break;
		};

		nCycle++;

		//--------------------------
		// 7. Check termination
		//--------------------------
		#ifdef TERMINATE_BY_CYCLE
		if (nCycle > SIM_CYCLE) {
			printf("[Cycle %3ld] Simulation is terminated.\n", nCycle);
			break;
		};
		#endif
	};

	//---------------------
	// 8. Destruct
	//---------------------
	delete (cpMST0);
	delete (cpMST1);
	delete (cpMST2);
	delete (cpMST3);
	delete (cpMST4);
	delete (cpMST5);
	delete (cpMMU0);
	delete (cpMMU1);
	delete (cpMMU2);
	delete (cpMMU3);
	delete (cpBUS);
	delete (cpSLV);

	// Measure time. Sim finished.
	clock_gettime(CLOCK_MONOTONIC, &Time_SimFinish);
	
	long Elapsed_time_seconds = Time_SimFinish.tv_sec  - Time_SimStart.tv_sec;
	long Elapsed_time_ns      = Time_SimFinish.tv_nsec - Time_SimStart.tv_nsec;
	
	if (Time_SimStart.tv_nsec > Time_SimFinish.tv_nsec) { // clock underflow
		--Elapsed_time_seconds;
		Elapsed_time_ns += 1000000000;
	};
	
	// printf("Elapsed_time_seconds without ns: %ld\n", Elapsed_time_seconds); 
	// printf("Elapsed_time_nanoseconds: %ld\n", Elapsed_time_ns);
	// printf("Total elapsed seconds: %e\n", (double)Elapsed_time_seconds + (double)Elapsed_time_ns/(double)1000000000);
	
	printf("Sim elapsed time (sec) = %1.3le\n", (double)Elapsed_time_seconds + (double)Elapsed_time_ns/(double)1000000000);
	// printf("Sim elapsed time (ns)  = %ld\n", Elapsed_time_ns);
	printf("-----------------------------------------\n");


	return (-1);
}

