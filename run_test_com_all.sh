cp ./test_com ./test_com_def
cp ./test_com ./test_com_def0
cp ./test_com ./test_com_def1
cp ./test_com ./test_com_def2
cp ./test_com ./test_com_def3
cp ./test_com ./test_com_def4

cp ./test_com ./test_com_def5
cp ./test_com ./test_com_def6
cp ./test_com ./test_com_def7
cp ./test_com ./test_com_def8
cp ./test_com ./test_com_def9
chmod 774 ./test_com_def*

sed -i 's/FREE_LIST = 0/FREE_LIST = 1/g' 			./test_com_def1
sed -i 's/FREE_LIST = 0/FREE_LIST = 2/g' 			./test_com_def2

sed -i 's/FREE_LIST = 0/FREE_LIST = 3/g' 			./test_com_def3
sed -i 's/FREE_LIST = 0/FREE_LIST = 4/g' 			./test_com_def4
sed -i 's/FREE_LIST = 0/FREE_LIST = 5/g' 			./test_com_def5

sed -i 's/FREE_LIST = 0/FREE_LIST = 6/g' 			./test_com_def6
sed -i 's/FREE_LIST = 0/FREE_LIST = 7/g' 			./test_com_def7
sed -i 's/FREE_LIST = 0/FREE_LIST = 8/g' 			./test_com_def8
sed -i 's/FREE_LIST = 0/FREE_LIST = 9/g' 			./test_com_def9

cp ./test_com_def1 ./test_com_BAT1
cp ./test_com_def2 ./test_com_BAT2
cp ./test_com_def3 ./test_com_BAT3
cp ./test_com_def4 ./test_com_BAT4
cp ./test_com_def5 ./test_com_BAT5
cp ./test_com_def6 ./test_com_BAT6
cp ./test_com_def7 ./test_com_BAT7
cp ./test_com_def8 ./test_com_BAT8
cp ./test_com_def9 ./test_com_BAT9
cp ./test_com_def0 ./test_com_BAT0

chmod 774 ./test_com_BAT*

sed -i 's/CONTIGUITY_DISABLE/CONTIGUITY_ENABLE/g' 			./test_com_BAT0
sed -i 's/CONTIGUITY_DISABLE/CONTIGUITY_ENABLE/g' 			./test_com_BAT1
sed -i 's/CONTIGUITY_DISABLE/CONTIGUITY_ENABLE/g' 			./test_com_BAT2
sed -i 's/CONTIGUITY_DISABLE/CONTIGUITY_ENABLE/g' 			./test_com_BAT3
sed -i 's/CONTIGUITY_DISABLE/CONTIGUITY_ENABLE/g' 			./test_com_BAT4
sed -i 's/CONTIGUITY_DISABLE/CONTIGUITY_ENABLE/g' 			./test_com_BAT5
sed -i 's/CONTIGUITY_DISABLE/CONTIGUITY_ENABLE/g' 			./test_com_BAT6
sed -i 's/CONTIGUITY_DISABLE/CONTIGUITY_ENABLE/g' 			./test_com_BAT7
sed -i 's/CONTIGUITY_DISABLE/CONTIGUITY_ENABLE/g' 			./test_com_BAT8
sed -i 's/CONTIGUITY_DISABLE/CONTIGUITY_ENABLE/g' 			./test_com_BAT9


sed -i 's/BCT_DISABLE/BCT_ENABLE/g' 			./test_com_BAT0
sed -i 's/BCT_DISABLE/BCT_ENABLE/g' 			./test_com_BAT1
sed -i 's/BCT_DISABLE/BCT_ENABLE/g' 			./test_com_BAT2
sed -i 's/BCT_DISABLE/BCT_ENABLE/g' 			./test_com_BAT3
sed -i 's/BCT_DISABLE/BCT_ENABLE/g' 			./test_com_BAT4
sed -i 's/BCT_DISABLE/BCT_ENABLE/g' 			./test_com_BAT5
sed -i 's/BCT_DISABLE/BCT_ENABLE/g' 			./test_com_BAT6
sed -i 's/BCT_DISABLE/BCT_ENABLE/g' 			./test_com_BAT7
sed -i 's/BCT_DISABLE/BCT_ENABLE/g' 			./test_com_BAT8
sed -i 's/BCT_DISABLE/BCT_ENABLE/g' 			./test_com_BAT9


echo "Start simulator"
./test_com_def0 && ./run > ./result/output_def0.txt
echo "Run test_com def 0"
./test_com_def1 && ./run > ./result/output_def1.txt
echo "Run test_com def 1"
./test_com_def2 && ./run > ./result/output_def2.txt
echo "Run test_com def 2"
./test_com_def3 && ./run > ./result/output_def3.txt
echo "Run test_com def 3"
./test_com_def4 && ./run > ./result/output_def4.txt
echo "Run test_com def 4"
./test_com_def5 && ./run > ./result/output_def5.txt
echo "Run test_com def 5"
./test_com_def6 && ./run > ./result/output_def6.txt
echo "Run test_com def 6"
./test_com_def7 && ./run > ./result/output_def7.txt
echo "Run test_com def 7"
./test_com_def8 && ./run > ./result/output_def8.txt
echo "Run test_com def 8"
./test_com_def9 && ./run > ./result/output_def9.txt
echo "Run test_com def 9"
./test_com_BAT0 && ./run > ./result/output_BAT0.txt
echo "Run test_com BAT 0"
./test_com_BAT1 && ./run > ./result/output_BAT1.txt
echo "Run test_com BAT 1"
./test_com_BAT2 && ./run > ./result/output_BAT2.txt
echo "Run test_com BAT 2"
./test_com_BAT3 && ./run > ./result/output_BAT3.txt
echo "Run test_com BAT 3"
./test_com_BAT4 && ./run > ./result/output_BAT4.txt
echo "Run test_com BAT 4"
./test_com_BAT5 && ./run > ./result/output_BAT5.txt
echo "Run test_com BAT 5"
./test_com_BAT6 && ./run > ./result/output_BAT6.txt
echo "Run test_com BAT 6"
./test_com_BAT7 && ./run > ./result/output_BAT7.txt
echo "Run test_com BAT 7"
./test_com_BAT8 && ./run > ./result/output_BAT8.txt
echo "Run test_com BAT 8"
./test_com_BAT9 && ./run > ./result/output_BAT9.txt
echo "Run test_com BAT 9"

