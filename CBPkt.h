//------------------------------------------------------------
// FileName	: CBPkt.h
// Version	: 0.78
// DATE 	: 10 Mar 2019
// Description	: B packet class header 
//------------------------------------------------------------
// Note
// 1. When member added, modify Copy_BPkt function in UD_Bus.cpp
//------------------------------------------------------------
#ifndef CBPKT_H
#define CBPKT_H

#include <stdio.h>
#include <string>
#include "Top.h"

using namespace std;

// B pkt
typedef struct tagSBPkt* SPBPkt;
typedef struct tagSBPkt{
	int nID;
}SBPkt;

// B pkt class
typedef class CBPkt* CPBPkt;
class CBPkt{

public:
	// 1. Contructor and Destructor
	CBPkt(string cName);
	CBPkt();
	~CBPkt();

	// 2. Control
	// Set value
	// EResultType	SetPkt(SPBPkt spPkt_new);
	EResultType	SetPkt(int nID);
	EResultType	SetName(string cName);
	EResultType	SetID(int nID);
	EResultType	SetFinalTrans(EResultType eResult);

	// Get value
	SPBPkt		GetPkt();
	string		GetName();
	int		GetID();
	EResultType	IsFinalTrans();

	// Debug
	EResultType	Display();
	EResultType	CheckPkt();

private:
	// Original pkt info
	SPBPkt		spPkt;

	// Control info
	string		cName;
	EResultType	eFinalTrans;
};

#endif
