//------------------------------------------------------------
// Filename	: UD_Bus.h 
// Version	: 0.7
// Date		: 3 Mar 2018
// Description	: Unified data type header 
//------------------------------------------------------------
// This is a global type for bus transaction
//------------------------------------------------------------
#ifndef UD_BUS_H
#define UD_BUS_H

#include "Top.h"
#include "CAxPkt.h"
#include "CRPkt.h"
#include "CWPkt.h"
#include "CBPkt.h"


//-------------------------------
// Transaction. Unified union data
//-------------------------------
typedef union tagUUD* UPUD;
typedef union tagUUD{
	CPAxPkt cpAR;
	CPRPkt	cpR;
	CPAxPkt cpAW;
	CPWPkt	cpW;
	CPBPkt	cpB;
}UUD;


//-------------------------------
// Linked list node (for UD FIFO)
//-------------------------------
typedef struct tagSLinkedUD* SPLinkedUD;
typedef struct tagSLinkedUD{
	UPUD		upData;
	int		nLatency;
	SPLinkedUD	spNext;
}SLinkedUD;


//-------------------------------
// Get value
//-------------------------------
int		GetID(UPUD upThis, EUDType eType);


//-------------------------------
// Control (pointer copy)
//-------------------------------
UPUD		Copy_UD(UPUD upThis, EUDType eType);
CPAxPkt		Copy_CAxPkt(CPAxPkt cpPkt);
CPRPkt		Copy_CRPkt(CPRPkt cpPkt);
CPWPkt		Copy_CWPkt(CPWPkt cpPkt);
CPBPkt		Copy_CBPkt(CPBPkt cpPkt);

EResultType	Delete_UD(UPUD upThis, EUDType eType);		// Check upThis deleted


//-------------------------------
// Debug
//-------------------------------
// EResultType	CheckUD(UPUD upThis, EDUType eType);
EResultType	Display_UD(UPUD upThis, EUDType eType);

#endif

