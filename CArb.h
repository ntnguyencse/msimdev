//------------------------------------------------------------
// FileName	: CArb.h
// Version	: 0.7
// DATE 	: 3 Mar 2018
// Description	: Arbiter header 
//------------------------------------------------------------
#ifndef CARB_H
#define CARB_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

#include "Top.h"

using namespace std;

//-----------------------------
// Arbiter 
//-----------------------------
typedef class CArb* CPArb;
class CArb{

public:
	// 1. Contructor and Destructor
	CArb(string cName, EArbType eArbType, int nCandidateNum);
	CArb(string cName, int nCandidateNum);
	~CArb();

	// 2. Control
	// Set value
	EResultType	Reset();
	EResultType	UpdateState();

	// Get value
	string		GetName();

	int		Arbitration(int nCandidateList[]);
	int		Arbitration_RR(int nCandidateList[]);
	int		Arbitration_Fixed(int nCandidateList[]);

	int		GetArbResult();

	// Debug
	EResultType	CheckArb();
	EResultType	Display();

private:

	// Control info
	string		cName;
	EArbType	eArbType;		// RR, Fixed
	int		nCandidateNum;		// Number of arbiter candidates 
	int		nPrevResult;		// Previous arbitrated result 
};

#endif

