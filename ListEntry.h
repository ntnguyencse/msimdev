#ifndef List_Entry_H
#define List_Entry_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

struct List_Entry_Node
{

    unsigned int startPPN;
    List_Entry_Node *next;
};

/*
    In this, we implement a linked-list. So that in the next part, base on this linked-list,
    we continue implementing freelist for buddy allocator.
*/

class List_Entry
{
private:
    /*
        A linked-list have a head-node.
        When we want to traverse, we get the address of the head-node using getListHead() function,
        and then keep traversing through the linked-list by point to the "next" attribute.

        Block size will use to identify if free list has any buddy.
    */
    List_Entry_Node *_head, *_tail;

public:
    List_Entry();
    ~List_Entry();

    //Add and remove a free-list block to freelist
    bool add(unsigned int startPPN);
    bool remove(unsigned int startPPN);

    /*
        Find buddy of a page with startPPN, if not return nullptr
    */
    List_Entry_Node *find(unsigned int startPPN);

    bool isEmpty();
    List_Entry_Node *getListHead();
};

#endif