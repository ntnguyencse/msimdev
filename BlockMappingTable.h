#ifndef BLOCK_MAPPING_TABLE_H
#define BLOCK_MAPPING_TABLE_H

#include <iostream>
#include "Top.h"

struct Block_Mapping_Node
{
    unsigned int _startVPN, _startPPN; // BlockSize;
    unsigned int _belongToFreeList;

    Block_Mapping_Node *next;

    SPPTE* _PageTable;
};

class Block_Mapping_Table
{
public:
    Block_Mapping_Node *_head, *_tail;

    Block_Mapping_Table();
    ~Block_Mapping_Table();

    void add(unsigned int startVPN, unsigned int startPPN, unsigned int belongToFreeList, SPPTE* PageTable);

    Block_Mapping_Node *getHead();
    Block_Mapping_Node *getTail();

    void dump();
};

#endif