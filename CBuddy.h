#ifndef Buddy_Allocator_H
#define Buddy_Allocator_H

#include "ListEntry.h"
#include "BlockMappingTable.h"
#include "Top.h"
 #include "CMMU.h"

#include <iomanip>
#include <iostream>

//#define MAX_ORDER 10
//#define TOTAL_PAGE_NUM 65536 //256
// #define PAGE_SIZE 4 * (2 << 10)

#define LIMIT_PAGE_TABLE_NUM 20

class CBuddy
{
private:
    std::string _name;
    int total_block=0;
    float numBlock;
    unsigned int _leftover_pages;
    unsigned int _currentVPN;

    List_Entry *_free_list;
    SPPTE *_PageTable;
    Block_Mapping_Table *_blockMappingTable;

    bool _existFreeList;
    float _current_AVG_Block_Size;

    /*
    If free-list[order] don't have fit block for request, split a large block into two small blocks.
        Using expand function below to do the task.
    */
    void expand(unsigned int IDX);

    /*s
    After deallocation, if a free-list has buddies, merge function will be called
    */
    void merge(unsigned int IDX, unsigned int START_PPN);

    //Add a block to free-list which has startPPN
    void addTo(const unsigned int IDX, const unsigned int START_PPN);

    //Remove a block from free-list which has startPPN
    void removeFrom(const unsigned int IDX, const unsigned int START_PPN);

    //Find the buddy of a free-list entry at _free_list[IDX]
    List_Entry_Node *find_buddy(unsigned int IDX, unsigned int START_PPN);

public:
    //Constructor of buddy allocator
    CBuddy(std::string NAME);

    //Destructor of buddy allocator
    ~CBuddy();

    //Initialize free-list
    // void Set_FreeList();
    void Set_FreeList(unsigned int FL_TYPE);

    //Get Page Table pointer
    SPPTE *Get_PageTable();

    //Allocation function
    void Do_Allocate(unsigned int START_VPN, unsigned int NUM_REQ_PAGES);

    //De-allocation function
    void Do_Deallocate(unsigned int START_VPN, unsigned int NUM_REQ_PAGES);

    //Get the fragmentation of the current request
    float Get_Current_Average_Block_Size();

    //Show the value of Block Mapping Info Table
    void Dump_MappingTable();

    //Show values of free-list
    void Dump_FreeList();
    int get_current_numBlock();
    int get_total_Block();
};

typedef CBuddy *CPBuddy;

#endif