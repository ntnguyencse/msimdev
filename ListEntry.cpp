#include "ListEntry.h"

List_Entry::List_Entry()
{

    _head = nullptr;
    _tail = nullptr;
}

List_Entry::~List_Entry()
{

    List_Entry_Node *ptr;

    while (_head != nullptr)
    {
        ptr = _head;
        _head = _head->next;
        delete (ptr);
    }
}

List_Entry_Node *List_Entry::getListHead()
{
    return _head;
}

bool List_Entry::isEmpty()
{
    /*
        A free-list is empty when head-node equal to nullptr of length = 0
    */
    return _head == nullptr;
}

bool List_Entry::add(unsigned int startPPN)
{

    /*
        Free-list node is inserted in ascending order
    */

    List_Entry_Node *node = new List_Entry_Node;
    if (node == nullptr)
    {
        // std::cout << "Can not create new free-list node." << std::endl;
        return false;
    }

    node->startPPN = startPPN;
    node->next = nullptr;

    /*
        If free-list doesn't have any node, we inserted to the first position
    */
    if (_head == nullptr)
    {
        _head = node;
        _tail = node;
        return true;
    }

    /*
        If free-list has at least one node, and the startPPN of new node smaller than
        head-node, insert to the first position
    */
    if (node->startPPN < _head->startPPN)
    {
        node->next = _head;
        _head = node;
        return true;
    }

    /*
        If new node has the startPPN greater than tail-node, insert to the last position 
        and change the tail pointer
    */
    if (node->startPPN > _tail->startPPN)
    {
        _tail->next = node;
        _tail = node;
        return true;
    }

    /*
        Unless, find the position that new node belong to
    */
    List_Entry_Node *current = _head;
    while ((current->next != NULL) && (current->next->startPPN < node->startPPN))
        current = current->next;

    if (current->next != nullptr)
    {
        node->next = current->next;
        current->next = node;
    }
    else
    {
        current->next = node;
    }
    return true;
}

bool List_Entry::remove(unsigned int startPPN)
{

    if (_head == nullptr)
        return false;

    //If that block is at the start of the free-list
    if (_head->startPPN == startPPN)
    {
        List_Entry_Node *temp = _head;
        _head = _head->next;

        delete (temp);

        return true;
    }

    List_Entry_Node *i = _head;
    while ((i->next != nullptr) && (i->next->startPPN != startPPN))
        i = i->next;

    //Not found that block
    if (i->next == nullptr)
        return false;

    //Else
    List_Entry_Node *temp = i->next;

    //If that block is at the end of the free-list
    if (temp->startPPN == _tail->startPPN)
    {
        _tail = i;
    }

    i->next = i->next->next;

    delete (temp);

    return true;
}

List_Entry_Node *List_Entry::find(unsigned int startPPN)
{

    for (List_Entry_Node *i = _head; i != nullptr; i = i->next)
    {
        if (i->startPPN == startPPN)
            return i;

        if (i->startPPN > startPPN)
            return nullptr;
    }

    return nullptr;
}