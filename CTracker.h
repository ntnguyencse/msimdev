//------------------------------------------------------------
// Filename	: CTracker.h 
// Version	: 0.81
// Date		: 28 Mar 2018
// Description	: Priority queue type header 
//------------------------------------------------------------
// Cache MO tracker
//------------------------------------------------------------
#ifndef CTRACKER_H
#define CTRACKER_H

#include <string>
#include "UD_Bus.h"

using namespace std;

//---------------------------------
// Cache entry transaction state tracker
//---------------------------------
typedef enum{
	ECACHE_STATE_TYPE_HIT,			// Doing service for hit
	ECACHE_STATE_TYPE_STALL,		// Stalled due to (address or ID) dependency

	// Cache
	ECACHE_STATE_TYPE_FILL,			// Doing line-fill
	ECACHE_STATE_TYPE_EVICT,		// Doing evict

	// MMU 
	ECACHE_STATE_TYPE_FIRST_PTW,		// Doing first-level PTW 
	ECACHE_STATE_TYPE_SECOND_PTW,
	ECACHE_STATE_TYPE_THIRD_PTW,

	// MMU RMM 
	ECACHE_STATE_TYPE_FIRST_RPTW,		// Doing first-level RPTW. RMM 
	ECACHE_STATE_TYPE_SECOND_RPTW,
	ECACHE_STATE_TYPE_THIRD_RPTW,

	// MMU AT 
	ECACHE_STATE_TYPE_FIRST_APTW,		// Doing first-level APTW. AT 
	ECACHE_STATE_TYPE_SECOND_APTW,

	ECACHE_STATE_TYPE_SECOND_APTW_DONE,	// Done. stalled
	ECACHE_STATE_TYPE_SECOND_PTW_DONE,

	ECACHE_STATE_TYPE_UNDEFINED
}ECacheStateType;


//------------------------------------------
// Convert enum to string
//------------------------------------------
string Convert_eCacheState2string(ECacheStateType eType);


//---------------------------------
// MMU transaction state tracker
//---------------------------------
// typedef enum{
//	EMMU_STATE_TYPE_HIT,		// Doing service for hit
//	EMMU_STATE_TYPE_STALL,		// Stalled due to address or ID dependency
//
//	EMMU_STATE_TYPE_FIRST_PTW,	// Doing first-level PTW 
//	EMMU_STATE_TYPE_SECOND_PTW,
//	EMMU_STATE_TYPE_THIRD_PTW,
//
//	EMMU_STATE_TYPE_UNDEFINED
// }EMMUStateType;


//---------------------------------
// MMU MO tracker entry
// 	Doubly linked list node (UD queue)
//---------------------------------
typedef struct tagSLinkedCUD* SPLinkedCUD;
typedef struct tagSLinkedCUD{
	UPUD            upData;
	EUDType         eUDType;

	SPLinkedCUD     spPrev;
	SPLinkedCUD     spNext;
	
	ECacheStateType	eState;
	int             nID;            // Transaction ID

	int		nCycle_wait;    // Waiting time since allocation

}SLinkedCUD;


//---------------------------------
// Priority queue class
//---------------------------------
typedef class CTracker* CPTracker;
class CTracker{

public:
        // 1. Contructor and Destructor
	CTracker(string cName, int nMaxNum);
	~CTracker();

	// 2. Control
	EResultType	Reset();

	// Set value
	EResultType	Push(UPUD upUD, EUDType eUDType, ECacheStateType eState);
	UPUD		Pop(int nID, EUDType eUDType);
	UPUD		Pop(SPLinkedCUD spNode);
	UPUD		Pop();
	
	EResultType	SetStateNode(int nID, EUDType eUDTYpe, ECacheStateType eNewState);				// Search head node matching ID, UDType. Update new state
	EResultType	SetStateNode(int nID, EUDType eUDTYpe, ECacheStateType eOldState, ECacheStateType eNewState);	// Search head node matching ID, UDType, state. Update new state

	EResultType	UpdateState();

	// Get value
	int		GetCurNum();
	int		GetMaxNum();
	UPUD		GetTop();
	EResultType	IsEmpty();
	EResultType	IsFull();

	SPLinkedCUD	GetNode(ECacheStateType eState);			// Search first node matching state
	SPLinkedCUD	GetNode(int nID, EUDType eUDType);			// Search first node matching ID and EUDType  
	SPLinkedCUD	GetNode(int nID);					// Search first node matching ID 
	SPLinkedCUD	GetNode_anyPTW();					// Search first node doing any PTW. APTW. RPTW.
	SPLinkedCUD	GetNode_PTW();						// Search first node doing PTW
	SPLinkedCUD	GetNode_APTW_AT();					// Search first node doing APTW. AT.
	SPLinkedCUD	GetNode_Pair_APTW_VA_AT(int64_t nVA, EUDType eUDType);	// Search first node doing APTW (for VA) matching EUDType
	SPLinkedCUD	GetNode_Pair_PTW_VA_AT(int64_t nVA, EUDType eUDType);	// Search first node doing  PTW (for VA) matching EUDType
	// SPLinkedCUD	GetNode_APTW_AT();					// Search first node doing any PTW. AT

	EResultType	IsIDMatch(int nID, EUDType eUDTYpe);			// Search any node matching ID and UDType
	EResultType	IsAddrMatch(int64_t nAddr);				// Search any node matching address.
	EResultType	IsAnyPTW_Match_VPN(int64_t nAddr);			// Search any node doing PTW matching VPN.
	EResultType	IsAddrMatch_Cache(int64_t nAddr);			// Search any node matching address cache-line

	EResultType	IsAnyLineFill();					// Search any node is doing line-fill 
	EResultType	IsSameID_LineFill(int nID, EUDType eUDType);		// Search any node matching ID and UDType is doing line-fill

	EResultType	IsIDMatchPTW(int nID, EUDType eUDTYpe); 		// Search any node matching ID and UDType. Check the node is doing PTW
	EResultType	IsUDMatchPTW(EUDType eUDTYpe);				// Search any node matching UDType. Check the node is doing PTW
	EResultType	IsAnyPTW();						// Search any node is doing PTW
	EResultType	IsRPTW_sameVPN_Match_RMM(int64_t nAddr);		// Search any node doing RPTW matching nAddr. RMM  

	// Stat
	int		GetMaxCycleWait();					// Max waiting cycle among entries

	// Debug
	EResultType	CheckTracker();
	EResultType	Display();

private:
	// Original info
	string		cName;
	int		nCurNum;
	int		nMaxNum;

	// Control info

	// Stat
	int		nMaxCycleWait;						// Max waiting time (among all entries)

public: 
	// Node 
	SPLinkedCUD	spCUDList_head;
	SPLinkedCUD	spCUDList_tail;
};

#endif

