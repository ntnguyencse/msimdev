#include "CBuddy.h"

//Constructor of buddy allocator
CBuddy::CBuddy(std::string NAME)
{

    _name = std::string(NAME);
    _leftover_pages = 0;
    _currentVPN = 0;

    _blockMappingTable = new Block_Mapping_Table();

    _PageTable = nullptr;

    _existFreeList = false;
}

void CBuddy::Set_FreeList(unsigned int FL_TYPE)
{

    //Create free-list instance
    _free_list = new List_Entry[MAX_ORDER];
    unsigned int temp = 1;
    for (unsigned int i = 0; i < MAX_ORDER; i++)
    {
        _free_list[i] = List_Entry();
        temp *= 2;
    }

    _existFreeList = true;

    //Initialize free-list values
    if ((FL_TYPE < 0) || (FL_TYPE >= MAX_ORDER))
        FL_TYPE = MAX_ORDER - 1;

    int i = 0, idx = FL_TYPE;
    unsigned int count = TOTAL_PAGE_NUM;
    unsigned int blockSize = (1 << idx);

    while (count < blockSize)
        idx--;

    if (idx < 0)
        idx = 0;

    count /= (1 << idx);

    while (count > 0)
    {
        addTo(idx, i);

        if (idx != MAX_ORDER - 1)
            i += 1 << (idx + 1);
        else
            i += 1 << idx;
        count--;
    }
}

SPPTE *CBuddy::Get_PageTable()
{
    return _PageTable;
}

/*
    If free-list[IDX] don't have fit block for request, split a large block into two small blocks.
    Using expand function below to do the task.
*/

void CBuddy::expand(unsigned int IDX)
{

    if (IDX == MAX_ORDER)
        return;

    if (_free_list[IDX].isEmpty())
        expand(IDX + 1);

    if (!_free_list[IDX].isEmpty())
    {

        List_Entry_Node *node = _free_list[IDX].getListHead();

        addTo(IDX - 1, node->startPPN);
        addTo(IDX - 1, node->startPPN + (1 << (IDX - 1)));

        removeFrom(IDX, node->startPPN);
    }
}

void CBuddy::Do_Allocate(unsigned int START_VPN, unsigned int NUM_REQ_PAGES)
{

    //If not construct free-list instance
    if (!_existFreeList)
        Set_FreeList(FREE_LIST);

    //If NUM_REQ_PAGES == 0, return null pointer
    if (NUM_REQ_PAGES == 0)
        return;

    //If not have enough page for allocating, return null pointer
    if ((_leftover_pages == 0) || (NUM_REQ_PAGES > _leftover_pages))
        return;

    //Construct page table for mapping info
    _PageTable = new SPTE *[NUM_REQ_PAGES];
    for (unsigned int i = 0; i < NUM_REQ_PAGES; i++)
    {
        _PageTable[i] = new SPTE;
        _PageTable[i]->nPPN = -1;
        _PageTable[i]->nBlockSize = 0;
    };

    _currentVPN = 0;
    numBlock = 0;
    _current_AVG_Block_Size = NUM_REQ_PAGES;

    while (NUM_REQ_PAGES > 0)
    {
        /*
            Determine targeted order i
        */
        unsigned int targeted = 0;
        bool High_order_has_entry = false;
        unsigned int blockSize = 0;
        for (unsigned int i = MAX_ORDER - 1; i >= 0; i--)
        {
            if ((!_free_list[i].isEmpty()) || (High_order_has_entry))
            {
                High_order_has_entry = true;

                blockSize = 1 << i;
                if (blockSize <= NUM_REQ_PAGES)
                {
                    targeted = i;
                    break;
                }
            }
        }

        /*
            Try to allocate a block for request
        */

        //If not have block for allocating, call the expand function
        if (_free_list[targeted].isEmpty())
        {
            if (High_order_has_entry)
                expand(targeted + 1);
        }

        List_Entry_Node *node = _free_list[targeted].getListHead();
        blockSize = 1 << targeted;
        numBlock = numBlock + 1;

        if (node != nullptr)
        {

            //Add an entry to block mapping table and remove it from free-list
            _blockMappingTable->add(START_VPN, node->startPPN, targeted, _PageTable);

            //Fill in the Page Table
            for (unsigned int i = 0; i < blockSize; i++)
            {
                _PageTable[_currentVPN]->nPPN = node->startPPN + i;
                _PageTable[_currentVPN]->nBlockSize = blockSize;
                _currentVPN++;
            }

            //This block was allocated, so remove it from free-list
            removeFrom(targeted, node->startPPN);

            NUM_REQ_PAGES -= blockSize;
        }
    }

    _current_AVG_Block_Size /= numBlock;
    total_block+=numBlock;

    return;
}

float CBuddy::Get_Current_Average_Block_Size()
{
    return _current_AVG_Block_Size;
}

//Add a block to free-list at idx index
void CBuddy::addTo(const unsigned int idx, const unsigned int startPPN)
{

    if ((idx >= MAX_ORDER) || (idx < 0))
        return;

    if ((!(startPPN & ((1 << idx) - 1)) == 0))
        return;

    if (_free_list[idx].add(startPPN))
        _leftover_pages += 1 << idx;
}

//Remove a block from free-list at idx index
void CBuddy::removeFrom(const unsigned int idx, const unsigned int startPPN)
{

    if ((idx >= MAX_ORDER) || (idx < 0))
        return;

    if ((!(startPPN & ((1 << idx) - 1)) == 0))
        return;

    if (_free_list[idx].remove(startPPN))
        _leftover_pages -= 1 << idx;
}

//Find the buddy of a free-list entry at _free_list[IDX]
List_Entry_Node *CBuddy::find_buddy(unsigned int IDX, unsigned int START_PPN)
{

    if ((IDX >= MAX_ORDER) || (IDX < 0))
        return nullptr;

    if ((!(START_PPN & ((1 << IDX) - 1)) == 0))
        return nullptr;

    unsigned int buddy_startPPN = START_PPN ^ (1 << IDX);

    return _free_list[IDX].find(buddy_startPPN);
}

void CBuddy::Do_Deallocate(unsigned int START_VPN, unsigned int NUM_REQ_PAGES)
{

    /*
        Traverse all allocated pages (base on mapping table)
    */
    if (!_blockMappingTable)
        return;

    unsigned int idx, startPPN;
    List_Entry_Node *buddy;

    Block_Mapping_Node *current_Block_Entry, *prev_Block_Entry;

    prev_Block_Entry = nullptr;
    current_Block_Entry = _blockMappingTable->getHead();
    bool deleted_PageTable = false;

    while (current_Block_Entry != nullptr)
    {
        if (current_Block_Entry->_startVPN == START_VPN)
        {
            idx = current_Block_Entry->_belongToFreeList;
            startPPN = current_Block_Entry->_startPPN;

            while (idx < MAX_ORDER)
            {
                buddy = find_buddy(idx, startPPN);

                /*
                If the buddy of this block is not available, just add this block to free-list
                    where it belong to
                */
                if ((buddy == nullptr) || (idx == MAX_ORDER - 1))
                {
                    addTo(idx, startPPN);
                    break;
                }
                /*
                    Unless, merge two block into one and move to the next level of free_list
                */
                else if (buddy != nullptr)
                {

                    //Merge with buddy
                    startPPN = startPPN & (~(1 << idx));

                    //Remove buddy from this _free_list level, try to add to the next level
                    removeFrom(idx, buddy->startPPN);

                    //Check the next level
                    idx++;
                }
            }

            //Remove the record from Block Mapping Table
            if (_blockMappingTable->_head == current_Block_Entry)
                _blockMappingTable->_head = current_Block_Entry->next;

            if (_blockMappingTable->_tail == current_Block_Entry)
                _blockMappingTable->_tail = prev_Block_Entry;

            if (prev_Block_Entry != nullptr)
                prev_Block_Entry->next = current_Block_Entry->next;

            if (!deleted_PageTable)
            {
                deleted_PageTable = true;
                delete[] current_Block_Entry->_PageTable;
            }

            delete current_Block_Entry;

            if (prev_Block_Entry != nullptr)
                current_Block_Entry = prev_Block_Entry->next;

        }
        else
        {
            prev_Block_Entry = current_Block_Entry;
            current_Block_Entry = current_Block_Entry->next;
        }
    }
}

//Show the value of Block Mapping Info Table
void CBuddy::Dump_MappingTable()
{

    std::cout << std::endl;
    _blockMappingTable->dump();
    std::cout << std::endl;
}

//Show values of free-list
void CBuddy::Dump_FreeList()
{
    std::cout << std::endl;
    std::cout << "FreeList_Dump -----------------------------------------" << std::endl;
    for (unsigned int i = 0; i < MAX_ORDER; i++)
    {
        std::cout << "freeList[" << i << "](" << (1 << i) << "): ";

        for (List_Entry_Node *node = _free_list[i].getListHead(); node != nullptr; node = node->next)
            std::cout << " <--(" << node->startPPN << "-" << node->startPPN + (1 << i) - 1 << ")--> ";

        std::cout << std::endl;
    }
    std::cout << std::endl;
}
int CBuddy::get_current_numBlock(){
    return (int)numBlock;
}
int CBuddy::get_total_Block(){
    return total_block;
}
CBuddy::~CBuddy()
{

    if (_free_list)
        delete[] _free_list;

    if (_blockMappingTable)
        delete _blockMappingTable;
}