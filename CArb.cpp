//-----------------------------------------------------------
// FileName	: CArb.cpp
// Version	: 0.71
// Date		: 19 Aug 2019
// Description	: Arbiter class definition
//-----------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <iostream>
#include <string>

#include "CArb.h"


// Construct
CArb::CArb(string cName, EArbType eArbType, int nCandidateNum) {

	// Generate and initialize
	this->cName = cName;
	this->eArbType = eArbType;
	this->nCandidateNum = nCandidateNum;
	this->nPrevResult = -1;
};


// Construct
CArb::CArb(string cName, int nCandidateNum) {

	// Generate and initialize
	this->cName = cName;

	#if defined BUS_FIXED_PRIORITY
	this->eArbType = EARB_TYPE_FixedPriority;
	#elif defined BUS_ROUND_ROBIN
	this->eArbType = EARB_TYPE_RR;
	#else 
	this->eArbType = EARB_TYPE_UNDEFINED;
	#endif

	this->nCandidateNum = nCandidateNum;
	this->nPrevResult = -1;
};


// Destruct
CArb::~CArb() {

};


// Reset 
EResultType CArb::Reset() {

	this->nPrevResult = -1;
	return (ERESULT_TYPE_SUCCESS);
};


// Update state 
EResultType CArb::UpdateState() {

	return (ERESULT_TYPE_SUCCESS);
};


// Get arbitration 
int CArb::Arbitration(int nCandidateList[]) {

	#ifdef DEBUG
	assert (nCandidateList != NULL);
	#endif
	
	int nArbResult = -1;
	#if defined BUS_ROUND_ROBIN
	nArbResult = this->Arbitration_RR(nCandidateList);
	#elif defined BUS_FIXED_PRIORITY
	nArbResult = this->Arbitration_Fixed(nCandidateList);
	#endif
	
	// Debug
	// this->CheckArb();
	
	return (nArbResult);
};


// Get arbitration 
int CArb::Arbitration_RR(int nCandidateList[]) {

	// Debug
	// this->CheckArb();

	// Start point search
	int nSearchStartPoint = -1;
	if (this->nPrevResult == this->nCandidateNum - 1) {
		nSearchStartPoint = 0;
	} 
	else {
		nSearchStartPoint = this->nPrevResult + 1;
	};

	// Search first	
	for (int nIndex = nSearchStartPoint; nIndex < this->nCandidateNum; nIndex++) {
		if (nCandidateList[nIndex] == 1) { // valid
			this->nPrevResult = nIndex;
			return (nIndex);
		};
	};

	// Search second
	if (nSearchStartPoint != 0) {

		for (int nIndex=0; nIndex < nSearchStartPoint; nIndex++) {
			if (nCandidateList[nIndex] == 1) {
				this->nPrevResult = nIndex;
				return (nIndex);
			};
		};
	};

	// Nothing arbitrated
	return (-1);
};


// Get arbitration 
int CArb::Arbitration_Fixed(int nCandidateList[]) {

	// Debug
	// this->CheckArb();
	
	for (int nIndex = 0; nIndex < nCandidateNum; nIndex++) {

		if (nCandidateList[nIndex] == 1) { // valid
			this->nPrevResult = nIndex;
			return (nIndex);
		};
	};

	// Nothing arbitrated
	return (-1);
};


// Get name
string CArb::GetName() {

	// Debug
	// this->CheckArb();
	return (this->cName);
};


// Get the latest arbiter result 
int CArb::GetArbResult() {

	// Debug
	// this->CheckArb();
	return (this->nPrevResult);
};


// Debug
EResultType CArb::CheckArb() {

	assert (this->nCandidateNum == this->nCandidateNum);
	assert (nPrevResult >= -1);
	assert (nPrevResult < this->nCandidateNum);
	return (ERESULT_TYPE_SUCCESS);
};


// Debug
EResultType CArb::Display() {

	// this->CheckArb();
	return (ERESULT_TYPE_SUCCESS);
};

