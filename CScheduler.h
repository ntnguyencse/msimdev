//------------------------------------------------------------
// FileName	: CScheduler.h
// Version	: 0.7
// DATE 	: 24 Jul 2019
// Description	: MC scheduler 
//------------------------------------------------------------
#ifndef CScheduler_H
#define CScheduler_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

#include "Top.h"
#include "Memory.h"
#include "UD_Bus.h"
#include "MUD_Mem.h"
#include "CQ.h"

using namespace std;

//-------------------------------------
// PTW top priority 
//-------------------------------------
#define PTW_HIGH_PRIORITY

//-------------------------------------
// Scheduling 
//-------------------------------------
// #define FIRST_COME_FIRST_SERVE 
#define BANK_HIT_FIRST 

//-------------------------------------
// Cmd scheduler 
//-------------------------------------
typedef class CScheduler* CPScheduler;
class CScheduler{

public:
	// 1. Contructor and Destructor
	CScheduler(string cName);
	~CScheduler();

	// 2. Control
	EResultType	Reset();

	// Get value
	string		GetName();

	// SPLinkedMUD	GetScheduledMUD(CPQ cpQ);						// Scheduler
	// SPLinkedMUD	GetScheduledMUD_ReadFirst(CPQ cpQ_AR, CPQ cpQ_AW);
	SPLinkedMUD	GetScheduledMUD_FIFO(CPQ cpQ_AR, CPQ cpQ_AW, int64_t nCycle);
	SPLinkedMUD	GetScheduledMUD_Aggressive(CPQ cpQ_AR, CPQ cpQ_AW, int64_t nCycle);	// Aggressive bank preparation
	SPLinkedMUD	GetScheduledMUD(CPQ cpQ_AR, CPQ cpQ_AW, int64_t nCycle);		// Priority bank praparation

	// Control
	// EResultType	UpdateState();

	// Debug
	EResultType	CheckScheduler();
	EResultType	Display();

private:
	// Original
	string		cName;

	// Control
	// CPQ		cpQ_AR;
	// CPQ		cpQ_AW;

	EResultType	IsAR_priority;							// Schedule AR priority
	EResultType	IsAW_priority; 
};

#endif

