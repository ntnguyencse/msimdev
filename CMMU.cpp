//---------------------------------------------------------------------------------------------------------------------------------------------------------	
// FileName	: CMMU.cpp
// Version	: 0.84
// Date		: 23 Nov 2019
// Contact	: JaeYoung.Hur@gmail.com
// Description	: MMU class definition
//---------------------------------------------------------------------------------------------------------------------------------------------------------	
// Spec
//	1. Arch				: ARM v7 MMU 32-bit architecture
//	2. Page table walk		: Single fetch, Block fetch
//	3. Address			: VA 64-bit. PA 32-bit
//	3. Page size                 	: 4kB 
//	4. AR, AW arbitration        	: Round-robin. AR and AW shared.
//	5. Multiple outstanding count	: 16
//---------------------------------------------------------------------------------------------------------------------------------------------------------	
// Flow control
//	1. Requests are served in order.
//	2. If there is PTW on-going, stall Ax.
//---------------------------------------------------------------------------------------------------------------------------------------------------------	
// ID outstanding policy
//	1. Hit-under-hit		: Allow
//	2. Hit-under-miss		: Not allow. If same ID was miss, stall Ax.
//	3. Miss-under-hit 		: Allow
//	4. Miss-under-miss		: Allow. Currently not support. Be careful order of 1st-level and 2nd-level PTW. Check ID-head
//---------------------------------------------------------------------------------------------------------------------------------------------------------	
// Tracker push info
//	1. Hit				: Original. VA
//	2. PTW				: Original. VA
//	3. RPTW				: Original. VA aligned 16kB (four pages). Four because contiguity evenly distributed every 4 pages.
//---------------------------------------------------------------------------------------------------------------------------------------------------------	
// MMU arch options 
//	1. Traditional			: 
//	2. PCA				: Page Contiguity ascending. TVLSI19 paper. Hur. 
//	3. BCT				: Buddy block contiguity. Hur. 
//	4. PCAD 			: PCA + descending. Hur. 
//	3. RMM				: Redundant Memory Mapping. ISCA15 paper. Karakostas  et al.
//	4. AT				: Anchor translation. ISCA17 paper. Park et al. 
//	5. CAMB				: Page table compaction. Hur. 
//---------------------------------------------------------------------------------------------------------------------------------------------------------	
// AxID 
//	1. PTW				: ARID 1 
//	2. RPTW (RMM)			: ARID 0 
//	3. APTW	(AT)			: ARID 0. Issue APTW earlier than PTW. 
//	4. Traditional			: Shift left 1 (request), Shift right 1 (response) 
//---------------------------------------------------------------------------------------------------------------------------------------------------------	
// Page table format : "PAGE_TABLE_GRANULE" 4 
// 	Contiguity representation page table. TVLSI19. Block size any. 
//	Contiguity evenly distributed in page table.
//	"-" indicates no llocation to TLB
//
//	   VPN			BlockSize	0  1  2  3   |	  4  5  6  7  |	 8 
//	1. Contiguity	  0%	   1		0  0  0  0   |	  0  0  0  0  |	 0 ... --> (0),   (1),   (2), ...
//	2. Contiguity	 25%	   2		1  -  0  0   |	  1  -  0  0  |	 1 ... --> (0,1), (4,5), (8, 9), ...
//	3. Contiguity	 50%	   3		2  -  -  0   |	  2  -  -  0  |	 2 ... --> (0-2), (4-6), (8-10), (12-14), ...                    
//	4. Contiguity	 75%	   4		3  -  -  -   |	  3  -  -  -	 3 ... --> (0-3), (4-7), (8-11), (12-15), (16-19), (20-23), ...
//	5. Contiguity	100%			MAX     	        	   ...  
//---------------------------------------------------------------------------------------------------------------------------------------------------------	
// Page table format : "PAGE_TABLE_8_GRANULE" 8
// 	Contiguity representation page table.
//
//	   VPN			BlockSize	0  1  2  3  4  5  6  7 |  8  9 10 11 12 13 14 15    16 
//	1. Contiguity	  0%	   1		0  0  0  0  0  0  0  0 |  0  0  0  0  0  0  0  0     0 ... --> (0),   (1),   (2), ...
//	2. Contiguity	 25%	   3		2  -  -  0  0  -  0  0 |  2  -  -  0  0  0  0  0     2 ... --> (0-2), (8-10), (16-18), ...
//	3. Contiguity	 50%	   5		4  -  -  -  -  0  0  0 |  4  -  -  -  -  0  0  0     4 ... --> (0-4), (8-12), (16-20), ...
//	4. Contiguity	 75%	   7		6  -  -  -  -  -  -  0	  6  -  -  -  -  -  -  0     6 ... --> (0-6), (8-14), (16-22), ...
//	5. Contiguity	100%			MAX	
//---------------------------------------------------------------------------------------------------------------------------------------------------------	
// Known issues
//	1. W FIFO			: In random (corner) case, when simulation finishes, "FIFO_W" may not be empty. 
//					  This is bug. Work-around is not to use "W_channel".
//	2. AR-AW interference		: Often "AR_fwd", "AW_fwd", "R_fwd" seems create read/write interference. 
//					  Work-around is that a single MMU does not conduct read and write concurrently. 
//	3. W data 			: W data is not referred. Data value is not relevant. Work-arond is not to use "W_Channel".
//	4. Addr dependency		: Flow control. Addr dependency. Assume no address dependency. 
//
//	5. Stall due to PTW		: When PTW for AR is on-going and AW is TLB-hit, we stall AW (because PTW on-going). This is performance issue.
//	6. Tracker alloc for hit	: When TLB is hit, tracker entry is allocated. This is not bug. We do it for debug. 
//	7. VA format			: In TLB hardwrae, format is "tag + index + tag" (if contiguity enable) or "tag + index" (if contiguity disable). 
//					  In perf model, we use "vpn" so tag number not necessary. 
//	8. Block fetch			: PCA supports "Block fetch", "Half fetch", "Quarter fetch".
//					  CAMB supports "Half fetch", "Quarter fetch".
//	9. Page table			: Contiguity represented every 4 or 8 pages (as above). 
//					  Random generation with different contiguity is future work. Random (weighted) number generation routine can be used.
//	10. CAMB			: "QUARTER_FETCH" and "HALF_FETCH" only supported. When TLB fill, allocate 4 or 8 PTEs.
//					  Assume BCT-style. 
//					  In TLB entry, "VPN" meaans "StartVPN". "PPN" means "StartPPN".
//	11. RMM support "Page Granule 4": RMM supports only "PAGE_TABLE_GRANULE4" format
//	12. PCAD			: Not implemented. When ascending pattern, PCAD, PCA, BCT are same.
//---------------------------------------------------------------------------------------------------------------------------------------------------------	
// To do 
//
//---------------------------------------------------------------------------------------------------------------------------------------------------------	

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <iostream>
#include <string>

#include "CMMU.h"


// Construct
CMMU::CMMU(string cName) {

	// Generate and initialize
	this->cName = cName;

	// Generate ports
	// Slave interface
	this->cpRx_AR = new CTRx("MMU_Rx_AR", ETRX_TYPE_RX, EPKT_TYPE_AR);
	this->cpRx_AW = new CTRx("MMU_Rx_AW", ETRX_TYPE_RX, EPKT_TYPE_AW);
	this->cpTx_R  = new CTRx("MMU_Tx_R",  ETRX_TYPE_TX, EPKT_TYPE_R);
	this->cpRx_W  = new CTRx("MMU_Rx_W",  ETRX_TYPE_RX, EPKT_TYPE_W);
	this->cpTx_B  = new CTRx("MMU_Tx_B",  ETRX_TYPE_TX, EPKT_TYPE_B);

	// Master interface
	this->cpTx_AR = new CTRx("MMU_Tx_AR", ETRX_TYPE_TX, EPKT_TYPE_AR);
	this->cpTx_AW = new CTRx("MMU_Tx_AW", ETRX_TYPE_TX, EPKT_TYPE_AW);
	this->cpRx_R  = new CTRx("MMU_Rx_R",  ETRX_TYPE_RX, EPKT_TYPE_R);
	this->cpTx_W  = new CTRx("MMU_Tx_W",  ETRX_TYPE_TX, EPKT_TYPE_W);
	this->cpRx_B  = new CTRx("MMU_Rx_B",  ETRX_TYPE_RX, EPKT_TYPE_B);

	// FIFO
	this->cpFIFO_AR   = new CFIFO("MMU_FIFO_AR", EUD_TYPE_AR, 40);
	// this->cpFIFO_R = new CFIFO("MMU_FIFO_R",  EUD_TYPE_R,  40);
	// this->cpFIFO_B = new CFIFO("MMU_FIFO_B",  EUD_TYPE_B,  40);
	this->cpFIFO_W    = new CFIFO("MMU_FIFO_W",  EUD_TYPE_W, 160);

	// Generate TLB memory 
	for (int i=0; i<MMU_NUM_SET; i++) {
		for (int j=0; j<MMU_NUM_WAY; j++) {
			this->spTLB[i][j] = new SMMUEntry;
			this->spTLB[i][j]->nValid 	= -1;
			this->spTLB[i][j]->nVPN		= -1;
			this->spTLB[i][j]->nPPN		= -1;
			this->spTLB[i][j]->ePageSize	= EPAGE_SIZE_TYPE_UNDEFINED;
			this->spTLB[i][j]->nTimeStamp	= -1;
			this->spTLB[i][j]->nContiguity	= -1;
		};
	};

	// Generate RTLB memory. RMM 
	for (int i=0; i<MMU_NUM_SET; i++) {
		for (int j=0; j<MMU_NUM_WAY; j++) {
			this->spRTLB_RMM[i][j] = new SMMUEntry;
			this->spRTLB_RMM[i][j]->nValid		= -1;
			this->spRTLB_RMM[i][j]->nVPN		= -1;
			this->spRTLB_RMM[i][j]->nPPN		= -1;
			this->spRTLB_RMM[i][j]->ePageSize	= EPAGE_SIZE_TYPE_UNDEFINED;
			this->spRTLB_RMM[i][j]->nTimeStamp	= -1;
			this->spRTLB_RMM[i][j]->nContiguity	= -1;
		};
	};

	// Generate FLPD memory 
	for (int j=0; j<NUM_FLPD_ENTRY; j++) {
		this->spFLPD[j] = new SMMUEntry;
		this->spFLPD[j]->nValid		= -1;
		this->spFLPD[j]->nVPN		= -1;
		this->spFLPD[j]->nPPN		= -1;
		this->spFLPD[j]->ePageSize	= EPAGE_SIZE_TYPE_UNDEFINED;
		this->spFLPD[j]->nTimeStamp	= -1;
		this->spFLPD[j]->nContiguity	= -1;
	};

	// MO tracker 
	this->cpTracker = new CTracker("MMU_MO_tracker", 40);

	// W control
	// this->IsPTW_AW_ongoing = ERESULT_TYPE_NO;

	this->nAR_START_ADDR = -1;	// RMM. nRTE calculation
	this->nAW_START_ADDR = -1;

	this->START_VPN = -1;		// Buddy page-table

	this->spPageTable = NULL;

	// Stat 
	this->nMO_AR    = -1;
	this->nMO_AW    = -1;
	this->nMO_Ax    = -1;

	this->nMax_MO_AR = -1;
	this->nMax_MO_AW = -1;
	this->nMax_MO_Ax = -1;

	this->nTotal_MO_AR = -1;
	this->nTotal_MO_AW = -1;
	this->nTotal_MO_Ax = -1;

	this->nMax_MOTracker_Occupancy   = -1;
	this->nTotal_MOTracker_Occupancy = -1;

	// Stat
	this->nAR_SI  = -1;
	this->nAR_MI  = -1;
	this->nAW_SI  = -1;
	this->nAW_MI  = -1;
	this->nHit_AR_TLB  = -1;
	this->nHit_AR_TLB  = -1;
	this->nHit_AW_FLPD = -1; 
	this->nHit_AW_FLPD = -1; 
	this->nPTW_total   = -1;
	this->nPTW_1st   = -1;
	this->nPTW_2nd   = -1;

	this->nRPTW_total_RMM = -1;	// RMM
	this->nRPTW_1st_RMM   = -1;
	this->nRPTW_2nd_RMM   = -1;
	this->nRPTW_3rd_RMM   = -1;

	this->nAPTW_total_AT = -1;	// AT 
	this->nAPTW_1st_AT   = -1;
	this->nAPTW_2nd_AT   = -1;

	this->nHit_AR_RTLB_RMM  = -1;	// RMM
	this->nHit_AW_RTLB_RMM  = -1;

	this->nTLB_evict  = -1;
	this->nFLPD_evict = -1;

	this->nRTLB_evict_RMM  = -1;	// RMM

	this->nMax_Tracker_Wait_Ax = -1;
	this->nTotal_Tracker_Wait_Ax = -1;
	this->nTotal_Tracker_Wait_AR = -1;
	this->nTotal_Tracker_Wait_AW = -1;

	this->IsPTW_AR_ongoing = ERESULT_TYPE_NO;
	this->IsPTW_AW_ongoing = ERESULT_TYPE_NO;

	this->IsRPTW_AR_ongoing_RMM = ERESULT_TYPE_NO; // RMM
	this->IsRPTW_AW_ongoing_RMM = ERESULT_TYPE_NO;

	this->Is_AR_priority = ERESULT_TYPE_NO;
	this->Is_AW_priority = ERESULT_TYPE_NO;

	this->nPTW_AR_ongoing_cycles = -1;
	this->nPTW_AW_ongoing_cycles = -1;

	this->nAR_stall_cycles_SI = -1;
	this->nAW_stall_cycles_SI = -1;

	this->nTotalPages_TLB_capacity = -1;

	this->AR_min_VPN = -1;
	this->AW_min_VPN = -1;
	this->AR_max_VPN = -1;
	this->AW_max_VPN = -1;
};


// Destruct
CMMU::~CMMU() {

	// Port 
	delete (this->cpRx_AR);
	delete (this->cpRx_AW);
	delete (this->cpTx_R );
	delete (this->cpRx_W );
	delete (this->cpTx_B );

	delete (this->cpTx_AR);
	delete (this->cpTx_AW);
	delete (this->cpRx_R );
	delete (this->cpTx_W );
	delete (this->cpRx_B );

	this->cpRx_AR = NULL;
	this->cpRx_AW = NULL;
	this->cpTx_R  = NULL;
	this->cpRx_W  = NULL;
	this->cpTx_B  = NULL;

	this->cpTx_AR = NULL;
	this->cpTx_AW = NULL;
	this->cpRx_R  = NULL;
	this->cpTx_W  = NULL;
	this->cpRx_B  = NULL;

	// FIFO
	delete (this->cpFIFO_AR);
	// delete (this->cpFIFO_R);
	// delete (this->cpFIFO_B);
	delete (this->cpFIFO_W);

	this->cpFIFO_AR = NULL;
	// this->cpFIFO_R = NULL;
	// this->cpFIFO_B = NULL;
	this->cpFIFO_W = NULL;


	// TLB memory 
	for (int i=0; i<MMU_NUM_SET; i++) {
		for (int j=0; j<MMU_NUM_WAY; j++) {
			delete (this->spTLB[i][j]);
			this->spTLB[i][j] = NULL;
		};
	};

	// RTLB memory. RMM
	for (int i=0; i<MMU_NUM_SET; i++) {
		for (int j=0; j<MMU_NUM_WAY; j++) {
			delete (this->spRTLB_RMM[i][j]);
			this->spRTLB_RMM[i][j] = NULL;
		};
	};

	// FLPD memory 
	for (int j=0; j<NUM_FLPD_ENTRY; j++) {
		delete (this->spFLPD[j]);
		this->spFLPD[j] = NULL;
	};

	// MO tracker 
	delete (this->cpTracker);
	this->cpTracker = NULL;
};


// Reset 
EResultType CMMU::Reset() {

	// Initialize
	// Port 
	this->cpRx_AR ->Reset();
	this->cpRx_AW ->Reset();
	this->cpTx_R  ->Reset();
	this->cpRx_W  ->Reset();
	this->cpTx_B  ->Reset();

	this->cpTx_AR ->Reset();
	this->cpTx_AW ->Reset();
	this->cpRx_R  ->Reset();
	this->cpTx_W  ->Reset();
	this->cpTx_B  ->Reset();

	// FIFO
	this->cpFIFO_AR  ->Reset();
	// this->cpFIFO_R->Reset();
	// this->cpFIFO_B->Reset();
	this->cpFIFO_W   ->Reset();

	// TLB memory 
	for (int i=0; i<MMU_NUM_SET; i++) {
		for (int j=0; j<MMU_NUM_WAY; j++) {
			this->spTLB[i][j]->nValid	= 0;
			this->spTLB[i][j]->nVPN		= -1;
			this->spTLB[i][j]->nPPN		= -1;
			this->spTLB[i][j]->ePageSize	= EPAGE_SIZE_TYPE_UNDEFINED;
			this->spTLB[i][j]->nTimeStamp	= 0;
			this->spTLB[i][j]->nContiguity	= 0;
		};
	};

	// RTLB memory. RMM 
	for (int i=0; i<MMU_NUM_SET; i++) {
		for (int j=0; j<MMU_NUM_WAY; j++) {
			this->spRTLB_RMM[i][j]->nValid      = 0;
			this->spRTLB_RMM[i][j]->nVPN        = -1;
			this->spRTLB_RMM[i][j]->nPPN        = -1;
			this->spRTLB_RMM[i][j]->ePageSize   = EPAGE_SIZE_TYPE_UNDEFINED;
			this->spRTLB_RMM[i][j]->nTimeStamp  = 0;
			this->spRTLB_RMM[i][j]->nContiguity = 0;
		};
	};

	// FLPD memory 
	for (int j=0; j<NUM_FLPD_ENTRY; j++) {
		this->spFLPD[j]->nValid		= 0;
		this->spFLPD[j]->nVPN		= -1;
		this->spFLPD[j]->nPPN		= -1;
		this->spFLPD[j]->ePageSize	= EPAGE_SIZE_TYPE_1MB;
		this->spFLPD[j]->nTimeStamp	= 0;
		this->spFLPD[j]->nContiguity	= 0;
	};

	// MO tracker 
	this->cpTracker->Reset();

	// Control
	this->nMO_AR    = 0;
	this->nMO_AW    = 0;
	this->nMO_Ax    = 0;

	// this->nAR_START_ADDR = 0; // Set before reset
	// this->nAW_START_ADDR = 0;

	// Stat
	this->nMax_MO_AR = 0;
	this->nMax_MO_AW = 0;
	this->nMax_MO_Ax = 0;

	this->nTotal_MO_AR = 0;
	this->nTotal_MO_AW = 0;
	this->nTotal_MO_Ax = 0;

	this->nMax_MOTracker_Occupancy   = 0;
	this->nTotal_MOTracker_Occupancy = 0;

	this->nAR_SI    = 0;
	this->nAR_MI    = 0;
	this->nAW_SI    = 0;
	this->nAW_MI    = 0;
	this->nHit_AR_TLB   = 0;
	this->nHit_AW_TLB   = 0; 
	this->nHit_AR_FLPD  = 0;
	this->nHit_AW_FLPD  = 0; 
	this->nPTW_total    = 0;
	this->nPTW_1st    = 0;
	this->nPTW_2nd    = 0;

	this->nRPTW_total_RMM = 0;	// RMM
	this->nRPTW_1st_RMM   = 0;
	this->nRPTW_2nd_RMM   = 0;
	this->nRPTW_3rd_RMM   = 0;

	this->nAPTW_total_AT = 0;	// AT 
	this->nAPTW_1st_AT   = 0;
	this->nAPTW_2nd_AT   = 0;

	this->nHit_AR_RTLB_RMM  = 0;	// RMM
	this->nHit_AW_RTLB_RMM  = 0;

	this->nTLB_evict  = 0;
	this->nFLPD_evict = 0;

	this->nRTLB_evict_RMM = 0;	// RMM

	this->nMax_Tracker_Wait_Ax   = 0;
	this->nTotal_Tracker_Wait_Ax = 0;
	this->nTotal_Tracker_Wait_AR = 0;
	this->nTotal_Tracker_Wait_AW = 0;

	this->IsPTW_AR_ongoing = ERESULT_TYPE_NO;
	this->IsPTW_AW_ongoing = ERESULT_TYPE_NO;

	this->IsRPTW_AR_ongoing_RMM = ERESULT_TYPE_NO; // RMM
	this->IsRPTW_AW_ongoing_RMM = ERESULT_TYPE_NO;

	this->nPTW_AR_ongoing_cycles = 0;
	this->nPTW_AW_ongoing_cycles = 0;

	this->Is_AR_priority = ERESULT_TYPE_YES; // Check AR first SI TLB check
	this->Is_AW_priority = ERESULT_TYPE_NO;

	this->nAR_stall_cycles_SI = 0;
	this->nAW_stall_cycles_SI = 0;

	this->nTotalPages_TLB_capacity = 0;

	this->AR_min_VPN = 0x7FFFFFFFFFFFF; 
	this->AW_min_VPN = 0x7FFFFFFFFFFFF;
	this->AR_max_VPN = 0;
	this->AW_max_VPN = 0;

	return (ERESULT_TYPE_SUCCESS);
};


// Update state 
EResultType CMMU::UpdateState(int64_t nCycle) {

	// Ax priority SI
	if (this->cpRx_AR->IsBusy() == ERESULT_TYPE_YES) { // AR accepted this cycle. Next AW higher priority
		this->Is_AR_priority = ERESULT_TYPE_NO;
		this->Is_AW_priority = ERESULT_TYPE_YES;
	}
	else if (this->cpRx_AW->IsBusy() == ERESULT_TYPE_YES) {
		this->Is_AR_priority = ERESULT_TYPE_YES;
		this->Is_AW_priority = ERESULT_TYPE_NO;
	};
	
	// Port 
	this->cpRx_AR   ->UpdateState();
	this->cpRx_AW   ->UpdateState();
	this->cpTx_R    ->UpdateState();
	// this->cpRx_W ->UpdateState();
	this->cpTx_B    ->UpdateState();

	this->cpTx_AR   ->UpdateState();
	this->cpTx_AW   ->UpdateState();
	this->cpRx_R    ->UpdateState();
	// this->cpTx_W ->UpdateState();
	this->cpRx_B    ->UpdateState();

	// FIFO
	this->cpFIFO_AR   ->UpdateState();
	// this->cpFIFO_R ->UpdateState();
	// this->cpFIFO_B ->UpdateState();
	// this->cpFIFO_W ->UpdateState();

	// Tracker
	this->cpTracker->UpdateState();

	#ifdef STAT_DETAIL
	// Stat
	// Max MO tracker occupancy
	if (this->cpTracker->GetCurNum() > this->nMax_MOTracker_Occupancy) {
		// Debug
		// assert (this->cpTracker->GetCurNum() == 1+ this->nMax_MOTracker_Occupancy); // Yes even when multiple PTW ongoing
		this->nMax_MOTracker_Occupancy++;	
	};

	// Avg MO tracker occupancy
	this->nTotal_MOTracker_Occupancy += this->cpTracker->GetCurNum();

	// Total MO
	this->nTotal_MO_AR += this->nMO_AR;
	this->nTotal_MO_AW += this->nMO_AW;
	this->nTotal_MO_Ax += this->nMO_Ax;

	// Max allocation cycle tracker all entries
	if (this->cpTracker->GetMaxCycleWait() > this->nMax_Tracker_Wait_Ax) {
		this->nMax_Tracker_Wait_Ax = this->cpTracker->GetMaxCycleWait();
	};
	#endif

	#ifdef DEBUG_MMU
	if (this->nMax_Tracker_Wait_Ax >= STARVATION_CYCLE) { // Starvation. If wait too long, something wrong
		printf("[Cycle %3ld: %s.UpdateState]  Starvation occurs (%d cycles) \n",     nCycle, this->cName.c_str(), this->nMax_Tracker_Wait_Ax);
		assert (0);
	};
	#endif

	#ifdef STAT_DETAIL 
	// PTW ongoing cycles
	if (this->IsPTW_AR_ongoing == ERESULT_TYPE_YES) {
		this->nPTW_AR_ongoing_cycles++;
	};
	if (this->IsPTW_AW_ongoing == ERESULT_TYPE_YES) {
		this->nPTW_AW_ongoing_cycles++;
	};

	// Stall cycles SI
	if (this->cpRx_AR->IsIdle() == ERESULT_TYPE_YES and this->cpRx_AR->GetPair()->IsBusy() == ERESULT_TYPE_YES) {
		this->nAR_stall_cycles_SI++;
	};
	if (this->cpRx_AW->IsIdle() == ERESULT_TYPE_YES and this->cpRx_AW->GetPair()->IsBusy() == ERESULT_TYPE_YES) {
		this->nAW_stall_cycles_SI++;
	};

	// TLB capacity
	for (int i=0; i<MMU_NUM_SET; i++) {
		for (int j=0; j<MMU_NUM_WAY; j++) {
			// if (this->spTLB[i][j]->nValid == 1) {
				this->nTotalPages_TLB_capacity += this->spTLB[i][j]->nContiguity;
			// };
		};
	};
	this->nTotalPages_TLB_capacity += (MMU_NUM_SET*MMU_NUM_WAY);
	#endif

	return (ERESULT_TYPE_SUCCESS);
};


// AR valid
EResultType CMMU::Do_AR_fwd_MMU_OFF(int64_t nCycle) {

	// Check Tx valid 
	if (this->cpTx_AR->IsBusy() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check remote-Tx valid 
	CPAxPkt cpAR = this->cpRx_AR->GetPair()->GetAx();
	if (cpAR == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	#ifdef DEBUG_MMU
	assert (this->cpRx_AR->IsBusy() == ERESULT_TYPE_NO);
	assert (this->cpRx_AR->GetPair()->IsBusy() == ERESULT_TYPE_YES);
	// cpAR->CheckPkt();
	#endif

	// Put Rx 
	this->cpRx_AR->PutAx(cpAR);

	#ifdef DEBUG_MMU
	printf("[Cycle %3ld: %s.Do_AR_fwd_MMU_OFF] %s put Rx_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
	assert (cpAR != NULL);
	#endif

	// Put Tx 
	this->cpTx_AR->PutAx(cpAR);

	#ifdef DEBUG_MMU
	printf("[Cycle %3ld: %s.Do_AR_fwd_MMU_OFF] %s put Tx_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
	#endif

	return (ERESULT_TYPE_SUCCESS);
};


// AW valid
EResultType CMMU::Do_AW_fwd_MMU_OFF(int64_t nCycle) {

	// Check Tx valid
	if (this->cpTx_AW->IsBusy() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check remote-Tx valid
	CPAxPkt cpAW = this->cpRx_AW->GetPair()->GetAx();
	if (cpAW == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	#ifdef DEBUG_MMU
	assert (this->cpRx_AW->IsBusy() == ERESULT_TYPE_NO);
	assert (this->cpRx_AW->GetPair()->IsBusy() == ERESULT_TYPE_YES);
	// cpAW->CheckPkt();
	#endif

	// Put Rx 
	this->cpRx_AW->PutAx(cpAW);

	#ifdef DEBUG_MMU
	printf("[Cycle %3ld: %s.Do_AW_fwd_MMU_OFF] %s put Rx_AW.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
	#endif
	
	#ifdef DEBUG_MMU
	assert (cpAW != NULL);
	#endif

	// Put Tx 
	this->cpTx_AW->PutAx(cpAW);

	#ifdef DEBUG_MMU
	printf("[Cycle %3ld: %s.Do_AW_fwd_MMU_OFF] %s put Tx_AW.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
	#endif

	return (ERESULT_TYPE_SUCCESS);
};


// W valid
EResultType CMMU::Do_W_fwd_MMU_OFF(int64_t nCycle) {

	// Check Tx valid
	if (this->cpTx_W->IsBusy() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check remote-Tx valid 
	CPAxPkt cpW = this->cpRx_W->GetPair()->GetAx();
	if (cpW == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	#ifdef DEBUG_MMU
	assert (this->cpRx_W->IsBusy() == ERESULT_TYPE_NO);
	assert (this->cpRx_W->GetPair()->IsBusy() == ERESULT_TYPE_YES);
	// cpW->CheckPkt();
	#endif

	// Put Rx 
	this->cpRx_W->PutAx(cpW);

	#ifdef DEBUG_MMU
	printf("[Cycle %3ld: %s.Do_W_fwd_MMU_OFF] %s put Rx_W.\n", nCycle, this->cName.c_str(), cpW->GetName().c_str());
	assert (cpW != NULL);
	#endif

	// Put Tx 
	this->cpTx_W->PutAx(cpW);

	#ifdef DEBUG_MMU
	printf("[Cycle %3ld: %s.Do_W_fwd_MMU_OFF] %s put Tx_W.\n", nCycle, this->cName.c_str(), cpW->GetName().c_str());
	#endif

	return (ERESULT_TYPE_SUCCESS);
};


// AR ready 
EResultType CMMU::Do_AR_bwd_MMU_OFF(int64_t nCycle) {

	// Check Tx ready
	if (this->cpTx_AR->GetAcceptResult() == ERESULT_TYPE_REJECT) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check Rx valid
	if (this->cpRx_AR->IsBusy() == ERESULT_TYPE_NO) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Set ready
	this->cpRx_AR->SetAcceptResult(ERESULT_TYPE_ACCEPT);

	return (ERESULT_TYPE_SUCCESS);
};


// AW ready 
EResultType CMMU::Do_AW_bwd_MMU_OFF(int64_t nCycle) {

	// Check Tx ready
	if (this->cpTx_AW->GetAcceptResult() == ERESULT_TYPE_REJECT) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check Rx valid
	if (this->cpRx_AW->IsBusy() == ERESULT_TYPE_NO) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Set ready
	this->cpRx_AW->SetAcceptResult(ERESULT_TYPE_ACCEPT);

	return (ERESULT_TYPE_SUCCESS);
};


// W ready 
EResultType CMMU::Do_W_bwd_MMU_OFF(int64_t nCycle) {

	// Check Tx ready
	if (this->cpTx_W->GetAcceptResult() == ERESULT_TYPE_REJECT) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check Rx valid
	if (this->cpRx_W->IsBusy() == ERESULT_TYPE_NO) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Set ready
	this->cpRx_W->SetAcceptResult(ERESULT_TYPE_ACCEPT);

	return (ERESULT_TYPE_SUCCESS);
};


// R valid 
EResultType CMMU::Do_R_fwd_MMU_OFF(int64_t nCycle) {

	// Check Tx valid
	if (this->cpTx_R->IsBusy() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check remote-Tx valid
	CPRPkt cpR = this->cpRx_R->GetPair()->GetR();
	if (cpR == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	#ifdef DEBUG_MMU
	assert (cpR != NULL);
	// cpR->CheckPkt();
	#endif

	// Put Rx 
	this->cpRx_R->PutR(cpR);

	#ifdef DEBUG_MMU
	printf("[Cycle %3ld: %s.Do_R_fwd_MMU_OFF] %s put Rx_R.\n", nCycle, this->cName.c_str(), cpR->GetName().c_str());
	#endif

	// Put Tx
	this->cpTx_R->PutR(cpR);

	#ifdef DEBUG_MMU
	printf("[Cycle %3ld: %s.Do_R_fwd_MMU_OFF] %s put Tx_R.\n", nCycle, this->cName.c_str(), cpR->GetName().c_str());
	#endif

	return (ERESULT_TYPE_SUCCESS);
};


// B valid 
EResultType CMMU::Do_B_fwd_MMU_OFF(int64_t nCycle) {

	// Check Tx valid
	if (this->cpTx_B->IsBusy() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check remote-Tx valid 
	CPBPkt cpB = this->cpRx_B->GetPair()->GetB();
	if (cpB == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	#ifdef DEBUG_MMU
	assert (cpB != NULL);
	// cpB->CheckPkt();
	#endif

	// Put Rx 
	this->cpRx_B->PutB(cpB);

	#ifdef DEBUG_MMU
	printf("[Cycle %3ld: %s.Do_B_fwd_MMU_OFF] %s put Rx_B.\n", nCycle, this->cName.c_str(), cpB->GetName().c_str());
	#endif

	// Put Tx
	this->cpTx_B->PutB(cpB);

	#ifdef DEBUG_MMU
	printf("[Cycle %3ld: %s.Do_B_fwd_MMU_OFF] %s put Tx_B.\n", nCycle, this->cName.c_str(), cpB->GetName().c_str());
	#endif

	return (ERESULT_TYPE_SUCCESS);
};


// R ready
EResultType CMMU::Do_R_bwd_MMU_OFF(int64_t nCycle) {

	// Check Tx ready
	if (this->cpTx_R->GetAcceptResult() == ERESULT_TYPE_REJECT) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check Rx valid
	if (this->cpRx_R->IsBusy() == ERESULT_TYPE_NO) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Set ready
	this->cpRx_R->SetAcceptResult(ERESULT_TYPE_ACCEPT);

	return (ERESULT_TYPE_SUCCESS);
};


// B ready 
EResultType CMMU::Do_B_bwd_MMU_OFF(int64_t nCycle) {

	// Check Tx ready
	if (this->cpTx_B->GetAcceptResult() == ERESULT_TYPE_REJECT) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check Rx valid
	if (this->cpRx_B->IsBusy() == ERESULT_TYPE_NO) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Set ready
	this->cpRx_B->SetAcceptResult(ERESULT_TYPE_ACCEPT);

	return (ERESULT_TYPE_SUCCESS);
};


//----------------------------------------------
// AR valid
//----------------------------------------------
// 	RTLB lookup. RMM
//----------------------------------------------
//	1. MMU receives transaction
//	2. Lookup RTLB
//	3. If RTLB hit, translate address
//	4. Lookup TLB. Traditional operation
//----------------------------------------------
//	RMM  : Range Memory Mapping (Karakostas_ISCA15 paper)
//	RTLB : Redundant TLB
//	RPTW : Redundant PTW
//	RT   : Range Table
//	RTE  : Range Table Entry
//----------------------------------------------
EResultType CMMU::Do_AR_fwd_SI_RMM(int64_t nCycle) {

	//-----------------------------------
	// 1. MMU receives transaction
	//-----------------------------------
	// Check AW priority and candidate 
	if (this->Is_AW_priority == ERESULT_TYPE_YES and this->cpRx_AW->GetPair()->GetAx() != NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check FIFO_AR 
	if (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};
	
	// Check remote-Tx valid. Get.
	CPAxPkt cpAR = this->cpRx_AR->GetPair()->GetAx();
	if (cpAR == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// AR info original
	int     nID = cpAR->GetID();
	int64_t nVA = cpAR->GetAddr();

	#ifdef STAT_DETAIL 
	int64_t nVPN = GetVPNNum(nVA);
	if (nVPN < this->AR_min_VPN) { this->AR_min_VPN = nVPN; };
	if (nVPN > this->AR_max_VPN) { this->AR_max_VPN = nVPN; };
	#endif

	// Check MO
	if (this->GetMO_Ax() >= MAX_MO_COUNT) {	// AR + AW
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check ID dependent node doing PTW. Check Performance
	// if (this->cpTracker->IsUDMatchPTW(EUD_TYPE_AR) == ERESULT_TYPE_YES) {	// Check any PTW
	// if (this->cpTracker->IsIDMatchPTW(nID, EUD_TYPE_AR) == ERESULT_TYPE_YES) {	// Check ID PTW
	if (this->cpTracker->IsAnyPTW() == ERESULT_TYPE_YES) {				// Check any PTW. RPTW, PTW independent
	// if (this->cpTracker->IsIDMatch(nID, EUD_TYPE_AR) == ERESULT_TYPE_YES) {	// Check ID dependent node is outstanding
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check address dependency
	if (this->cpTracker->IsAnyPTW_Match_VPN(nVA) == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Put Rx
	this->cpRx_AR->PutAx(cpAR);

	#ifdef DEBUG_MMU
	assert (this->cpRx_AR->IsBusy() == ERESULT_TYPE_NO);
	printf("[Cycle %3ld: %s.Do_AR_fwd_SI_RMM] %s put Rx_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
	// cpAR->Display();
	#endif

	//-----------------------------------
	// 2. Lookup RTLB 
	//-----------------------------------
	EResultType IsRTLBHit_RMM = this->IsRTLBHit_RMM(nVA, nCycle);	

	#ifdef DEBUG_MMU
	if (IsRTLBHit_RMM == ERESULT_TYPE_YES) {	
	     // printf("[Cycle %3ld: %s.Do_AR_fwd_SI_RMM] %s RTLB hit.\n",  nCycle, this->cName.c_str(), cpAR->GetName().c_str());
	}
	else {
	     // printf("[Cycle %3ld: %s.Do_AR_fwd_SI_RMM] %s RTLB miss.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
	};
	#endif

	int nPA = -1;

	//-----------------------------------
	// 3. RTLB hit. Translate address
	//-----------------------------------
	if (IsRTLBHit_RMM == ERESULT_TYPE_YES) {	

		// Push tracker
		UPUD upAR_new1 = new UUD;
		upAR_new1->cpAR = Copy_CAxPkt(cpAR);	// Original VA
		this->cpTracker->Push(upAR_new1, EUD_TYPE_AR, ECACHE_STATE_TYPE_HIT);	

		#ifdef DEBUG_MMU
		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_RMM] %s push MO_tracker.\n", nCycle, this->cName.c_str(), upAR_new1->cpAR->GetName().c_str());
		// this->cpTracker->CheckTracker();
		#endif

		// Maintain
		Delete_UD(upAR_new1, EUD_TYPE_AR);
	
		// Push FIFO_AR
		UPUD upAR_new2 = new UUD;
		upAR_new2->cpAR = Copy_CAxPkt(cpAR);

		// Get addr physical
		nPA = this->GetPA_RMM(nVA);

		#ifdef DEBUG_MMU
		assert (nPA >= MIN_ADDR);
		assert (nPA <= MAX_ADDR);
		#endif

		// Set addr physical 
		upAR_new2->cpAR->SetAddr(nPA);
	
		// Encode ID
		nID = nID << 1; // RPTW ID is 0x0
		upAR_new2->cpAR->SetID(nID);

		#ifdef DEBUG_MMU
		assert (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_NO);
		#endif 
		this->cpFIFO_AR->Push(upAR_new2, MMU_FIFO_AR_TLB_HIT_LATENCY);

		#ifdef DEBUG_MMU
		assert (upAR_new2 != NULL);
		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_RMM] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), upAR_new2->cpAR->GetName().c_str());
		// cpAR->Display();
		// this->cpFIFO_AR->Display();
		// this->cpFIFO_AR->CheckFIFO();
		#endif

		// Maintain
		Delete_UD(upAR_new2, EUD_TYPE_AR);

		// Stat
		this->nHit_AR_RTLB_RMM++;

		return (ERESULT_TYPE_SUCCESS);
	};

	//---------------------------------
	// 4. Lookup TLB. Traditional operation 
	//---------------------------------
	EResultType IsTLBHit = this->IsTLBHit(nVA, nCycle);	

	#ifdef DEBUG_MMU
	if (IsTLBHit == ERESULT_TYPE_YES) {	
		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_RMM] %s TLB hit.\n",  nCycle, this->cName.c_str(), cpAR->GetName().c_str());
	}
	else {
		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_RMM] %s TLB miss.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
	};
	#endif

	// TLB hit. Translate address
	if (IsTLBHit == ERESULT_TYPE_YES) {	

		// Push tracker
		UPUD upAR_new3 = new UUD;
		upAR_new3->cpAR = Copy_CAxPkt(cpAR); // Original VA
		this->cpTracker->Push(upAR_new3, EUD_TYPE_AR, ECACHE_STATE_TYPE_HIT);	

		#ifdef DEBUG_MMU
		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_RMM] %s push MO_tracker.\n", nCycle, this->cName.c_str(), upAR_new3->cpAR->GetName().c_str());
		// this->cpTracker->CheckTracker();
		#endif

		// Maintain
		Delete_UD(upAR_new3, EUD_TYPE_AR);
	
		// Push FIFO_AR
		UPUD upAR_new4 = new UUD;
		upAR_new4->cpAR = Copy_CAxPkt(cpAR);

		// Get addr physical
		nPA = this->GetPA(nVA);

		#ifdef DEBUG_MMU
		assert (nPA >= MIN_ADDR);
		assert (nPA <= MAX_ADDR);
		#endif

		// Set addr physical 
		upAR_new4->cpAR->SetAddr(nPA);
	
		// Encode ID
		nID = nID << 1; // PTW ID is 0x1
		upAR_new4->cpAR->SetID(nID);

		#ifdef DEBUG_MMU
		assert (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_NO);
		#endif

		this->cpFIFO_AR->Push(upAR_new4, MMU_FIFO_AR_TLB_HIT_LATENCY);

		#ifdef DEBUG_MMU
		assert (upAR_new4 != NULL);
		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_RMM] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), upAR_new4->cpAR->GetName().c_str());
		// cpAR->Display();
		// this->cpFIFO_AR->Display();
		// this->cpFIFO_AR->CheckFIFO();
		#endif

		// Maintain
		Delete_UD(upAR_new4, EUD_TYPE_AR);

		// Stat
		this->nHit_AR_TLB++;

		return (ERESULT_TYPE_SUCCESS);
	};

	// TLB miss. Generate PTW 
	#ifdef DEBUG_MMU
	assert (IsTLBHit == ERESULT_TYPE_NO);	
	#endif

	// Check FLPD cache
	EResultType IsFLPDHit = this->IsFLPDHit(nVA, nCycle);	

	// Generate AR PTW 
	// char cPktName[50];
	CPAxPkt cpAR_new5 = NULL;

	int64_t nAddr_PTW = -1;

	// FLPD miss. Generate 1st PTW
	if (IsFLPDHit == ERESULT_TYPE_NO) {  // FLPD miss
		
		// Address 1st-level page table	
		nAddr_PTW = GetFirst_PTWAddr(nVA);

		// Set pkt
		char cPktName[50];
		sprintf(cPktName, "1st_PTW_for_%s", cpAR->GetName().c_str());
		cpAR_new5 = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);

		//                nID,       nAddr,      nLen
		cpAR_new5->SetPkt(ARID_PTW,  nAddr_PTW,  0);
		cpAR_new5->SetSrcName(this->cName);
		cpAR_new5->SetTransType(ETRANS_TYPE_FIRST_PTW);
		cpAR_new5->SetVA(nVA);

		#ifdef DEBUG_MMU
		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_RMM] %s generated.\n", nCycle, this->cName.c_str(), cPktName);
		// cpAR_new5->Display();
		#endif

		// Stat
		this->nPTW_1st++; 
	} 
	else if (IsFLPDHit == ERESULT_TYPE_YES) {  // FLPD hit. Generate 2nd PTW
		
		// Address 2nd-level page table  
		nAddr_PTW = GetSecond_PTWAddr(nVA);

		// Set pkt
		char cPktName[50];
		sprintf(cPktName, "2nd_PTW_for_%s", cpAR->GetName().c_str());
		cpAR_new5 = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);

		//                   nID,       nAddr,      nLen
		// cpAR_new5->SetPkt(ARID_PTW,  nAddr_PTW,  0);
		cpAR_new5->SetID(ARID_PTW);
		cpAR_new5->SetAddr(nAddr_PTW);
		cpAR_new5->SetSrcName(this->cName);
		cpAR_new5->SetTransType(ETRANS_TYPE_SECOND_PTW);
		cpAR_new5->SetVA(nVA);

		#if defined SINGLE_FETCH
		cpAR_new5->SetLen(0);
		#elif defined BLOCK_FETCH
		cpAR_new5->SetLen(MAX_BURST_LENGTH - 1);	// MAX_BURST_LENGTH 4 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
		#elif defined HALF_FETCH
		cpAR_new5->SetLen(MAX_BURST_LENGTH/2 - 1);	// BURST_LENGTH     2 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
		#elif defined QUARTER_FETCH
		cpAR_new5->SetLen(MAX_BURST_LENGTH/4 - 1);	// BURST_LENGTH     1 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
		#endif

		#ifdef DEBUG_MMU
		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_RMM] %s generated.\n", nCycle, this->cName.c_str(), cPktName);
		// cpAR_new5->Display();
		#endif

		// Stat
		this->nHit_AR_FLPD++;	
		this->nPTW_2nd++; 
	} 
	else {
		assert (0);
	};


	// Push tracker
	UPUD upAR_new5 = new UUD;
	upAR_new5->cpAR = Copy_CAxPkt(cpAR);

	if (IsFLPDHit == ERESULT_TYPE_NO) {
		this->cpTracker->Push(upAR_new5, EUD_TYPE_AR, ECACHE_STATE_TYPE_FIRST_PTW);	
	} 
	else if (IsFLPDHit == ERESULT_TYPE_YES) {
		this->cpTracker->Push(upAR_new5, EUD_TYPE_AR, ECACHE_STATE_TYPE_SECOND_PTW);	
	} 
	else {
		assert (0);
	};

	#ifdef DEBUG_MMU
	// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_RMM] %s push MO_tracker.\n", nCycle, this->cName.c_str(), upAR_new5->cpAR->GetName().c_str());
	// this->cpTracker->CheckTracker();
	#endif

	// Maintain
	Delete_UD(upAR_new5, EUD_TYPE_AR);

	// Push FIFO AR
	UPUD upAR_new8 = new UUD;
	upAR_new8->cpAR = cpAR_new5;

	this->cpFIFO_AR->Push(upAR_new8, MMU_FIFO_AR_TLB_MISS_LATENCY);

	#ifdef DEBUG_MMU
	// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_RMM] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), upAR_new8->cpAR->GetName().c_str());
	// cpAR_new5->Display();
	// this->cpFIFO_AR->Display();
	// this->cpFIFO_AR->CheckFIFO();
	#endif

	// Maintain
	Delete_UD(upAR_new8, EUD_TYPE_AR);

	// Stat
	this->nPTW_total++; 
	this->IsPTW_AR_ongoing = ERESULT_TYPE_YES;

	return (ERESULT_TYPE_SUCCESS);
};


//---------------------------------------------------------------------------	
// Check RPTW need 
//---------------------------------------------------------------------------	
//	Contiguity
//		In this experiment, page table and contiguity are given info.
//		Contiguity evenly distributed in page table.
//		"-" indicates no llocation to TLB
//		VPN		0		1  2  3		4  5  6  7	8 ...
//		Contiguity   0%	0		0  0  0		0  0  0  0	0 ...
//		Contiguity  25%	1		-  0  0		1  -  0  0	1 ...
//		Contiguity  50%	2		-  -  0		2  -  -  0	2 ...
//		Contiguity  75%	3		-  -  -		3  -  -  -	3 ...
//		Contiguity 100%	TOTAL_PAGE_NUM  -  -  -		-  -  -  -	- ...
//---------------------------------------------------------------------------	
//	Operation
//		1. Check RTE number is less than max (MAX_RTE_NUM_RMM) value
//		2. Check RTLB hit
//		3. Check (Tracker) RPTW on-going for same VPN  
//		4. Check VPN is within contiguous range. PTE provides this info. 
//---------------------------------------------------------------------------	
EResultType CMMU::IsRPTW_need_RMM(int64_t nVA, EUDType eUDType, int64_t nCycle) {

	//-----------------------
	// 1. Check RTE number
	//-----------------------
	int nRTE = -1;

	// Get RTE number 	
	if (eUDType == EUD_TYPE_AR) {
		nRTE = (nVA - this->nAR_START_ADDR)/16384;		// RTE number granularity 4 pages (= 16kB)
	}
	else if (eUDType == EUD_TYPE_AW) {
		nRTE = (nVA - this->nAW_START_ADDR)/16384;
	}
	else {
		assert (0);
	};
	assert (nRTE >= 0);

	// Check RTE number 	
	if (nRTE >= MAX_RTE_NUM_RMM) {					// For example, 124
		return (ERESULT_TYPE_NO);
	};


	//-----------------------
	// 2. Check VPN is in RTLB 
	//-----------------------
	// Check RTLB hit 
	EResultType IsRTLBHit_RMM = this->IsRTLBHit_RMM(nVA, nCycle);
	if (IsRTLBHit_RMM == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_NO);
	};

	//-----------------------
	// 3. Check (tracker) same RPTW already on-going
	//-----------------------
	int64_t nVA_16k_aligned = nVA & 0x7FFFFFFFFFFFC000;		// 16kB (=2^14 bytes) aligned

	// Check tracker
	EResultType IsRPTW_Match = this->cpTracker->IsRPTW_sameVPN_Match_RMM(nVA_16k_aligned);

	// Check same RPTW already on-going
	if (IsRPTW_Match == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_NO);
	};	

	//-----------------------
	// 4. Check contiguity
	//-----------------------
	int64_t nVPN = nVA >> BIT_4K_PAGE;				// 4kB = 2^12 bytes
	int64_t nVPN_16k_aligned = nVA_16k_aligned >> BIT_4K_PAGE; 	// When 50% contiguity, need 8k_align? When VPN even, need RPTW
	int64_t nVPN_diff = -1;
	nVPN_diff = nVPN - nVPN_16k_aligned;
	assert (nVPN_diff >= 0);

	#ifdef CONTIGUITY_ENABLE

	#ifdef CONTIGUITY_0_PERCENT
		return (ERESULT_TYPE_NO);
	#endif

	#ifdef CONTIGUITY_25_PERCENT
	if (nVPN_diff <= 1 ) {
		return (ERESULT_TYPE_YES);
	};
	#endif

	#ifdef CONTIGUITY_50_PERCENT					// check every 2
	if (nVPN_diff <= 2 ) {
		return (ERESULT_TYPE_YES);
	};
	#endif

	#ifdef CONTIGUITY_75_PERCENT
	if (nVPN_diff <= 3 ) {
		return (ERESULT_TYPE_YES);
	};
	#endif

	#ifdef CONTIGUITY_100_PERCENT
	if (nVPN_diff <= TOTAL_PAGE_NUM) {
		return (ERESULT_TYPE_YES);
	};
	#endif

	#endif

	return (ERESULT_TYPE_NO);

};


//--------------------------------------------------------------------
// AR valid.  AT (Anchor Translation) 
// 	TLB lookup
//--------------------------------------------------------------------
// Algorithm "regular"
//		   Regular 	Anchor		Contiguity	Operation	Allocation	
//		1. Hit		-		-		Translate
//		2. Miss		Allocated	Match		Translate
//
//		3. Miss		Allocated	Not match	(1) PTW		(2) Fill "regular" in TLB
//		4. Miss		Not allocated	Match		(1) PTW, APTW	(2) Fill "anchor"  in TLB
//		5. Miss		Not allocated	Not match	(1) PTW, APTW	(2) Fill "regular" in TLB
//
//--------------------------------------------------------------------
// Algorithm "anchor"		
//		   Regular 	Anchor		Contiguity	Operation	Allocation	
//		1. -		Allocated	-		Translate
//
//		2. -		Not allocated	-		(1) APTW	(2) Fill "anchor" in TLB
//--------------------------------------------------------------------
// Operation
// 		0. Get Ax. Check TLB hit
// 		1. If hit, translate address
// 		2. Miss. Suppose "regular" or "anchor". If "anchor"  not in TLB, generate "anchor" APTW 
// 		3. Miss. Suppose "regular".             If "regular" not in TLB, generate "regular" PTW
//--------------------------------------------------------------------
// Note	
// 		1. TLB entry types "regular" or "anchor".	
//		2. APTW and PTW same 1MB boundary.
// 		   Issue APTW earlier than PTW. 
// 		   FFinish APTW always earlier than PTW.
// 		3. When both PTW and APTW FLPD miss, one should wait until other PTW finishes. 
// 		   To improve performance, issue both "1st PTW" and "1st APTW".
//--------------------------------------------------------------------
// EResultType CMMU::Do_AR_fwd_SI_AT(int64_t nCycle) {
//	
//	//----------------------------------------------------
//	// 0. Get Ax. Check TLB hit
//	//----------------------------------------------------
//	
//	// Check AW priority and candidate 
//	if (this->Is_AW_priority == ERESULT_TYPE_YES and this->cpRx_AW->GetPair()->GetAx() != NULL) {
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	// Check FIFO_AR 
//	if (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_YES) {
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	// Check remote-Tx valid. Get.
//	CPAxPkt cpAR = this->cpRx_AR->GetPair()->GetAx();
//	if (cpAR == NULL) {
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	// AR info original
//	//
//	int     nID = cpAR->GetID();
//	int64_t nVA = cpAR->GetAddr();
//	
//	#ifdef STAT_DETAIL 
//	int64_t nVPN = GetVPNNum(nVA);
//	if (nVPN < this->AR_min_VPN) { this->AR_min_VPN = nVPN; };
//	if (nVPN > this->AR_max_VPN) { this->AR_max_VPN = nVPN; };
//	#endif
//	
//	// Check MO
//	if (this->GetMO_Ax() >= MAX_MO_COUNT) {	// AR + AW
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	// // Check ID dependent node doing PTW
//	// if (this->cpTracker->IsUDMatchPTW(EUD_TYPE_AR) == ERESULT_TYPE_YES) {		// Check any PTW. Check performance
//	// if (this->cpTracker->IsIDMatchPTW(nID, EUD_TYPE_AR) == ERESULT_TYPE_YES) {		// Check ID PTW
//	if (this->cpTracker->IsAnyPTW() == ERESULT_TYPE_YES) {					// Check any PTW
//	// if (this->cpTracker->IsIDMatch(nID, EUD_TYPE_AR) == ERESULT_TYPE_YES) {		// Check ID dependent node is outstanding
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	// Check address dependency
//	if (this->cpTracker->IsAnyPTW_Match_VPN(nVA) == ERESULT_TYPE_YES) {
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	#ifdef DEBUG_MMU
//	assert (this->cpRx_AR->IsBusy() == ERESULT_TYPE_NO);
//	#endif
//	
//	// Put Rx
//	this->cpRx_AR->PutAx(cpAR);
//	
//	#ifdef DEBUG_MMU
//	printf("[Cycle %3ld: %s.Do_AR_fwd_SI_AT] %s put Rx_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//	// cpAR->Display();
//	#endif
//	
//	// Check regular or anchor
//	EResultType IsVA_Regular = this->IsVA_Regular_AT(nVA);
//	// EResultType IsVA_Anchor  = this->IsVA_Anchor_AT(nVA);
//	
//	// Lookup TLB (regular or anchor)
//	EResultType IsTLB_Allocate = this->IsTLB_Allocate_AT(nVA, nCycle);
//	EResultType IsTLB_Contiguity_Match = this->IsTLB_Contiguity_Match_AT(nVA, nCycle);
//	
//	#ifdef DEBUG_MMU
//	if (IsTLB_Allocate == ERESULT_TYPE_YES) {	
//		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_AT] %s TLB not allocate VA.\n",  nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//	}
//	else {
//		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_AT] %s TLB not allocate VA.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//	};
//	
//	if (IsTLB_Contiguity_Match == ERESULT_TYPE_YES) {	
//		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_AT] %s TLB contiguity matched.\n",  nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//	}
//	else {
//		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_AT] %s TLB contiguity matched.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//	};
//	#endif
//	
//	// Check anchor allocated 
//	EResultType IsTLB_Allocate_Anchor = this->IsTLB_Allocate_Anchor_AT(nVA);
//	
//	// // Check anchor allocated but not contiguity match
//	// EResultType IsAnchor_InTLB_But_Not_ContiguityMatch = ERESULT_TYPE_NO;
//	// 
//	// if (IsTLB_Allocate_Anchor == ERESULT_TYPE_YES and IsTLB_Contiguity_Match == ERESULT_TYPE_NO) {
//	// 	IsAnchor_InTLB_But_Not_ContiguityMatch = ERESULT_TYPE_YES;
//	// };
//	
//	//-------------------------------------------------
//	// 1. Hit. Translate address
//	//-------------------------------------------------
//	if (IsTLB_Allocate == ERESULT_TYPE_YES or IsTLB_Contiguity_Match == ERESULT_TYPE_YES) {	
//	
//		// Push tracker
//		UPUD upAR_new = new UUD;
//		upAR_new->cpAR = Copy_CAxPkt(cpAR);				// Original VA
//		this->cpTracker->Push(upAR_new, EUD_TYPE_AR, ECACHE_STATE_TYPE_HIT);	
//		
//		#ifdef DEBUG_MMU
//		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_AT] %s push MO_tracker.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//		// this->cpTracker->CheckTracker();
//		#endif
//	
//		// Maintain
//		Delete_UD(upAR_new, EUD_TYPE_AR);
//	
//		// Push FIFO_AR 
//		UPUD upAR_new2 = new UUD;
//		upAR_new2->cpAR = Copy_CAxPkt(cpAR);
//	
//		// Get addr physical 
//		int nPA = this->GetPA(nVA);
//	
//		#ifdef DEBUG_MMU
//		assert (nPA >= MIN_ADDR);
//		assert (nPA <= MAX_ADDR);
//		#endif
//	
//		// Set addr physical
//		upAR_new2->cpAR->SetAddr(nPA);
//	
//		// ID encode
//		nID = nID << 1;							// PTW ID 0x1
//		upAR_new2->cpAR->SetID(nID);
//	
//		#ifdef DEBUG_MMU
//		assert (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_NO);
//		#endif
//	
//		this->cpFIFO_AR->Push(upAR_new2, MMU_FIFO_AR_TLB_HIT_LATENCY);  // Translated, ID-encoded
//	
//		#ifdef DEBUG_MMU
//		assert (upAR_new2 != NULL);
//		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_AT] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//		// cpAR->Display();
//		// this->cpFIFO_AR->Display();
//		// this->cpFIFO_AR->CheckFIFO();
//		#endif
//	
//		// Maintain
//		Delete_UD(upAR_new2, EUD_TYPE_AR);
//	
//		// Stat
//		this->nHit_AR_TLB++;
//	
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	#ifdef DEBUG_MMU
//	assert (IsTLB_Allocate == ERESULT_TYPE_NO or IsTLB_Contiguity_Match == ERESULT_TYPE_NO);	
//	#endif
//	
//	
//	//-------------------------------------------------
//	// 2. TLB miss. Suppose "regular" or "anchor". 
//	//    If "anchor" not in TLB, generate APTW
//	//-------------------------------------------------
//	// Check "anchor" not in TLB. Generate APTW 
//	if (IsTLB_Allocate_Anchor == ERESULT_TYPE_NO) {	
//	
//		// Get VA anchor
//		int64_t nVA_anchor = GetVA_Anchor_AT(nVA);	
//	
//		// Check FLPD
//		EResultType IsFLPDHit = this->IsFLPDHit(nVA_anchor, nCycle);	
//		
//		// Generate APTW 
//		char cPktName[50];
//		CPAxPkt cpAR_new = NULL;
//		
//		int64_t nAddr_PTW = -1;
//		
//		// Generate "anchor" APTW
//		if (IsFLPDHit == ERESULT_TYPE_NO) {  // FLPD miss
//			
//			// Get address for 1st-level table	
//			nAddr_PTW = GetFirst_PTWAddr(nVA_anchor);
//		
//			// Set pkt
//			sprintf(cPktName, "1st_APTW_for_%s", cpAR->GetName().c_str());
//			cpAR_new = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);
//		
//			//               nID,       nAddr,      nLen
//			cpAR_new->SetPkt(ARID_APTW, nAddr_PTW,  0);
//			cpAR_new->SetSrcName(this->cName);
//			cpAR_new->SetTransType(ETRANS_TYPE_FIRST_APTW);
//			cpAR_new->SetVA(nVA_anchor);
//	
//			#ifdef DEBUG_MMU
//			// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_AT] %s generated.\n", nCycle, this->cName.c_str(), cPktName);
//			// cpAR_new->Display();
//			#endif
//		
//			// Stat
//			this->nAPTW_1st_AT++; 
//		} 
//		else if (IsFLPDHit == ERESULT_TYPE_YES) {  // FLPD hit
//			
//			// Get address for 2nd-level table  
//			nAddr_PTW = GetSecond_PTWAddr(nVA_anchor);
//		
//			// Set pkt
//			sprintf(cPktName, "2nd_APTW_for_%s", cpAR->GetName().c_str());
//			cpAR_new = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);
//		
//			//                  nID,       nAddr,      nLen
//			// cpAR_new->SetPkt(ARID_APTW, nAddr_PTW,  0);
//			cpAR_new->SetID(ARID_APTW);
//			cpAR_new->SetAddr(nAddr_PTW);
//			cpAR_new->SetSrcName(this->cName);
//			cpAR_new->SetTransType(ETRANS_TYPE_SECOND_APTW);
//			cpAR_new->SetVA(nVA_anchor);
//		
//			#if defined SINGLE_FETCH
//			cpAR_new->SetLen(0);
//			#elif defined BLOCK_FETCH
//			cpAR_new->SetLen(MAX_BURST_LENGTH - 1);		// MAX_BURST_LENGTH 4 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
//			#elif defined HALF_FETCH
//			cpAR_new->SetLen(MAX_BURST_LENGTH/2 - 1);	// BURST_LENGTH     2 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
//			#elif defined QUARTER_FETCH
//			cpAR_new->SetLen(MAX_BURST_LENGTH/4 - 1);	// BURST_LENGTH     1 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
//			#endif
//	
//			#ifdef DEBUG_MMU	
//			// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_AT] %s generated.\n", nCycle, this->cName.c_str(), cPktName);
//			// cpAR_new->Display();
//			#endif
//		
//			// Stat
//			this->nHit_AR_FLPD++;	
//			this->nAPTW_2nd_AT++; 
//		} 
//		else {
//			assert (0);
//		};
//		
//		// Push tracker "anchor"
//		UPUD upAR_new = new UUD;
//		upAR_new->cpAR = Copy_CAxPkt(cpAR);	// Original
//		
//		if (IsFLPDHit == ERESULT_TYPE_NO) {
//			this->cpTracker->Push(upAR_new, EUD_TYPE_AR, ECACHE_STATE_TYPE_FIRST_APTW);	
//		} 
//		else if (IsFLPDHit == ERESULT_TYPE_YES) {
//			this->cpTracker->Push(upAR_new, EUD_TYPE_AR, ECACHE_STATE_TYPE_SECOND_APTW);	
//		} 
//		else {
//			assert (0);
//		};
//	
//		#ifdef DEBUG_MMU	
//		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_AT] %s push MO_tracker.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//		// this->cpTracker->CheckTracker();
//		#endif
//		
//		// Maintain "anchor"
//		Delete_UD(upAR_new, EUD_TYPE_AR);
//		
//		UPUD upAR_new2 = new UUD;
//		upAR_new2->cpAR = cpAR_new;		// PTW
//		
//		// Push FIFO AR "anchor"
//		this->cpFIFO_AR->Push(upAR_new2, MMU_FIFO_AR_TLB_MISS_LATENCY);
//	
//		#ifdef DEBUG_MMU
//		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_AT] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//		// cpAR_new->Display();
//		// this->cpFIFO_AR->Display();
//		// this->cpFIFO_AR->CheckFIFO();
//		#endif
//		
//		// Maintain "anchor"
//		Delete_UD(upAR_new2, EUD_TYPE_AR);
//		
//		// Stat
//		this->nAPTW_total_AT++; 
//		this->IsPTW_AR_ongoing = ERESULT_TYPE_YES;
//	};
//	
//	
//	//-------------------------------------------------
//	// 3. TLB miss. Suppose "regular". 
//	//    If "regular" not in TLB, generate PTW
//	//-------------------------------------------------
//	
//	// Miss. Generate "regular" PTW 
//	if (IsVA_Regular == ERESULT_TYPE_YES and IsTLB_Allocate == ERESULT_TYPE_NO) {	
//	
//		// Check FLPD
//		EResultType IsFLPDHit = this->IsFLPDHit(nVA, nCycle);	
//		
//		// Generate AR PTW 
//		char cPktName[50];
//		CPAxPkt cpAR_new = NULL;
//		
//		int64_t nAddr_PTW = -1;
//		
//		// Generate "regular" PTW
//		if (IsFLPDHit == ERESULT_TYPE_NO) {  // FLPD miss
//			
//			// Get address for 1st-level table	
//			nAddr_PTW = GetFirst_PTWAddr(nVA);
//		
//			// Set pkt
//			sprintf(cPktName, "1st_PTW_for_%s", cpAR->GetName().c_str());
//			cpAR_new = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);
//		
//			//               nID,       nAddr,      nLen
//			cpAR_new->SetPkt(ARID_PTW,  nAddr_PTW,  0);
//			cpAR_new->SetSrcName(this->cName);
//			cpAR_new->SetTransType(ETRANS_TYPE_FIRST_PTW);
//			cpAR_new->SetVA(nVA);
//	
//			#ifdef DEBUG_MMU
//			// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_AT] %s generated.\n", nCycle, this->cName.c_str(), cPktName);
//			// cpAR_new->Display();
//			#endif
//		
//			// Stat
//			this->nPTW_1st++; 
//		} 
//		else if (IsFLPDHit == ERESULT_TYPE_YES) {  // FLPD hit
//			
//			// Get address for 2nd-level table  
//			nAddr_PTW = GetSecond_PTWAddr(nVA);
//		
//			// Set pkt
//			sprintf(cPktName, "2nd_PTW_for_%s", cpAR->GetName().c_str());
//			cpAR_new = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);
//		
//			//                  nID,       nAddr,      nLen
//			// cpAR_new->SetPkt(ARID_PTW,  nAddr_PTW,  0);
//			cpAR_new->SetID(ARID_PTW);
//			cpAR_new->SetAddr(nAddr_PTW);
//			cpAR_new->SetSrcName(this->cName);
//			cpAR_new->SetTransType(ETRANS_TYPE_SECOND_PTW);
//			cpAR_new->SetVA(nVA);
//		
//			#if defined SINGLE_FETCH
//			cpAR_new->SetLen(0);
//			#elif defined BLOCK_FETCH
//			cpAR_new->SetLen(MAX_BURST_LENGTH - 1);		// MAX_BURST_LENGTH 4 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
//			#elif defined HALF_FETCH
//			cpAR_new->SetLen(MAX_BURST_LENGTH/2 - 1);	// BURST_LENGTH     2 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
//			#elif defined QUARTER_FETCH
//			cpAR_new->SetLen(MAX_BURST_LENGTH/4 - 1);	// BURST_LENGTH     1 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
//			#endif
//	
//			#ifdef DEBUG_MMU	
//			// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_AT] %s generated.\n", nCycle, this->cName.c_str(), cPktName);
//			// cpAR_new->Display();
//			#endif
//		
//			// Stat
//			this->nHit_AR_FLPD++;	
//			this->nPTW_2nd++; 
//		} 
//		else {
//			assert (0);
//		};
//		
//		// Push tracker "regular"
//		UPUD upAR_new = new UUD;
//		upAR_new->cpAR = Copy_CAxPkt(cpAR);	// Original
//		
//		if (IsFLPDHit == ERESULT_TYPE_NO) {
//			this->cpTracker->Push(upAR_new, EUD_TYPE_AR, ECACHE_STATE_TYPE_FIRST_PTW);	
//		} 
//		else if (IsFLPDHit == ERESULT_TYPE_YES) {
//			this->cpTracker->Push(upAR_new, EUD_TYPE_AR, ECACHE_STATE_TYPE_SECOND_PTW);	
//		} 
//		else {
//			assert (0);
//		};
//		
//		#ifdef DEBUG_MMU	
//		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_AT] %s push MO_tracker.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//		// this->cpTracker->CheckTracker();
//		#endif
//		
//		// Maintain "regular"
//		Delete_UD(upAR_new, EUD_TYPE_AR);
//		
//		UPUD upAR_new2 = new UUD;
//		upAR_new2->cpAR = cpAR_new;		// PTW
//		
//		// Push FIFO AR "regular"
//		this->cpFIFO_AR->Push(upAR_new2, MMU_FIFO_AR_TLB_MISS_LATENCY);
//	
//		#ifdef DEBUG_MMU
//		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_AT] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//		// cpAR_new->Display();
//		// this->cpFIFO_AR->Display();
//		// this->cpFIFO_AR->CheckFIFO();
//		#endif
//		
//		// Maintain "regular"
//		Delete_UD(upAR_new2, EUD_TYPE_AR);
//		
//		// Stat
//		this->nPTW_total++; 
//		this->IsPTW_AR_ongoing = ERESULT_TYPE_YES;
//	};
//	
//	return (ERESULT_TYPE_SUCCESS);
// };




//----------------------------------------------
// AR valid
//----------------------------------------------
// 	TLB lookup
//----------------------------------------------
EResultType CMMU::Do_AR_fwd_SI(int64_t nCycle) {

	// Check AW priority and candidate 
	if (this->Is_AW_priority == ERESULT_TYPE_YES and this->cpRx_AW->GetPair()->GetAx() != NULL) { // AW priority and candidate
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check FIFO_AR 
	if (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};
	
	// Check remote-Tx valid. Get.
	CPAxPkt cpAR = this->cpRx_AR->GetPair()->GetAx();
	if (cpAR == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// AR info original
	int     nID = cpAR->GetID();
	int64_t nVA = cpAR->GetAddr();

	#ifdef STAT_DETAIL 
	int64_t nVPN = GetVPNNum(nVA);
	if (nVPN < this->AR_min_VPN) { this->AR_min_VPN = nVPN; };
	if (nVPN > this->AR_max_VPN) { this->AR_max_VPN = nVPN; };
	#endif

	// Check MO
	if (this->GetMO_Ax() >= MAX_MO_COUNT) {	// AR + AW
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check ID dependent node doing PTW
	// if (this->cpTracker->IsUDMatchPTW(EUD_TYPE_AR) == ERESULT_TYPE_YES) {		// Check any PTW. Check performance
	// if (this->cpTracker->IsIDMatchPTW(nID, EUD_TYPE_AR) == ERESULT_TYPE_YES) {		// Check ID PTW
	if (this->cpTracker->IsAnyPTW() == ERESULT_TYPE_YES) {					// Check any PTW
	// if (this->cpTracker->IsIDMatch(nID, EUD_TYPE_AR) == ERESULT_TYPE_YES) {		// Check ID dependent node is outstanding
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check address dependency
	if (this->cpTracker->IsAnyPTW_Match_VPN(nVA) == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	#ifdef DEBUG_MMU
	assert (this->cpRx_AR->IsBusy() == ERESULT_TYPE_NO);
	#endif

	// Put Rx
	this->cpRx_AR->PutAx(cpAR);

	#ifdef DEBUG_MMU
	printf("[Cycle %3ld: %s.Do_AR_fwd_SI] %s put Rx_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
	// cpAR->Display();
	#endif

	// Lookup TLB 
	EResultType IsTLBHit = this->IsTLBHit(nVA, nCycle);	

	#ifdef DEBUG_MMU
	if (IsTLBHit == ERESULT_TYPE_YES) {	
		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI] %s TLB hit.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
	}
	else {
		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI] %s TLB miss.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
	};
	#endif

	// Hit. Translate address
	if (IsTLBHit == ERESULT_TYPE_YES) {	

		// Push tracker
		UPUD upAR_new = new UUD;
		upAR_new->cpAR = Copy_CAxPkt(cpAR);	// Original VA
		this->cpTracker->Push(upAR_new, EUD_TYPE_AR, ECACHE_STATE_TYPE_HIT);	

		#ifdef DEBUG_MMU
		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI] %s push MO_tracker.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
		// this->cpTracker->CheckTracker();
		#endif

		// Maintain
		Delete_UD(upAR_new, EUD_TYPE_AR);
	
		// Push FIFO_AR 
		UPUD upAR_new2 = new UUD;
		upAR_new2->cpAR = Copy_CAxPkt(cpAR);

		// Get addr physical 
		int nPA = this->GetPA(nVA);

		#ifdef DEBUG_MMU
		assert (nPA >= MIN_ADDR);
		assert (nPA <= MAX_ADDR);
		#endif

		// Set addr physical
		upAR_new2->cpAR->SetAddr(nPA);

		// ID encode
		nID = nID << 1; // PTW ID is 0x1
		upAR_new2->cpAR->SetID(nID);

		#ifdef DEBUG_MMU
		assert (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_NO);
		#endif
		this->cpFIFO_AR->Push(upAR_new2, MMU_FIFO_AR_TLB_HIT_LATENCY);  // Translated, ID-encoded original

		#ifdef DEBUG_MMU
		assert (upAR_new2 != NULL);
		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
		// cpAR->Display();
		// this->cpFIFO_AR->Display();
		// this->cpFIFO_AR->CheckFIFO();
		#endif

		// Maintain
		Delete_UD(upAR_new2, EUD_TYPE_AR);

		// Stat
		this->nHit_AR_TLB++;

		return (ERESULT_TYPE_SUCCESS);
	};

	// Miss. Generate AR PTW 
	#ifdef DEBUG_MMU
	assert (IsTLBHit == ERESULT_TYPE_NO);	
	#endif

	// Check FLPD cache
	EResultType IsFLPDHit = this->IsFLPDHit(nVA, nCycle);	

	// Generate AR PTW 
	char cPktName[50];
	CPAxPkt cpAR_new = NULL;

	// int64_t nVA = cpAR->GetAddr();
	int64_t nAddr_PTW = -1;

	// Generate PTW
	if (IsFLPDHit == ERESULT_TYPE_NO) {  // FLPD miss
		
		// Get address for 1st-level table	
		nAddr_PTW = GetFirst_PTWAddr(nVA);

		// Set pkt
		sprintf(cPktName, "1st_PTW_for_%s", cpAR->GetName().c_str());
		cpAR_new = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);

		//               nID,       nAddr,      nLen
		cpAR_new->SetPkt(ARID_PTW,  nAddr_PTW,  0);
		cpAR_new->SetSrcName(this->cName);
		cpAR_new->SetTransType(ETRANS_TYPE_FIRST_PTW);
		cpAR_new->SetVA(nVA);

		#ifdef DEBUG_MMU
		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI] %s generated.\n", nCycle, this->cName.c_str(), cPktName);
		// cpAR_new->Display();
		#endif

		// Stat
		this->nPTW_1st++; 
	} 
	else if (IsFLPDHit == ERESULT_TYPE_YES) {  // FLPD hit
		
		// Get address for 2nd-level table  
		nAddr_PTW = GetSecond_PTWAddr(nVA);

		// Set pkt
		sprintf(cPktName, "2nd_PTW_for_%s", cpAR->GetName().c_str());
		cpAR_new = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);

		//                  nID,       nAddr,      nLen
		// cpAR_new->SetPkt(ARID_PTW,  nAddr_PTW,  0);
		cpAR_new->SetID(ARID_PTW);
		cpAR_new->SetAddr(nAddr_PTW);
		cpAR_new->SetSrcName(this->cName);
		cpAR_new->SetTransType(ETRANS_TYPE_SECOND_PTW);
		cpAR_new->SetVA(nVA);

		#if defined SINGLE_FETCH
		cpAR_new->SetLen(0);
		#elif defined BLOCK_FETCH
		cpAR_new->SetLen(MAX_BURST_LENGTH - 1);		// MAX_BURST_LENGTH 4 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
		#elif defined HALF_FETCH
		cpAR_new->SetLen(MAX_BURST_LENGTH/2 - 1);	// BURST_LENGTH     2 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
		#elif defined QUARTER_FETCH
		cpAR_new->SetLen(MAX_BURST_LENGTH/4 - 1);	// BURST_LENGTH     1 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
		#endif

		#ifdef DEBUG_MMU
		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI] %s generated.\n", nCycle, this->cName.c_str(), cPktName);
		// cpAR_new->Display();
		#endif

		// Stat
		this->nHit_AR_FLPD++;	
		this->nPTW_2nd++; 
	} 
	else {
		assert (0);
	};


	// Push tracker
	UPUD upAR_new = new UUD;
	upAR_new->cpAR = Copy_CAxPkt(cpAR);	// Original

	if (IsFLPDHit == ERESULT_TYPE_NO) {
		this->cpTracker->Push(upAR_new, EUD_TYPE_AR, ECACHE_STATE_TYPE_FIRST_PTW);	
	} 
	else if (IsFLPDHit == ERESULT_TYPE_YES) {
		this->cpTracker->Push(upAR_new, EUD_TYPE_AR, ECACHE_STATE_TYPE_SECOND_PTW);	
	} 
	else {
		assert (0);
	};

	#ifdef DEBUG_MMU	
	// printf("[Cycle %3ld: %s.Do_AR_fwd_SI] %s push MO_tracker.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
	// this->cpTracker->CheckTracker();
	#endif

	// Maintain
	Delete_UD(upAR_new, EUD_TYPE_AR);

	UPUD upAR_new2 = new UUD;
	upAR_new2->cpAR = cpAR_new;		// PTW

	// Push FIFO AR
	this->cpFIFO_AR->Push(upAR_new2, MMU_FIFO_AR_TLB_MISS_LATENCY);

	#ifdef DEBUG_MMU
	// printf("[Cycle %3ld: %s.Do_AR_fwd_SI] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
	// cpAR_new->Display();
	// this->cpFIFO_AR->Display();
	// this->cpFIFO_AR->CheckFIFO();
	#endif

	// Maintain
	Delete_UD(upAR_new2, EUD_TYPE_AR);

	// Stat
	this->nPTW_total++; 
	this->IsPTW_AR_ongoing = ERESULT_TYPE_YES;

	return (ERESULT_TYPE_SUCCESS);
};


//--------------------------------
// AR valid
//--------------------------------
// 	Master interface 
//--------------------------------
EResultType CMMU::Do_AR_fwd_MI(int64_t nCycle) {
	
	// Check Tx valid
	if (this->cpTx_AR->IsBusy() == ERESULT_TYPE_YES) {
	        return (ERESULT_TYPE_FAIL);
	};
	
	// Check FIFO_AR
	if (this->cpFIFO_AR->IsEmpty() == ERESULT_TYPE_YES) {
	        return (ERESULT_TYPE_FAIL);
	};
	
	// Debug
	// this->cpFIFO_AR->CheckFIFO();
	
	// Pop
	UPUD upAR_new = this->cpFIFO_AR->Pop();

	// Check
	if (upAR_new == NULL) {
	        return (ERESULT_TYPE_FAIL);
	};

	#ifdef DEBUG_MMU
	assert (upAR_new != NULL);
	// printf("[Cycle %3ld: %s.Do_AR_fwd_MI] (%s) popped FIFO_AR.\n", nCycle, this->cName.c_str(), upAR_new->cpAR->GetName().c_str());
	// this->cpFIFO_AR->CheckFIFO();
	// this->cpFIFO_AR->Display();
	assert (this->cpTx_AR->IsBusy() == ERESULT_TYPE_NO);
	#endif
	
	// Put Tx
	CPAxPkt cpAR_new = upAR_new->cpAR;
	this->cpTx_AR->PutAx(cpAR_new);

	#ifdef DEBUG_MMU
	printf("[Cycle %3ld: %s.Do_AR_fwd_MI] (%s) put Tx_AR.\n", nCycle, this->cName.c_str(), cpAR_new->GetName().c_str());
	// cpAR_new->Display();
	// cpAR_new->CheckPkt();
	#endif
	
	// Stat	
	this->nAR_MI++;

	// Maintain
	Delete_UD(upAR_new, EUD_TYPE_AR);
	
	return (ERESULT_TYPE_SUCCESS);
};


//--------------------------------------------------------------------
// AW valid.  AT (Anchor Translation) 
// 	TLB lookup
//--------------------------------------------------------------------
// Algorithm "regular"
//		   Regular 	Anchor		Contiguity	Operation	Allocation	
//		1. Hit		-		-		Translate
//		2. Miss		Allocated	Match		Translate
//
//		3. Miss		Allocated	Not match	(1) PTW		(2) Fill "regular" in TLB
//		4. Miss		Not allocated	Match		(1) PTW, APTW	(2) Fill "anchor"  in TLB
//		5. Miss		Not allocated	Not match	(1) PTW, APTW	(2) Fill "regular" in TLB
//
//--------------------------------------------------------------------
// Algorithm "anchor"		
//		   Regular 	Anchor		Contiguity	Operation	Allocation	
//		1. -		Allocated	-		Translate
//
//		2. -		Not allocated	-		(1) APTW	(2) Fill "anchor" in TLB
//--------------------------------------------------------------------
// Operation
// 		0. Get Ax. Check TLB hit
// 		1. If hit, translate address
// 		2. Miss. Suppose "regular" or "anchor". If "anchor"  not in TLB, generate "anchor" APTW 
// 		3. Miss. Suppose "regular".             If "regular" not in TLB, generate "regular" PTW
//--------------------------------------------------------------------
// Note	
// 		1. Algorithm same as AR. 
// 		   Only difference is to put Tx when hit. 
//
//		2. AR and AW share a TLB. Priority between AR and AW is switched (to do round-robin arbitration).
//		3. When MMU receives AW, the MMU checks whether hit or miss.
//		4. Then MMU allocates in the tracker.
//		5. In the Do_W_fwd(), we wait for the WLAST. After that, MMU provides a hit service (to translate address) or miss service (to generate PTW)
//		6. This routine is independent of W
//		7. This function is similar to AR
//		8. Do we allow multiple outstanding PTW? In this work, we allow one PTW because a single master operates a single process.
//		9. When PTW on going for AR, do we allow outstanding AW? In this work, we stall when any PTW is on going.
//--------------------------------------------------------------------
// EResultType CMMU::Do_AW_fwd_AT(int64_t nCycle) {
//	
//	//----------------------------------------------------
//	// 0. Get Ax. Check TLB hit
//	//----------------------------------------------------
//	
//	// Check Tx valid
//	if (this->cpTx_AW->IsBusy() == ERESULT_TYPE_YES) {
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	// Check AR priority and busy 
//	if (this->Is_AR_priority == ERESULT_TYPE_YES and this->cpRx_AR->GetPair()->GetAx() != NULL) {
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	// Check FIFO_AR 
//	if (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_YES) {
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	// Check remote-Tx valid. Get.
//	CPAxPkt cpAW = this->cpRx_AW->GetPair()->GetAx();
//	if (cpAW == NULL) {
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	// Get AW info original
//	//
//	// int  nID = cpAW->GetID();
//	int64_t nVA = cpAW->GetAddr();
//	
//	#ifdef STAT_DETAIL 
//	int64_t nVPN = GetVPNNum(nVA);
//	if (nVPN < this->AR_min_VPN) { this->AR_min_VPN = nVPN; };
//	if (nVPN > this->AR_max_VPN) { this->AR_max_VPN = nVPN; };
//	#endif
//	
//	// Check MO
//	if (this->GetMO_Ax() >= MAX_MO_COUNT) {	// AR + AW
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	// // Check ID dependent node doing PTW
//	// if (this->cpTracker->IsUDMatchPTW(EUD_TYPE_AR) == ERESULT_TYPE_YES) {		// Check any PTW. Check performance
//	// if (this->cpTracker->IsIDMatchPTW(nID, EUD_TYPE_AR) == ERESULT_TYPE_YES) {		// Check ID PTW
//	if (this->cpTracker->IsAnyPTW() == ERESULT_TYPE_YES) {					// Check any PTW
//	// if (this->cpTracker->IsIDMatch(nID, EUD_TYPE_AR) == ERESULT_TYPE_YES) {		// Check ID dependent node is outstanding
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	// Check address dependency
//	if (this->cpTracker->IsAnyPTW_Match_VPN(nVA) == ERESULT_TYPE_YES) {
//		return (ERESULT_TYPE_SUCCESS);
//	 };
//	
//	#ifdef DEBUG_MMU
//	assert (this->cpRx_AW->IsBusy() == ERESULT_TYPE_NO);
//	#endif
//	
//	// Put Rx
//	this->cpRx_AW->PutAx(cpAW);
//	
//	#ifdef DEBUG_MMU
//	printf("[Cycle %3ld: %s.Do_AW_fwd_AT] %s put Rx_AW.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
//	// cpAW->Display();
//	#endif
//	
//	// Check regular or anchor
//	EResultType IsVA_Regular = this->IsVA_Regular_AT(nVA);
//	// EResultType IsVA_Anchor  = this->IsVA_Anchor_AT(nVA);
//	
//	// Lookup TLB (regular or anchor)
//	EResultType IsTLB_Allocate = this->IsTLB_Allocate_AT(nVA, nCycle);
//	EResultType IsTLB_Contiguity_Match = this->IsTLB_Contiguity_Match_AT(nVA, nCycle);
//	
//	#ifdef DEBUG_MMU	
//	if (IsTLB_Allocate == ERESULT_TYPE_YES) {	
//		// printf("[Cycle %3ld: %s.Do_AW_fwd_AT] %s TLB not allocate VA.\n",  nCycle, this->cName.c_str(), cpAW->GetName().c_str());
//	}
//	else {
//		// printf("[Cycle %3ld: %s.Do_AW_fwd_AT] %s TLB not allocate VA.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
//	};
//	
//	if (IsTLB_Contiguity_Match == ERESULT_TYPE_YES) {	
//		// printf("[Cycle %3ld: %s.Do_AW_fwd_AT] %s TLB contiguity matched.\n",  nCycle, this->cName.c_str(), cpAW->GetName().c_str());
//	}
//	else {
//		// printf("[Cycle %3ld: %s.Do_AW_fwd_AT] %s TLB contiguity matched.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
//	};
//	#endif
//	
//	// Check anchor allocated 
//	EResultType IsTLB_Allocate_Anchor = this->IsTLB_Allocate_Anchor_AT(nVA);
//	
//	// // Check anchor allocated but not contiguity match
//	// EResultType IsAnchor_InTLB_But_Not_ContiguityMatch = ERESULT_TYPE_NO;
//	// 
//	// if (IsTLB_Allocate_Anchor == ERESULT_TYPE_YES and IsTLB_Contiguity_Match == ERESULT_TYPE_NO) {
//	// 	IsAnchor_InTLB_But_Not_ContiguityMatch = ERESULT_TYPE_YES;
//	// };
//	
//	//-------------------------------------------------
//	// 1. Hit. Translate address
//	//-------------------------------------------------
//	if (IsTLB_Allocate == ERESULT_TYPE_YES or IsTLB_Contiguity_Match == ERESULT_TYPE_YES) {	
//	
//		// Push tracker
//		UPUD upAW_new = new UUD;
//		upAW_new->cpAR = Copy_CAxPkt(cpAW);				// Original VA
//		this->cpTracker->Push(upAW_new, EUD_TYPE_AW, ECACHE_STATE_TYPE_HIT);	
//	
//		#ifdef DEBUG_MMU
//		// printf("[Cycle %3ld: %s.Do_AW_fwd_AT] %s push MO_tracker.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
//		// this->cpTracker->CheckTracker();
//		#endif
//	
//		// Maintain
//		Delete_UD(upAW_new, EUD_TYPE_AW);
//	
//		// Get addr physical 
//		int nPA = this->GetPA(nVA);
//	
//		#ifdef DEBUG_MMU
//		assert (nPA >= MIN_ADDR);
//		assert (nPA <= MAX_ADDR);
//		#endif
//	
//		// Set addr physical
//		cpAW->SetAddr(nPA);
//	
//		#ifdef DEBUG_MMU
//		assert (this->cpTx_AW->IsBusy() == ERESULT_TYPE_NO);
//		#endif
//	
//		// Put Tx
//		this->cpTx_AW->PutAx(cpAW);
//	
//		#ifdef DEBUG_MMU
//		printf("[Cycle %3ld: %s.Do_AW_fwd_AT] (%s) put Tx_AW.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
//		// cpAW->Display();
//		#endif
//	
//		// Debug
//		// printf("[Cycle %3ld: %s.Do_AR_fwd_SI_AT] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//		// cpAW->Display();
//		// this->cpFIFO_AR->Display();
//		// this->cpFIFO_AR->CheckFIFO();
//	
//	
//		// Stat
//		this->nHit_AW_TLB++;
//		this->nAW_MI++;
//	
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	#ifdef DEBUG_MMU
//	assert (IsTLB_Allocate == ERESULT_TYPE_NO or IsTLB_Contiguity_Match == ERESULT_TYPE_NO);	
//	#endif
//	
//	//-------------------------------------------------
//	//    Steps 2,3,..,6 are same as AR.
//	//-------------------------------------------------
//	
//	//-------------------------------------------------
//	// 2. TLB miss. Suppose "regular" or "anchor". 
//	//    If "anchor" not in TLB, generate APTW
//	//-------------------------------------------------
//	
//	// Check "anchor" not in TLB. Generate APTW 
//	if (IsTLB_Allocate_Anchor == ERESULT_TYPE_NO) {	
//	
//		// Get VA anchor
//		int64_t nVA_anchor = GetVA_Anchor_AT(nVA);	
//	
//		// Check FLPD
//		EResultType IsFLPDHit = this->IsFLPDHit(nVA_anchor, nCycle);	
//		
//		// Generate APTW 
//		char cPktName[50];
//		CPAxPkt cpAR_new = NULL;
//		
//		int64_t nAddr_PTW = -1;
//		
//		// Generate "anchor" APTW
//		if (IsFLPDHit == ERESULT_TYPE_NO) {  // FLPD miss
//			
//			// Get address for 1st-level table	
//			nAddr_PTW = GetFirst_PTWAddr(nVA_anchor);
//		
//			// Set pkt
//			sprintf(cPktName, "1st_APTW_for_%s", cpAW->GetName().c_str());
//			cpAR_new = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);
//		
//			//               nID,       nAddr,      nLen
//			cpAR_new->SetPkt(ARID_APTW, nAddr_PTW,  0);
//			cpAR_new->SetSrcName(this->cName);
//			cpAR_new->SetTransType(ETRANS_TYPE_FIRST_APTW);
//			cpAR_new->SetVA(nVA_anchor);
//	
//			#ifdef DEBUG_MMU
//			// printf("[Cycle %3ld: %s.Do_AW_fwd_AT] %s generated.\n", nCycle, this->cName.c_str(), cPktName);
//			// cpAR_new->Display();
//			#endif
//		
//			// Stat
//			this->nAPTW_1st_AT++; 
//		} 
//		else if (IsFLPDHit == ERESULT_TYPE_YES) {  // FLPD hit
//			
//			// Get address for 2nd-level table  
//			nAddr_PTW = GetSecond_PTWAddr(nVA_anchor);
//		
//			// Set pkt
//			sprintf(cPktName, "2nd_APTW_for_%s", cpAW->GetName().c_str());
//			cpAR_new = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);
//		
//			//                  nID,       nAddr,      nLen
//			// cpAR_new->SetPkt(ARID_APTW, nAddr_PTW,  0);
//			cpAR_new->SetID(ARID_APTW);
//			cpAR_new->SetAddr(nAddr_PTW);
//			cpAR_new->SetSrcName(this->cName);
//			cpAR_new->SetTransType(ETRANS_TYPE_SECOND_APTW);
//			cpAR_new->SetVA(nVA_anchor);
//		
//			#if defined SINGLE_FETCH
//			cpAR_new->SetLen(0);
//			#elif defined BLOCK_FETCH
//			cpAR_new->SetLen(MAX_BURST_LENGTH - 1);		// MAX_BURST_LENGTH 4 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
//			#elif defined HALF_FETCH
//			cpAR_new->SetLen(MAX_BURST_LENGTH/2 - 1);	// BURST_LENGTH     2 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
//			#elif defined QUARTER_FETCH
//			cpAR_new->SetLen(MAX_BURST_LENGTH/4 - 1);	// BURST_LENGTH     1 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
//			#endif
//		
//			#ifdef DEBUG_MMU
//			// printf("[Cycle %3ld: %s.Do_AW_fwd_AT] %s generated.\n", nCycle, this->cName.c_str(), cPktName);
//			// cpAR_new->Display();
//			#endif
//		
//			// Stat
//			this->nHit_AW_FLPD++;
//			this->nAPTW_2nd_AT++;
//		} 
//		else {
//			assert (0);
//		};
//		
//		// Push tracker "anchor"
//		UPUD upAW_new = new UUD;
//		upAW_new->cpAW = Copy_CAxPkt(cpAW);	// Original
//		
//		if (IsFLPDHit == ERESULT_TYPE_NO) {
//			this->cpTracker->Push(upAW_new, EUD_TYPE_AW, ECACHE_STATE_TYPE_FIRST_APTW);	
//		} 
//		else if (IsFLPDHit == ERESULT_TYPE_YES) {
//			this->cpTracker->Push(upAW_new, EUD_TYPE_AW, ECACHE_STATE_TYPE_SECOND_APTW);	
//		} 
//		else {
//			assert (0);
//		};
//	
//		#ifdef DEBUG_MMU	
//		// printf("[Cycle %3ld: %s.Do_AW_fwd_AT] %s push MO_tracker.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
//		// this->cpTracker->CheckTracker();
//		#endif
//		
//		// Maintain "anchor"
//		Delete_UD(upAW_new, EUD_TYPE_AW);
//		
//		UPUD upAR_new2 = new UUD;
//		upAR_new2->cpAR = cpAR_new;		// PTW
//		
//		// Push FIFO AR "anchor"
//		this->cpFIFO_AR->Push(upAR_new2, MMU_FIFO_AR_TLB_MISS_LATENCY);
//	
//		#ifdef DEBUG_MMU
//		// printf("[Cycle %3ld: %s.Do_AW_fwd_AT] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
//		// cpAR_new->Display();
//		// this->cpFIFO_AR->Display();
//		// this->cpFIFO_AR->CheckFIFO();
//		#endif
//		
//		// Maintain "anchor"
//		Delete_UD(upAR_new2, EUD_TYPE_AR);
//		
//		// Stat
//		this->nAPTW_total_AT++; 
//		this->IsPTW_AW_ongoing = ERESULT_TYPE_YES;
//	};
//	
//	
//	//-------------------------------------------------
//	// 3. TLB miss. Suppose "regular". 
//	//    If "regular" not in TLB, generate PTW
//	//-------------------------------------------------
//	
//	// Miss. Generate "regular" PTW 
//	if (IsVA_Regular == ERESULT_TYPE_YES and IsTLB_Allocate == ERESULT_TYPE_NO) {	
//	
//		// Check FLPD
//		EResultType IsFLPDHit = this->IsFLPDHit(nVA, nCycle);	
//		
//		// Generate AR PTW 
//		char cPktName[50];
//		CPAxPkt cpAR_new = NULL;
//		
//		int64_t nAddr_PTW = -1;
//		
//		// Generate "regular" PTW
//		if (IsFLPDHit == ERESULT_TYPE_NO) {  // FLPD miss
//			
//			// Get address for 1st-level table	
//			nAddr_PTW = GetFirst_PTWAddr(nVA);
//		
//			// Set pkt
//			sprintf(cPktName, "1st_PTW_for_%s", cpAW->GetName().c_str());
//			cpAR_new = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);
//		
//			//               nID,       nAddr,      nLen
//			cpAR_new->SetPkt(ARID_PTW,  nAddr_PTW,  0);
//			cpAR_new->SetSrcName(this->cName);
//			cpAR_new->SetTransType(ETRANS_TYPE_FIRST_PTW);
//			cpAR_new->SetVA(nVA);
//	
//			#ifdef DEBUG_MMU
//			// printf("[Cycle %3ld: %s.Do_AW_fwd_AT] %s generated.\n", nCycle, this->cName.c_str(), cPktName);
//			// cpAR_new->Display();
//			#endif
//		
//			// Stat
//			this->nPTW_1st++; 
//		} 
//		else if (IsFLPDHit == ERESULT_TYPE_YES) {  // FLPD hit
//			
//			// Get address for 2nd-level table  
//			nAddr_PTW = GetSecond_PTWAddr(nVA);
//		
//			// Set pkt
//			sprintf(cPktName, "2nd_PTW_for_%s", cpAW->GetName().c_str());
//			cpAR_new = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);
//		
//			//                  nID,       nAddr,      nLen
//			// cpAR_new->SetPkt(ARID_PTW,  nAddr_PTW,  0);
//			cpAR_new->SetID(ARID_PTW);
//			cpAR_new->SetAddr(nAddr_PTW);
//			cpAR_new->SetSrcName(this->cName);
//			cpAR_new->SetTransType(ETRANS_TYPE_SECOND_PTW);
//			cpAR_new->SetVA(nVA);
//		
//			#if defined SINGLE_FETCH
//			cpAR_new->SetLen(0);
//			#elif defined BLOCK_FETCH
//			cpAR_new->SetLen(MAX_BURST_LENGTH - 1);		// MAX_BURST_LENGTH 4 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
//			#elif defined HALF_FETCH
//			cpAR_new->SetLen(MAX_BURST_LENGTH/2 - 1);	// BURST_LENGTH     2 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
//			#elif defined QUARTER_FETCH
//			cpAR_new->SetLen(MAX_BURST_LENGTH/4 - 1);	// BURST_LENGTH     1 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
//			#endif
//	
//			#ifdef DEBUG_MMU	
//			// printf("[Cycle %3ld: %s.Do_AW_fwd_AT] %s generated.\n", nCycle, this->cName.c_str(), cPktName);
//			// cpAR_new->Display();
//			#endif
//		
//			// Stat
//			this->nHit_AW_FLPD++;	
//			this->nPTW_2nd++; 
//		} 
//		else {
//			assert (0);
//		};
//		
//		// Push tracker "regular"
//		UPUD upAW_new = new UUD;
//		upAW_new->cpAW = Copy_CAxPkt(cpAW);	// Original
//		
//		if (IsFLPDHit == ERESULT_TYPE_NO) {
//			this->cpTracker->Push(upAW_new, EUD_TYPE_AW, ECACHE_STATE_TYPE_FIRST_PTW);	
//		} 
//		else if (IsFLPDHit == ERESULT_TYPE_YES) {
//			this->cpTracker->Push(upAW_new, EUD_TYPE_AW, ECACHE_STATE_TYPE_SECOND_PTW);	
//		} 
//		else {
//			assert (0);
//		};
//		
//		#ifdef DEBUG_MMU	
//		// printf("[Cycle %3ld: %s.Do_AW_fwd_AT] %s push MO_tracker.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
//		// this->cpTracker->CheckTracker();
//		#endif
//		
//		// Maintain "regular"
//		Delete_UD(upAW_new, EUD_TYPE_AW);
//		
//		UPUD upAR_new2 = new UUD;
//		upAR_new2->cpAR = cpAR_new;		// PTW
//		
//		// Push FIFO AR "regular"
//		this->cpFIFO_AR->Push(upAR_new2, MMU_FIFO_AR_TLB_MISS_LATENCY);
//	
//		#ifdef DEBUG_MMU
//		// printf("[Cycle %3ld: %s.Do_AW_fwd_AT] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
//		// cpAR_new->Display();
//		// this->cpFIFO_AR->Display();
//		// this->cpFIFO_AR->CheckFIFO();
//		#endif
//		
//		// Maintain "regular"
//		Delete_UD(upAR_new2, EUD_TYPE_AR);
//		
//		// Stat
//		this->nPTW_total++; 
//		this->IsPTW_AW_ongoing = ERESULT_TYPE_YES;
//	};
//	
//	return (ERESULT_TYPE_SUCCESS);
// };



//----------------------------------------------
// AW valid. RMM
//----------------------------------------------
// 	RTLB lookup. 
//----------------------------------------------
//	1. MMU receives transaction
//	2. Lookup RTLB
//	3. If RTLB hit, RTLB translates address
//	4. Lookup TLB. Traditional operation
//----------------------------------------------
//	Similar to AR. Differences are:
//	(1) Check AR priority
//	(2) No FIFO_AR. Directly Put Tx 
//----------------------------------------------
//	RMM  : Range Memory Mapping (Karakostas_ISCA15 paper)
//	RTLB : Redundant TLB
//	RPTW : Redundant PTW
//	RT   : Range Table
//	RTE  : Range Table Entry
//----------------------------------------------
EResultType CMMU::Do_AW_fwd_RMM(int64_t nCycle) {

	//-----------------------------------
	// 1. MMU receives transaction
	//-----------------------------------
	// Check Tx valid
	if (this->cpTx_AW->IsBusy() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check AR priority and busy 
	if (this->Is_AR_priority == ERESULT_TYPE_YES and this->cpRx_AR->IsBusy() == ERESULT_TYPE_YES) { // AR priority and accepted 
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check remote-Tx valid. Get.
	CPAxPkt cpAW = this->cpRx_AW->GetPair()->GetAx();
	if (cpAW == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Get AW info
	// int  nID = cpAW->GetID();
	int64_t nVA = cpAW->GetAddr();

	// Stat
	int64_t nVPN = GetVPNNum(nVA);
	if (nVPN < this->AW_min_VPN) { this->AW_min_VPN = nVPN; };
	if (nVPN > this->AW_max_VPN) { this->AW_max_VPN = nVPN; };

	// Check MO
	if (this->GetMO_Ax() >= MAX_MO_COUNT) {	// AR + AW
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check ID dependent node doing PTW. Check Performance
	// if (this->cpTracker->IsUDMatchPTW(EUD_TYPE_AW) == ERESULT_TYPE_YES) {		// Check any PTW
	// if (this->cpTracker->IsIDMatchPTW(nID, EUD_TYPE_AW) == ERESULT_TYPE_YES) {		// Check ID PTW
	if (this->cpTracker->IsAnyPTW() == ERESULT_TYPE_YES) {					// Check any PTW. RPTW, PTW independent
	// if (this->cpTracker->IsIDMatch(nID, EUD_TYPE_AW) == ERESULT_TYPE_YES) {		// Check ID dependent node is outstanding
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check address dependency
	if (this->cpTracker->IsAnyPTW_Match_VPN(nVA) == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	#ifdef DEBUG_MMU
	assert (this->cpRx_AW->IsBusy() == ERESULT_TYPE_NO);
	#endif

	// Put Rx
	this->cpRx_AW->PutAx(cpAW);

	#ifdef DEBUG_MMU
	printf("[Cycle %3ld: %s.Do_AW_fwd_RMM] %s put Rx_AW.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
	// cpAW->Display();
	#endif

	//-----------------------------------
	// 2. Lookup RTLB 
	//-----------------------------------
	EResultType IsRTLBHit_RMM = this->IsRTLBHit_RMM(nVA, nCycle);	

	#ifdef DEBUG_MMU	
	if (IsRTLBHit_RMM == ERESULT_TYPE_YES) {	
		// printf("[Cycle %3ld: %s.Do_AW_fwd_RMM] %s RTLB hit.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
	}
	else {
		// printf("[Cycle %3ld: %s.Do_AW_fwd_RMM] %s RTLB miss.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
	};
	#endif

	int nPA = -1;

	//-----------------------------------
	// 3. If RTLB hit, RTLB translates address
	//-----------------------------------
	// RTLB Hit. Translate address
	if (IsRTLBHit_RMM == ERESULT_TYPE_YES) {	

		// Push tracker
		UPUD upAW_new1 = new UUD;
		upAW_new1->cpAW = Copy_CAxPkt(cpAW);	// Original VA
		this->cpTracker->Push(upAW_new1, EUD_TYPE_AW, ECACHE_STATE_TYPE_HIT);	

		#ifdef DEBUG_MMU
		// printf("[Cycle %3ld: %s.Do_AW_fwd_RMM] %s push MO_tracker.\n", nCycle, this->cName.c_str(), upAW_new1->cpAW->GetName().c_str());
		// this->cpTracker->CheckTracker();
		#endif

		// Maintain
		Delete_UD(upAW_new1, EUD_TYPE_AW);
	
		// // Push FIFO_AW
		// UPUD upAW_new2 = new UUD;
		// upAW_new2->cpAW = Copy_CAxPkt(cpAW);
		// 
		// // Encode ID
		// nID = nID << 1; // RPTW ID is 0x0
		// upAW_new2->cpAW->SetID(nID);
		// 
		// assert (this->cpFIFO_AW->IsFull() == ERESULT_TYPE_NO);
		// this->cpFIFO_AW->Push(upAW_new2, MMU_FIFO_AR_TLB_HIT_LATENCY);
		// 
		// // Debug
		// assert (upAR_new2 != NULL);
		// // printf("[Cycle %3ld: %s.Do_AW_fwd_RMM] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), upAR_new2->cpAR->GetName().c_str());
		// // cpAR->Display();
		// // this->cpFIFO_AR->Display();
		// // this->cpFIFO_AR->CheckFIFO();
		// 
		// // Maintain
		// Delete_UD(upAR_new2, EUD_TYPE_AR);

		// Get addr physical
		nPA = this->GetPA_RMM(nVA);

		#ifdef DEBUG_MMU
		assert (nPA >= MIN_ADDR);
		assert (nPA <= MAX_ADDR);
		#endif

		// Set addr physical 
		cpAW->SetAddr(nPA);

		#ifdef DEBUG_MMU
		assert (this->cpTx_AW->IsBusy() == ERESULT_TYPE_NO);
		#endif

		// Put Tx
		this->cpTx_AW->PutAx(cpAW);

		#ifdef DEBUG_MMU
		printf("[Cycle %3ld: %s.Do_AW_fwd_RMM] (%s) put Tx_AW.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
		// cpAW->Display();
		#endif

		// Stat
		this->nHit_AW_RTLB_RMM++;
		this->nAW_MI++;

		return (ERESULT_TYPE_SUCCESS);
	};

	//---------------------------------
	// 4. Lookup TLB. Traditional operation 
	//---------------------------------
	EResultType IsTLBHit = this->IsTLBHit(nVA, nCycle);	

	#ifdef DEBUG_MMU	
	if (IsTLBHit == ERESULT_TYPE_YES) {	
		// printf("[Cycle %3ld: %s.Do_AW_fwd_RMM] %s TLB hit.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
	}
	else {
		// printf("[Cycle %3ld: %s.Do_AW_fwd_RMM] %s TLB miss.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
	};
	#endif

	// TLB hit. Translate address
	if (IsTLBHit == ERESULT_TYPE_YES) {	

		// Push tracker
		UPUD upAW_new3 = new UUD;
		upAW_new3->cpAW = Copy_CAxPkt(cpAW);	// Original VA
		this->cpTracker->Push(upAW_new3, EUD_TYPE_AW, ECACHE_STATE_TYPE_HIT);	

		#ifdef DEBUG_MMU
		// printf("[Cycle %3ld: %s.Do_AW_fwd_RMM] %s push MO_tracker.\n", nCycle, this->cName.c_str(), upAW_new3->cpAW->GetName().c_str());
		// this->cpTracker->CheckTracker();
		#endif

		// Maintain
		Delete_UD(upAW_new3, EUD_TYPE_AW);
	
		// // Push FIFO_AW
		// UPUD upAW_new4 = new UUD;
		// upAW_new4->cpAW = Copy_CAxPkt(cpAW);

		// // Encode ID
		// nID = nID << 1; // PTW ID is 0x1
		// upAW_new4->cpAW->SetID(nID);

		// assert (this->cpFIFO_AW->IsFull() == ERESULT_TYPE_NO);
		// this->cpFIFO_AW->Push(upAW_new4, MMU_FIFO_AW_TLB_HIT_LATENCY);

		// // Debug
		// assert (upAW_new4 != NULL);
		// // printf("[Cycle %3ld: %s.Do_AW_fwd_RMM] %s push FIFO_AW.\n", nCycle, this->cName.c_str(), upAW_new4->cpAW->GetName().c_str());
		// // cpAW->Display();
		// // this->cpFIFO_AW->Display();
		// // this->cpFIFO_AW->CheckFIFO();

		// // Maintain
		// Delete_UD(upAW_new4, EUD_TYPE_AW);

		// Get addr physical
		nPA = this->GetPA(nVA);

		#ifdef DEBUG_MMU
		assert (nPA >= MIN_ADDR);
		assert (nPA <= MAX_ADDR);
		#endif

		// Set addr physical 
		cpAW->SetAddr(nPA);
	
		#ifdef DEBUG_MMU
		assert (this->cpTx_AW->IsBusy() == ERESULT_TYPE_NO);
		#endif

		// Put Tx
		this->cpTx_AW->PutAx(cpAW);

		#ifdef DEBUG_MMU
		printf("[Cycle %3ld: %s.Do_AW_fwd_RMM] (%s) put Tx_AW.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
		// cpAW->Display();
		#endif

		// Stat
		this->nHit_AW_TLB++;
		this->nAW_MI++;

		return (ERESULT_TYPE_SUCCESS);
	};

	// TLB miss. Generate AR PTW 
	#ifdef DEBUG_MMU
	assert (IsTLBHit == ERESULT_TYPE_NO);	
	#endif

	// Check FLPD cache
	EResultType IsFLPDHit = this->IsFLPDHit(nVA, nCycle);	

	// Generate AR PTW 
	// char cPktName[50];
	CPAxPkt cpAR_new5 = NULL;

	int64_t nAddr_PTW = -1;

	// FLPD miss. Generate 1st PTW
	if (IsFLPDHit == ERESULT_TYPE_NO) {  // FLPD miss
		
		// Address 1st-level page table	
		nAddr_PTW = GetFirst_PTWAddr(nVA);

		// Set pkt
		char cPktName[50];
		sprintf(cPktName, "1st_PTW_for_%s", cpAW->GetName().c_str());
		cpAR_new5 = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);

		//                nID,       nAddr,      nLen
		cpAR_new5->SetPkt(ARID_PTW,  nAddr_PTW,  0);
		cpAR_new5->SetSrcName(this->cName);
		cpAR_new5->SetTransType(ETRANS_TYPE_FIRST_PTW);
		cpAR_new5->SetVA(nVA);

		#ifdef DEBUG_MMU
		// printf("[Cycle %3ld: %s.Do_AW_fwd_RMM] %s generated.\n", nCycle, this->cName.c_str(), cPktName);
		// cpAR_new5->Display();
		#endif

		// Stat
		this->nPTW_1st++; 
	} 
	else if (IsFLPDHit == ERESULT_TYPE_YES) {				// FLPD hit. Generate 2nd PTW
		
		// Address 2nd-level page table  
		nAddr_PTW = GetSecond_PTWAddr(nVA);

		// Set pkt
		char cPktName[50];
		sprintf(cPktName, "2nd_PTW_for_%s", cpAW->GetName().c_str());
		cpAR_new5 = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);

		//                  nID,       nAddr,      nLen
		// cpAR_new5->SetPkt(ARID_PTW, nAddr_PTW,  0);
		cpAR_new5->SetID(ARID_PTW);
		cpAR_new5->SetAddr(nAddr_PTW);
		cpAR_new5->SetSrcName(this->cName);
		cpAR_new5->SetTransType(ETRANS_TYPE_SECOND_PTW);
		cpAR_new5->SetVA(nVA);

		#if defined SINGLE_FETCH
		cpAR_new5->SetLen(0);
		#elif defined BLOCK_FETCH
		cpAR_new5->SetLen(MAX_BURST_LENGTH - 1);			// MAX_BURST_LENGTH 4 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
		#elif defined HALF_FETCH
		cpAR_new5->SetLen(MAX_BURST_LENGTH/2 - 1);			// BURST_LENGTH     2 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
		#elif defined QUARTER_FETCH
		cpAR_new5->SetLen(MAX_BURST_LENGTH/4 - 1);			// BURST_LENGTH     1 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
		#endif

		#ifdef DEBUG_MMU	
		// printf("[Cycle %3ld: %s.Do_AW_fwd_RMM] %s generated.\n", nCycle, this->cName.c_str(), cPktName);
		// cpAR_new5->Display();
		#endif

		// Stat
		this->nHit_AR_FLPD++;	
		this->nPTW_2nd++; 
	} 
	else {
		assert (0);
	};


	// Push AW tracker (PTW doing for AW)
	UPUD upAW_new5 = new UUD;
	upAW_new5->cpAW = Copy_CAxPkt(cpAW);

	if (IsFLPDHit == ERESULT_TYPE_NO) {
		this->cpTracker->Push(upAW_new5, EUD_TYPE_AW, ECACHE_STATE_TYPE_FIRST_PTW);	
	} 
	else if (IsFLPDHit == ERESULT_TYPE_YES) {
		this->cpTracker->Push(upAW_new5, EUD_TYPE_AW, ECACHE_STATE_TYPE_SECOND_PTW);	
	} 
	else {
		assert (0);
	};

	// W control
	// this->IsPTW_AW_ongoing = ERESULT_TYPE_YES;

	#ifdef DEBUG_MMU	
	// printf("[Cycle %3ld: %s.Do_AW_fwd_RMM] %s push MO_tracker.\n", nCycle, this->cName.c_str(), upAW_new5->cpAW->GetName().c_str());
	// this->cpTracker->CheckTracker();
	#endif

	// Maintain
	Delete_UD(upAW_new5, EUD_TYPE_AR);

	// Push FIFO AR
	UPUD upAR_new8 = new UUD;
	upAR_new8->cpAR = cpAR_new5;

	#ifdef DEBUG_MMU
	assert (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_NO);
	assert (upAR_new8 != NULL);
	#endif

	this->cpFIFO_AR->Push(upAR_new8, MMU_FIFO_AR_TLB_MISS_LATENCY);

	#ifdef DEBUG_MMU
	// printf("[Cycle %3ld: %s.Do_AW_fwd_RMM] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), upAR_new8->cpAR->GetName().c_str());
	// cpAR_new5->Display();
	// this->cpFIFO_AR->Display();
	// this->cpFIFO_AR->CheckFIFO();
	#endif

	// Maintain
	Delete_UD(upAR_new8, EUD_TYPE_AR);

	// Stat
	this->nPTW_total++; 
	this->IsPTW_AR_ongoing = ERESULT_TYPE_YES;

	return (ERESULT_TYPE_SUCCESS);
};


//--------------------------------------------------------------------------------
// AW valid
//--------------------------------------------------------------------------------
//	AR and AW share a TLB. Priority between AR and AW is switched (to do round-robin arbitration).
//	When MMU receives AW, the MMU checks whether hit or miss.
//	Then MMU allocates in the tracker.
//	In the Do_W_fwd(), we wait for the WLAST. After that, MMU provides a hit service (to translate address) or miss service (to generate PTW)
//	This routine is independent of W
//	This function is similar to AR
//--------------------------------------------------------------------------------
//	Do we allow multiple outstanding PTW? In this work, we allow one PTW because a single master operates a single process.
//	When PTW on going for AR, do we allow outstanding AW? In this work, we stall when any PTW is on going.
//--------------------------------------------------------------------------------
EResultType CMMU::Do_AW_fwd(int64_t nCycle) {

	// Check Tx valid
	if (this->cpTx_AW->IsBusy() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check Rx_AR
	// When AR accepted, stall AW
	// if (this->cpRx_AR->IsBusy() == ERESULT_TYPE_YES) {
	// 	return (ERESULT_TYPE_SUCCESS);
	// };

	// Check AR priority and busy 
	if (this->Is_AR_priority == ERESULT_TYPE_YES and this->cpRx_AR->IsBusy() == ERESULT_TYPE_YES) { // AR priority and accepted 
		return (ERESULT_TYPE_SUCCESS);
	};
	
	// Check remote-Tx. Get.
	CPAxPkt cpAW = this->cpRx_AW->GetPair()->GetAx();
	if (cpAW == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Get AW info
	// int nID = cpAW->GetID();
	int64_t nVA = cpAW->GetAddr();

	// Stat
	int64_t nVPN = GetVPNNum(nVA);
	if (nVPN < this->AW_min_VPN) { this->AW_min_VPN = nVPN; };
	if (nVPN > this->AW_max_VPN) { this->AW_max_VPN = nVPN; };

	// Check MO
	// if (this->GetMO_Ax() >= MAX_MO_COUNT) {	// AR + AW. Be careful
	//	return (ERESULT_TYPE_SUCCESS);
	// };

	// Check ID dependent node doing PTW
	// if (this->cpTracker->IsUDMatchPTW(EUD_TYPE_AW) == ERESULT_TYPE_YES) {		// Check any PTW 
	if (this->cpTracker->IsAnyPTW() == ERESULT_TYPE_YES) {					// Check any PTW 
	// if (this->cpTracker->IsIDMatchPTW(nID, EUD_TYPE_AW) == ERESULT_TYPE_YES) {		// Check ID PTW 
	// if (this->cpTracker->IsIDMatch(nID, EUD_TYPE_AW) == ERESULT_TYPE_YES) {		// Check ID outstanding
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check address dependency
	if (this->cpTracker->IsAnyPTW_Match_VPN(nVA) == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Lookup TLB 
	EResultType IsTLBHit = this->IsTLBHit(nVA, nCycle);	

	#ifdef DEBUG_MMU
	if (IsTLBHit == ERESULT_TYPE_YES) {	
		// printf("[Cycle %3ld: %s.Do_AW_fwd] %s TLB hit.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
	}
	else {
		// printf("[Cycle %3ld: %s.Do_AW_fwd] %s TLB miss.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
	};
	#endif

	#ifdef DEBUG_MMU
	assert (this->cpRx_AW->IsBusy() == ERESULT_TYPE_NO);
	#endif	

	// Put Rx	
	this->cpRx_AW->PutAx(cpAW);

	#ifdef DEBUG_MMU
	printf("[Cycle %3ld: %s.Do_AW_fwd] (%s) put Rx_AW.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
	// cpAW->Display();
	#endif
	

	// TLB hit. Translate address
	if (IsTLBHit == ERESULT_TYPE_YES) {	

		// Push tracker
		UPUD upAW_new = new UUD;
		upAW_new->cpAW = Copy_CAxPkt(cpAW);	// Original VA  
		this->cpTracker->Push(upAW_new, EUD_TYPE_AW, ECACHE_STATE_TYPE_HIT);	

		#ifdef DEBUG_MMU
		// printf("[Cycle %3ld: %s.Do_AW_fwd] %s push MO_tracker.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
		// this->cpTracker->CheckTracker();
		#endif

		// Maintain
		Delete_UD(upAW_new, EUD_TYPE_AW);
	
		// Get addr physical
		int nPA = this->GetPA(nVA);

		#ifdef DEBUG_MMU
		assert (nPA >= MIN_ADDR);
		assert (nPA <= MAX_ADDR);
		#endif

		// Set addr physical
		cpAW->SetAddr(nPA);
		
		#ifdef DEBUG_MMU
		assert (this->cpTx_AW->IsBusy() == ERESULT_TYPE_NO);
		#endif

		// Put Tx
		this->cpTx_AW->PutAx(cpAW);

		#ifdef DEBUG_MMU
		printf("[Cycle %3ld: %s.Do_AW_fwd] (%s) put Tx_AW.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
		// cpAW->Display();
		#endif

		// Stat
		this->nHit_AW_TLB++;
		this->nAW_MI++;

		return (ERESULT_TYPE_SUCCESS);
	};

	// TLB miss. Generate AR PTW 
	#ifdef DEBUG_MMU
	assert (IsTLBHit == ERESULT_TYPE_NO);	
	#endif

	// Check FLPD cache
	EResultType IsFLPDHit = this->IsFLPDHit(nVA, nCycle);	

	// Generate AR PTW 
	char cPktName[50];
	CPAxPkt cpAR_new = NULL;

	int64_t nAddr_PTW = -1;

	// Generate PTW
	if (IsFLPDHit == ERESULT_TYPE_NO) {  // FLPD miss
		sprintf(cPktName, "1st_PTW_for_%s", cpAW->GetName().c_str());
	
		// Set pkt
		cpAR_new = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);
		
		// Get address 1st-level page table	
		nAddr_PTW = GetFirst_PTWAddr(nVA);

		//               nID,       nAddr,      nLen
		cpAR_new->SetPkt(ARID_PTW,  nAddr_PTW,  0);
		cpAR_new->SetSrcName(this->cName);
		cpAR_new->SetTransType(ETRANS_TYPE_FIRST_PTW);
		cpAR_new->SetVA(nVA);

		#ifdef DEBUG_MMU
		// printf("[Cycle %3ld: %s.Do_AW_fwd] (%s) generated.\n", nCycle, this->cName.c_str(), cPktName);
		// cpAR_new->Display();
		#endif

		// Stat
		this->nPTW_1st++;		
	} 
	else if (IsFLPDHit == ERESULT_TYPE_YES) {  // FLPD hit
		sprintf(cPktName, "2nd_PTW_for_%s", cpAW->GetName().c_str());

		// Set pkt
		cpAR_new = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);
		
		// Get address 2nd-level table  
		nAddr_PTW = GetSecond_PTWAddr(nVA);

		//                  nID,       nAddr,      nLen
		// cpAR_new->SetPkt(ARID_PTW,  nAddr_PTW,  0);
		cpAR_new->SetAddr(nAddr_PTW);
		cpAR_new->SetID(ARID_PTW);
		cpAR_new->SetSrcName(this->cName);
		cpAR_new->SetTransType(ETRANS_TYPE_SECOND_PTW);
		cpAR_new->SetVA(nVA);
		#if defined SINGLE_FETCH
		cpAR_new->SetLen(0);
		#elif defined BLOCK_FETCH
		cpAR_new->SetLen(MAX_BURST_LENGTH - 1);		// MAX_BURST_LENGTH 4 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
		#elif defined HALF_FETCH
		cpAR_new->SetLen(MAX_BURST_LENGTH/2 - 1);	// BURST_LENGTH     2 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
		#elif defined QUARTER_FETCH
		cpAR_new->SetLen(MAX_BURST_LENGTH/4 - 1);	// BURST_LENGTH     1 when MAX_TRANS_SIZE 64 bytes and BURST_SIZE 16 bytes
		#endif

		#ifdef DEBUG_MMU
		// printf("[Cycle %3ld: %s.Do_AW_fwd] (%s) generated.\n", nCycle, this->cName.c_str(), cPktName);
		// cpAR_new->Display();
		#endif

		// Stat
		this->nHit_AW_FLPD++;	
		this->nPTW_2nd++;		
	} 
	else {
		assert (0);
	};


	// Push tracker PTW
	UPUD upAW_new = new UUD;
	upAW_new->cpAW = Copy_CAxPkt(cpAW);	// Original AW

	if (IsFLPDHit == ERESULT_TYPE_NO) {
		this->cpTracker->Push(upAW_new, EUD_TYPE_AW, ECACHE_STATE_TYPE_FIRST_PTW);	
	} 
	else if (IsFLPDHit == ERESULT_TYPE_YES) {
		this->cpTracker->Push(upAW_new, EUD_TYPE_AW, ECACHE_STATE_TYPE_SECOND_PTW);	
	} 
	else {
		assert (0);
	};

	// W control
	// this->IsPTW_AW_ongoing = ERESULT_TYPE_YES;

	#ifdef DEBUG_MMU
	// printf("[Cycle %3ld: %s.Do_AW_fwd] (%s) push MO_tracker.\n", nCycle, this->cName.c_str(), cpAW->GetName().c_str());
	// this->cpTracker->CheckTracker();
	#endif

	// Maintain
	Delete_UD(upAW_new, EUD_TYPE_AW);

	// Push FIFO AR
	UPUD upAR_new = new UUD;
	upAR_new->cpAR = cpAR_new;	// PTW

	#ifdef DEBUG_MMU
	assert (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_NO);
	assert (upAR_new != NULL);
	#endif

	this->cpFIFO_AR->Push(upAR_new, MMU_FIFO_AR_TLB_MISS_LATENCY);

	#ifdef DEBUG_MMU
	// printf("[Cycle %3ld: %s.Do_AW_fwd] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), cpAR_new->GetName().c_str());
	// cpAR_new->Display();
	// this->cpFIFO_AR->Display();
	// this->cpFIFO_AR->CheckFIFO();
	#endif

	// Maintain
	Delete_UD(upAR_new, EUD_TYPE_AR);

	// Stat
	this->nPTW_total++;
	this->IsPTW_AW_ongoing = ERESULT_TYPE_YES;

	return (ERESULT_TYPE_SUCCESS);
};


// W valid SI
EResultType CMMU::Do_W_fwd_SI(int64_t nCycle) {

	// Check FIFO_W
	if (this->cpFIFO_W->IsFull() == ERESULT_TYPE_YES) {
	        return (ERESULT_TYPE_SUCCESS);
	};

	// Check remote-Tx valid. Get.
	CPWPkt cpW = this->cpRx_W->GetPair()->GetW();
	if (cpW == NULL) {
	        return (ERESULT_TYPE_SUCCESS);
	};

	// Put Rx
	this->cpRx_W->PutW(cpW);

	#ifdef DEBUG_MMU
	if (cpW->IsLast() == ERESULT_TYPE_YES) {	
		printf("[Cycle %3ld: %s.Do_W_fwd_SI] (%s) WLAST put Rx_W.\n", nCycle, this->cName.c_str(), cpW->GetName().c_str());
	} 
	else {
		printf("[Cycle %3ld: %s.Do_W_fwd_SI] (%s) put Rx_W.\n", nCycle, this->cName.c_str(), cpW->GetName().c_str());
	};
	#endif

	// Push W
	UPUD upW_new = new UUD;
	upW_new->cpW = Copy_CWPkt(cpW);
	
	// Push W
	this->cpFIFO_W->Push(upW_new, 0);

	#ifdef DEBUG_MMU
	if (cpW->IsLast() == ERESULT_TYPE_YES) {	
		// printf("[Cycle %3ld: %s.Do_W_fwd_SI] (%s) WLAST push FIFO_W.\n", nCycle, this->cName.c_str(), upW_new->cpW->GetName().c_str());
	} 
	else {
		// printf("[Cycle %3ld: %s.Do_W_fwd_SI] (%s) push FIFO_W.\n", nCycle, this->cName.c_str(), upW_new->cpW->GetName().c_str());
	};
	// this->cpFIFO_W->Display();
	// this->cpFIFO_W->CheckFIFO();
	#endif

	// Maintain
	Delete_UD(upW_new, EUD_TYPE_W);

	return (ERESULT_TYPE_SUCCESS);
};


// W valid MI
EResultType CMMU::Do_W_fwd_MI(int64_t nCycle) {

	// Check Tx
	if (this->cpTx_W->IsBusy() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};
	
	// Check FIFO_W
	if (this->cpFIFO_W->IsEmpty() == ERESULT_TYPE_YES) {
	        return (ERESULT_TYPE_SUCCESS);
	};

	// Get
	CPWPkt cpW = this->cpFIFO_W->GetTop()->cpW;
	int nID = cpW->GetID();

	// Check MO tracker
	if (this->cpTracker->GetNode(nID, EUD_TYPE_AW) == NULL) {
	        return (ERESULT_TYPE_SUCCESS);
	};

	#ifdef DEBUG_MMU
	assert (cpW != NULL);
	assert (this->cpTracker->GetNode(nID, EUD_TYPE_AW) != NULL);
	#endif

	// Check state 
	ECacheStateType eState = this->cpTracker->GetNode(nID, EUD_TYPE_AW)->eState;	
	
	// Hit. Translate address. Put Ax. Put W. 
	if (eState == ECACHE_STATE_TYPE_HIT) {	 // Progress W only when AW is doing HIT. This makes W dependent on AW. 

		// Pop
		UPUD upW_new = this->cpFIFO_W->Pop();

		#ifdef DEBUG_MMU
		assert (upW_new != NULL);
		// this->cpFIFO_W->CheckFIFO();
		// this->cpFIFO_W->Display();
		#endif
		
		// Get W 
		CPWPkt cpW_new = upW_new->cpW;
		
		// Debug
		// cpW_new->CheckPkt();
		
		// Put W
		this->cpTx_W->PutW(cpW_new);

		#ifdef DEBUG_MMU
		if (cpW_new->IsLast() == ERESULT_TYPE_YES) {		
			printf("[Cycle %3ld: %s.Do_W_fwd_MI] (%s) WLAST put Tx_W.\n", nCycle, this->cName.c_str(), cpW_new->GetName().c_str()); 
		} 
		else {
			printf("[Cycle %3ld: %s.Do_W_fwd_MI] (%s) put Tx_W.\n", nCycle, this->cName.c_str(), cpW_new->GetName().c_str()); 
		};
		#endif
	
		// Maintain
		Delete_UD(upW_new, EUD_TYPE_W);
	};

	return (ERESULT_TYPE_SUCCESS);
};


// AR ready 
EResultType CMMU::Do_AR_bwd(int64_t nCycle) {

	// Check Rx
	if (this->cpRx_AR->IsBusy() == ERESULT_TYPE_NO) {
		return (ERESULT_TYPE_SUCCESS);
	};

	#ifdef DEBUG_MMU
	assert (this->cpRx_AR->IsBusy() == ERESULT_TYPE_YES);
	#endif

	// Set ready
	this->cpRx_AR->SetAcceptResult(ERESULT_TYPE_ACCEPT);

	#ifdef DEBUG_MMU
	printf("[Cycle %3ld: %s.Do_AR_bwd] (%s) is handshaked at SI.\n", nCycle, this->cName.c_str(), this->cpRx_AR->GetAx()->GetName().c_str());
	#endif

	// MO stat
	this->Increase_MO_AR();
	this->Increase_MO_Ax();
	
	// Stat
	this->nAR_SI++;
	
	// Stat Max MO	
	if (this->GetMO_AR() > this->nMax_MO_AR) { this->nMax_MO_AR++; };
	if (this->GetMO_Ax() > this->nMax_MO_Ax) { this->nMax_MO_Ax++; };

	return (ERESULT_TYPE_SUCCESS);
};


// AW ready 
EResultType CMMU::Do_AW_bwd(int64_t nCycle) {

	// Check Rx
	if (this->cpRx_AW->IsBusy() == ERESULT_TYPE_NO) {
		return (ERESULT_TYPE_SUCCESS);
	};

	#ifdef DEBUG_MMU
	assert (this->cpRx_AW->IsBusy() == ERESULT_TYPE_YES);
	#endif

	// Set ready
	this->cpRx_AW->SetAcceptResult(ERESULT_TYPE_ACCEPT);

	#ifdef DEBUG_MMU
	printf("[Cycle %3ld: %s.Do_AW_bwd] (%s) is handshaked at SI.\n", nCycle, this->cName.c_str(), this->cpRx_AW->GetAx()->GetName().c_str());
	#endif

	// MO stat
	this->Increase_MO_AW();
	this->Increase_MO_Ax();

	// Stat
	this->nAW_SI++;

	// Stat Max MO	
	if (this->GetMO_AW() > this->nMax_MO_AW) { this->nMax_MO_AW++; };
	if (this->GetMO_Ax() > this->nMax_MO_Ax) { this->nMax_MO_Ax++; };

	return (ERESULT_TYPE_SUCCESS);
};


// W ready 
EResultType CMMU::Do_W_bwd(int64_t nCycle) {

	// Check Rx 
	if (this->cpRx_W->IsBusy() == ERESULT_TYPE_YES) {
		// Set ready 
		this->cpRx_W->SetAcceptResult(ERESULT_TYPE_ACCEPT);
	};

	return (ERESULT_TYPE_SUCCESS);
};


//-------------------------------------------------
// R valid
//-------------------------------------------------
//	1. MMU receives R transaction
//	2. If not PTW, send to SI 
//	3. If 2nd PTW, update TLB. Translate address. Send to MI 
//	4. If 1st PTW, update FLPD. Generate 2nd PTW
//-------------------------------------------------
EResultType CMMU::Do_R_fwd(int64_t nCycle) {

	//------------------------------
	// 1. MMU receives R transaction
	//------------------------------
	// Check Tx valid
	if (this->cpTx_R->IsBusy() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check remote-Tx valid	
	CPRPkt cpR = this->cpRx_R->GetPair()->GetR();
	if (cpR == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	#ifdef DEBUG_MMU
	assert (this->cpRx_R->GetPair()->IsBusy() == ERESULT_TYPE_YES);
	#endif

	// Check FIFO_AR PTW 
	if (cpR->GetID() == ARID_PTW and this->cpFIFO_AR->IsFull() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Put Rx
	this->cpRx_R->PutR(cpR);
	#ifdef DEBUG_MMU
	assert (cpR != NULL);
	#endif

	#ifdef DEBUG_MMU
	if (cpR->IsLast() == ERESULT_TYPE_YES) {
		printf("[Cycle %3ld: %s.Do_R_fwd] (%s) RID 0x%x put Rx_R.\n", nCycle, this->cName.c_str(), cpR->GetName().c_str(), cpR->GetID());
		// cpR->Display();
	} 
	else {
		printf("[Cycle %3ld: %s.Do_R_fwd] (%s) RID 0x%x put Rx_R.\n", nCycle, this->cName.c_str(), cpR->GetName().c_str(), cpR->GetID());
		// cpR->Display();
	};
	#endif

	//------------------------------
	// 2. If not PTW, forward to SI 
	//------------------------------
	int nID = cpR->GetID();
	if (nID != ARID_PTW) {

		// ID decode 
		nID = nID >> 1;
		cpR->SetID(nID); 

		// Put Tx
		this->cpTx_R->PutR(cpR);

		return (ERESULT_TYPE_SUCCESS);
	};

	// Check RLAST PTW
	if (cpR->IsLast() != ERESULT_TYPE_YES) {  // wait RLAST
	 	 return (ERESULT_TYPE_SUCCESS);
	};

	#ifdef DEBUG_MMU
	assert (cpR->IsLast() == ERESULT_TYPE_YES);
	assert (cpR->GetID() == ARID_PTW);	
	#endif

	// Get Ax info
	int64_t nVA = -1;

	// Get tracker node first matching PTW
	SPLinkedCUD spNode = this->cpTracker->GetNode(ECACHE_STATE_TYPE_FIRST_PTW);
	if (spNode == NULL) {
		spNode = this->cpTracker->GetNode(ECACHE_STATE_TYPE_SECOND_PTW);
	};
	#ifdef DEBUG_MMU
	assert (spNode != NULL);
	#endif

	// Get original Ax info
	CPAxPkt cpAR = NULL;
	CPAxPkt cpAW = NULL;
	EUDType eUDType = spNode->eUDType;
	if (eUDType == EUD_TYPE_AR) {
		cpAR = spNode->upData->cpAR;
		nID  = spNode->upData->cpAR->GetID();
		nVA  = spNode->upData->cpAR->GetAddr();
	} 
	else if (eUDType == EUD_TYPE_AW) {
		cpAW = spNode->upData->cpAW;
		nID  = spNode->upData->cpAW->GetID();
		nVA  = spNode->upData->cpAW->GetAddr();
	} 
	else {
		assert (0);
	};

	#ifdef DEBUG_MMU
	assert (nVA != -1);
	assert (nID != -1);
	assert (eUDType != EUD_TYPE_UNDEFINED);
	#endif

	//------------------------------------
	// 3. If 2nd PTW, update TLB. Translate address. Send to MI 
	//------------------------------------
	if (spNode->eState == ECACHE_STATE_TYPE_SECOND_PTW) { // PTW finished

		// Update TLB 
		#if defined SINGLE_FETCH
		this->FillTLB_SF(nVA, nCycle);
		#elif defined BLOCK_FETCH
		this->FillTLB_BF(nVA, nCycle);
		#elif defined HALF_FETCH
			#ifdef CAMB_ENABLE
			this->FillTLB_CAMB(nVA, nCycle);
			#else
			this->FillTLB_BF(nVA, nCycle);
			#endif
		#elif defined QUARTER_FETCH
			#ifdef CAMB_ENABLE
			this->FillTLB_CAMB(nVA, nCycle);
			#else
			this->FillTLB_BF(nVA, nCycle);
			#endif
		#endif

		// Update tracker state 
		// 1. Search head node matching ID, UDType, ECACHE_STATE_TYPE_SECOND_PTW
		// 2. Update state (into ECACHE_STATE_TYPE_HIT)
		this->cpTracker->SetStateNode(nID, eUDType, ECACHE_STATE_TYPE_SECOND_PTW, ECACHE_STATE_TYPE_HIT);

		// Translate addr 
		int nPA = GetPA(nVA);
		#ifdef DEBUG_MMU
		assert (nPA >= MIN_ADDR);
		assert (nPA <= MAX_ADDR);
		#endif
		
		// Set transaction info	
		if (eUDType == EUD_TYPE_AR) {
			#ifdef DEBUG_MMU
			assert (cpAR != NULL);
			#endif
			cpAR->SetID(nID << 1);
			cpAR->SetAddr(nPA);

			// Push FIFO AR
			UPUD upAR_new = new UUD;
			upAR_new->cpAR = Copy_CAxPkt(cpAR);	// Translated original
			#ifdef DEBUG_MMU
			assert (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_NO);
			#endif

			this->cpFIFO_AR->Push(upAR_new, MMU_FIFO_AR_PTW_FINISH_LATENCY);

			#ifdef DEBUG_MMU
			assert (upAR_new != NULL);
			// printf("[Cycle %3ld: %s.Do_R_fwd] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
			// cpAR->Display();
			// this->cpFIFO_AR->Display();
			// this->cpFIFO_AR->CheckFIFO();
			#endif

			// Maintain
			Delete_UD(upAR_new, EUD_TYPE_AR);	

			// Stat
			this->IsPTW_AR_ongoing = ERESULT_TYPE_NO;
		}
		else if (eUDType == EUD_TYPE_AW) {

			#ifdef DEBUG_MMU
			assert (cpAW != NULL);
			#endif

			cpAW->SetAddr(nPA);

			#ifdef DEBUG_MMU
			assert (this->cpTx_AW->IsBusy() == ERESULT_TYPE_NO);
			#endif

			// Put Tx_AW
			this->cpTx_AW->PutAx(cpAW);

			// Stat
			this->nAW_MI++;

			// W control
			this->IsPTW_AW_ongoing = ERESULT_TYPE_NO;
		}
		else {
			assert (0);
		};
	};

	//---------------------------------
	// 4. If 1st PTW, update FLPD. Generate 2nd PTW
	//---------------------------------
	if (spNode->eState == ECACHE_STATE_TYPE_FIRST_PTW) { // PTW not finished

		// Generate AR PTW 
		char cPktName[50];
		if (eUDType == EUD_TYPE_AR) {
			sprintf(cPktName, "2nd_PTW_for_%s", cpAR->GetName().c_str());
		} 
		else if (eUDType == EUD_TYPE_AW) {
			sprintf(cPktName, "2nd_PTW_for_%s", cpAW->GetName().c_str());
		} 
		else {
			assert (0);	
		};

		// Get address 2nd-level page table	
		int nAddr_SecondPTW = GetSecond_PTWAddr(nVA); 
	
		// Generate PTW 
		CPAxPkt cpAR_new = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);

		//                  nID,       nAddr,           nLen
		// cpAR_new->SetPkt(ARID_PTW,  nAddr_SecondPTW, 0);
		cpAR_new->SetID(ARID_PTW);
		cpAR_new->SetAddr(nAddr_SecondPTW);
		cpAR_new->SetSrcName(this->cName);
		cpAR_new->SetTransType(ETRANS_TYPE_SECOND_PTW);
		cpAR_new->SetVA(nVA);
		#if defined SINGLE_FETCH
		cpAR_new->SetLen(0);
		#elif defined BLOCK_FETCH
		cpAR_new->SetLen(MAX_BURST_LENGTH - 1);
		#elif defined HALF_FETCH
		cpAR_new->SetLen(MAX_BURST_LENGTH/2 - 1);
		#elif defined QUARTER_FETCH
		cpAR_new->SetLen(MAX_BURST_LENGTH/4 - 1);
		#endif

		#ifdef DEBUG_MMU
		// printf("[Cycle %3ld: %s.Do_R_fwd] (%s) generated for second PTW.\n", nCycle, this->cName.c_str(), cPktName);
		// cpAR_new->Display();
		#endif

		// Push FIFO AR
		UPUD upAR_new = new UUD;
		upAR_new->cpAR = cpAR_new;
		#ifdef DEBUG_MMU
		assert (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_NO);
		#endif

		this->cpFIFO_AR->Push(upAR_new, MMU_FIFO_AR_2ND_PTW_LATENCY);

		#ifdef DEBUG_MMU
		assert (upAR_new != NULL);
		// printf("[Cycle %3ld: %s.Do_R_fwd] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
		// cpAR->Display();
		// this->cpFIFO_AR->Display();
		// this->cpFIFO_AR->CheckFIFO();
		#endif
	
		// Maintain
		Delete_UD(upAR_new, EUD_TYPE_AR);

		// Get victim entry number FLPD
		int nVictimFLPD = GetVictimFLPD();
		int64_t nVPN = nVA >> BIT_1M_PAGE; // 1MB page. BIT_1M_PAGE 20

		// Update FLPD 
		this->FillFLPD(nVictimFLPD, nVPN, nCycle);

		// Update tracker state 
		// (1) Search head node matching ID, eUDType, ECACHE_STATE_TYPE_FIRST_PTW
		// (2) Update state (into ECACHE_STATE_TYPE_SECOND_PTW)
		this->cpTracker->SetStateNode(nID, eUDType, ECACHE_STATE_TYPE_FIRST_PTW, ECACHE_STATE_TYPE_SECOND_PTW);

		// Stat
		this->nPTW_total++;
		this->nPTW_2nd++;
	};

	return (ERESULT_TYPE_SUCCESS);
};


//--------------------------------------------------------------------
// R valid. AT
//--------------------------------------------------------------------
//	Algorithm "regular"
//		   Regular	Anchor		Contiguity	Operation	Allocation			Tracker pop
//		
//		1. Miss		Allocated	Not match	(1) PTW		(2) Fill "regular" in TLB	-
//		2. Miss		Not allocated	Match		(1) PTW, APTW	(2) Fill "anchor"  in TLB	PTW "regular"
//		3. Miss		Not allocated	Not match	(1) PTW, APTW	(2) Fill "regular" in TLB	APTW "anchor"
//
//--------------------------------------------------------------------
//	Algorithm "anchor"           
//		  Regular	Anchor		Contiguity	Operation	Allocation      
//
//		1. -		Not allocated	-		(1) APTW	(2) Fill "anchor"  in TLB	-
//--------------------------------------------------------------------
// 	Operation
//		1. MMU receives R transaction
//		2. If not (A)PTW, send to SI 
//
//		   Get PTW info.
//		3. If "1st APTW", update FLPD. Generate "2nd APTW".
//		4. If "1st PTW",  update FLPD. Generate "2nd PTW". 
//
//		5. If "2nd APTW", update tracker state "DONE". 
//		   Check contiguity match.
//
//		   (5.1) If match, update TLB. 
//		         Check "pair PTW" in tracker. 
//		   	 (5.1a) If there is no pair, translate address. Send to MI. 
//
//		   (5.2) Otherwise, wait "2nd PTW" finish.
//
//		6. If "2nd PTW", check "pair APTW".  
//		   (6.1) If there is no "pair APTW" in tracker, update TLB. Translate address. Send to MI.
//
//		   (6.2) If there is "pair APTW", check "2nd APTW" finished. 
//		       Check APTW contiguity match. 
//		       (6.2a) If     match, do not update TLB for PTW. Translate address. Send to MI. Pop APTW tracker.
//		       (6.2b) If not match,        update TLB for PTW. Translate address. Send to MI. Pop APTW tracker.
//--------------------------------------------------------------------
// EResultType CMMU::Do_R_fwd_AT(int64_t nCycle) {
//	
//	//------------------------------
//	// 1. MMU receives R transaction
//	//------------------------------
//	// Check Tx valid
//	if (this->cpTx_R->IsBusy() == ERESULT_TYPE_YES) {
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	// Check remote-Tx valid. Get.	
//	CPRPkt cpR = this->cpRx_R->GetPair()->GetR();
//	if (cpR == NULL) {
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	#ifdef DEBUG_MMU
//	assert (this->cpRx_R->GetPair()->IsBusy() == ERESULT_TYPE_YES);
//	#endif
//	
//	// Check FIFO_AR PTW 
//	if (cpR->GetID() == ARID_PTW and cpR->GetID() == ARID_APTW) { 
//		if (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_YES) {
//			return (ERESULT_TYPE_SUCCESS);
//		};
//	};
//	
//	// Put Rx
//	this->cpRx_R->PutR(cpR);
//	#ifdef DEBUG_MMU
//	assert (cpR != NULL);
//	#endif
//	
//	#ifdef DEBUG_MMU
//	if (cpR->IsLast() == ERESULT_TYPE_YES) {
//		printf("[Cycle %3ld: %s.Do_R_fwd_AT] (%s) RID 0x%x put Rx_R.\n", nCycle, this->cName.c_str(), cpR->GetName().c_str(), cpR->GetID());
//		// cpR->Display();
//	} 
//	else {
//		printf("[Cycle %3ld: %s.Do_R_fwd_AT] (%s) RID 0x%x put Rx_R.\n", nCycle, this->cName.c_str(), cpR->GetName().c_str(), cpR->GetID());
//		// cpR->Display();
//	};
//	#endif
//	
//	//------------------------------
//	// 2. If not (A)PTW, forward to SI 
//	//------------------------------
//	int nID = cpR->GetID();
//	if (nID != ARID_PTW and nID != ARID_APTW) {
//	
//		// ID decode 
//		nID = nID >> 1;
//		cpR->SetID(nID); 
//	
//		// Put Tx
//		this->cpTx_R->PutR(cpR);
//	
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	// Check RLAST PTW
//	if (cpR->IsLast() != ERESULT_TYPE_YES) {  // wait RLAST
//	 	 return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	#ifdef DEBUG_MMU
//	assert (cpR->IsLast() == ERESULT_TYPE_YES);
//	assert (nID == ARID_PTW or nID == ARID_APTW);	
//	#endif
//	
//	//------------------------------
//	// 3.0. Get PTW info 
//	//------------------------------
//	// Get Ax info
//	int64_t nVA = -1;
//	
//	// Get tracker node first matching (A)PTW
//	SPLinkedCUD spNode = NULL; 
//	
//	if (nID == ARID_PTW) {
//		spNode = this->cpTracker->GetNode_PTW();
//	}
//	else if (nID == ARID_APTW) {
//		spNode = this->cpTracker->GetNode_APTW_AT();
//	}
//	else {
//		assert (0);
//	};
//	// SPLinkedCUD spNode = this->cpTracker->GetNode(nID);
//	// SPLinkedCUD spNode = this->cpTracker->GetNode_anyPTW();
//	
//	// SPLinkedCUD spNode = this->cpTracker->GetNode(ECACHE_STATE_TYPE_FIRST_PTW);
//	// if (spNode == NULL) {
//	// 	spNode = this->cpTracker->GetNode(ECACHE_STATE_TYPE_SECOND_PTW);
//	// };
//	
//	#ifdef DEBUG_MMU
//	assert (spNode != NULL);
//	#endif
//	
//	// Get original Ax info
//	CPAxPkt cpAR = NULL;
//	CPAxPkt cpAW = NULL;
//	EUDType eUDType = spNode->eUDType;
//	if (eUDType == EUD_TYPE_AR) {
//		cpAR = spNode->upData->cpAR;
//		nID  = spNode->upData->cpAR->GetID();
//		nVA  = spNode->upData->cpAR->GetAddr();
//	} 
//	else if (eUDType == EUD_TYPE_AW) {
//		cpAW = spNode->upData->cpAW;
//		nID  = spNode->upData->cpAW->GetID();
//		nVA  = spNode->upData->cpAW->GetAddr();
//	} 
//	else {
//		assert (0);
//	};
//	
//	#ifdef DEBUG_MMU
//	assert (nVA >= 0);
//	assert (nID >= 0);
//	assert (eUDType != EUD_TYPE_UNDEFINED);
//	#endif
//	
//	// // Get PTW info
//	// EResultType IsPTW  = IsVA_Regular_AT(nVA);
//	// EResultType IsAPTW = IsVA_Anchor_AT(nVA);
//	
//	
//	//------------------------------------------------------
//	// 3. If "1st APTW", update FLPD. Generate "2nd APTW".
//	//------------------------------------------------------
//	if (spNode->eState == ECACHE_STATE_TYPE_FIRST_APTW) {				// Doing APTW
//	
//		// Debug
//		// assert (nID == ARID_APTW);
//	
//		// Generate 2nd APTW 
//		char cPktName[50];
//		if (eUDType == EUD_TYPE_AR) {
//			sprintf(cPktName, "2nd_APTW_for_%s", cpAR->GetName().c_str());
//		} 
//		else if (eUDType == EUD_TYPE_AW) {
//			sprintf(cPktName, "2nd_APTW_for_%s", cpAW->GetName().c_str());
//		} 
//		else {
//			assert (0);	
//		};
//	
//		// Get address 2nd-level page table	
//		int nAddr_SecondPTW = GetSecond_PTWAddr(nVA); 
//	
//		// Generate APTW 
//		CPAxPkt cpAR_new = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);
//	
//		//                  nID,       nAddr,           nLen
//		// cpAR_new->SetPkt(ARID_APTW, nAddr_SecondPTW, 0);
//		cpAR_new->SetID(ARID_APTW);
//		cpAR_new->SetAddr(nAddr_SecondPTW);
//		cpAR_new->SetSrcName(this->cName);
//		cpAR_new->SetTransType(ETRANS_TYPE_SECOND_APTW);
//		cpAR_new->SetVA(nVA);
//		#if defined SINGLE_FETCH
//		cpAR_new->SetLen(0);
//		#elif defined BLOCK_FETCH
//		cpAR_new->SetLen(MAX_BURST_LENGTH - 1);
//		#elif defined HALF_FETCH
//		cpAR_new->SetLen(MAX_BURST_LENGTH/2 - 1);
//		#elif defined QUARTER_FETCH
//		cpAR_new->SetLen(MAX_BURST_LENGTH/4 - 1);
//		#endif
//	
//		#ifdef DEBUG_MMU
//		// printf("[Cycle %3ld: %s.Do_R_fwd_AT] (%s) generated for second APTW.\n", nCycle, this->cName.c_str(), cPktName);
//		// cpAR_new->Display();
//		#endif
//	
//		// Push FIFO AR
//		UPUD upAR_new = new UUD;
//		upAR_new->cpAR = cpAR_new;
//		#ifdef DEBUG_MMU
//		assert (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_NO);
//		#endif
//	
//		this->cpFIFO_AR->Push(upAR_new, MMU_FIFO_AR_2ND_PTW_LATENCY);
//	
//		#ifdef DEBUG_MMU
//		assert (upAR_new != NULL);
//		// printf("[Cycle %3ld: %s.Do_R_fwd_AT] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//		// cpAR->Display();
//		// this->cpFIFO_AR->Display();
//		// this->cpFIFO_AR->CheckFIFO();
//		#endif
//	
//		// Maintain
//		Delete_UD(upAR_new, EUD_TYPE_AR);
//	
//		// Fill FLPD
//		// Check hit. 1st PTW could fill already.
//		EResultType IsFLPDHit = this->IsFLPDHit(nVA, nCycle);
//		if (IsFLPDHit == ERESULT_TYPE_NO) {
//			// Get victim entry number FLPD
//			int nVictim_entry = GetVictimFLPD();
//			int64_t nVPN = nVA >> BIT_1M_PAGE;				// 1MB page. BIT_1M_PAGE 20
//	
//			// Update FLPD 
//			this->FillFLPD(nVictim_entry, nVPN, nCycle);
//		};
//	
//		// Update tracker state 
//		// 	Search head node matching ID, eUDType, ECACHE_STATE_TYPE_FIRST_APTW
//		// 	Update state (into ECACHE_STATE_TYPE_SECOND_APTW)
//		this->cpTracker->SetStateNode(nID, eUDType, ECACHE_STATE_TYPE_FIRST_APTW, ECACHE_STATE_TYPE_SECOND_APTW);
//	
//		// Stat
//		this->nAPTW_total_AT++;
//		this->nAPTW_2nd_AT++;
//	
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	
//	//------------------------------------------------------
//	// 4. If "1st PTW", update FLPD if miss. Generate "2nd PTW".
//	//------------------------------------------------------
//	if (spNode->eState == ECACHE_STATE_TYPE_FIRST_PTW) {				// Doing PTW
//	
//		// Debug
//		// assert (nID == ARID_PTW); 
//	
//		// Generate 2nd PTW 
//		char cPktName[50];
//		if (eUDType == EUD_TYPE_AR) {
//			sprintf(cPktName, "2nd_PTW_for_%s", cpAR->GetName().c_str());
//		} 
//		else if (eUDType == EUD_TYPE_AW) {
//			sprintf(cPktName, "2nd_PTW_for_%s", cpAW->GetName().c_str());
//		} 
//		else {
//			assert (0);	
//		};
//	
//		// Get address 2nd-level page table	
//		int nAddr_SecondPTW = GetSecond_PTWAddr(nVA); 
//	
//		// Generate PTW 
//		CPAxPkt cpAR_new = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);
//	
//		//                  nID,       nAddr,           nLen
//		// cpAR_new->SetPkt(ARID_PTW,  nAddr_SecondPTW, 0);
//		cpAR_new->SetID(ARID_PTW);
//		cpAR_new->SetAddr(nAddr_SecondPTW);
//		cpAR_new->SetSrcName(this->cName);
//		cpAR_new->SetTransType(ETRANS_TYPE_SECOND_PTW);
//		cpAR_new->SetVA(nVA);
//		#if defined SINGLE_FETCH
//		cpAR_new->SetLen(0);
//		#elif defined BLOCK_FETCH
//		cpAR_new->SetLen(MAX_BURST_LENGTH - 1);
//		#elif defined HALF_FETCH
//		cpAR_new->SetLen(MAX_BURST_LENGTH/2 - 1);
//		#elif defined QUARTER_FETCH
//		cpAR_new->SetLen(MAX_BURST_LENGTH/4 - 1);
//		#endif
//	
//		#ifdef DEBUG_MMU
//		// printf("[Cycle %3ld: %s.Do_R_fwd_AT] (%s) generated for second PTW.\n", nCycle, this->cName.c_str(), cPktName);
//		// cpAR_new->Display();
//		#endif
//	
//		// Push FIFO AR
//		UPUD upAR_new = new UUD;
//		upAR_new->cpAR = cpAR_new;
//		#ifdef DEBUG_MMU
//		assert (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_NO);
//		#endif
//	
//		this->cpFIFO_AR->Push(upAR_new, MMU_FIFO_AR_2ND_PTW_LATENCY);
//	
//		#ifdef DEBUG_MMU
//		assert (upAR_new != NULL);
//		// printf("[Cycle %3ld: %s.Do_R_fwd_AT] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//		// cpAR->Display();
//		// this->cpFIFO_AR->Display();
//		// this->cpFIFO_AR->CheckFIFO();
//		#endif
//	
//		// Maintain
//		Delete_UD(upAR_new, EUD_TYPE_AR);
//	
//		// Fill FLPD
//		// Check hit. 1st APTW could fill already.
//		EResultType IsFLPDHit = this->IsFLPDHit(nVA, nCycle);
//		if (IsFLPDHit == ERESULT_TYPE_NO) {
//			// Get victim entry number FLPD
//			int nVictim_entry = GetVictimFLPD();
//			int64_t nVPN = nVA >> BIT_1M_PAGE;			// 1MB page. BIT_1M_PAGE 20
//	
//			// Update FLPD 
//			this->FillFLPD(nVictim_entry, nVPN, nCycle);
//		};
//	
//		// Update tracker state 
//		//	Search head node matching ID, eUDType, ECACHE_STATE_TYPE_FIRST_PTW
//		//	Update state (into ECACHE_STATE_TYPE_SECOND_PTW)
//		this->cpTracker->SetStateNode(nID, eUDType, ECACHE_STATE_TYPE_FIRST_PTW, ECACHE_STATE_TYPE_SECOND_PTW);
//	
//		// Stat
//		this->nPTW_total++;
//		this->nPTW_2nd++;
//	
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	
//	//-----------------------------------
//	// 5. If "2nd APTW", update tracker state "DONE". 
//	//   Check contiguity match.
//	//
//	//   (5.1) If match, update TLB. 
//	//         Check "pair PTW" in tracker. 
//	//   	 (5.1a) If there is no pair, translate address. Send to MI. 
//	//
//	//   (5.2) Otherwise, wait "2nd PTW" finish.
//	//-----------------------------------
//	// Check What if APTW only? 
//	//       How to POP tracker?
//	//-----------------------------------
//	if (spNode->eState == ECACHE_STATE_TYPE_SECOND_APTW) { // PTW finished
//	
//		// Debug
//		// assert (nID == ARID_APTW);
//	
//		// Update tracker state 
//		// 	Search head node matching ID, eUDType, ECACHE_STATE_TYPE_SECOND_APTW
//		// 	Update state (into ECACHE_STATE_TYPE_SECOND_APTW_DONE)
//		this->cpTracker->SetStateNode(nID, eUDType, ECACHE_STATE_TYPE_SECOND_APTW, ECACHE_STATE_TYPE_SECOND_APTW_DONE);
//	
//		// Check contiguity match
//		EResultType IsAnchor_Contiguity_Match = IsAnchor_Cover_AT(nVA);
//	
//		// Update TLB
//		if (IsAnchor_Contiguity_Match == ERESULT_TYPE_YES) {
//	
//			// Get VA aligned
//			int64_t nVA_anchor = GetVA_Anchor_AT(nVA);
//			
//			// Update TLB
//			#if defined SINGLE_FETCH
//			this->FillTLB_SF(nVA_anchor, nCycle);
//			#elif defined BLOCK_FETCH
//			this->FillTLB_BF(nVA_anchor, nCycle);
//			#elif defined HALF_FETCH
//			this->FillTLB_BF(nVA_anchor, nCycle);
//			#elif defined QUARTER_FETCH
//			this->FillTLB_BF(nVA_anchor, nCycle);
//			#endif
//		};	
//	
//		//---------------------------------------
//		// (5.1a) Chek "pair PTW" in tracker. If no pair, translate address. Send to MI.
//		//---------------------------------------
//		SPLinkedCUD spNode_PTW = this->cpTracker->GetNode_Pair_PTW_VA_AT(nVA, eUDType);
//		
//		// No pair. Translate address. Send to MI.
//		if (spNode_PTW == NULL) {								// No pair
//		
//			// Translate addr
//			int nPA = GetPA(nVA);
//			#ifdef DEBUG_MMU
//			assert (nPA >= MIN_ADDR);
//			assert (nPA <= MAX_ADDR);
//			#endif
//			
//			// Update tracker state 
//			// 	Search head node matching ID, UDType, ECACHE_STATE_TYPE_SECOND_APTW_DONE 
//			// 	Update state (into ECACHE_STATE_TYPE_HIT)
//			this->cpTracker->SetStateNode(nID, eUDType, ECACHE_STATE_TYPE_SECOND_APTW_DONE, ECACHE_STATE_TYPE_HIT);
//	
//			// Set transaction info	
//			if (eUDType == EUD_TYPE_AR) {
//				#ifdef DEBUG_MMU
//				assert (cpAR != NULL);
//				#endif
//				cpAR->SetID(nID << 1);
//				cpAR->SetAddr(nPA);
//			
//				// Push FIFO AR
//				UPUD upAR_new = new UUD;
//				upAR_new->cpAR = Copy_CAxPkt(cpAR);	// Translated original
//				#ifdef DEBUG_MMU
//				assert (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_NO);
//				#endif
//			
//				this->cpFIFO_AR->Push(upAR_new, MMU_FIFO_AR_PTW_FINISH_LATENCY);
//		
//				#ifdef DEBUG_MMU	
//				assert (upAR_new != NULL);
//				// printf("[Cycle %3ld: %s.Do_R_fwd_AT] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//				// cpAR->Display();
//				// this->cpFIFO_AR->Display();
//				// this->cpFIFO_AR->CheckFIFO();
//				#endif
//			
//				// Maintain
//				Delete_UD(upAR_new, EUD_TYPE_AR);	
//			
//				// Stat
//				this->IsPTW_AR_ongoing = ERESULT_TYPE_NO;
//			}
//			else if (eUDType == EUD_TYPE_AW) {
//	
//				#ifdef DEBUG_MMU
//				assert (cpAW != NULL);
//				#endif
//	
//				cpAW->SetAddr(nPA);
//			
//				#ifdef DEBUG_MMU
//				assert (this->cpTx_AW->IsBusy() == ERESULT_TYPE_NO);
//				#endif
//	
//				// Put Tx_AW
//				this->cpTx_AW->PutAx(cpAW);
//			
//				// Stat
//				this->nAW_MI++;
//			
//				// W control
//				this->IsPTW_AW_ongoing = ERESULT_TYPE_NO;
//			}
//			else {
//				assert (0);
//			};
//		
//			return (ERESULT_TYPE_SUCCESS);
//		};
//	
//		//----------------------------------------	
//		//  (5.2) Otherwise, wait "2nd PTW" finish.
//		//----------------------------------------	
//		#ifdef DEBUG_MMU
//		assert (spNode_PTW != NULL);
//		#endif
//	
//		return (ERESULT_TYPE_SUCCESS);
//	};
//	
//	
//	//------------------------------------
//	// 6. If "2nd PTW", check "pair APTW".  
//	//   (6.1) If there is no "pair APTW" in tracker, update TLB. Translate address. Send to MI.
//	//
//	//   (6.2) If there is "pair APTW", check "2nd APTW" finished. 
//	//       When "APTW" finished, check APTW contiguity match. 
//	//       (6.2a) If     match, do not update TLB for PTW. Translate address. Send to MI. Pop APTW tracker.
//	//       (6.2b) If not match,        update TLB for PTW. Translate address. Send to MI. Pop APTW tracker.
//	//------------------------------------
//	
//	#ifdef DEBUG_MMU	
//	assert (spNode->eState == ECACHE_STATE_TYPE_SECOND_PTW);
//	#endif
//	
//	if (spNode->eState == ECACHE_STATE_TYPE_SECOND_PTW) { 						// PTW finished
//	
//		// Debug
//		// assert (nID == ARID_PTW);
//	
//		//-------------------------------------------------
//		// (6.1) Check there is no "pair APTW" in tracker, update TLB. Translate address. Send to MI.
//		//-------------------------------------------------
//		// int64_t nVA_anchor = GetVA_Anchor_AT(nVA);
//		SPLinkedCUD spNode_APTW = this->cpTracker->GetNode_Pair_APTW_VA_AT(nVA, eUDType);
//		
//		// No pair. Update TLB. Translate address. Send to MI.
//		if (spNode_APTW == NULL) {								// No pair
//		
//			// Update TLB 
//			#if defined SINGLE_FETCH
//			this->FillTLB_SF(nVA, nCycle);
//			#elif defined BLOCK_FETCH
//			this->FillTLB_BF(nVA, nCycle);
//			#elif defined HALF_FETCH
//			this->FillTLB_BF(nVA, nCycle);
//			#elif defined QUARTER_FETCH
//			this->FillTLB_BF(nVA, nCycle);
//			#endif
//			
//			// Update tracker state 
//			// 	Search head node matching ID, UDType, ECACHE_STATE_TYPE_SECOND_PTW
//			// 	Update state (into ECACHE_STATE_TYPE_HIT)
//			this->cpTracker->SetStateNode(nID, eUDType, ECACHE_STATE_TYPE_SECOND_PTW, ECACHE_STATE_TYPE_HIT);
//			
//			// Translate addr 
//			int nPA = GetPA(nVA);
//	
//			#ifdef DEBUG_MMU
//			assert (nPA >= MIN_ADDR);
//			assert (nPA <= MAX_ADDR);
//			#endif
//			
//			// Set transaction info	
//			if (eUDType == EUD_TYPE_AR) {
//				#ifdef DEBUG_MMU
//				assert (cpAR != NULL);
//				#endif
//				cpAR->SetID(nID << 1);
//				cpAR->SetAddr(nPA);
//			
//				// Push FIFO AR
//				UPUD upAR_new = new UUD;
//				upAR_new->cpAR = Copy_CAxPkt(cpAR);	// Translated original
//				#ifdef DEBUG_MMU
//				assert (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_NO);
//				#endif
//			
//				this->cpFIFO_AR->Push(upAR_new, MMU_FIFO_AR_PTW_FINISH_LATENCY);
//		
//				#ifdef DEBUG_MMU	
//				assert (upAR_new != NULL);
//				// printf("[Cycle %3ld: %s.Do_R_fwd_AT] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//				// cpAR->Display();
//				// this->cpFIFO_AR->Display();
//				// this->cpFIFO_AR->CheckFIFO();
//				#endif
//			
//				// Maintain
//				Delete_UD(upAR_new, EUD_TYPE_AR);	
//			
//				// Stat
//				this->IsPTW_AR_ongoing = ERESULT_TYPE_NO;
//			}
//			else if (eUDType == EUD_TYPE_AW) {
//				#ifdef DEBUG_MMU
//				assert (cpAW != NULL);
//				#endif
//	
//				cpAW->SetAddr(nPA);
//			
//				#ifdef DEBUG_MMU
//				assert (this->cpTx_AW->IsBusy() == ERESULT_TYPE_NO);
//				#endif
//	
//				// Put Tx_AW
//				this->cpTx_AW->PutAx(cpAW);
//			
//				// Stat
//				this->nAW_MI++;
//			
//				// W control
//				this->IsPTW_AW_ongoing = ERESULT_TYPE_NO;
//			}
//			else {
//				assert (0);
//			};
//		
//			return (ERESULT_TYPE_SUCCESS);
//		};
//		
//		
//		//-----------------------------
//		// (6.2a) If "2nd APTW" match, do not update TLB. Translate address. Send to MI. Pop APTW tracker.
//		//-----------------------------
//		
//		#ifdef DEBUG_MMU
//		assert (spNode_APTW != NULL);
//		#endif
//		
//		// Check "2nd APTW" finish
//		if (spNode_APTW->eState != ECACHE_STATE_TYPE_SECOND_APTW_DONE) {
//		
//			return (ERESULT_TYPE_SUCCESS);
//		};
//		
//		
//		// Check "2nd APTW" contiguity match
//		EResultType IsAnchor_Contiguity_Match = IsAnchor_Cover_AT(nVA);
//		
//		if (IsAnchor_Contiguity_Match == ERESULT_TYPE_YES) {
//		
//			// Update tracker state 
//			// 	Search head node matching ID, UDType, ECACHE_STATE_TYPE_SECOND_PTW
//			// 	Update state (into ECACHE_STATE_TYPE_HIT)
//			this->cpTracker->SetStateNode(nID, eUDType, ECACHE_STATE_TYPE_SECOND_PTW, ECACHE_STATE_TYPE_HIT);
//			
//			// Translate addr 
//			int nPA = GetPA(nVA);
//			#ifdef DEBUG_MMU
//			assert (nPA >= MIN_ADDR);
//			assert (nPA <= MAX_ADDR);
//			#endif
//			
//			// Set transaction info	
//			if (eUDType == EUD_TYPE_AR) {
//				#ifdef DEBUG_MMU
//				assert (cpAR != NULL);
//				#endif
//				cpAR->SetID(nID << 1);
//				cpAR->SetAddr(nPA);
//			
//				// Push FIFO AR
//				UPUD upAR_new = new UUD;
//				upAR_new->cpAR = Copy_CAxPkt(cpAR);	// Translated original
//				#ifdef DEBUG_MMU
//				assert (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_NO);
//				#endif
//			
//				this->cpFIFO_AR->Push(upAR_new, MMU_FIFO_AR_PTW_FINISH_LATENCY);
//		
//				#ifdef DEBUG_MMU	
//				assert (upAR_new != NULL);
//				// printf("[Cycle %3ld: %s.Do_R_fwd_AT] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//				// cpAR->Display();
//				// this->cpFIFO_AR->Display();
//				// this->cpFIFO_AR->CheckFIFO();
//				#endif
//			
//				// Maintain
//				Delete_UD(upAR_new, EUD_TYPE_AR);	
//			
//				// Stat
//				this->IsPTW_AR_ongoing = ERESULT_TYPE_NO;
//			}
//			else if (eUDType == EUD_TYPE_AW) {
//	
//				#ifdef DEBUG_MMU
//				assert (cpAW != NULL);
//				#endif
//	
//				cpAW->SetAddr(nPA);
//			
//				#ifdef DEBUG_MMU
//				assert (this->cpTx_AW->IsBusy() == ERESULT_TYPE_NO);
//				#endif
//	
//				// Put Tx_AW
//				this->cpTx_AW->PutAx(cpAW);
//			
//				// Stat
//				this->nAW_MI++;
//			
//				// W control
//				this->IsPTW_AW_ongoing = ERESULT_TYPE_NO;
//			}
//			else {
//				assert (0);
//			};
//	
//			// Pop APTW tracker 
//			UPUD upAR_new2 = this->cpTracker->Pop(spNode_APTW);
//	
//			#ifdef DEBUG_MMU
//			// printf("[Cycle %3ld: %s.Do_R_fwd_AT] (%s) pop MO_tracker.\n", nC
//			// this->cpTracker->CheckTracker();
//			assert (upAR_new2 != NULL);
//			#endif
//			
//			// Maintain
//			Delete_UD(upAR_new2, eUDType);
//	
//		
//			return (ERESULT_TYPE_SUCCESS);
//		};
//		
//		
//		//-----------------------------	
//		// (6.2b) If "2nd APTW" not match, update TLB. Translate addres. Send to MI. Pop APTW tracker.
//		//-----------------------------	
//	
//		#ifdef DEBUG_MMU	
//		assert (IsAnchor_Contiguity_Match == ERESULT_TYPE_NO);
//		#endif
//		
//		// Update TLB 
//		#if defined SINGLE_FETCH
//		this->FillTLB_SF(nVA, nCycle);
//		#elif defined BLOCK_FETCH
//		this->FillTLB_BF(nVA, nCycle);
//		#elif defined HALF_FETCH
//		this->FillTLB_BF(nVA, nCycle);
//		#elif defined QUARTER_FETCH
//		this->FillTLB_BF(nVA, nCycle);
//		#endif
//		
//		// Update tracker state 
//		// 	Search head node matching ID, UDType, ECACHE_STATE_TYPE_SECOND_PTW
//		// 	Update state (into ECACHE_STATE_TYPE_HIT)
//		this->cpTracker->SetStateNode(nID, eUDType, ECACHE_STATE_TYPE_SECOND_PTW, ECACHE_STATE_TYPE_HIT);
//		
//		// Translate addr 
//		int nPA = GetPA(nVA);
//		#ifdef DEBUG_MMU
//		assert (nPA >= MIN_ADDR);
//		assert (nPA <= MAX_ADDR);
//		#endif
//		
//		// Set transaction info	
//		if (eUDType == EUD_TYPE_AR) {
//			#ifdef DEBUG_MMU
//			assert (cpAR != NULL);
//			#endif
//			cpAR->SetID(nID << 1);
//			cpAR->SetAddr(nPA);
//		
//			// Push FIFO AR
//			UPUD upAR_new = new UUD;
//			upAR_new->cpAR = Copy_CAxPkt(cpAR);	// Translated original
//			#ifdef DEBUG_MMU
//			assert (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_NO);
//			#endif
//		
//			this->cpFIFO_AR->Push(upAR_new, MMU_FIFO_AR_PTW_FINISH_LATENCY);
//	
//			#ifdef DEBUG_MMU	
//			assert (upAR_new != NULL);
//			// printf("[Cycle %3ld: %s.Do_R_fwd_AT] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), cpAR->GetName().c_str());
//			// cpAR->Display();
//			// this->cpFIFO_AR->Display();
//			// this->cpFIFO_AR->CheckFIFO();
//			#endif
//		
//			// Maintain
//			Delete_UD(upAR_new, EUD_TYPE_AR);	
//		
//			// Stat
//			this->IsPTW_AR_ongoing = ERESULT_TYPE_NO;
//		}
//		else if (eUDType == EUD_TYPE_AW) {
//	
//			#ifdef DEBUG_MMU
//			assert (cpAW != NULL);
//			#endif
//	
//			cpAW->SetAddr(nPA);
//		
//			#ifdef DEBUG_MMU
//			assert (this->cpTx_AW->IsBusy() == ERESULT_TYPE_NO);
//			#endif
//	
//			// Put Tx_AW
//			this->cpTx_AW->PutAx(cpAW);
//		
//			// Stat
//			this->nAW_MI++;
//		
//			// W control
//			this->IsPTW_AW_ongoing = ERESULT_TYPE_NO;
//		}
//		else {
//			assert (0);
//		};
//	
//		// Pop APTW tracker 
//		UPUD upAR_new2 = this->cpTracker->Pop(spNode_APTW);
//		// printf("[Cycle %3ld: %s.Do_R_fwd_AT] (%s) pop MO_tracker.\n", nC
//		// this->cpTracker->CheckTracker();
//	
//		#ifdef DEBUG_MMU	
//		assert (upAR_new2 != NULL);
//		#endif
//		
//		// Maintain
//		Delete_UD(upAR_new2, eUDType);
//	};
//	
//	return (ERESULT_TYPE_SUCCESS);
// };


//-------------------------------------------
// R valid. RMM
//-------------------------------------------
//	1. MMU receives R transaction
//	2. If not PTW or RPTW, forward to SI 
//	3. If 2nd PTW, update TLB. Translate address. Send to MI 
//	   If RPTW needed, generate RPTW  
//	4. If 1st PTW, generate 2nd PTW
//	5. If RPTW finished, update RTLB 
//------------------------------------------
EResultType CMMU::Do_R_fwd_RMM(int64_t nCycle) {

	//---------------------------
	// 1. MMU receives R transaction
	//---------------------------
	// Check Tx valid
	if (this->cpTx_R->IsBusy() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check remote-Tx valid. Get.	
	CPRPkt cpR = this->cpRx_R->GetPair()->GetR();
	if (cpR == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	#ifdef DEBUG_MMU
	assert (this->cpRx_R->GetPair()->IsBusy() == ERESULT_TYPE_YES);
	#endif

	// Check PTW or RPTW. Check FIFO_AR
	if ((cpR->GetID() == ARID_PTW or cpR->GetID() == ARID_RPTW_RMM) and this->cpFIFO_AR->IsFull() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Put Rx
	this->cpRx_R->PutR(cpR);
	#ifdef DEBUG_MMU
	assert (cpR != NULL);
	#endif

	#ifdef DEBUG_MMU
	if (cpR->IsLast() == ERESULT_TYPE_YES) {
		printf("[Cycle %3ld: %s.Do_R_fwd_RMM] (%s) RID 0x%x put Rx_R.\n", nCycle, this->cName.c_str(), cpR->GetName().c_str(), cpR->GetID());
		// cpR->Display();
	} 
	else {
		printf("[Cycle %3ld: %s.Do_R_fwd_RMM] (%s) RID 0x%x put Rx_R.\n", nCycle, this->cName.c_str(), cpR->GetName().c_str(), cpR->GetID());
		// cpR->Display();
	};
	#endif

	//---------------------------
	// 2. If not PTW or RPTW, forward to SI 
	//---------------------------
	int nID = cpR->GetID();
	if (nID != ARID_PTW and nID != ARID_RPTW_RMM) {

		// Decode ID
		nID = nID >> 1;
		cpR->SetID(nID); 

		// Put Tx
		this->cpTx_R->PutR(cpR);

		return (ERESULT_TYPE_SUCCESS);
	};

	// Check RLAST PTW
	if (cpR->IsLast() != ERESULT_TYPE_YES) {  // wait RLAST
	 	 return (ERESULT_TYPE_SUCCESS);
	};

	#ifdef DEBUG_MMU
	assert (cpR->IsLast() == ERESULT_TYPE_YES);
	assert (cpR->GetID() == ARID_PTW or cpR->GetID() == ARID_RPTW_RMM);	
	#endif

	// Get Ax info
	int64_t nVA = -1;

	// Get tracker node first matching PTW 
	SPLinkedCUD spNode = NULL; 

	if (nID == ARID_PTW) {
		spNode = this->cpTracker->GetNode(ECACHE_STATE_TYPE_FIRST_PTW);
		if (spNode == NULL) {
			spNode = this->cpTracker->GetNode(ECACHE_STATE_TYPE_SECOND_PTW);
		};
	}
	else if (nID == ARID_RPTW_RMM) {
		spNode = this->cpTracker->GetNode(ECACHE_STATE_TYPE_FIRST_RPTW);
		if (spNode == NULL) {
			spNode = this->cpTracker->GetNode(ECACHE_STATE_TYPE_SECOND_RPTW);
		};
		if (spNode == NULL) {
			spNode = this->cpTracker->GetNode(ECACHE_STATE_TYPE_THIRD_RPTW);
		};
	}
	else {
		assert (0);
	};

	// spNode = this->cpTracker->GetNode(nID, EUD_TYPE_AR); // This nID should be Orignal AxID

	#ifdef DEBUG_MMU
	assert (spNode != NULL);
	#endif

	// Get original Ax info
	CPAxPkt cpAR = NULL;
	CPAxPkt cpAW = NULL;
	EUDType eUDType = spNode->eUDType;
	if (eUDType == EUD_TYPE_AR) {
		cpAR = spNode->upData->cpAR;
		nID  = spNode->upData->cpAR->GetID();
		nVA  = spNode->upData->cpAR->GetAddr();
	} 
	else if (eUDType == EUD_TYPE_AW) {
		cpAW = spNode->upData->cpAW;
		nID  = spNode->upData->cpAW->GetID();
		nVA  = spNode->upData->cpAW->GetAddr();
	} 
	else {
		assert (0);
	};

	#ifdef DEBUG_MMU
	assert (nVA != -1);
	assert (nID != -1);
	assert (eUDType != EUD_TYPE_UNDEFINED);
	#endif

	//------------------------------
	// 3. If 2nd PTW, update TLB. Translate address. Send to MI 
	//    If RPTW needed, generate RPTW  
	//------------------------------
	if (spNode->eState == ECACHE_STATE_TYPE_SECOND_PTW) { // PTW finished

		// Update TLB 
		#if defined SINGLE_FETCH
		this->FillTLB_SF_RMM(nVA, nCycle); // No contiguity in TLB
		#elif defined BLOCK_FETCH
		this->FillTLB_BF(nVA, nCycle);
		#elif defined HALF_FETCH
		this->FillTLB_BF(nVA, nCycle);
		#elif defined QUARTER_FETCH
		this->FillTLB_BF(nVA, nCycle);
		#endif

		// Update tracker state 
		// (1) Search head node matching ID, UDType, ECACHE_STATE_TYPE_SECOND_PTW
		// (2) Update state (into ECACHE_STATE_TYPE_HIT)
		this->cpTracker->SetStateNode(nID, eUDType, ECACHE_STATE_TYPE_SECOND_PTW, ECACHE_STATE_TYPE_HIT);

		// Translate addr 
		int nPA = GetPA(nVA);
		#ifdef DEBUG_MMU
		assert (nPA >= MIN_ADDR);
		assert (nPA <= MAX_ADDR);
		#endif
		
		// Set transaction info	
		if (eUDType == EUD_TYPE_AR) {
			#ifdef DEBUG_MMU
			assert (cpAR != NULL);
			#endif
			cpAR->SetID(nID << 1);
			cpAR->SetAddr(nPA);

			// Push FIFO AR
			UPUD upAR_new1 = new UUD;
			upAR_new1->cpAR = Copy_CAxPkt(cpAR);
			#ifdef DEBUG_MMU
			assert (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_NO);
			#endif

			this->cpFIFO_AR->Push(upAR_new1, MMU_FIFO_AR_PTW_FINISH_LATENCY);

			#ifdef DEBUG_MMU
			assert (upAR_new1 != NULL);
			// printf("[Cycle %3ld: %s.Do_R_fwd_RMM] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), upAR_new1->cpAR->GetName().c_str());
			// cpAR->Display();
			// this->cpFIFO_AR->Display();
			// this->cpFIFO_AR->CheckFIFO();
			#endif

			// Maintain
			Delete_UD(upAR_new1, EUD_TYPE_AR);	

			// Control 
			this->IsPTW_AR_ongoing = ERESULT_TYPE_NO;
		} 
		else if (eUDType == EUD_TYPE_AW) {

			#ifdef DEBUG_MMU
			assert (cpAW != NULL);
			#endif

			cpAW->SetAddr(nPA);

			#ifdef DEBUG_MMU
			assert (this->cpTx_AW->IsBusy() == ERESULT_TYPE_NO);
			#endif

			// Put Tx_AW
			this->cpTx_AW->PutAx(cpAW);

			// Stat
			this->nAW_MI++;

			// W control
			this->IsPTW_AW_ongoing = ERESULT_TYPE_NO;
		}
		else {
			assert (0);
		};

		//-----------------------------------
		// Finished PTW. Generate RPTW. 
		//-----------------------------------
		// In this experiment, page table and contiguity are given info.
		//-----------------------------------
		// Do RPTW when
		// (1) Both RTLB and TLB are miss 
		// (2) VPN is within contiguous range 
		// (3) RTE number is less than max value (MAX_RTE_NUM_RMM. e.g.124)
		//-----------------------------------

		// // Check RTE number
		// int nRTE = -1;
		// if (eUDType == EUD_TYPE_AR) {
		// 	nRTE = (nVA - this->nAR_START_ADDR)/16384;
		// }
		// else if (eUDType == EUD_TYPE_AW) {
		// 	nRTE = (nVA - this->nAW_START_ADDR)/16384;
		// }
		// else {
		// 	assert (0);
		// };
		// assert (nRTE >= 0);
		
		// Check RPTW needed
		EResultType Is_RPTW_need = IsRPTW_need_RMM(nVA, eUDType, nCycle);
		
		if (Is_RPTW_need == ERESULT_TYPE_YES) { // Within maximum RTE number 
		
			// Address range table
			char cPktName[50];
			// CPAxPkt cpAR_new = NULL;
			int64_t nVA_aligned = nVA & 0x7FFFFFFFFFFFC000; // 16kB (=2^14 bytes) aligned 
			
			// Set pkt
			if (eUDType == EUD_TYPE_AR) {
				sprintf(cPktName, "1st_RPTW_for_%s", cpAR->GetName().c_str());
			}
			else if (eUDType == EUD_TYPE_AW) {
				sprintf(cPktName, "1st_RPTW_for_%s", cpAW->GetName().c_str());
			}
			else {
				assert (0);
			};

			CPAxPkt cpAR_new3 = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);
			
			//                   ID,            Addr,        Len
			// cpAR_new3->SetPkt(ARID_RPTW_RMM, nVA_aligned, 0);
			cpAR_new3->SetID(ARID_RPTW_RMM);		// 0
			cpAR_new3->SetAddr(nVA_aligned);		// Original VA aligned with 16kB (four pages). 
			cpAR_new3->SetSrcName(this->cName);
			cpAR_new3->SetTransType(ETRANS_TYPE_FIRST_RPTW);
			cpAR_new3->SetVA(nVA);
			cpAR_new3->SetLen(0);				// Single fetch
		
			#ifdef DEBUG_MMU	
			// printf("[Cycle %3ld: %s.Do_R_fwd_RMM] %s generated.\n", nCycle, this->cName.c_str(), cPktName);
			// cpAR_new3->Display();
			#endif
			
			// Stat
			this->nRPTW_1st_RMM++;
			
			// Push tracker
			UPUD upAR_new3 = new UUD;
			upAR_new3->cpAR = Copy_CAxPkt(cpAR_new3);
			
			this->cpTracker->Push(upAR_new3, EUD_TYPE_AR, ECACHE_STATE_TYPE_FIRST_RPTW);

			#ifdef DEBUG_MMU
			// printf("[Cycle %3ld: %s.Do_R_fwd_RMM] %s push MO_tracker.\n", nCycle, this->cName.c_str(), upAR_new3->cpAR->GetName().c_str());
			// this->cpTracker->CheckTracker();
			#endif
			
			// Maintain
			Delete_UD(upAR_new3, EUD_TYPE_AR);
			
			// Push FIFO AR
			UPUD upAR_new4 = new UUD;
			upAR_new4->cpAR = cpAR_new3;
			
			this->cpFIFO_AR->Push(upAR_new4, MMU_FIFO_AR_TLB_MISS_LATENCY);

			#ifdef DEBUG_MMU
			// printf("[Cycle %3ld: %s.Do_R_fwd_RMM] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), upAR_new4->cpAR->GetName().c_str());
			// cpAR_new3->Display();
			// this->cpFIFO_AR->Display();
			// this->cpFIFO_AR->CheckFIFO();
			#endif
			
			// Maintain
			Delete_UD(upAR_new4, EUD_TYPE_AR);
			
			// Stat
			this->nRPTW_total_RMM++;
			this->IsRPTW_AR_ongoing_RMM = ERESULT_TYPE_YES;
		};


		return (ERESULT_TYPE_SUCCESS);
	};

	char cPktName[50];

	//------------------------------
	// 4. If 1st PTW, generate 2nd PTW
	//------------------------------
	if (spNode->eState == ECACHE_STATE_TYPE_FIRST_PTW) { // PTW not finished

		// Generate AR PTW 
		// char cPktName[50];
		if (eUDType == EUD_TYPE_AR) {
			sprintf(cPktName, "2nd_PTW_for_%s", cpAR->GetName().c_str());
		} 
		else if (eUDType == EUD_TYPE_AW) {
			sprintf(cPktName, "2nd_PTW_for_%s", cpAW->GetName().c_str());
		} 
		else {
			assert (0);	
		};

		// Address for 2nd-level table	
		int nAddr_SecondPTW = GetSecond_PTWAddr(nVA); 
	
		// Generate PTW 
		CPAxPkt cpAR_new2 = new CAxPkt(cPktName, ETRANS_DIR_TYPE_READ);

		//                  nID,       nAddr,           nLen
		// cpAR_new2->SetPkt(ARID_PTW, nAddr_SecondPTW, 0);
		cpAR_new2->SetID(ARID_PTW);
		cpAR_new2->SetAddr(nAddr_SecondPTW);
		cpAR_new2->SetSrcName(this->cName);
		cpAR_new2->SetTransType(ETRANS_TYPE_SECOND_PTW);
		cpAR_new2->SetVA(nVA);
		#if defined SINGLE_FETCH
		cpAR_new2->SetLen(0);
		#elif defined BLOCK_FETCH
		cpAR_new2->SetLen(MAX_BURST_LENGTH - 1);
		#elif defined HALF_FETCH
		cpAR_new2->SetLen(MAX_BURST_LENGTH/2 - 1);
		#elif defined QUARTER_FETCH
		cpAR_new2->SetLen(MAX_BURST_LENGTH/4 - 1);
		#endif

		#ifdef DEBUG_MMU
		// printf("[Cycle %3ld: %s.Do_R_fwd_RMM] (%s) generated for second PTW.\n", nCycle, this->cName.c_str(), cPktName);
		// cpAR_new2->Display();
		#endif

		// FIFO AR push
		UPUD upAR_new2 = new UUD;
		upAR_new2->cpAR = cpAR_new2;
		#ifdef DEBUG_MMU
		assert (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_NO);
		#endif

		this->cpFIFO_AR->Push(upAR_new2, MMU_FIFO_AR_2ND_PTW_LATENCY);

		#ifdef DEBUG_MMU
		assert (upAR_new2 != NULL);
		// printf("[Cycle %3ld: %s.Do_R_fwd_RMM] %s push FIFO_AR.\n", nCycle, this->cName.c_str(), upAR_new2->cpAR->GetName().c_str());
		// cpAR->Display();
		// this->cpFIFO_AR->Display();
		// this->cpFIFO_AR->CheckFIFO();
		#endif
	
		// Maintain
		Delete_UD(upAR_new2, EUD_TYPE_AR);

		// Get victim entry number FLPD
		int nVictimFLPD = GetVictimFLPD();
		int64_t nVPN = nVA >> BIT_1M_PAGE; // 1MB page. BIT_1M_PAGE 20

		// Update FLPD 
		this->FillFLPD(nVictimFLPD, nVPN, nCycle);

		// Update tracker state 
		// (1) Search head node matching ID, eUDType, ECACHE_STATE_TYPE_FIRST_PTW
		// (2) Update state (into ECACHE_STATE_TYPE_SECOND_PTW)
		this->cpTracker->SetStateNode(nID, eUDType, ECACHE_STATE_TYPE_FIRST_PTW, ECACHE_STATE_TYPE_SECOND_PTW);

		// Stat
		this->nPTW_total++;
		this->nPTW_2nd++;

		return (ERESULT_TYPE_SUCCESS);
	};

	//--------------------------------------------
	// 5. If RPTW finished, update RTLB 
	//--------------------------------------------
	#ifdef DEBUG_MMU
	assert (spNode->eState == ECACHE_STATE_TYPE_FIRST_RPTW);
	#endif

	// Update RTLB
	this->FillRTLB_SF_RMM(nVA, nCycle);
	
	// Maintain tracker
	UPUD upAR_new3 = this->cpTracker->Pop(nID, EUD_TYPE_AR);

	#ifdef DEBUG_MMU
	// printf("[Cycle %3ld: %s.Do_R_fwd_RMM] (%s) pop MO_tracker.\n", nCycle, this->cName.c_str(), upAR_new3->cpAR->GetName().c_str());
	// this->cpTracker->CheckTracker();
	#endif

	#ifdef DEBUG_MMU	
	assert (upAR_new3 != NULL);
	#endif
	
	// Maintain
	Delete_UD(upAR_new3, EUD_TYPE_AR);
	
	return (ERESULT_TYPE_SUCCESS);
};


// B valid 
EResultType CMMU::Do_B_fwd(int64_t nCycle) {

	// Check Tx valid
	if (this->cpTx_B->IsBusy() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};
	
	// Check remote-Tx. Get. 
	CPBPkt cpB = this->cpRx_B->GetPair()->GetB();
	if (cpB == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	#ifdef DEBUG_MMU
	assert (cpB != NULL);
	#endif

	// Put Rx 
	this->cpRx_B->PutB(cpB);

	// Put Tx 
	this->cpTx_B->PutB(cpB);

	return (ERESULT_TYPE_SUCCESS);
};


// R ready
EResultType CMMU::Do_R_bwd(int64_t nCycle) {

	// Check Rx valid
	CPRPkt cpR = this->cpRx_R->GetR();
	if (cpR == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check PTW 
	if (cpR->GetID() == ARID_PTW) {
		// Set ready
		this->cpRx_R->SetAcceptResult(ERESULT_TYPE_ACCEPT);
	};

	#ifdef AT_ENABLE
	// Check APTW. AT 
	if (cpR->GetID() == ARID_APTW) {
		// Set ready
		this->cpRx_R->SetAcceptResult(ERESULT_TYPE_ACCEPT);
	};
	#endif

	#ifdef RMM_ENABLE
	// Check RPTW. RMM 
	if (cpR->GetID() == ARID_RPTW_RMM) {
		// Set ready
		this->cpRx_R->SetAcceptResult(ERESULT_TYPE_ACCEPT);
	};
	#endif

	// Check Tx ready
	EResultType eAcceptResult = this->cpTx_R->GetAcceptResult();

	// Traditional 
	if (cpR->GetID() != ARID_PTW and eAcceptResult == ERESULT_TYPE_ACCEPT) {
		// Set ready
		this->cpRx_R->SetAcceptResult(ERESULT_TYPE_ACCEPT);
	}; 

	// Check valid SI
	if (this->cpTx_R->IsBusy() == ERESULT_TYPE_YES) {
		cpR = this->cpTx_R->GetR();
	} 
	else {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Maintain tracker 
	if (eAcceptResult == ERESULT_TYPE_ACCEPT) {
		if (cpR->IsLast() == ERESULT_TYPE_YES) {

			#ifdef DEBUG_MMU
			printf("[Cycle %3ld: %s.Do_R_bwd] RLAST sent to master.\n", nCycle, this->cName.c_str());
			#endif

			// Stat
			this->Decrease_MO_AR();
			this->Decrease_MO_Ax();

			int nID = cpR->GetID(); // Original AxID. Not PTW ID. Be careful.

			#ifdef STAT_DETAIL
			// Stat
			SPLinkedCUD spNode = this->cpTracker->GetNode(nID, EUD_TYPE_AR);
			#ifdef DEBUG_MMU
			assert (spNode->nCycle_wait >= 0);
			#endif
			this->nTotal_Tracker_Wait_Ax += spNode->nCycle_wait;
			this->nTotal_Tracker_Wait_AR += spNode->nCycle_wait;
			// printf("[Cycle %3ld: %s.Do_R_bwd] %d cycles allocated tracker (%s) \n", nCycle, this->cName.c_str(), spNode->nCycle_wait, spNode->upData->cpAR->GetName().c_str());
			#endif
	
			// Maintain tracker
			UPUD upAR_new = this->cpTracker->Pop(nID, EUD_TYPE_AR);

			#ifdef DEBUG_MMU
			// printf("[Cycle %3ld: %s.Do_R_bwd] (%s) pop MO_tracker.\n", nCycle, this->cName.c_str(), upAR_new->cpAR->GetName().c_str());
			// this->cpTracker->CheckTracker();
			#endif

			#ifdef DEBUG_MMU
			assert (upAR_new != NULL);
			#endif

			// Maintain
			Delete_UD(upAR_new, EUD_TYPE_AR);
		};

		#ifdef DEBUG_MMU
		printf("[Cycle %3ld: %s.Do_R_bwd] R sent to master.\n", nCycle, this->cName.c_str());
		#endif
	};

	return (ERESULT_TYPE_SUCCESS);
};


// B ready 
EResultType CMMU::Do_B_bwd(int64_t nCycle) {

	// Check Tx ready
	if (this->cpTx_B->GetAcceptResult() == ERESULT_TYPE_REJECT) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check Rx valid
	if (this->cpRx_B->IsBusy() == ERESULT_TYPE_NO) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Set ready
	this->cpRx_B->SetAcceptResult(ERESULT_TYPE_ACCEPT);

	// Get B 
	CPBPkt cpB = this->cpTx_B->GetB();
	if (cpB == NULL) {
		assert (0);
		return (ERESULT_TYPE_SUCCESS);
	};

	// Get ready
	EResultType eAcceptResult = this->cpTx_B->GetAcceptResult();

	// Maintain tracker 
	if (eAcceptResult == ERESULT_TYPE_ACCEPT) {

		#ifdef DEBUG_MMU
		printf("[Cycle %3ld: %s.Do_B_bwd] B sent to master.\n", nCycle, this->cName.c_str());
		#endif
	
		// MO
		this->Decrease_MO_AW();
		this->Decrease_MO_Ax();

		int nID = cpB->GetID();

		#ifdef STAT_DETAIL 
		// Stat
		SPLinkedCUD spNode = this->cpTracker->GetNode(nID, EUD_TYPE_AW);
		assert (spNode->nCycle_wait >= 0);
		this->nTotal_Tracker_Wait_Ax += spNode->nCycle_wait;
		this->nTotal_Tracker_Wait_AW += spNode->nCycle_wait;
		// printf("[Cycle %3ld: %s.Do_B_bwd] %d cycles allocated tracker (%s) \n", nCycle, this->cName.c_str(), spNode->nCycle_wait, spNode->upData->cpAW->GetName().c_str());
		#endif

		// Maintain tracker
		UPUD upAW_new = this->cpTracker->Pop(nID, EUD_TYPE_AW);
		// printf("[Cycle %3ld: %s.Do_B_bwd] (%s) pop MO_tracker.\n", nCycle, this->cName.c_str(), upAW_new->cpAW->GetName().c_str());
		// this->cpTracker->CheckTracker();

		#ifdef DEBUG_MMU
		assert (upAW_new != NULL);
		#endif

		// Maintain
		Delete_UD(upAW_new, EUD_TYPE_AW);
	};

	return (ERESULT_TYPE_SUCCESS);
};


//----------------------------------------------------------
// Fill TLB 
// 	Single fetch 
// 	1. PCA
// 	2. BCT
// 	3. AT
//----------------------------------------------------------
EResultType CMMU::FillTLB_SF(int64_t nVA, int64_t nCycle) {

	// Debug
	// this->CheckMMU();

	int64_t nVPN = this->GetVPNNum(nVA);

	#ifdef DEBUG_MMU
	assert (this->IsTLBHit(nVPN) == ERESULT_TYPE_NO);
	#endif

	int nSet = this->GetSetNum(nVPN);
	int nWay = this->GetVictimTLB(nVPN);

	// Fill new entry
	this->spTLB[nSet][nWay]->nValid = 1;
	this->spTLB[nSet][nWay]->nVPN   = nVPN;	

	#if defined PAGE_SIZE_4KB
	this->spTLB[nSet][nWay]->ePageSize = EPAGE_SIZE_TYPE_4KB;
	this->spTLB[nSet][nWay]->nPPN      = rand() % 0x3FFFF;			// Mapping table random 20-bit. nPPN is signed int type. MSB suppose 0. PPN 20-bit 

	// #ifdef BUDDY_PAGE_TABLE
	#ifdef BUDDY_ENABLE
	int nIndexPTE = nVPN - (this->START_VPN);
	int nPPN = this->spPageTable[nIndexPTE]->nPPN;
	assert(nIndexPTE >= 0);
	assert(nPPN >= 0);
	#endif

	#elif defined PAGE_SIZE_1MB
	this->spTLB[nSet][nWay]->ePageSize = EPAGE_SIZE_TYPE_1MB;
	this->spTLB[nSet][nWay]->nPPN      = rand() % 0x7FF;			// Mapping table random 12-bit. nPPN signed int type. MSB suppose 0. PPN 20-bit
	#endif

	#ifdef VPN_SAME_PPN
	this->spTLB[nSet][nWay]->nPPN = nVPN;					// VPN equal to PPN
	#endif

	// Allocate time 
	this->spTLB[nSet][nWay]->nTimeStamp = nCycle;				// Replacement 


#ifdef CONTIGUITY_ENABLE

//---------------------------------------
#ifdef PCA_ENABLE

	#ifdef CONTIGUITY_0_PERCENT
	this->spTLB[nSet][nWay]->nContiguity = 0;					// no page contiguous 
	#endif

	#ifdef CONTIGUITY_25_PERCENT
	int64_t nVPN_aligned = nVPN - (nVPN % PAGE_TABLE_GRANULE);			// VPN aligned
	int64_t nVPN_diff = nVPN - nVPN_aligned; 
	this->spTLB[nSet][nWay]->nContiguity = 0;

	if (PAGE_TABLE_GRANULE == 4) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	1  -  0	 0	1  -  0  0	1 ...
	//---------------------------------------------------	
		if (nVPN_diff < 2) {
			this->spTLB[nSet][nWay]->nContiguity = 1 - nVPN_diff;		// Next 1 page contiguous 
		};
	}
	else if (PAGE_TABLE_GRANULE == 8) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	2  -  -	 0	2  -  -  0	2 ...
	//---------------------------------------------------	
		if (nVPN_diff < 3) {
			this->spTLB[nSet][nWay]->nContiguity = 2 - nVPN_diff;		// Next 2 pages contiguous 
		};
	}
	else {
		assert (0);
	};
	#endif

	#ifdef CONTIGUITY_50_PERCENT
	int64_t nVPN_aligned = nVPN - (nVPN % PAGE_TABLE_GRANULE);			// VPN aligned
	int64_t nVPN_diff = nVPN - nVPN_aligned; 
	this->spTLB[nSet][nWay]->nContiguity = 0;

	if (PAGE_TABLE_GRANULE == 4) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	2  -  -	 0	2  -  -  0	2 ...
	//---------------------------------------------------	
		if (nVPN_diff < 3) {
			this->spTLB[nSet][nWay]->nContiguity = 2 - nVPN_diff;		// Next 2 pages contiguous 
		};
	}
	else if (PAGE_TABLE_GRANULE == 8) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	4  -  -	 -	-  0  0  0	2 ...
	//---------------------------------------------------	
		if (nVPN_diff < 5) {
			this->spTLB[nSet][nWay]->nContiguity = 4 - nVPN_diff;		// Next 4 pages contiguous 
		};
	}
	else {	
		assert (0);
	};
	#endif

	#ifdef CONTIGUITY_75_PERCENT
	int64_t nVPN_aligned = nVPN - (nVPN % PAGE_TABLE_GRANULE);			// VPN aligned
	int64_t nVPN_diff = nVPN - nVPN_aligned; 
	this->spTLB[nSet][nWay]->nContiguity = 0;

	if (PAGE_TABLE_GRANULE == 4) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	3  -  -	 -	3  -  -  0	3 ...
	//---------------------------------------------------	
		if (nVPN_diff < 4) {
			this->spTLB[nSet][nWay]->nContiguity = 3 - nVPN_diff;		// Next 3 pages contiguous
		};
	}
	else if (PAGE_TABLE_GRANULE == 8) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	6  -  -	 -	-  -  -  0	6 ...
	//---------------------------------------------------	
		if (nVPN_diff < 6) {
			this->spTLB[nSet][nWay]->nContiguity = 6 - nVPN_diff;		// Next 6 pages contiguous
		};
	}
	else {
		assert (0);
	};
	#endif

	#ifdef CONTIGUITY_100_PERCENT
	int64_t nVPN_aligned = nVPN >> MAX_ORDER << MAX_ORDER;				// 2^(MAX_ORDER) page aligned
	int64_t nVPN_diff = nVPN - nVPN_aligned; 
	this->spTLB[nSet][nWay]->nContiguity = 0;

	if (nVPN_diff < TOTAL_PAGE_NUM) {
		this->spTLB[nSet][nWay]->nContiguity = TOTAL_PAGE_NUM - nVPN_diff - 1;	// next pages contiguous
	};
	#endif
#endif	// "PCA_ENABLE"
//---------------------------------------


//---------------------------------------
#ifdef BCT_ENABLE

	#ifdef BUDDY_ENABLE
 	int nBlockSize = this->spPageTable[nIndexPTE]->nBlockSize;
 	int64_t nVPN_aligned = nVPN - (nVPN % nBlockSize);                      // VPN aligned FIXME Check      
 	this->spTLB[nSet][nWay]->nVPN = nVPN_aligned;
 	this->spTLB[nSet][nWay]->nContiguity = nBlockSize - 1;

	return (ERESULT_TYPE_SUCCESS);
	#endif


	#ifdef CONTIGUITY_0_PERCENT
	this->spTLB[nSet][nWay]->nVPN = nVPN;	
	this->spTLB[nSet][nWay]->nContiguity = 0;				// no page contiguous 
	#endif

	#ifdef CONTIGUITY_25_PERCENT
	int64_t nVPN_aligned = nVPN - (nVPN % PAGE_TABLE_GRANULE);		// VPN aligned 
	int64_t nVPN_diff = nVPN - nVPN_aligned; 
	this->spTLB[nSet][nWay]->nVPN = nVPN;
	this->spTLB[nSet][nWay]->nContiguity = 0;

	if (PAGE_TABLE_GRANULE == 4) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	1  -  0	 0	1  -  0  0	1 ...
	//---------------------------------------------------	
		if (nVPN_diff < 2) {
			this->spTLB[nSet][nWay]->nVPN = nVPN_aligned;
			this->spTLB[nSet][nWay]->nContiguity = 1;		// Next 1 page contiguous. Block size 2 
		};
	}
	else if (PAGE_TABLE_GRANULE == 8) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	2  -  -	 0	0  0  0  0	2 ...
	//---------------------------------------------------	
		if (nVPN_diff < 3) {
			this->spTLB[nSet][nWay]->nVPN = nVPN_aligned;
			this->spTLB[nSet][nWay]->nContiguity = 2;		// Next 2 pages contiguous. Block size 3 
		};
	}
	else {
		assert(0);
	};
	#endif

	#ifdef CONTIGUITY_50_PERCENT
	int64_t nVPN_aligned = nVPN - (nVPN % PAGE_TABLE_GRANULE);		// VPN aligned
	int64_t nVPN_diff = nVPN - nVPN_aligned; 
	this->spTLB[nSet][nWay]->nVPN = nVPN;
	this->spTLB[nSet][nWay]->nContiguity = 0;

	if (PAGE_TABLE_GRANULE == 4) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	2  -  -	 0	2  -  -  0	2 ...
	//---------------------------------------------------	
		if (nVPN_diff < 3) {
			this->spTLB[nSet][nWay]->nVPN = nVPN_aligned;
			this->spTLB[nSet][nWay]->nContiguity = 2;		// Next 2 pages contiguous. Block size 3 
		};
	}
	else if (PAGE_TABLE_GRANULE == 8) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	4  -  -	 -	-  0  0  0	4 ...
	//---------------------------------------------------	
		if (nVPN_diff < 5) {
			this->spTLB[nSet][nWay]->nVPN = nVPN_aligned;
			this->spTLB[nSet][nWay]->nContiguity = 4;		// Next 4 pages contiguous. Block size 5 
		};
	}
	else {
		assert (0);
	};
	#endif

	#ifdef CONTIGUITY_75_PERCENT
	int64_t nVPN_aligned = nVPN - (nVPN % PAGE_TABLE_GRANULE);		// VPN aligned
	int64_t nVPN_diff = nVPN - nVPN_aligned; 
	this->spTLB[nSet][nWay]->nVPN = nVPN;
	this->spTLB[nSet][nWay]->nContiguity = 0;

	if (PAGE_TABLE_GRANULE == 4) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	3  -  -	 -	3  -  -  -	3 ...
	//---------------------------------------------------	
		if (nVPN_diff < 4) {
			this->spTLB[nSet][nWay]->nVPN = nVPN_aligned;
			this->spTLB[nSet][nWay]->nContiguity = 3;		// Next 3 pages contiguous. Block size 4 
		};
	}
	else if (PAGE_TABLE_GRANULE == 8) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	6  -  -	 -	-  -  -  0	: ...
	//---------------------------------------------------	
		if (nVPN_diff < 7) {
			this->spTLB[nSet][nWay]->nVPN = nVPN_aligned;
			this->spTLB[nSet][nWay]->nContiguity = 6;		// Next 6 pages contiguous. Block size 7 
		};
	}
	else {
		assert(0);
	};
	#endif

	#ifdef CONTIGUITY_100_PERCENT
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	TOTAL_PAGE_NUM -1 		  ... 
	//---------------------------------------------------	
	int64_t nVPN_aligned = nVPN >> MAX_ORDER << MAX_ORDER;			// 2^(MAX_ORDER) page aligned
	int64_t nVPN_diff = nVPN - nVPN_aligned; 
	this->spTLB[nSet][nWay]->nVPN = nVPN;
	this->spTLB[nSet][nWay]->nContiguity = 0;

	if (nVPN_diff < TOTAL_PAGE_NUM) {
		this->spTLB[nSet][nWay]->nVPN = nVPN_aligned;
		this->spTLB[nSet][nWay]->nContiguity = TOTAL_PAGE_NUM -1;	// Next pages contiguous. Block size "TOTAL_PAGE_NUM"
	};
	#endif
#endif	// "BCT_ENABLE" 
//---------------------------------------

//---------------------------------------
#ifdef AT_ENABLE

	#ifdef CONTIGUITY_0_PERCENT
	this->spTLB[nSet][nWay]->nContiguity = 0;				// no page contiguous 
	#endif

	#ifdef CONTIGUITY_25_PERCENT
	this->spTLB[nSet][nWay]->nContiguity = 0;
	
	if (PAGE_TABLE_GRANULE == 4) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	1  -  0	 0	1  -  0  0	1 ...
	//---------------------------------------------------	
		if (nVPN % 4 == 0) {
			this->spTLB[nSet][nWay]->nContiguity = 1;		// Next 1 page contiguous 
		};
	}
	else if (PAGE_TABLE_GRANULE == 8) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	2  -  -	 0	0  0  0  0	1 ...
	//---------------------------------------------------	
		if (nVPN % 8 == 0) {
			this->spTLB[nSet][nWay]->nContiguity = 2;		// Next 2 pages contiguous 
		};
	}
	else {
		assert (0);
	};
	#endif

	#ifdef CONTIGUITY_50_PERCENT
	this->spTLB[nSet][nWay]->nContiguity = 0;

	if (PAGE_TABLE_GRANULE == 4) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	2  -  -	 0	2  -  -  0	2 ...
	//---------------------------------------------------	
		if (nVPN % 4 == 0) {
			this->spTLB[nSet][nWay]->nContiguity = 2;		// Next 2 pages contiguous 
		};
	}
	else if (PAGE_TABLE_GRANULE == 8) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	4  -  -	 -	-  0  0  0	4 ...
	//---------------------------------------------------	
		if (nVPN % 8 == 0) {
			this->spTLB[nSet][nWay]->nContiguity = 4;		// Next 4 pages contiguous 
		};
	}
	else {
		assert(0);
	};
	#endif

	#ifdef CONTIGUITY_75_PERCENT
	this->spTLB[nSet][nWay]->nContiguity = 0;

	if (PAGE_TABLE_GRANULE == 4) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...	
	// Contiguity	3  -  -	 -	3  -  -  -	3 ...
	//---------------------------------------------------	
		if (nVPN % 4 == 0) {
			this->spTLB[nSet][nWay]->nContiguity = 3;		// Next 3 pages contiguous 
		};
	}
	else if (PAGE_TABLE_GRANULE == 8) {
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...
	// Contiguity	6  -  -	 -	-  -  -  0	6 ...
	//---------------------------------------------------	
		if (nVPN % 8 == 0) {
			this->spTLB[nSet][nWay]->nContiguity = 6;		// Next 6 pages contiguous
		};
	};
	#endif

	#ifdef CONTIGUITY_100_PERCENT
	//---------------------------------------------------	
	// VPN		0  1  2	 3	4  5  6  7	8 ...			// Anchor every "TOTAL_PAGE_NUM"
	// Contiguity	TOTAL_PAGE_NUM -1 		  .... 
	//---------------------------------------------------	
	this->spTLB[nSet][nWay]->nContiguity = 0;
	
	if (nVPN % TOTAL_PAGE_NUM == 0) {
		this->spTLB[nSet][nWay]->nContiguity = TOTAL_PAGE_NUM - 1;	// Next pages contiguous
	};
	#endif
#endif	// "AT_ENABLE"
//---------------------------------------

#endif	// "CONTIGUITY_ENABLE"

	return (ERESULT_TYPE_SUCCESS);
};


//----------------------------------------------------------
// Fill TLB. RMM 
// 	Single fetch 
// 	No contiguity by default
//----------------------------------------------------------
EResultType CMMU::FillTLB_SF_RMM(int64_t nVA, int64_t nCycle) {

	// Debug
	// this->CheckMMU();

	int64_t nVPN = this->GetVPNNum(nVA);
	int nSet = this->GetSetNum(nVPN);
	int nWay = this->GetVictimTLB(nVPN);

	// Fill new entry
	this->spTLB[nSet][nWay]->nValid = 1;
	this->spTLB[nSet][nWay]->nVPN   = nVPN;	

	#if defined PAGE_SIZE_4KB
	this->spTLB[nSet][nWay]->ePageSize = EPAGE_SIZE_TYPE_4KB;
	this->spTLB[nSet][nWay]->nPPN      = rand() % 0x3FFFF;	// Mapping table random 20-bit. nPPN is signed int type. MSB suppose 0. Arch32 PPN
	#elif defined PAGE_SIZE_1MB
	this->spTLB[nSet][nWay]->ePageSize = EPAGE_SIZE_TYPE_1MB;
	this->spTLB[nSet][nWay]->nPPN      = rand() % 0x7FF;	// Mapping table random 12-bit. nPPN signed int type. MSB suppose 0. Arch32 PPN
	#endif

	#ifdef VPN_SAME_PPN
	this->spTLB[nSet][nWay]->nPPN   = nVPN;			// VPN equal to PPN
	#endif

	// Allocate time 
	this->spTLB[nSet][nWay]->nTimeStamp = nCycle;		// For replacement 

	this->spTLB[nSet][nWay]->nContiguity = 0;		// no page contiguous 

	return (ERESULT_TYPE_SUCCESS);
};


//----------------------------------------------------------
// Fill RTLB. RMM 
//----------------------------------------------------------
// 	Single fetch. Contiguity enabled by default. 
// 	RPTW conducted only when PTW indicates contiguity. 
//----------------------------------------------------------
EResultType CMMU::FillRTLB_SF_RMM(int64_t nVA, int64_t nCycle) {

	// Debug
	// this->CheckMMU();

	int64_t nVPN = this->GetVPNNum(nVA);
	int nSet = this->GetSetNum(nVPN);
	int nWay = this->GetVictimRTLB_RMM(nVPN);

	// Fill new entry
	this->spRTLB_RMM[nSet][nWay]->nValid = 1;
	this->spRTLB_RMM[nSet][nWay]->nVPN   = nVPN - (nVPN % PAGE_TABLE_GRANULE);	// StartVPN. Pages aligned

	#if defined PAGE_SIZE_4KB
	this->spRTLB_RMM[nSet][nWay]->ePageSize = EPAGE_SIZE_TYPE_4KB;
	this->spRTLB_RMM[nSet][nWay]->nPPN   = rand() % 0x3FFFF;		// Mapping table random 20-bit. nPPN is signed int type. MSB suppose 0. Arch32 PPN
	#elif defined PAGE_SIZE_1MB
	this->spRTLB_RMM[nSet][nWay]->ePageSize = EPAGE_SIZE_TYPE_1MB;
	this->spRTLB_RMM[nSet][nWay]->nPPN   = rand() % 0x7FF;			// Mapping table random 12-bit. nPPN signed int type. MSB suppose 0. Arch32 PPN
	#endif

	#ifdef VPN_SAME_PPN
	this->spRTLB_RMM[nSet][nWay]->nPPN   = nVPN;				// VPN equal to PPN
	#endif

	// Allocate time 
	this->spRTLB_RMM[nSet][nWay]->nTimeStamp = nCycle;			// For replacement 
	
	if (PAGE_TABLE_GRANULE == 4) {
		#ifdef CONTIGUITY_0_PERCENT
		this->spRTLB_RMM[nSet][nWay]->nContiguity = 0; 			// no page contiguous 
		#endif
		
		#ifdef CONTIGUITY_25_PERCENT
		this->spRTLB_RMM[nSet][nWay]->nContiguity = 1;			// Next 1 page contiguous 
		#endif
		
		#ifdef CONTIGUITY_50_PERCENT
		this->spRTLB_RMM[nSet][nWay]->nContiguity = 2;			// Next 2 pages contiguous 
		#endif
		
		#ifdef CONTIGUITY_75_PERCENT
		this->spRTLB_RMM[nSet][nWay]->nContiguity = 3;			// Next 3 pages contiguous 
		#endif
		
		#ifdef CONTIGUITY_100_PERCENT
		this->spRTLB_RMM[nSet][nWay]->nContiguity = TOTAL_PAGE_NUM;	// Next pages contiguous
		#endif
	}
	else if (PAGE_TABLE_GRANULE == 8) {
		#ifdef CONTIGUITY_0_PERCENT
		this->spRTLB_RMM[nSet][nWay]->nContiguity = 0; 			// no page contiguous 
		#endif
		
		#ifdef CONTIGUITY_25_PERCENT
		this->spRTLB_RMM[nSet][nWay]->nContiguity = 2;			// Next 2 page contiguous 
		#endif
		
		#ifdef CONTIGUITY_50_PERCENT
		this->spRTLB_RMM[nSet][nWay]->nContiguity = 4;			// Next 4 pages contiguous 
		#endif
		
		#ifdef CONTIGUITY_75_PERCENT
		this->spRTLB_RMM[nSet][nWay]->nContiguity = 6;			// Next 6 pages contiguous 
		#endif
		
		#ifdef CONTIGUITY_100_PERCENT
		this->spRTLB_RMM[nSet][nWay]->nContiguity = TOTAL_PAGE_NUM - 1;	// Next pages contiguous
		#endif
	}
	else {
		assert (0);
	};

	return (ERESULT_TYPE_SUCCESS);
};


//----------------------------------------------------------
// Fill TLB 
// 	Fetch multiple PTEs 
// 	(1) Traditional
// 	(2) PCA 
//----------------------------------------------------------
EResultType CMMU::FillTLB_BF(int64_t nVA, int64_t nCycle) {

	// Debug
	// this->CheckMMU();

	int nSet = -1;
	int nWay = -1;
	int64_t nVPN = -1;
	
	int NUM_ENTRY = NUM_PTE_PTW;					// "BLOCK_FETCH": 16 PTEs per PTW, when TLB entries 16
	
	nVPN = this->GetVPNNum(nVA);
	nSet = this->GetSetNum(nVPN);

	nVPN = nVPN & (~(NUM_PTE_PTW-1)); 				// Align VPNs, 0xFFFF_FFF0, when NUM_PTE_PTW 16

	for (int j=0; j<NUM_ENTRY; j++) {

		int64_t nNewVPN = nVPN + j;

#ifdef CONTIGUITY_ENABLE

#ifdef PCA_ENABLE
		//------------------------------------------------------------
		// PCA 
		//------------------------------------------------------------
		#ifdef CONTIGUITY_0_PERCENT
		//---------------------------------------------------	
		// VPN		0  1  2	 3	4  5  6  7	8 ...
		// Contiguity	0  0  0	 0	0  0  0  0	0 ...
		//---------------------------------------------------	
		// Find victim to replace	
		nWay = this->GetVictimTLB(nNewVPN); 				// Be careful. Not nVPN

		this->spTLB[nSet][nWay]->nContiguity = 0;			// No page contiguous
		#endif
		
		#ifdef CONTIGUITY_25_PERCENT
		if (PAGE_TABLE_GRANULE == 4) {
			//---------------------------------------------------	
			// VPN		0  1  2	 3	4  5  6  7	8 ...
			// Contiguity	1  -  0	 0	1  -  0  0	1 ...
			//---------------------------------------------------	
			if (nNewVPN % 4 == 1) continue;				// Do not allocate

			// Find victim to replace	
			nWay = this->GetVictimTLB(nNewVPN); 			// Be careful. Not nVPN

			this->spTLB[nSet][nWay]->nContiguity = 0;

			if (nNewVPN % 4 == 0) {
				this->spTLB[nSet][nWay]->nContiguity = 1;	// Next 1 page contiguous 
			};
		}
		else if (PAGE_TABLE_GRANULE == 8) {
			//---------------------------------------------------	
			// VPN		0  1  2	 3  4  5  6  7	   8 ...
			// Contiguity	2  -  -	 0  0  0  0  0	   2 ...
			//---------------------------------------------------	
			if (nNewVPN % 8 == 1) continue;				// Do not allocate
			if (nNewVPN % 8 == 2) continue;				

			// Find victim to replace	
			nWay = this->GetVictimTLB(nNewVPN); 			// Be careful. Not nVPN

			this->spTLB[nSet][nWay]->nContiguity = 0;

			if (nNewVPN % 8 == 0) {
				this->spTLB[nSet][nWay]->nContiguity = 2;	// Next 2 page contiguous 
			};
		}
		else {
			assert (0);
		};
		#endif
			
		#ifdef CONTIGUITY_50_PERCENT	
		if (PAGE_TABLE_GRANULE == 4) {
			//---------------------------------------------------	
			// VPN		0  1  2	 3	4  5  6  7	8 ...
			// Contiguity	2  -  -	 0	2  -  -  0	2 ...
			//---------------------------------------------------	
			if (nNewVPN % 4 == 1) continue;
			if (nNewVPN % 4 == 2) continue;

			// Find victim to replace	
			nWay = this->GetVictimTLB(nNewVPN); 			// Be careful. Not nVPN

			this->spTLB[nSet][nWay]->nContiguity = 0;

			if (nNewVPN % 4 == 0) {
				this->spTLB[nSet][nWay]->nContiguity = 2;	// Next 2 pages contiguous 
			};
		}
		else if (PAGE_TABLE_GRANULE == 8) {
			//---------------------------------------------------	
			// VPN		0  1  2	 3  4  5  6  7	   8 ...
			// Contiguity	4  -  -	 -  -  0  0  0	   4 ...
			//---------------------------------------------------	
			if (nNewVPN % 8 == 1) continue;
			if (nNewVPN % 8 == 2) continue;
			if (nNewVPN % 8 == 3) continue;
			if (nNewVPN % 8 == 4) continue;

			// Find victim to replace	
			nWay = this->GetVictimTLB(nNewVPN); 			// Be careful. Not nVPN

			this->spTLB[nSet][nWay]->nContiguity = 0;

			if (nNewVPN % 8 == 0) {
				this->spTLB[nSet][nWay]->nContiguity = 4;	// Next 4 pages contiguous 
			};
		}
		else {
			assert(0);
		};
		#endif
			
		#ifdef CONTIGUITY_75_PERCENT
		if (PAGE_TABLE_GRANULE == 4) {
			//---------------------------------------------------	
			// VPN		0  1  2	 3	4  5  6  7	8 ...
			// Contiguity	3  -  -	 -	3  -  -  -	3 ...
			//---------------------------------------------------	
			if (nNewVPN % 4 == 1) continue;
			if (nNewVPN % 4 == 2) continue;
			if (nNewVPN % 4 == 3) continue;

			// Find victim to replace	
			nWay = this->GetVictimTLB(nNewVPN); 			// Be careful. Not nVPN

			this->spTLB[nSet][nWay]->nContiguity = 0;

			if (nNewVPN % 4 == 0) {
				this->spTLB[nSet][nWay]->nContiguity = 3;	// Next 3 pages contiguous 
			};
		}
		else if (PAGE_TABLE_GRANULE == 8) {
			//---------------------------------------------------	
			// VPN		0  1  2	 3  4  5  6  7	   8 ...
			// Contiguity	6  -  -	 -  -  -  -  0	   6 ...
			//---------------------------------------------------	
			if (nNewVPN % 8 == 1) continue;
			if (nNewVPN % 8 == 2) continue;
			if (nNewVPN % 8 == 3) continue;
			if (nNewVPN % 8 == 4) continue;
			if (nNewVPN % 8 == 5) continue;
			if (nNewVPN % 8 == 6) continue;

			// Find victim to replace	
			nWay = this->GetVictimTLB(nNewVPN); 			// Be careful. Not nVPN

			this->spTLB[nSet][nWay]->nContiguity = 0;

			if (nNewVPN % 8 == 0) {
				this->spTLB[nSet][nWay]->nContiguity = 6;	// Next 6 pages contiguous 
			};
		}
		else {
			assert (0);
		};
		#endif

		#ifdef CONTIGUITY_100_PERCENT
		//---------------------------------------------------	
		// VPN		0		  1  2  3   4  5  6  7  8 ...
		// Contiguity   TOTAL_PAGE_NUM-1  -  -  -   -  -  - 
		//---------------------------------------------------	
		int64_t nVPN_aligned = nNewVPN >> MAX_ORDER << MAX_ORDER;			// 2^(MAX_ORDER) page aligned
		int64_t nVPN_diff = nNewVPN - nVPN_aligned;
		if (nVPN_diff > 0 and nVPN_diff < TOTAL_PAGE_NUM) {
			continue;
		};

		// Find victim to replace	
		nWay = this->GetVictimTLB(nNewVPN); 						// Be careful. Not nVPN

		this->spTLB[nSet][nWay]->nContiguity = 0;
		
		if (nVPN_diff < TOTAL_PAGE_NUM) {
		        this->spTLB[nSet][nWay]->nContiguity = TOTAL_PAGE_NUM - nVPN_diff - 1;	// Next pages contiguous
		};
		#endif

#endif	// "PCA_ENABLE"
#endif	// "CONTIGUITY_ENABLE"

		// Find victim to replace	
		nWay = this->GetVictimTLB(nNewVPN); 						// Be careful. Not nVPN

		// Fill new entry
		this->spTLB[nSet][nWay]->nValid    = 1;
		this->spTLB[nSet][nWay]->nVPN      = nNewVPN;
		
		#if defined PAGE_SIZE_4KB
		this->spTLB[nSet][nWay]->ePageSize = EPAGE_SIZE_TYPE_4KB;
		this->spTLB[nSet][nWay]->nPPN      = rand() % 0x3FFFF;				// Mapping table random 20-bit. nPPN is signed int type. MSB suppose 0
		#elif defined PAGE_SIZE_1MB
		this->spTLB[nSet][nWay]->ePageSize = EPAGE_SIZE_TYPE_1MB;
		this->spTLB[nSet][nWay]->nPPN      = rand() % 0x7FF;				// Mapping table random 12-bit. nPPN signed int type. MSB suppose 0.
		#endif
		
		#ifdef VPN_SAME_PPN
		this->spTLB[nSet][nWay]->nPPN      = nNewVPN;					// VPN equal to PPN
		#endif
		
		// Allocate time 
		this->spTLB[nSet][nWay]->nTimeStamp = nCycle + j;				// For replacement. Add j to make order anyway  
	};		

	return (ERESULT_TYPE_SUCCESS);
};


//----------------------------------------------------------
// Fill TLB CAMB
// 	Fetch multiple PTEs 
// 	CAMB only
// 	Assumptions
// 		(1) "CONTIGUITY_ENABLE"
// 		(2) PPN random generated
// 		(3) Page size 4kB
//----------------------------------------------------------
// EResultType CMMU::FillTLB_CAMB(int64_t nVA, int64_t nCycle) {
//
//       // Debug
//       // this->CheckMMU();
//
// #ifdef CONTIGUITY_ENABLE
//	
//	int64_t nVPN  = this->GetVPNNum(nVA);
//	int64_t nAVPN = nVPN & (~(NUM_PTE_PTW-1));				// Align VPNs, 0xFFFF_FFF0, when NUM_PTE_PTW 16
//	int64_t nNewVPN = -1;
//	
//	int nSet = -1; 
//	int nWay = -1;
//	
//	int NUM_ENTRY = NUM_PTE_PTW;						// "BLOCK_FETCH": 16 PTEs per PTW, when TLB entries 16
//	
//	for (int j=0; j<NUM_ENTRY; j++) {
//	
//		nNewVPN = nAVPN + j;
//	
//	
//		#ifdef CONTIGUITY_0_PERCENT
//		//---------------------------------------------------	
//		// VPN		0  1  2	 3	4  5  6  7	8 ...
//		// Contiguity	0  0  0	 0	0  0  0  0	0 ...
//		//---------------------------------------------------	
//		// Check TLB hit (to avoid redundant allocation)
//		EResultType IsTLBHit = this->IsTLBHit(nNewVPN);
//		if (IsTLBHit == ERESULT_TYPE_YES) {
//			// Do not allocate
//			continue;
//		};
//		
//		// Find victim to replace	
//		nWay = this->GetVictimTLB(nNewVPN); 				// Be careful. Not nAVPN
//		nSet = this->GetSetNum(nNewVPN);
//	
//		// Fill new entry
//		this->spTLB[nSet][nWay]->nContiguity = 0;			// no page contiguous 
//	
//		this->spTLB[nSet][nWay]->nValid      = 1;
//		this->spTLB[nSet][nWay]->nVPN        = nNewVPN;
//		
//		this->spTLB[nSet][nWay]->ePageSize   = EPAGE_SIZE_TYPE_4KB;
//		this->spTLB[nSet][nWay]->nPPN        = rand() % 0x3FFFF;	// Mapping table random 20-bit. nPPN is signed int type. MSB suppose 0
//		this->spTLB[nSet][nWay]->nTimeStamp  = nCycle + j;		// For replacement. Add j to make order anyway  
//		#endif
//		
//	
//		#ifdef CONTIGUITY_25_PERCENT
//		if (PAGE_TABLE_GRANULE == 4) {
//			//-------------------------------------------------------------------------	
//			// Original
//			// 	VPN		0  1  2	 3	4  5  6  7	8  9 10 11    12...
//			// 	Contiguity	1  -  0	 0	1  -  0  0	1  ...			// Next 1 page contiguous
//			//-------------------------------------------------------------------------	
//			// CAMB (N = 4, PAGE_TABLE_GRANULE 4)
//			// 	Index		0  1  2	 3	4  5  6  7	8  9 10 11    12...
//			// 	Contiguity	1  0  0	 1	1  0  0  1	1  ...
//			//-------------------------------------------------------------------------	
//			//                         2  1  1         2  1  1
//			//-------------------------------------------------------------------------	
//			// CAMB (N = 8, PAGE_TABLE_GRANULE 4) 
//			// 	Index		0  1  2	 3	4  5  6  7	8  9 10 11    12...
//			// 	Contiguity	1  0  0	 1	0  0  1  0	1  ...
//			//-------------------------------------------------------------------------	
//			//                         2  1  1      2  1  1  2
//			//-------------------------------------------------------------------------	
//			// Get StartVPN of a block	
//			if (NUM_ENTRY == 4) {				// Quarter fetch
//				if      (j == 0) nNewVPN = nAVPN;    
//				else if (j == 1) nNewVPN = nAVPN + 2;	// + 2
//				else if (j == 2) nNewVPN = nAVPN + 3;	// + 1
//				else if (j == 3) nNewVPN = nAVPN + 4;	// + 1
//				else             assert (0);
//			};
//	
//			if (NUM_ENTRY == 8) {				// Half fetch
//				if      (j == 0) nNewVPN = nAVPN;    
//				else if (j == 1) nNewVPN = nAVPN + 2;	// + 2
//				else if (j == 2) nNewVPN = nAVPN + 3;	// + 1
//				else if (j == 3) nNewVPN = nAVPN + 4;	// + 1
//				else if (j == 4) nNewVPN = nAVPN + 6;	// + 2
//				else if (j == 5) nNewVPN = nAVPN + 7;	// + 1
//				else if (j == 6) nNewVPN = nAVPN + 8;	// + 1
//				else if (j == 7) nNewVPN = nAVPN + 10;	// + 2
//				else             assert (0);
//			};
//	
//			// Check TLB hit (to avoid redundant allocation)
//			EResultType IsTLBHit = this->IsTLBHit(nNewVPN);
//			if (IsTLBHit == ERESULT_TYPE_YES) {
//				// Do not allocate
//				continue;
//			};
//			
//			// Find victim to replace	
//			nWay = this->GetVictimTLB(nNewVPN); 				// Be careful. Not nAVPN
//			nSet = this->GetSetNum(nNewVPN);
//	
//			this->spTLB[nSet][nWay]->nContiguity = 0;
//	
//			// Fill new entry
//			if (NUM_ENTRY == 4) {
//				if (j == 0 or j == 3) {
//					this->spTLB[nSet][nWay]->nContiguity = 1;	// Next 1 page contiguous 
//				};
//			};
//	
//			if (NUM_ENTRY == 8) {
//				if (j == 0 or j == 3 or j == 6) {
//					this->spTLB[nSet][nWay]->nContiguity = 1;	// Next 1 page contiguous 
//				};
//			};
//	
//			this->spTLB[nSet][nWay]->nValid     = 1;
//			this->spTLB[nSet][nWay]->nVPN       = nNewVPN;
//			
//			this->spTLB[nSet][nWay]->ePageSize  = EPAGE_SIZE_TYPE_4KB;
//			this->spTLB[nSet][nWay]->nPPN       = rand() % 0x3FFFF;		// Mapping table random 20-bit. nPPN is signed int type. MSB suppose 0
//			this->spTLB[nSet][nWay]->nTimeStamp = nCycle + j;		// For replacement. Add j to make order anyway  
//		}
//		else if (PAGE_TABLE_GRANULE == 8) {
//			//-------------------------------------------------------------------------	
//			// Original
//			// 	VPN		0  1  2	 3    4  5  6  7	     8  9 10 11 12...
//			// 	Contiguity	2  -  -	 0    0  0  0  0	     2  ...			// Next 2 pages contiguous
//			//-------------------------------------------------------------------------	
//			// CAMB (N = 4, PAGE_TABLE_GRANULE 8). Quarter fetch. NUM_ENTRY is 4.
//			// 	Index		0  1  2	 3    4  5  6  7	     8  9 10 11 12...
//			// 	Contiguity	2  0  0	 0    0  0  0  0	     2  ...			// Check. [0-3] and [4-7] different pattern
//			//-------------------------------------------------------------------------	
//			//                         3  1  1       1  1  1
//			//-------------------------------------------------------------------------	
//			// CAMB (N = 8, PAGE_TABLE_GRANULE 8). Half fetch. NUM_ENTRY is 8.
//			// 	Index		0  1  2	 3    4  5  6  7	     8  9 10 11 12...
//			// 	Contiguity	2  0  0	 0    0  0  2  0	     2  ...
//			//-------------------------------------------------------------------------	
//			//                         3  1  1    1  1  1  3
//			//-------------------------------------------------------------------------	
//			// Get StartVPN of a block	
//			if ((NUM_ENTRY == 4) and (nAVPN % 8 == 0)) {	// Quarter fetch. [0-3]	
//				if      (j == 0) nNewVPN = nAVPN;    
//				else if (j == 1) nNewVPN = nAVPN + 3;	// + 3
//				else if (j == 2) nNewVPN = nAVPN + 4;	// + 1
//				else if (j == 3) nNewVPN = nAVPN + 5;	// + 1
//				else             assert (0);
//			}
//			else if ((NUM_ENTRY == 4) and (nAVPN % 8 != 0)) { // Quarter fetch. [4-7]
//				if      (j == 0) nNewVPN = nAVPN;    
//				else if (j == 1) nNewVPN = nAVPN + 1;	// + 1
//				else if (j == 2) nNewVPN = nAVPN + 2;	// + 1
//				else if (j == 3) nNewVPN = nAVPN + 3;	// + 1
//				else             assert (0);
//			};
//	
//			if (NUM_ENTRY == 8) {				// Half fetch
//				if      (j == 0) nNewVPN = nAVPN;    
//				else if (j == 1) nNewVPN = nAVPN + 3;	// + 3
//				else if (j == 2) nNewVPN = nAVPN + 4;	// + 1
//				else if (j == 3) nNewVPN = nAVPN + 5;	// + 1
//				else if (j == 4) nNewVPN = nAVPN + 6;	// + 1
//				else if (j == 5) nNewVPN = nAVPN + 7;	// + 1
//				else if (j == 6) nNewVPN = nAVPN + 8;	// + 1
//				else if (j == 7) nNewVPN = nAVPN + 11;	// + 3
//				else             assert (0);
//			};
//	
//			// Check TLB hit (to avoid redundant allocation)
//			EResultType IsTLBHit = this->IsTLBHit(nNewVPN);
//			if (IsTLBHit == ERESULT_TYPE_YES) {
//				// Do not allocate
//				continue;
//			};
//			
//			// Find victim to replace	
//			nWay = this->GetVictimTLB(nNewVPN); 				// Be careful. Not nAVPN
//			nSet = this->GetSetNum(nNewVPN);
//	
//			this->spTLB[nSet][nWay]->nContiguity = 0;
//	
//			// Fill new entry
//			if (NUM_ENTRY == 4) {						// Quarter fetch
//				if (j == 0) {
//					this->spTLB[nSet][nWay]->nContiguity = 2;	// Next 2 page contiguous 
//				};
//			};
//	
//			if (NUM_ENTRY == 8) {						// Half fetch
//				if (j == 0 or j == 6) {
//					this->spTLB[nSet][nWay]->nContiguity = 2;	// Next 2 page contiguous 
//				};
//			};
//	
//			this->spTLB[nSet][nWay]->nValid     = 1;
//			this->spTLB[nSet][nWay]->nVPN       = nNewVPN;
//			
//			this->spTLB[nSet][nWay]->ePageSize  = EPAGE_SIZE_TYPE_4KB;
//			this->spTLB[nSet][nWay]->nPPN       = rand() % 0x3FFFF;		// Mapping table random 20-bit. nPPN is signed int type. MSB suppose 0
//			this->spTLB[nSet][nWay]->nTimeStamp = nCycle + j;		// For replacement. Add j to make order anyway  
//		}
//		else {
//			assert (0); 
//		};
//		#endif
//			
//	
//		#ifdef CONTIGUITY_50_PERCENT
//		if (PAGE_TABLE_GRANULE == 4) {
//			//-------------------------------------------------------------------------	
//			// Original
//			// 	VPN		0  1  2	 3	4  5  6  7	8  9 10 11    12...
//			// 	Contiguity	2  -  -	 0	2  -  -  0	2 ...
//			//-------------------------------------------------------------------------	
//			// CAMB (N = 4, PAGE_TABLE_GRANULE 4)
//			// 	Index		0  1  2	 3	4  5  6  7	8  9 10 11    12...
//			// 	Contiguity	2  0  2	 0	2  0  2  0	2 ...
//			//-------------------------------------------------------------------------	
//			// CAMB (N = 8, PAGE_TABLE_GRANULE 4)
//			// 	Index		0  1  2	 3	4  5  6  7	8  9 10 11    12...
//			// 	Contiguity	2  0  2	 0	2  0  2  0	2 ...
//			//-------------------------------------------------------------------------	
//			//                         3  1  3      1  3  1  3      1
//			//-------------------------------------------------------------------------	
//			// Get StartVPN of a block	
//			if      (j == 0) nNewVPN = nAVPN;    
//			else if (j == 1) nNewVPN = nAVPN + 3;	// + 3 
//			else if (j == 2) nNewVPN = nAVPN + 4;	// + 1
//			else if (j == 3) nNewVPN = nAVPN + 7;	// + 3
//			else if (j == 4) nNewVPN = nAVPN + 8;	// + 1
//			else if (j == 5) nNewVPN = nAVPN + 11;	// + 3
//			else if (j == 6) nNewVPN = nAVPN + 12;	// + 1
//			else if (j == 7) nNewVPN = nAVPN + 15;	// + 3
//			else             assert (0);					// Check. "QUARTER_FETCH" support only?
//	
//			// Check TLB hit (to avoid redundant allocation)
//			EResultType IsTLBHit = this->IsTLBHit(nNewVPN);
//			if (IsTLBHit == ERESULT_TYPE_YES) {
//				// Do not allocate
//				continue;
//			};
//			
//			// Find victim to replace	
//			nWay = this->GetVictimTLB(nNewVPN); 				// Be careful. Not nAVPN
//			nSet = this->GetSetNum(nNewVPN);
//	
//			// Fill new entry
//			if (j == 0 or j == 2 or j == 4 or j == 6) {
//				this->spTLB[nSet][nWay]->nContiguity = 2;		// Next 2 page contiguous 
//			}
//			else {
//				this->spTLB[nSet][nWay]->nContiguity = 0;
//			};
//	
//			this->spTLB[nSet][nWay]->nValid     = 1;
//			this->spTLB[nSet][nWay]->nVPN       = nNewVPN;
//			
//			this->spTLB[nSet][nWay]->ePageSize  = EPAGE_SIZE_TYPE_4KB;
//			this->spTLB[nSet][nWay]->nPPN       = rand() % 0x3FFFF;		// Mapping table random 20-bit. nPPN is signed int type. MSB suppose 0
//			this->spTLB[nSet][nWay]->nTimeStamp = nCycle + j;		// For replacement. Add j to make order anyway  
//		}
//		else if (PAGE_TABLE_GRANULE == 8) {
//			//-------------------------------------------------------------------------	
//			// Original
//			// 	VPN		0  1  2	 3	4  5  6  7		8  9 10 11    12...
//			// 	Contiguity	4  -  -	 -	-  0  0  0		4 ...			// Next 4 pages contiguous
//			//-------------------------------------------------------------------------	
//			// CAMB (N = 4, PAGE_TABLE_GRANULE 8)
//			// 	Index		0  1  2	 3	4  5  6  7		8  9 10 11    12...
//			// 	Contiguity	4  0  0	 0	4  0  0  0		4 ...
//			//-------------------------------------------------------------------------	
//			// CAMB (N = 8, PAGE_TABLE_GRANULE 8)
//			// 	Index		0  1  2	 3	4  5  6  7		8  9 10 11    12...
//			// 	Contiguity	4  0  0	 0	4  0  0  0		4 ...
//			//-------------------------------------------------------------------------	
//			//                         5  1  1      1  5  1  1     		5
//			//-------------------------------------------------------------------------	
//			// Consider AVPN aligned with 8 
//			// nAVPN = nVPN & 0xFFFFFFF8; 					// Align VPNs, 0xFFFF_FFF8, align with 8 
//			nAVPN = nVPN - (nVPN % 8);
//	
//			// Get StartVPN of a block	
//			if      (j == 0) nNewVPN = nAVPN;    
//			else if (j == 1) nNewVPN = nAVPN + 5;	// + 5 
//			else if (j == 2) nNewVPN = nAVPN + 6;	// + 1
//			else if (j == 3) nNewVPN = nAVPN + 7;	// + 1
//			else if (j == 4) nNewVPN = nAVPN + 8;	// + 1
//			else if (j == 5) nNewVPN = nAVPN + 13;	// + 5
//			else if (j == 6) nNewVPN = nAVPN + 14;	// + 1
//			else if (j == 7) nNewVPN = nAVPN + 15;	// + 1
//			else             assert (0);					// Check. "QUARTER_FETCH" support only
//	
//			// Check TLB hit (to avoid redundant allocation)
//			EResultType IsTLBHit = this->IsTLBHit(nNewVPN);
//			if (IsTLBHit == ERESULT_TYPE_YES) {
//				// Do not allocate
//				continue;
//			};
//			
//			// Find victim to replace	
//			nWay = this->GetVictimTLB(nNewVPN); 				// Be careful. Not nAVPN
//			nSet = this->GetSetNum(nNewVPN);
//	
//			// Fill new entry
//			if (j == 0 or j == 4) {
//				this->spTLB[nSet][nWay]->nContiguity = 4;		// Next 4 pages contiguous 
//			}
//			else {
//				this->spTLB[nSet][nWay]->nContiguity = 0;
//			};
//	
//			this->spTLB[nSet][nWay]->nValid     = 1;
//			this->spTLB[nSet][nWay]->nVPN       = nNewVPN;
//			
//			this->spTLB[nSet][nWay]->ePageSize  = EPAGE_SIZE_TYPE_4KB;
//			this->spTLB[nSet][nWay]->nPPN       = rand() % 0x3FFFF;		// Mapping table random 20-bit. nPPN is signed int type. MSB suppose 0
//			this->spTLB[nSet][nWay]->nTimeStamp = nCycle + j;		// For replacement. Add j to make order anyway  
//		}
//		else {
//			assert (0);
//		};
//		#endif
//			
//	
//		#ifdef CONTIGUITY_75_PERCENT
//		if (PAGE_TABLE_GRANULE == 4) {
//			//-------------------------------------------------------------------------	
//			// Original
//			// 	VPN		0  1  2	 3	4  5  6  7	8  9 10 11    12...
//			// 	Contiguity	3  -  -	 -	3  -  -  -	3 ...
//			//-------------------------------------------------------------------------	
//			// CAMB (N = 4, PAGE_TABLE_GRANULE 4). Quarter fetch. 
//			// 	VPN		0  1  2	 3	4  5  6  7	8  9 10 11    12...
//			// 	Contiguity	3  3  3	 3	3  3  3  3	3 ...
//			//-------------------------------------------------------------------------	
//			//			   4  4  4      4  4  4  4
//			//-------------------------------------------------------------------------	
//			// CAMB (N = 8, PAGE_TABLE_GRANULE 4). Half fetch.
//			// 	VPN		0  1  2	 3	4  5  6  7	8  9 10 11    12...
//			// 	Contiguity	3  3  3	 3	3  3  3  3	3 ...
//			//-------------------------------------------------------------------------	
//			//			   4  4  4      4  4  4  4
//			//-------------------------------------------------------------------------	
//			// Get StartVPN of a block	
//			if      (j == 0) nNewVPN = nAVPN;    
//			else if (j == 1) nNewVPN = nAVPN + 4; 
//			else if (j == 2) nNewVPN = nAVPN + 8;
//			else if (j == 3) nNewVPN = nAVPN + 12;
//			else if (j == 4) nNewVPN = nAVPN + 16;
//			else if (j == 5) nNewVPN = nAVPN + 20;
//			else if (j == 6) nNewVPN = nAVPN + 24;
//			else if (j == 7) nNewVPN = nAVPN + 28;
//			else             assert (0);					// Check "QUARTER_FETCH" support only
//	
//			// Check TLB hit (to avoid redundant allocation)
//			EResultType IsTLBHit = this->IsTLBHit(nNewVPN);
//			if (IsTLBHit == ERESULT_TYPE_YES) {
//				// Do not allocate
//				continue;
//			};
//			
//			// Find victim to replace	
//			nWay = this->GetVictimTLB(nNewVPN); 				// Be careful. Not nAVPN
//			nSet = this->GetSetNum(nNewVPN);
//	
//			// Fill new entry
//			this->spTLB[nSet][nWay]->nContiguity = 3;			// Next 3 pages contiguous 
//	
//			this->spTLB[nSet][nWay]->nValid     = 1;
//			this->spTLB[nSet][nWay]->nVPN       = nNewVPN;
//			
//			this->spTLB[nSet][nWay]->ePageSize  = EPAGE_SIZE_TYPE_4KB;
//			this->spTLB[nSet][nWay]->nPPN       = rand() % 0x3FFFF;		// Mapping table random 20-bit. nPPN is signed int type. MSB suppose 0
//			this->spTLB[nSet][nWay]->nTimeStamp = nCycle + j;		// For replacement. Add j to make order anyway  
//		}
//		else if (PAGE_TABLE_GRANULE == 8) {
//			//-------------------------------------------------------------------------	
//			// Original
//			// 	VPN		0  1  2	 3	4  5  6  7		8  9 10 11    12...
//			// 	Contiguity	6  -  -	 -	-  -  -  0		6 ...				// Next 6 pages contiguous
//			//-------------------------------------------------------------------------	
//			// CAMB (N = 4, PAGE_TABLE_GRANULE 8). Quarter fetch. 
//			// 	VPN		0  1  2	 3	4  5  6  7		8  9 10 11    12...
//			// 	Contiguity	6  0  6	 0	6  0  6  0		6 ...				// Check. When VPN 4, StartVPN is not AVPN
//			//-------------------------------------------------------------------------	
//			//                         7  1  7      1  7  1  7     		
//			//-------------------------------------------------------------------------	
//			// CAMB (N = 8, PAGE_TABLE_GRANULE 8). Half fetch. 
//			// 	VPN		0  1  2	 3	4  5  6  7		8  9 10 11    12...
//			// 	Contiguity	6  0  6	 0	6  0  6  0		6 ...
//			//-------------------------------------------------------------------------	
//			//                         7  1  7      1  7  1  7     		
//			//-------------------------------------------------------------------------	
//			
//			// Consider AVPN aligned with 8 
//			// nAVPN = nVPN & 0xFFFFFFF8; 		// Align VPNs, 0xFFFF_FFF8, align with 8 
//			nAVPN = nVPN - (nVPN % 8);
//	
//			// Get StartVPN of a block	
//			if      (j == 0) nNewVPN = nAVPN;    
//			else if (j == 1) nNewVPN = nAVPN + 7;	// + 7 
//			else if (j == 2) nNewVPN = nAVPN + 8;	// + 1
//			else if (j == 3) nNewVPN = nAVPN + 15;	// + 7
//			else if (j == 4) nNewVPN = nAVPN + 16;	// + 1
//			else if (j == 5) nNewVPN = nAVPN + 23;	// + 7
//			else if (j == 6) nNewVPN = nAVPN + 24;	// + 1
//			else if (j == 7) nNewVPN = nAVPN + 31;	// + 7
//			else             assert (0);
//	
//			// Check TLB hit (to avoid redundant allocation)
//			EResultType IsTLBHit = this->IsTLBHit(nNewVPN);
//			if (IsTLBHit == ERESULT_TYPE_YES) {
//				// Do not allocate
//				continue;
//			};
//			
//			// Find victim to replace	
//			nWay = this->GetVictimTLB(nNewVPN); 				// Be careful. Not nAVPN
//			nSet = this->GetSetNum(nNewVPN);
//	
//			// Fill new entry	
//			if (NUM_ENTRY == 4) {						// Quarter fetch
//				if (j == 0 or j == 2 or j == 4 or j == 6) {
//					this->spTLB[nSet][nWay]->nContiguity = 6;	// Next 6 pages contiguous 
//				}
//				else {
//					this->spTLB[nSet][nWay]->nContiguity = 0;
//				};
//			};
//	
//			if (NUM_ENTRY == 8) {						// Half fetch
//				if (j == 0 or j == 2 or j == 4 or j == 6) {
//					this->spTLB[nSet][nWay]->nContiguity = 6;	// Next 6 pages contiguous 
//				}
//				else {
//					this->spTLB[nSet][nWay]->nContiguity = 0;
//				};
//			};
//	
//			this->spTLB[nSet][nWay]->nValid     = 1;
//			this->spTLB[nSet][nWay]->nVPN       = nNewVPN;
//			
//			this->spTLB[nSet][nWay]->ePageSize  = EPAGE_SIZE_TYPE_4KB;
//			this->spTLB[nSet][nWay]->nPPN       = rand() % 0x3FFFF;		// Mapping table random 20-bit. nPPN is signed int type. MSB suppose 0
//			this->spTLB[nSet][nWay]->nTimeStamp = nCycle + j;		// For replacement. Add j to make order anyway  
//		}
//		else {
//			assert (0);
//		};
//		#endif
//	
//	
//		#ifdef CONTIGUITY_100_PERCENT
//		//----------------------------------------------------------------	
//		// Original
//		// 	VPN		0		1  2  3	 4  5  6  7  8 ...TOTAL_PAGE_NUM-1 ...
//		// 	Contiguity   TOTAL_PAGE_NUM-1  	decrement		  TOTAL_PAGE_NUM    decrement			
//		//----------------------------------------------------------------	
//		// CAMB (PAGE_TABLE_GRANULE 4 or 8)
//		// 	VPN		0  1  2	 3	4  5  6  7	8 ...
//		// 	Contiguity	TOTAL_PAGE_NUM-1 in all entries				// Check.
//		//----------------------------------------------------------------	
//		// Get AVPN
//		nAVPN = nVPN - (nVPN % TOTAL_PAGE_NUM);
//	
//		// Get StartVPN of a block	
//		if      (j == 0) nNewVPN = nAVPN;    
//		else if (j == 1) nNewVPN = nAVPN + (TOTAL_PAGE_NUM * 1); 
//		else if (j == 2) nNewVPN = nAVPN + (TOTAL_PAGE_NUM * 2);
//		else if (j == 3) nNewVPN = nAVPN + (TOTAL_PAGE_NUM * 3);
//		else if (j == 4) nNewVPN = nAVPN + (TOTAL_PAGE_NUM * 4);
//		else if (j == 5) nNewVPN = nAVPN + (TOTAL_PAGE_NUM * 5);
//		else if (j == 6) nNewVPN = nAVPN + (TOTAL_PAGE_NUM * 6);
//		else if (j == 7) nNewVPN = nAVPN + (TOTAL_PAGE_NUM * 7);
//		else             assert (0);					// Check "QUARTER_FETCH" support only?
//	
//		// Check TLB hit (to avoid redundant allocation)
//		EResultType IsTLBHit = this->IsTLBHit(nNewVPN);
//		if (IsTLBHit == ERESULT_TYPE_YES) {
//			// Do not allocate
//			continue;
//		};
//		
//		// Find victim to replace	
//		nWay = this->GetVictimTLB(nNewVPN); 				// Be careful. Not nAVPN
//		nSet = this->GetSetNum(nNewVPN);
//	
//		
//		// Fill new entry
//		this->spTLB[nSet][nWay]->nContiguity = TOTAL_PAGE_NUM - 1;	// next pages contiguous 
//	
//		this->spTLB[nSet][nWay]->nValid     = 1;
//		this->spTLB[nSet][nWay]->nVPN       = nNewVPN;
//		
//		this->spTLB[nSet][nWay]->ePageSize  = EPAGE_SIZE_TYPE_4KB;
//		this->spTLB[nSet][nWay]->nPPN       = rand() % 0x3FFFF;		// Mapping table random 20-bit. nPPN is signed int type. MSB suppose 0
//		this->spTLB[nSet][nWay]->nTimeStamp = nCycle + j;		// For replacement. Add j to make order anyway  
//		#endif
//	};		
//
// #endif	// "CONTIGUITY_ENABLE"
//
//	return (ERESULT_TYPE_SUCCESS);
// };


// Fill FLPD 
EResultType CMMU::FillFLPD(int nEntry, int64_t nVPN, int64_t nCycle) {

	// Debug
	// this->CheckMMU();

	// Fill new entry
	this->spFLPD[nEntry]->nValid     = 1;
	this->spFLPD[nEntry]->nVPN       = nVPN;
	this->spFLPD[nEntry]->nPPN       = rand() % 0x7FF;			// Mapping table random 12-bit. nPPN signed int type. MSB suppose 0.
	this->spFLPD[nEntry]->ePageSize  = EPAGE_SIZE_TYPE_1MB;
	this->spFLPD[nEntry]->nTimeStamp = nCycle;

	#ifdef VPN_SAME_PPN
	this->spFLPD[nEntry]->nPPN = nVPN;					// VPN equals to PPN 
	#endif	

	return (ERESULT_TYPE_SUCCESS);
};


//-------------------------------------------------------
// Get Tag number 
// 	Tag number is the upper (32 - MMU_BIT_SET - BIT_4K_PAGE) bits. Arch32
// 	Tag number is the upper (64 - MMU_BIT_SET - BIT_4K_PAGE) bits. Arch64
//
// 	Note	: Tag number is calculated considering "Tag + Index" structure. In perf model, we use "vpn" so tag number not necessary. 
//-------------------------------------------------------
int64_t CMMU::GetTagNum(int64_t nVA) {

	// Debug
	// this->CheckMMU();

	int64_t nTagNum = nVA >> (MMU_BIT_SET + BIT_4K_PAGE);

	return (nTagNum);
};


//-------------------------------------------------------
// Check TLB hit 
//-------------------------------------------------------
//	Check hit 
//		"Requested VPN" within "(VPN, VPN + contiguity)"
//	Assume
//		Page size 4kB
//---------------------------------------------------
EResultType CMMU::IsTLBHit(int64_t nVPN) {

	// Debug
	// this->CheckMMU();

	// Set number
	int nSet = this->GetSetNum(nVPN);

	// Find Tag
	for (int j=0; j<MMU_NUM_WAY; j++) {
		// Check valid and Tag
		if (this->spTLB[nSet][j]->nValid != 1) {
			continue;
		}; 

		// Check requested VPN hit
		int64_t nVPN_low  = this->spTLB[nSet][j]->nVPN;	
		int64_t nVPN_high = this->spTLB[nSet][j]->nVPN + this->spTLB[nSet][j]->nContiguity;

		if ( nVPN_low <= nVPN and nVPN_high >= nVPN ) {	
			return (ERESULT_TYPE_YES);
		};
	};

	return (ERESULT_TYPE_NO);
};



//-------------------------------------------------------
// Check TLB hit 
// 	If LRU, update timestamp (to remember access time) 
//-------------------------------------------------------
//	Check hit
//		1. Traditional	: "Requested VPN" = "VPN"
//		2. PCA		: "Requested VPN" within "(VPN, VPN + contiguity)"
//		3. BCT		: "Requested VPN" within "(StartVPN, StartVPN + contiguity)". Contiguity 2^n
//		4. PCAD		: "Requested VPN" within "(StartVPN, StartVPN + contiguity)" 
//		5. AT		: "Requested VPN" within "(StartVPN, StartVPN + contiguity)" 
//---------------------------------------------------
EResultType CMMU::IsTLBHit(int64_t nVA, int64_t nCycle) {

	// Debug
	// this->CheckMMU();

	int64_t nVPN = GetVPNNum(nVA);

	// Set number
	int nSet = this->GetSetNum(nVPN);

	// Find Tag
	for (int j=0; j<MMU_NUM_WAY; j++) {
		// Check valid and Tag
		if (this->spTLB[nSet][j]->nValid != 1) {
			continue;
		}; 

		//---------------------------------
		// Contiguity enable
		//---------------------------------
#ifdef CONTIGUITY_ENABLE	
#ifdef PAGE_SIZE_4KB
		// Check requested VPN hit
		int64_t nVPN_low  = this->spTLB[nSet][j]->nVPN;
		int64_t nVPN_high = this->spTLB[nSet][j]->nVPN + this->spTLB[nSet][j]->nContiguity;

		if ( nVPN_low <= nVPN and nVPN_high >= nVPN ) {						// Hit. Within VPN range
			#ifdef MMU_REPLACEMENT_LRU
			this->spTLB[nSet][j]->nTimeStamp = nCycle;
			#endif
			return (ERESULT_TYPE_YES);
		};
			
#endif
#endif

		//---------------------------------
		// Traditional case. Contiguity disable.
		//---------------------------------
		if (this->spTLB[nSet][j]->ePageSize == EPAGE_SIZE_TYPE_4KB) {
		
			if (this->spTLB[nSet][j]->nVPN == nVPN) {					// Hit. same VPN.
				#ifdef MMU_REPLACEMENT_LRU
				this->spTLB[nSet][j]->nTimeStamp = nCycle;
				#endif
				return (ERESULT_TYPE_YES);
			};
		} 
		else if (this->spTLB[nSet][j]->ePageSize == EPAGE_SIZE_TYPE_1MB) {
			if (this->spTLB[nSet][j]->nVPN == nVPN) {					// Hit. same VPN. 
				#ifdef MMU_REPLACEMENT_LRU
				this->spTLB[nSet][j]->nTimeStamp = nCycle;
				#endif
				return (ERESULT_TYPE_YES);
			};
		} 
		else {
			assert (0);
		};
	};

	return (ERESULT_TYPE_NO);
};


//-------------------------------------------------------
// Check TLB hit. AT. "Regular" or "Anchor"
// 	If LRU, update timestamp (to remember access time) 
//	Traditional way : "Requested VPN" = "VPN"
//---------------------------------------------------
// EResultType CMMU::IsTLB_Allocate_AT(int64_t nVA, int64_t nCycle) {
//	
//	// Debug
//	// this->CheckMMU();
//	
//	int64_t nVPN = GetVPNNum(nVA);
//	
//	// Set number
//	int nSet = this->GetSetNum(nVPN);
//	
//	// Find Tag
//	for (int j=0; j<MMU_NUM_WAY; j++) {
//		// Check valid and Tag
//		if (this->spTLB[nSet][j]->nValid != 1) {
//			continue;
//		}; 
//	
//		//---------------------------------
//		// Traditional case. Contiguity disable.
//		//---------------------------------
//		if (this->spTLB[nSet][j]->ePageSize == EPAGE_SIZE_TYPE_4KB) {
//		
//			if (this->spTLB[nSet][j]->nVPN == nVPN) {					// Hit. same VPN.
//				#ifdef MMU_REPLACEMENT_LRU
//				this->spTLB[nSet][j]->nTimeStamp = nCycle;
//				#endif
//				return (ERESULT_TYPE_YES);
//			};
//		} 
//		else if (this->spTLB[nSet][j]->ePageSize == EPAGE_SIZE_TYPE_1MB) {
//			if (this->spTLB[nSet][j]->nVPN == nVPN) {					// Hit. same VPN. 
//				#ifdef MMU_REPLACEMENT_LRU
//				this->spTLB[nSet][j]->nTimeStamp = nCycle;
//				#endif
//				return (ERESULT_TYPE_YES);
//			};
//		} 
//		else {
//			assert (0);
//		};
//	};
//	
//	return (ERESULT_TYPE_NO);
// };


//-------------------------------------------------------
// Check contiguity match in TLB
// 	If LRU, update timestamp (to remember access time) 
//-------------------------------------------------------
//	AT		: "Requested VPN" within "(StartVPN, StartVPN + contiguity)" 
//---------------------------------------------------
//EResultType CMMU::IsTLB_Contiguity_Match_AT(int64_t nVA, int64_t nCycle) {
//	
//	// Debug
//	// this->CheckMMU();
//	
//	int64_t nVPN = GetVPNNum(nVA);
//	
//	int64_t nVPN_anchor = nVPN - (nVPN % PAGE_TABLE_GRANULE);			// VPN align	
//	
//	#ifdef CONTIGUITY_100_PERCENT
//	nVPN_anchor = (nVPN >> MAX_ORDER) << MAX_ORDER;					// 2^(MAX_ORDER) page align	
//	#endif
//	
//	// Set number
//	int nSet = this->GetSetNum(nVPN);
//	
//	// Find Tag
//	for (int j=0; j<MMU_NUM_WAY; j++) {
//		// Check valid and Tag
//		if (this->spTLB[nSet][j]->nValid != 1) {
//			continue;
//		}; 
//	
//		// Check anchor allocated 
//		if (nVPN_anchor != this->spTLB[nSet][j]->nVPN) {
//			continue;
//		};
//	
//		int64_t nVPN_high = this->spTLB[nSet][j]->nVPN + this->spTLB[nSet][j]->nContiguity;
//	
//		// Check anchor hit
//		if (nVPN <= nVPN_high) {						// Hit. Within VPN range
//			#ifdef MMU_REPLACEMENT_LRU
//			this->spTLB[nSet][j]->nTimeStamp = nCycle;
//			#endif
//			return (ERESULT_TYPE_YES);
//		};
//	
//	};
//	
//	return (ERESULT_TYPE_NO);
// };


//-------------------------------------------------------
// Check Anchor entry allocated in TLB. AT.
//-------------------------------------------------------
// EResultType CMMU::IsTLB_Allocate_Anchor_AT(int64_t nVA) {				// VA original
//	
//	// Debug
//	// this->CheckMMU();
//	
//	int64_t nVPN = GetVPNNum(nVA);
//	
//	int64_t nVPN_anchor = nVPN - (nVPN % PAGE_TABLE_GRANULE);			// VPN align	
//	
//	#ifdef CONTIGUITY_100_PERCENT
//	nVPN_anchor = (nVPN >> MAX_ORDER) << MAX_ORDER;					// 2^(MAX_ORDER) page align	
//	#endif
//	
//	// Set number
//	int nSet = this->GetSetNum(nVPN);
//	
//	// Find Tag
//	for (int j=0; j<MMU_NUM_WAY; j++) {
//		// Check valid and Tag
//		if (this->spTLB[nSet][j]->nValid != 1) {
//			continue;
//		}; 
//	
//		// Check anchor allocated 
//		if (nVPN_anchor == this->spTLB[nSet][j]->nVPN) {
//			return (ERESULT_TYPE_YES);
//		};
//	};
//	
//	return (ERESULT_TYPE_NO);
// };


//-------------------------------------------------------
// Check anchor (of VA) can cover 
// 	Given page table and VA, possible to check.
//-------------------------------------------------------
// EResultType CMMU::IsAnchor_Cover_AT(int64_t nVA) {					// Can anchor cover VA
//	
//	// Debug
//	// this->CheckMMU();
//	
//	int64_t nVPN = GetVPNNum(nVA);
//	
//	int64_t nVPN_anchor = nVPN - (nVPN % PAGE_TABLE_GRANULE);			// VPN align	
//	
//	#ifdef CONTIGUITY_100_PERCENT
//	nVPN_anchor = (nVPN >> MAX_ORDER) << MAX_ORDER;					// 2^(MAX_ORDER) page align	
//	#endif
//	
//	if (PAGE_TABLE_GRANULE == 4) {
//	
//		#ifdef CONTIGUITY_0_PERCENT
//		//---------------------------------------------------	
//		// VPN		0  1  2	 3	4  5  6  7	8 ...
//		// Contiguity	0  0  0	 0	0  0  0  0	0 ...
//		//---------------------------------------------------	
//		if (nVPN - nVPN_anchor == 0) { return (ERESULT_TYPE_YES); };
//		#endif
//		
//		#ifdef CONTIGUITY_25_PERCENT
//		//---------------------------------------------------	
//		// VPN		0  1  2	 3	4  5  6  7	8 ...
//		// Contiguity	1  -  0	 0	1  -  0  0	1 ...
//		//---------------------------------------------------	
//		if (nVPN - nVPN_anchor <= 1) { return (ERESULT_TYPE_YES); };
//		#endif
//		
//		#ifdef CONTIGUITY_50_PERCENT
//		//---------------------------------------------------	
//		// VPN		0  1  2	 3	4  5  6  7	8 ...
//		// Contiguity	2  -  -	 0	2  -  -  0	2 ...
//		//---------------------------------------------------	
//		if (nVPN - nVPN_anchor <= 2) { return (ERESULT_TYPE_YES); };
//		#endif
//		
//		#ifdef CONTIGUITY_75_PERCENT
//		//---------------------------------------------------	
//		// VPN		0  1  2	 3	4  5  6  7	8 ...
//		// Contiguity	3  -  -	 -	3  -  -  -	3 ...
//		//---------------------------------------------------	
//		if (nVPN - nVPN_anchor <= 3) { return (ERESULT_TYPE_YES); };
//		#endif
//		
//		#ifdef CONTIGUITY_100_PERCENT
//		//---------------------------------------------------	
//		// VPN		0  1  2	 3	4  5  6  7	8 ...
//		// Contiguity	MAX    	 	      	 	  ...
//		//---------------------------------------------------	
//		if (nVPN - nVPN_anchor <= TOTAL_PAGE_NUM) { return (ERESULT_TYPE_YES); };
//		#endif
//	}
//	else if (PAGE_TABLE_GRANULE == 8) {
//	
//		#ifdef CONTIGUITY_0_PERCENT
//		if (nVPN - nVPN_anchor == 0) { return (ERESULT_TYPE_YES); };
//		#endif
//		
//		#ifdef CONTIGUITY_25_PERCENT
//		if (nVPN - nVPN_anchor <= 2) { return (ERESULT_TYPE_YES); };
//		#endif
//		
//		#ifdef CONTIGUITY_50_PERCENT
//		if (nVPN - nVPN_anchor <= 4) { return (ERESULT_TYPE_YES); };
//		#endif
//		
//		#ifdef CONTIGUITY_75_PERCENT
//		if (nVPN - nVPN_anchor <= 6) { return (ERESULT_TYPE_YES); };
//		#endif
//		
//		#ifdef CONTIGUITY_100_PERCENT
//		if (nVPN - nVPN_anchor <= TOTAL_PAGE_NUM) { return (ERESULT_TYPE_YES); };
//		#endif
//	}
//	else {
//		assert (0);
//	};
//	
//	return (ERESULT_TYPE_NO);
// };


//-------------------------------------------------------
// Check regular entry. AT.
//-------------------------------------------------------
// EResultType CMMU::IsVA_Regular_AT(int64_t nVA) {
//	
//	// Debug
//	// this->CheckMMU();
//	
//	int64_t nVPN = GetVPNNum(nVA);
//	
//	int64_t nVPN_anchor = nVPN - (nVPN % PAGE_TABLE_GRANULE);			// VPN align	
//	
//	#ifdef CONTIGUITY_100_PERCENT
//	nVPN_anchor = (nVPN >> MAX_ORDER) << MAX_ORDER;					// 2^(MAX_ORDER) page align	
//	#endif
//	
//	// Check regular
//	if (nVPN != nVPN_anchor) {
//		return (ERESULT_TYPE_YES);
//	};
//	
//	return (ERESULT_TYPE_NO);
// };


//-------------------------------------------------------
// Check anchor entry. AT.
//-------------------------------------------------------
// EResultType CMMU::IsVA_Anchor_AT(int64_t nVA) {
//	
//	// Debug
//	// this->CheckMMU();
//	
//	int64_t nVPN = GetVPNNum(nVA);
//	
//	int64_t nVPN_anchor = nVPN - (nVPN % PAGE_TABLE_GRANULE);			// VPN align	
//	
//	#ifdef CONTIGUITY_100_PERCENT
//	nVPN_anchor = (nVPN >> MAX_ORDER) << MAX_ORDER;					// 2^(MAX_ORDER) page align	
//	#endif
//	
//	// Check regular
//	if (nVPN == nVPN_anchor) {
//		return (ERESULT_TYPE_YES);
//	};
//	
//	return (ERESULT_TYPE_NO);
// };


//----------------------------------------------
// Check RTLB hit. RMM 
//	In RMM, contiguity enabled by default.
// 	If LRU, update timestamp (to remember access time) 
//----------------------------------------------
//	Check hit
//		RMM		: "Requested VPN" within "(StartVPN, StartVPN + contiguity)" 
//----------------------------------------------
EResultType CMMU::IsRTLBHit_RMM(int64_t nVA, int64_t nCycle) {

	// Debug
	// this->CheckMMU();

	int64_t nVPN = GetVPNNum(nVA);

	// Set number
	int nSet = this->GetSetNum(nVPN);

	// Find Tag
	for (int j=0; j<MMU_NUM_WAY; j++) {
		// Check valid and Tag
		if (this->spRTLB_RMM[nSet][j]->nValid != 1) {
			continue;
		}; 

		//---------------------------------
		// Contiguity enable
		//---------------------------------
		#ifdef PAGE_SIZE_4KB
		int64_t nVPN_low  = this->spRTLB_RMM[nSet][j]->nVPN;
		int64_t nVPN_high = this->spRTLB_RMM[nSet][j]->nVPN + this->spRTLB_RMM[nSet][j]->nContiguity;

		if ( nVPN_low <= nVPN and nVPN_high >= nVPN ) {			// Hit. Within VPN range
			#ifdef MMU_REPLACEMENT_LRU
			this->spRTLB_RMM[nSet][j]->nTimeStamp = nCycle;
			#endif
			return (ERESULT_TYPE_YES);
		};
		#endif

	};

	return (ERESULT_TYPE_NO);
};


//--------------------------------------------------------
// Check FLPD hit 
// 	If LRU, update timestamp (to remember access time) 
//--------------------------------------------------------
EResultType CMMU::IsFLPDHit(int64_t nVA, int64_t nCycle) {

	// Debug
	// this->CheckMMU();

	// Find Tag
	for (int j=0; j<NUM_FLPD_ENTRY; j++) {
		// Check valid and Tag
		if (this->spFLPD[j]->nValid != 1) {
			continue;
		}; 

		if (this->spFLPD[j]->nVPN == (nVA >> BIT_1M_PAGE)) {		// 1MB. 20 bits
			#ifdef MMU_REPLACEMENT_LRU
			this->spFLPD[j]->nTimeStamp = nCycle;	
			#endif

			return (ERESULT_TYPE_YES);
		};
	};
	return (ERESULT_TYPE_NO);
};


//--------------------------------------
// Get VPN number 
//--------------------------------------
int64_t CMMU::GetVPNNum(int64_t nVA) {

	// Debug
	// this->CheckMMU();

	#if defined PAGE_SIZE_4KB
	int64_t nVPNNum = nVA >> BIT_4K_PAGE;
	#elif defined PAGE_SIZE_1MB
	int64_t nVPNNum = nVA >> BIT_1M_PAGE;
	#endif

	return (nVPNNum);
};


//--------------------------------------
// Get Set number 
//--------------------------------------
//	1. Traditional	: Tag + Index 
//	2. PCA		: Tag1 + index + tag2. Tag2 7 bits 
//	3. BCT		: Tag1 + index + tag2. Tag2 7 bits 
//	4. PCAD		: Tag1 + index + tag2. Tag2 7 bits 
//	5. RMM		: Tag1 + index + tag2. Tag2 7 bits 
//	6. AT		: Tag1 + index + tag2. Tag2 7 bits 
//--------------------------------------
int CMMU::GetSetNum(int64_t nVPN) {

	// Debug
	// this->CheckMMU();

	// Set index (block number) modulo (number of sets)
	int nSet  = nVPN % MMU_NUM_SET;			// Check bug in SA cache. << Tag1 bits. 

	#ifdef CONTIGUITY_ENABLE
	nSet = (nVPN >> MAX_ORDER) % MMU_NUM_SET;
	#endif

	return (nSet);
};


//--------------------------------------
// Get physical address
//--------------------------------------
//	1. Traditional
//	2. PCA		: PPN = (Requested VPN - "StartVPN") + "PPN"
//	3. BCT		: PPN = (Requested VPN - "StartVPN") + "PPN" 
//	4. PCAD		: PPN = (Requested VPN - "StartVPN") + "PPN" 
//	5. AT		: PPN = (Requested VPN - "StartVPN") + "PPN" 
//--------------------------------------
int CMMU::GetPA(int64_t nVA) {

	// Debug
	// this->CheckMMU();

	int64_t nVPN = GetVPNNum(nVA);

	// Get set number
	int nSet = this->GetSetNum(nVPN);

	int nPPN    = -1;
	int nPA     = -1;
	int nOffset = -1;

	#ifdef PAGE_SIZE_4KB
	nOffset = nVA & 0x00000FFF;  // Offset 12 bit
	#endif
	#ifdef PAGE_SIZE_1MB
	nOffset = nVA & 0x000FFFFF;  // Offset 20 bit
	#endif

	// Find Tag
	for (int j=0; j<MMU_NUM_WAY; j++) {

		// Check valid and Tag
		if (this->spTLB[nSet][j]->nValid != 1) {
			continue;
		}; 

		//---------------------------------
		// Contiguity enable
		//---------------------------------
#ifdef CONTIGUITY_ENABLE	
#ifdef PAGE_SIZE_4KB
		int64_t nVPN_low  = this->spTLB[nSet][j]->nVPN;
		int64_t nVPN_high = this->spTLB[nSet][j]->nVPN + this->spTLB[nSet][j]->nContiguity;
		int64_t nVPN_diff = nVPN - this->spTLB[nSet][j]->nVPN;

		#ifdef DEBUG_MMU
		assert (nVPN_low  >= 0);
		assert (nVPN_high >= 0);
		assert (nVPN_high <= 0x7FFFFFFFFFFFF);
		assert (this->spTLB[nSet][j]->nPPN >= 0);
		assert (this->spTLB[nSet][j]->nPPN <= 0x3FFFF);
		#endif

		if ( nVPN_low <= nVPN and nVPN_high >= nVPN ) {			// Hit. Within VPN range
			nPPN = nVPN_diff + this->spTLB[nSet][j]->nPPN;		// PPN contiguous
			nPA = (nPPN << BIT_4K_PAGE ) + nOffset; 

			#ifdef DEBUG_MMU
			assert (nPPN >= 0);
			assert (nPPN <= 0x7FFFF);
			assert (nPA >= MIN_ADDR);	
			assert (nPA <= MAX_ADDR);	
			assert (nVPN_diff >= 0);
			#endif

			return (nPA);
		}; 
#endif
#endif


		//---------------------------------
		// Traditional. Contiguity disable
		//---------------------------------
		if (this->spTLB[nSet][j]->ePageSize == EPAGE_SIZE_TYPE_4KB) {
			if (this->spTLB[nSet][j]->nVPN == nVPN) {		// Hit. Same.
				nPPN = this->spTLB[nSet][j]->nPPN;
				nPA = (nPPN << BIT_4K_PAGE) + nOffset; 
			}; 
		} 
		else if (this->spTLB[nSet][j]->ePageSize == EPAGE_SIZE_TYPE_1MB) {
			if (this->spTLB[nSet][j]->nVPN == nVPN) {		// Hit. Same
				nPPN = this->spTLB[nSet][j]->nPPN;
				nPA = (nPPN << BIT_1M_PAGE) + nOffset; 
			}; 
		} 
		else {
			assert (0); 
		};
	};

	#ifdef VPN_SAME_PPN
	assert (nPA == nVA);
	#endif

	#ifdef DEBUG_MMU
	if (nPA == -1) printf("This function call only when TLB is hit\n");
	assert (nPA != -1);
	#endif
	
	return (nPA);
};


//--------------------------------------
// Get anchor VA (from requested VA) 
//--------------------------------------
// 	Page size 4kB
//--------------------------------------
// int64_t CMMU::GetVA_Anchor_AT(int64_t nVA) {
//	
//	// Debug
//	// this->CheckMMU();
//	
//	int64_t nVPN = GetVPNNum(nVA);
//	
//	int64_t nVPN_anchor = nVPN - (nVPN % PAGE_TABLE_GRANULE);			// VPN align	
//	
//	#ifdef CONTIGUITY_100_PERCENT
//	nVPN_anchor = (nVPN >> MAX_ORDER) << MAX_ORDER;					// 2^(MAX_ORDER) page align	
//	#endif
//	
//	int64_t nOffset = nVA & 0x00000FFF;						// Offset 12 bit
//	
//	int64_t nVA_anchor = (nVPN_anchor << BIT_4K_PAGE ) + nOffset; 
//	
//	return (nVA_anchor);
//};


//--------------------------------------
// Get physical address. RMM
// 	Logic same as GetPA function.
// 	In RMM, contiguity enabled by default.
//--------------------------------------
//	AT	: PPN = (Requested VPN - "StartVPN") + "PPN" 
//--------------------------------------
int CMMU::GetPA_RMM(int64_t nVA) {

	// Debug
	// this->CheckMMU();

	int64_t nVPN = GetVPNNum(nVA);

	// Get set number
	int nSet = this->GetSetNum(nVPN);

	int nPPN = -1;
	int nPA = -1;
	int nOffset = -1;

	#ifdef PAGE_SIZE_4KB
	nOffset = nVA & 0x00000FFF;			// Offset 12 bit
	#endif
	#ifdef PAGE_SIZE_1MB
	nOffset = nVA & 0x000FFFFF;			// Offset 20 bit
	#endif

	// Find Tag
	for (int j=0; j<MMU_NUM_WAY; j++) {

		// Check valid and Tag
		if (this->spRTLB_RMM[nSet][j]->nValid != 1) {
			continue;
		}; 

		//---------------------------------
		// Contiguity enable
		//---------------------------------
		#ifdef PAGE_SIZE_4KB
		int64_t nVPN_low  = this->spRTLB_RMM[nSet][j]->nVPN;
		int64_t nVPN_high = this->spRTLB_RMM[nSet][j]->nVPN + this->spRTLB_RMM[nSet][j]->nContiguity;
		int64_t nVPN_diff = nVPN - this->spRTLB_RMM[nSet][j]->nVPN;

		#ifdef DEBUG_MMU
		assert (nVPN_low  >= 0);
		// assert (nVPN_low  <= 0x3FFFF);
		assert (nVPN_high >= 0);
		assert (nVPN_high <= 0x7FFFFFFFFFFFF);
		// assert (nVPN_diff >= 0);					// Check
		assert (this->spRTLB_RMM[nSet][j]->nPPN >= 0);
		assert (this->spRTLB_RMM[nSet][j]->nPPN <= 0x3FFFF);
		#endif

		if ( nVPN_low <= nVPN and nVPN_high >= nVPN ) {			// Hit. Within VPN range
			nPPN = nVPN_diff + this->spRTLB_RMM[nSet][j]->nPPN;	// PPN contiguous
			nPA = (nPPN << BIT_4K_PAGE ) + nOffset; 

			#ifdef DEBUG_MMU
			assert (nPPN >= 0);
			assert (nPPN <= 0x7FFFF);
			assert (nPA >= MIN_ADDR);	
			assert (nPA <= MAX_ADDR);	
			#endif

			return (nPA);
		}; 
		#endif
	};

	#ifdef VPN_SAME_PPN
	assert (nPA == nVA);
	#endif

	#ifdef DEBUG_MMU
	// This function called only when TLB hit 
	assert (nPA != -1);
	#endif
	
	return (nPA);
};


// Get 1st PTW address
int CMMU::GetFirst_PTWAddr(int64_t nVA) {

	// Debug
	// this->CheckMMU();

	//                Upper 18 bits TTBA            VA[31:20)                   4 byte align
	// int nAddr_PTW = ((FIRST_TTBA & 0xFFFFC000) + (((nVA & 0xFFF00000) >> 20) << 2));	// Max address 0xFFFF FFFF. nAddr unsigned
	int nAddr_PTW    = ((FIRST_TTBA & 0x7FFFC000) + (((nVA & 0x7FF00000) >> 20) << 2));	// Max address 0x7FFF FFFF. nAddr int

	return (nAddr_PTW);
};


// Get 2nd PTW address
int CMMU::GetSecond_PTWAddr(int64_t nVA) {

	// Debug
	// this->CheckMMU();

	//-------------------------------------------
	// Single fetch
	//-------------------------------------------
	//                 Upper 22 bits TTBA            VA[31:20] x 1kB                       VA[19:12]                   4 byte align
	// int nAddr_PTW = ((SECOND_TTBA & 0xFFFFFC00) + (((nVA & 0xFFF00000) >> 20) << 10) +  (((nVA & 0x000FF000) >> 12) << 2));	// Max address 0xFFFF FFFF. nAddr unsigned
	int nAddr_PTW    = ((SECOND_TTBA & 0x7FFFFC00) + (((nVA & 0x7FF00000) >> 20) << 10) +  (((nVA & 0x000FF000) >> 12) << 2));	// Max address 0x7FFF FFFF. nAddr int


	#ifdef BLOCK_FETCH
	//-------------------------------------------
	// Block fetch
	// 64 byte aligned to single fetch. Lower 6 bits zero.
	//-------------------------------------------
	nAddr_PTW        = nAddr_PTW & 0xFFFFFFC0;
	#endif

	#ifdef HALF_FETCH
	//-------------------------------------------
	// Half block fetch
	// 32 byte aligned to single fetch. Lower 5 bits zero.
	//-------------------------------------------
	nAddr_PTW        = nAddr_PTW & 0xFFFFFFE0;
	#endif

	#ifdef QUARTER_FETCH
	//-------------------------------------------
	// Quarter block fetch
	// 16 byte aligned to single fetch. Lower 4 bits zero.
	//-------------------------------------------
	nAddr_PTW        = nAddr_PTW & 0xFFFFFFF0;
	#endif

	return (nAddr_PTW);
};


// Find way number to replace
int CMMU::GetVictimTLB(int64_t nVPN) {

	// Debug
	// this->CheckMMU();

	int nVictimWay = -1;
	int nSet = GetSetNum(nVPN);

	//----------------------------------
	// Check range hit
	//----------------------------------
	#ifdef CONTIGUITY_ENABLE	
	// Find Tag
	for (int j=0; j<MMU_NUM_WAY; j++) {

		// Check valid and Tag
		if (this->spTLB[nSet][j]->nValid != 1) {
			continue;
		}; 

		//---------------------------------
		// Contiguity enable
		//---------------------------------
		#ifdef PAGE_SIZE_4KB
		int64_t nVPN_low  = this->spTLB[nSet][j]->nVPN;
		int64_t nVPN_high = this->spTLB[nSet][j]->nVPN + this->spTLB[nSet][j]->nContiguity;

		if ( nVPN_low <= nVPN and nVPN_high >= nVPN ) { // Range hit. Within VPN range
			nVictimWay = j;

			// Stat
			this->nTLB_evict++;

			#ifdef DEBUG_MMU
			assert (nVictimWay < MMU_NUM_WAY);
			assert (nVictimWay >= 0 );
			#endif

			return (nVictimWay);
		};
		#endif
	};
	#endif

	// Check empty slot 
	for (int j=0; j<MMU_NUM_WAY ;j++) {
		if (this->spTLB[nSet][j]->nValid != 1) {
			return (j);
		}
	};

	#if defined MMU_REPLACEMENT_RANDOM
	// Choose victim way randomly (0,1,...,MMU_NUM_WAY-1) 
	nVictimWay = rand() % (MMU_NUM_WAY);

	#elif defined MMU_REPLACEMENT_LRU
	// LRU: Choose victim way with minimum (least recently used) timestamp
	nVictimWay = 0;
	int nMinTimeStamp = this->spTLB[nSet][0]->nTimeStamp;
	for (int j=1; j<MMU_NUM_WAY; j++) {
		#ifdef DEBUG_MMU
		assert (this->spTLB[nSet][j]->nValid == 1);
		#endif
		if (nMinTimeStamp > this->spTLB[nSet][j]->nTimeStamp) {
			nMinTimeStamp = this->spTLB[nSet][j]->nTimeStamp;
			nVictimWay = j;	
		};
	};

	#ifdef DEBUG_MMU
	assert (nMinTimeStamp > 0);
	#endif

	#endif

	// Stat
	this->nTLB_evict++;

	#ifdef DEBUG_MMU
	assert (nVictimWay < MMU_NUM_WAY);
	assert (nVictimWay >= 0 );
	if (MMU_NUM_WAY == 1) {
		assert (nVictimWay == 0); // Direct mapped cache
	};
	#endif

	return (nVictimWay);
};


// Find way number to replace. RMM
int CMMU::GetVictimRTLB_RMM(int64_t nVPN) {

	// Debug
	// this->CheckMMU();

	int nVictimWay = -1;
	int nSet = GetSetNum(nVPN);

	//----------------------------------
	// Check range hit
	//----------------------------------
	#ifdef CONTIGUITY_ENABLE	
	// Find Tag
	for (int j=0; j<MMU_NUM_WAY; j++) {

		// Check valid and Tag
		if (this->spRTLB_RMM[nSet][j]->nValid != 1) {
			continue;
		}; 

		//---------------------------------
		// Contiguity enable
		//---------------------------------
		#ifdef PAGE_SIZE_4KB
		int64_t nVPN_low  = this->spRTLB_RMM[nSet][j]->nVPN;
		int64_t nVPN_high = this->spRTLB_RMM[nSet][j]->nVPN + this->spRTLB_RMM[nSet][j]->nContiguity;

		if (nVPN_low <= nVPN and nVPN_high >= nVPN) { // Range hit. Within VPN range
			nVictimWay = j;

			// Stat
			this->nRTLB_evict_RMM++;

			#ifdef DEBUG_MMU
			assert (nVictimWay < MMU_NUM_WAY);
			assert (nVictimWay >= 0 );
			#endif

			return (nVictimWay);
		};
		#endif
	};
	#endif

	//----------------------------------
	// No range hit. Get victim using replacment algorithm 
	//----------------------------------
	// Check empty slot 
	for (int j=0; j<MMU_NUM_WAY ;j++) {
		if (this->spRTLB_RMM[nSet][j]->nValid != 1) {
			return (j);
		}
	};

	#if defined MMU_REPLACEMENT_RANDOM
	// Choose victim way randomly (0,1,...,MMU_NUM_WAY-1) 
	nVictimWay = rand() % (MMU_NUM_WAY);

	#elif defined MMU_REPLACEMENT_LRU
	// LRU: Choose victim way with minimum (least recently used) timestamp
	nVictimWay = 0;
	int nMinTimeStamp = this->spRTLB_RMM[nSet][0]->nTimeStamp;
	for (int j=1; j<MMU_NUM_WAY; j++) {
		#ifdef DEBUG_MMU
		assert (this->spRTLB_RMM[nSet][j]->nValid == 1);
		#endif
		if (nMinTimeStamp > this->spRTLB_RMM[nSet][j]->nTimeStamp) {
			nMinTimeStamp = this->spRTLB_RMM[nSet][j]->nTimeStamp;
			nVictimWay = j;	
		};
	};

	#ifdef DEBUG_MMU
	assert (nMinTimeStamp > 0);
	#endif

	#endif

	// Stat
	this->nRTLB_evict_RMM++;

	#ifdef DEBUG_MMU
	assert (nVictimWay < MMU_NUM_WAY);
	assert (nVictimWay >= 0 );
	if (MMU_NUM_WAY == 1) {
		assert (nVictimWay == 0); // Direct mapped cache
	};
	#endif

	return (nVictimWay);
};


// Find way number replacement FLPD cache 
int CMMU::GetVictimFLPD() {

	// Debug
	// this->CheckMMU();

	// Check empty slot 
	for (int j=0; j<NUM_FLPD_ENTRY; j++) {
		if (this->spFLPD[j]->nValid != 1) {
			return (j);
		};
	};

	#if defined MMU_REPLACEMENT_RANDOM
	// Choose victim way randomly (0,1,...,NUM_FLPD_ENTRY -1) 
	int nVictimWay = rand() % (NUM_FLPD_ENTRY);

	#else
	// Choose victim way with minimum (least recently used) timestamp
	int nVictimWay = 0;
	int nMinTimeStamp = this->spFLPD[0]->nTimeStamp;
	for (int j=1; j<NUM_FLPD_ENTRY; j++) {
		#ifdef DEBUG_MMU
		assert (this->spFLPD[j]->nValid == 1);
		#endif
		if (nMinTimeStamp > this->spFLPD[j]->nTimeStamp) {
			nMinTimeStamp = this->spFLPD[j]->nTimeStamp;
			nVictimWay = j;	
		};
	};

	#ifdef DEBUG_MMU
	if (MMU_NUM_WAY == 1) { // Direct mapped cache
		assert (nVictimWay == 0);
	};
	assert (nMinTimeStamp > 0);
	#endif

	// Stat
	this->nFLPD_evict++;

	#endif

	#ifdef DEBUG_MMU
	assert (nVictimWay < NUM_FLPD_ENTRY);
	assert (nVictimWay >= 0 );
	#endif

	return (nVictimWay);
};


// Get AR MO count
int CMMU::GetMO_AR() {

	#ifdef DEBUG_MMU
	assert (this->nMO_AR >= 0);
	#endif
	return (this->nMO_AR);
};


// Get AW MO count
int CMMU::GetMO_AW() {

	#ifdef DEBUG_MMU
	assert (this->nMO_AW >= 0);
	#endif
	return (this->nMO_AW);
};


// Get Ax MO count
int CMMU::GetMO_Ax() {

	#ifdef DEBUG_MMU
	assert (this->nMO_Ax >= 0);
	#endif
	return (this->nMO_Ax);
};


// Increase AR MO count 
EResultType CMMU::Increase_MO_AR() {

	this->nMO_AR++;
	return (ERESULT_TYPE_SUCCESS);
};


// Decrease AR MO count 
EResultType CMMU::Decrease_MO_AR() {

	this->nMO_AR--;
	#ifdef DEBUG_MMU
	assert (this->nMO_AR >= 0);
	#endif
	return (ERESULT_TYPE_SUCCESS);
};

// Increase AW MO count 
EResultType CMMU::Increase_MO_AW() {

	this->nMO_AW++;
	return (ERESULT_TYPE_SUCCESS);
};


// Decrease AW MO count 
EResultType CMMU::Decrease_MO_AW() {

	this->nMO_AW--;
	#ifdef DEBUG_MMU
	assert (this->nMO_AW >= 0);
	#endif
	return (ERESULT_TYPE_SUCCESS);
};


// Increase Ax MO count 
EResultType CMMU::Increase_MO_Ax() {

	this->nMO_Ax++;
	return (ERESULT_TYPE_SUCCESS);
};


// Decrease Ax MO count 
EResultType CMMU::Decrease_MO_Ax() {

	this->nMO_Ax--;
	#ifdef DEBUG_MMU
	assert (this->nMO_Ax >= 0);
	#endif
	return (ERESULT_TYPE_SUCCESS);
};


// Set start VA AR 
EResultType CMMU::Set_nAR_START_ADDR(int64_t nVA) {

	this->nAR_START_ADDR = nVA;
	return (ERESULT_TYPE_YES);
};


// Set start VA AW 
EResultType CMMU::Set_nAW_START_ADDR(int64_t nVA) {

	this->nAW_START_ADDR = nVA;
	return (ERESULT_TYPE_YES);
};


// Set start VPN 
EResultType CMMU::Set_START_VPN(int64_t nVPN) {

	this->START_VPN = nVPN;
	return (ERESULT_TYPE_YES);
};


// Set page-table 
EResultType CMMU::Set_PageTable(SPPTE* spPageTable) {

	this->spPageTable = spPageTable;
	return (ERESULT_TYPE_YES);
};


// Get TLB reach 
float CMMU::GetTLB_reach(int64_t nCycle) {

	float TLB_reach = (float)(this->nTotalPages_TLB_capacity)/nCycle; 

	return (TLB_reach);
};


// Get TLB hit rate
float CMMU::GetTLBHitRate() {

	float TLB_hit_rate = (float)( this->nHit_AR_TLB + this->nHit_AW_TLB ) / ( this->nAR_SI + this->nAW_SI );
	return (TLB_hit_rate);
};


// Get nAR_SI 
int CMMU::Get_nAR_SI() {

	return (this->nAR_SI);
};


// Get nAW_SI 
int CMMU::Get_nAW_SI() {

	return (this->nAW_SI);
};


// Get nHit_AR_TLB 
int CMMU::Get_nHit_AR_TLB() {

	return (this->nHit_AR_TLB);
};


// Get nHit_AW_TLB 
int CMMU::Get_nHit_AW_TLB() {

	return (this->nHit_AW_TLB);
};



// Get Ax pkt name
string CMMU::GetName() {

	// Debug
	// this->CheckMMU();
	return (this->cName);
};


// Debug
EResultType CMMU::CheckMMU() {

	// Check Ax priority
	if (this->Is_AR_priority == ERESULT_TYPE_YES) {
		assert (this->Is_AW_priority == ERESULT_TYPE_NO);
	}
	else {
		assert (this->Is_AR_priority == ERESULT_TYPE_NO);
		assert (this->Is_AW_priority == ERESULT_TYPE_YES);
	};

	return (ERESULT_TYPE_SUCCESS);
};


// Debug
EResultType CMMU::CheckLink() {

	assert (this->cpTx_AR != NULL);
	assert (this->cpTx_AW != NULL);
	assert (this->cpTx_W  != NULL);
	assert (this->cpRx_R  != NULL);
	assert (this->cpRx_B  != NULL);
	assert (this->cpTx_AR->GetPair() != NULL);
	assert (this->cpTx_AW->GetPair() != NULL);
	assert (this->cpTx_W ->GetPair() != NULL);
	assert (this->cpRx_R ->GetPair() != NULL);
	assert (this->cpRx_B ->GetPair() != NULL);
	assert (this->cpTx_AR->GetTRxType() != this->cpTx_AR->GetPair()->GetTRxType());
	assert (this->cpTx_AW->GetTRxType() != this->cpTx_AW->GetPair()->GetTRxType());
	assert (this->cpTx_W ->GetTRxType() != this->cpTx_W ->GetPair()->GetTRxType());
	assert (this->cpRx_R ->GetTRxType() != this->cpRx_R ->GetPair()->GetTRxType());
	assert (this->cpRx_B ->GetTRxType() != this->cpRx_B ->GetPair()->GetTRxType());
	assert (this->cpTx_AR->GetPktType() == this->cpTx_AR->GetPair()->GetPktType());
	assert (this->cpTx_AW->GetPktType() == this->cpTx_AW->GetPair()->GetPktType());
	assert (this->cpTx_W ->GetPktType() == this->cpTx_W ->GetPair()->GetPktType());
	assert (this->cpRx_R ->GetPktType() == this->cpRx_R ->GetPair()->GetPktType());
	assert (this->cpRx_B ->GetPktType() == this->cpRx_B ->GetPair()->GetPktType());
	assert (this->cpTx_AR->GetPair()->GetPair()== this->cpTx_AR);
	assert (this->cpTx_AW->GetPair()->GetPair()== this->cpTx_AW);
	assert (this->cpTx_W ->GetPair()->GetPair()== this->cpTx_W);
	assert (this->cpRx_R ->GetPair()->GetPair()== this->cpRx_R);
	assert (this->cpRx_B ->GetPair()->GetPair()== this->cpRx_B);
	
	return (ERESULT_TYPE_SUCCESS);
};


// EResultType CMMU::Display() {
//
//	// Debug
//	// this->CheckMMU();
//	return (ERESULT_TYPE_SUCCESS);
// };


// Stat 
EResultType CMMU::PrintStat(int64_t nCycle, FILE *fp) {

	// Debug
	// this->CheckMMU();

	// printf("--------------------------------------------------------\n");      
	// printf("\t MMU display\n");
	printf("--------------------------------------------------------\n");      
	printf("\t Name : %s\n", this->cName.c_str());
	printf("--------------------------------------------------------\n");      
	#ifdef MMU_OFF
	printf("\t MMU                                : OFF\n");
	#endif
	#ifdef MMU_ON_
	printf("\t MMU                                : ON\n");
	#endif
	#ifdef RMM_ENABLE
	printf("\t RMM (Redundant Memory Mapping)     : ON\n");
	#endif
	printf("\t Number of TLB sets                 : %d\n", MMU_NUM_SET);
	printf("\t Number of TLB ways                 : %d\n", MMU_NUM_WAY);
	printf("\t Number of FLPD entries             : %d\n", NUM_FLPD_ENTRY);
	printf("\t Max allowed Ax MO                  : %d\n", MAX_MO_COUNT);

	#if defined MMU_REPLACEMENT_RANDOM
	printf("\t Replacement policy                 : Random \n");
	#elif defined MMU_REPLACEMENT_FIFO
	printf("\t Replacement policy                 : FIFO \n");
	#elif defined MMU_REPLACEMENT_LRU
	printf("\t Replacement policy                 : LRU \n");
	#endif

	#if defined SINGLE_FETCH
	printf("\t PTW policy                         : Single fetch\n");
	#elif defined BLOCK_FETCH
	printf("\t PTW policy                         : Block fetch\n");
	#elif defined HALF_FETCH
	printf("\t PTW policy                         : Half block fetch\n");
	#elif defined QUARTER_FETCH
	printf("\t PTW policy                         : Quarter block fetch\n");
	#endif

	#ifdef CONTIGUITY_DISABLE
	printf("\t Contiguity                         : Disable (Reference) \n");
	#endif

	#ifdef CONTIGUITY_ENABLE
	#ifdef CONTIGUITY_0_PERCENT 
	printf("\t Contiguity                         : 0 percent \n");
	#endif
	#ifdef CONTIGUITY_25_PERCENT 
	printf("\t Contiguity                         : 25 percent \n");
	#endif
	#ifdef CONTIGUITY_50_PERCENT 
	printf("\t Contiguity                         : 50 percent \n");
	#endif
	#ifdef CONTIGUITY_75_PERCENT 
	printf("\t Contiguity                         : 75 percent \n");
	#endif
	#endif

	#ifdef VPN_SAME_PPN 
	printf("\t PPN                                : Same as VPN\n");
	#else
	printf("\t PPN                                : Random generation\n\n");
	#endif


	printf("\t Number of AR slave interface       : %d\n",		this->nAR_SI);
	printf("\t Number of AR master interface      : %d\n",		this->nAR_MI);
	printf("\t Number of AW slave interface       : %d\n",		this->nAW_SI);
	printf("\t Number of AW master interface      : %d\n\n",	this->nAW_MI);

	printf("\t------------------------------------\n");
	printf("\t Number of TLB hit AR               : %d\n",		this->nHit_AR_TLB);
	printf("\t Number of TLB hit AW               : %d\n",		this->nHit_AW_TLB);
	if (this->nAR_SI > 0) {
		printf("\t (%s) TLB hit rate AR             : %1.3f\n",	this->cName.c_str(), (float)(this->nHit_AR_TLB) / (this->nAR_SI));
	};
	if (this->nAW_SI > 0) {
		printf("\t (%s) TLB hit rate AW             : %1.3f\n",	this->cName.c_str(), (float)(this->nHit_AW_TLB) / (this->nAW_SI));
	};
	printf("\t (%s) TLB hit rate Avg              : %1.3f\n",	this->cName.c_str(), (float)( this->nHit_AR_TLB + this->nHit_AW_TLB ) / ( this->nAR_SI + this->nAW_SI ));

	printf("\t Number of FLPD hit AR              : %d\n",		this->nHit_AR_FLPD);
	printf("\t Number of FLPD hit AW              : %d\n\n",	this->nHit_AW_FLPD);
	if (this->nAR_SI > 0) {
		printf("\t FLPD hit rate AR                 : %1.3f\n",	(float)(this->nHit_AR_FLPD) / (this->nAR_SI - this->nHit_AR_TLB));
	};
	if (this->nAW_SI > 0) {
		printf("\t FLPD hit rate AW                 : %1.3f\n",	(float)(this->nHit_AW_FLPD) / (this->nAW_SI - this->nHit_AW_TLB));
	};

	printf("\t (%s) Number of PTWs total          : %d\n",		this->cName.c_str(), this->nPTW_total);
	printf("\t Number of PTWs 1st                 : %d\n",		this->nPTW_1st);
	printf("\t Number of PTWs 2nd                 : %d\n",		this->nPTW_2nd);

	#ifdef STAT_DETAIL 
	printf("\t Total cycles PTW for AR            : %d\n",		this->nPTW_AR_ongoing_cycles);
	printf("\t Total cycles PTW for AW            : %d\n\n",	this->nPTW_AW_ongoing_cycles);
	#endif

	printf("\t Number of TLB evicts               : %d\n",		this->nTLB_evict);
	printf("\t Number of FLPD evicts              : %d\n\n",	this->nFLPD_evict);

	#ifdef RMM_ENABLE	
	printf("\t------------------------------------\n");
	printf("\t Number of RTLB hit AR              : %d\n",		this->nHit_AR_RTLB_RMM);
	printf("\t Number of RTLB hit AW              : %d\n",		this->nHit_AW_RTLB_RMM);
	printf("\t (%s) RTLB hit rate AR              : %1.3f\n",	this->cName.c_str(), (float)(this->nHit_AR_RTLB_RMM) / (this->nAR_SI));
	printf("\t (%s) RTLB hit rate AW              : %1.3f\n",	this->cName.c_str(), (float)(this->nHit_AW_RTLB_RMM) / (this->nAW_SI));

	printf("\t (%s) Number of RPTWs total         : %d\n",		this->cName.c_str(), this->nRPTW_total_RMM);
	printf("\t Number of RPTWs 1st                : %d\n",		this->nRPTW_1st_RMM);
	// printf("\t Number of RPTWs 2nd             : %d\n",		this->nRPTW_2nd_RMM);
	// printf("\t Number of RPTWs 3rd             : %d\n",		this->nRPTW_3rd_RMM);

	printf("\t Number of RTLB evicts              : %d\n\n",	this->nRTLB_evict_RMM);
	printf("\t------------------------------------\n");
	#endif

	#ifdef AT_ENABLE	
	printf("\t------------------------------------\n");
	printf("\t (%s) Number of APTWs total         : %d\n",		this->cName.c_str(), this->nAPTW_total_AT);
	printf("\t Number of APTWs 1st                : %d\n",		this->nAPTW_1st_AT);
	printf("\t Number of APTWs 2nd                : %d\n",		this->nAPTW_2nd_AT);

	printf("\t------------------------------------\n");
	#endif

	#ifdef STAT_DETAIL 
	printf("\t Min AR VPN                         : 0x%lx\n",	this->AR_min_VPN);
	printf("\t Max AR VPN                         : 0x%lx\n",	this->AR_max_VPN);
	printf("\t Min AW VPN                         : 0x%lx\n",	this->AW_min_VPN);
	printf("\t Max AW VPN                         : 0x%lx\n\n",	this->AW_max_VPN);
	#endif

	#ifdef STAT_DETAIL
	printf("\t Stall cycles AR SI                 : %d\n",		this->nAR_stall_cycles_SI);
	printf("\t Stall cycles AW SI                 : %d\n\n",	this->nAW_stall_cycles_SI);
	#endif

	printf("\t Max MO AR Slave interface          : %d\n",		this->nMax_MO_AR);
	printf("\t Max MO AW Slave interface          : %d\n",		this->nMax_MO_AW);
	printf("\t Max MO Ax Slave interface          : %d\n\n",	this->nMax_MO_Ax);

	#ifdef STAT_DETAIL 
	printf("\t Total TLB capacity                 : %ld\n",		this->nTotalPages_TLB_capacity);
	printf("\t (%s) Avg TLB capacity              : %1.2f\n\n",	this->cName.c_str(), (float)(this->nTotalPages_TLB_capacity)/nCycle);

	printf("\t Max MO tracker Occupancy           : %d\n",		this->nMax_MOTracker_Occupancy);
	// printf("\t Total MO tracker Occupancy      : %d\n",		this->nTotal_MOTracker_Occupancy);
	printf("\t Avg MO tracker Occupancy           : %1.2f\n\n",	(float)(this->nTotal_MOTracker_Occupancy)/nCycle);

	printf("\t Max MO tracker alloc cycles all Ax : %d\n",		this->nMax_Tracker_Wait_Ax);
	printf("\t Avg MO tracker alloc cycles Ax     : %1.2f\n",	(float)(this->nTotal_Tracker_Wait_Ax)/(this->nAR_SI + this->nAW_SI));
	printf("\t Avg MO tracker alloc cycles AR     : %1.2f\n",	(float)(this->nTotal_Tracker_Wait_AR)/(this->nAR_SI));
	printf("\t Avg MO tracker alloc cycles AW     : %1.2f\n\n",	(float)(this->nTotal_Tracker_Wait_AW)/(this->nAW_SI));

	printf("\t Avg MO AR Slave interface          : %1.2f\n", 	(float)(this->nTotal_MO_AR)/nCycle);
	printf("\t Avg MO AW Slave interface          : %1.2f\n", 	(float)(this->nTotal_MO_AW)/nCycle);
	printf("\t Avg MO Ax Slave interface          : %1.2f\n", 	(float)(this->nTotal_MO_Ax)/nCycle);
	#endif

	// printf("--------------------------------------------------------\n");      

	// Debug
	assert (this->nMO_AR == 0);
	// assert (this->nMO_AW == 0);			// Cache evict may be in on-going. This is not an issue.
	// assert (this->nMO_Ax == 0);
	// assert (this->cpFIFO_R->GetCurNum() == 0);
	assert (this->cpFIFO_W->GetCurNum() == 0);
	// assert (this->cpFIFO_B->GetCurNum() == 0);

	#ifndef BACKGROUND_TRAFFIC_ON 
	assert (this->cpTracker->GetCurNum() == 0);
	#endif

	#ifdef BACKGROUND_TRAFFIC_ON 
	assert (this->nAR_MI == this->nAR_SI + this->nPTW_total + this->nAPTW_total_AT + this->nRPTW_total_RMM);
	#else
	assert (this->nAR_MI == this->nAR_SI + this->nPTW_total + this->nAPTW_total_AT);
	#endif


	//--------------------------------------------------------------------------
	// FILE out
	//--------------------------------------------------------------------------
	#ifdef FILE_OUT
	// fprintf(fp, "--------------------------------------------------------\n");      
	// fprintf(fp, "\t MMU display\n");
	fprintf(fp, "--------------------------------------------------------\n");      
	fprintf(fp, "\t Name: %s\n", this->cName.c_str());
	fprintf(fp, "--------------------------------------------------------\n");      
	#ifdef MMU_OFF
	fprintf(fp, "\t MMU OFF\n");
	#endif
	#ifdef MMU_ON_
	fprintf(fp, "\t MMU ON\n");
	#endif
	fprintf(fp, "\t Number of TLB sets: %d\n",     MMU_NUM_SET);
	fprintf(fp, "\t Number of TLB ways: %d\n",     MMU_NUM_WAY);
	fprintf(fp, "\t Number of FLPD entries: %d\n", NUM_FLPD_ENTRY);
	fprintf(fp, "\t Max allowed Ax MO: %d\n",      MAX_MO_COUNT);

	#if defined SINGLE_FETCH 
	fprintf(fp, "\t Page table walk policy: Single fetch\n");
	#elif defined BLOCK_FETCH 
	fprintf(fp, "\t Page table walk policy: Block fetch\n");
	#elif defined HALF_FETCH 
	fprintf(fp, "\t Page table walk policy: Half fetch\n");
	#elif defined QUARTER_FETCH 
	fprintf(fp, "\t Page table walk policy: Quarter fetch\n");
	#endif

	#if defined MMU_REPLACEMENT_RANDOM
	fprintf(fp, "\t Replacement policy: Random \n");
	#elif defined MMU_REPLACEMENT_FIFO
	fprintf(fp, "\t Replacement policy: FIFO \n");
	#elif defined MMU_REPLACEMENT_LRU
	fprintf(fp, "\t Replacement policy: LRU \n");
	#endif

	#ifdef CONTIGUITY_DISABLE
	fprintf(fp, "\t Contiguity: Disable (Reference)\n");
	#endif

	#ifdef CONTIGUITY_ENABLE
	#ifdef CONTIGUITY_0_PERCENT 
	fprintf(fp, "\t Contiguity: 0 percent \n");
	#endif
	#ifdef CONTIGUITY_25_PERCENT 
	fprintf(fp, "\t Contiguity: 25 percent \n");
	#endif
	#ifdef CONTIGUITY_50_PERCENT 
	fprintf(fp, "\t Contiguity: 50 percent \n");
	#endif
	#ifdef CONTIGUITY_75_PERCENT 
	fprintf(fp, "\t Contiguity: 75 percent \n");
	#endif
	#endif

	#ifdef VPN_SAME_PPN 
	fprintf(fp, "\t PPN same as VPN\n");
	#else
	fprintf(fp, "\t PPN random generation\n\n");
	#endif


	fprintf(fp, "\t Number of AR slave interface:  %d\n",		this->nAR_SI);
	fprintf(fp, "\t Number of AR master interface: %d\n",		this->nAR_MI);
	fprintf(fp, "\t Number of AW slave interface:  %d\n",		this->nAW_SI);
	fprintf(fp, "\t Number of AW master interface: %d\n\n",		this->nAW_MI);

	fprintf(fp, "\t Number of TLB hit for AR: %d\n",		this->nHit_AR_TLB);
	fprintf(fp, "\t Number of TLB hit for AW: %d\n",		this->nHit_AW_TLB);
	fprintf(fp, "\t TLB hit rate for AR (%s): %1.2f\n",		this->cName.c_str(), (float)(this->nHit_AR_TLB) / (this->nAR_SI));
	fprintf(fp, "\t TLB hit rate for AW (%s): %1.2f\n",		this->cName.c_str(), (float)(this->nHit_AW_TLB) / (this->nAW_SI));
	fprintf(fp, "\t Number of FLPD hit for AR: %d\n",		this->nHit_AR_FLPD);
	fprintf(fp, "\t Number of FLPD hit for AW: %d\n\n",		this->nHit_AW_FLPD);

	fprintf(fp, "\t Number of PTWs total: %d\n",			this->nPTW_total);
	fprintf(fp, "\t Number of PTWs 1st: %d\n",			this->nPTW_1st);
	fprintf(fp, "\t Number of PTWs 2nd: %d\n",			this->nPTW_2nd);
	fprintf(fp, "\t Total cycles PTW for AR: %d\n",			this->nPTW_AR_ongoing_cycles);
	fprintf(fp, "\t Total cycles PTW for AW: %d\n\n",		this->nPTW_AW_ongoing_cycles);

	fprintf(fp, "\t Number of TLB evicts: %d\n",			this->nTLB_evict);
	fprintf(fp, "\t Number of FLPD evicts:%d\n\n",			this->nFLPD_evict);

	#ifdef RMM_ENABLE	
	fprintf(fp, "\t-------------------------------\n");
	fprintf(fp, "\t Number of RTLB hit AR : %d\n",			this->nHit_AR_RTLB_RMM);
	fprintf(fp, "\t Number of RTLB hit AW : %d\n",			this->nHit_AW_RTLB_RMM);
	fprintf(fp, "\t %s RTLB hit rate AR : %1.3f\n",			this->cName.c_str(), (float)(this->nHit_AR_RTLB_RMM) / (this->nAR_SI));
	fprintf(fp, "\t %s RTLB hit rate AW : %1.3f\n",			this->cName.c_str(), (float)(this->nHit_AW_RTLB_RMM) / (this->nAW_SI));
	
	fprintf(fp, "\t Number of RPTWs 1st : %d\n",			this->nRPTW_1st_RMM);
	fprintf(fp, "\t Number of RPTWs 2nd : %d\n",			this->nRPTW_2nd_RMM);
	fprintf(fp, "\t Number of RPTWs 3rd : %d\n",			this->nRPTW_3rd_RMM);

	fprintf(fp, "\t Number of RTLB evicts : %d\n\n",		this->nRTLB_evict_RMM);
	fprintf(fp, "\t-------------------------------\n");
	#endif

	#ifdef AT_ENABLE	
	fprintf(fp, "\t------------------------------------\n");
	fprintf(fp, "\t (%s) Number of APTWs total : %d\n",		this->cName.c_str(), this->nAPTW_total_AT);
	fprintf(fp, "\t Number of APTWs 1st : %d\n",			this->nAPTW_1st_AT);
	fprintf(fp, "\t Number of APTWs 2nd : %d\n",			this->nAPTW_2nd_AT);

	fprintf(fp, "\t------------------------------------\n");
	#endif

	fprintf(fp, "\t Stall cycles AR SI: %d\n",			this->nAR_stall_cycles_SI);
	fprintf(fp, "\t Stall cycles AW SI: %d\n\n",			this->nAW_stall_cycles_SI);

	fprintf(fp, "\t Max MO AR Slave interface: %d\n",		this->nMax_MO_AR);
	fprintf(fp, "\t Max MO AW Slave interface: %d\n",		this->nMax_MO_AW);
	fprintf(fp, "\t Max MO Ax Slave interface: %d\n\n",		this->nMax_MO_Ax);

	fprintf(fp, "\t Total TLB capacity: %ld\n",			this->nTotalPages_TLB_capacity);
	fprintf(fp, "\t Avg TLB capacity (%s): %1.2f\n\n",		this->cName.c_str(), (float)(this->nTotalPages_TLB_capacity)/nCycle);

	fprintf(fp, "\t Max MO tracker Occupancy: %d\n",		this->nMax_MOTracker_Occupancy);
	// fprintf(fp, "\t Total MO tracker Occupancy: %d\n",		this->nTotal_MOTracker_Occupancy);
	fprintf(fp, "\t Avg MO tracker Occupancy: %1.2f\n\n",		(float)(this->nTotal_MOTracker_Occupancy)/nCycle);

	fprintf(fp, "\t Max MO tracker alloc cycles all Ax: %d\n",	this->nMax_Tracker_Wait_Ax);
	fprintf(fp, "\t Avg MO tracker alloc cycles Ax: %1.2f\n",	(float)(this->nTotal_Tracker_Wait_Ax)/(this->nAR_SI + this->nAW_SI));
	fprintf(fp, "\t Avg MO tracker alloc cycles AR: %1.2f\n",	(float)(this->nTotal_Tracker_Wait_AR)/(this->nAR_SI));
	fprintf(fp, "\t Avg MO tracker alloc cycles AW: %1.2f\n\n",	(float)(this->nTotal_Tracker_Wait_AW)/(this->nAW_SI));

	fprintf(fp, "\t Avg MO AR Slave interface: %1.2f\n",		(float)(this->nTotal_MO_AR)/nCycle);
	fprintf(fp, "\t Avg MO AW Slave interface: %1.2f\n",		(float)(this->nTotal_MO_AW)/nCycle);
	fprintf(fp, "\t Avg MO Ax Slave interface: %1.2f\n",		(float)(this->nTotal_MO_Ax)/nCycle);

	// fprintf(fp, "--------------------------------------------------------\n");      
	#endif

	return (ERESULT_TYPE_SUCCESS);
};

