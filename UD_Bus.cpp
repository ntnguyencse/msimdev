//-----------------------------------------------------------
// Filename     : UD_Bus.cpp 
// Version	: 0.78
// Date         : 10 Dec 2018
// Description	: Unified data definition
//-----------------------------------------------------------
// Delete_UD. Be careful.
// Example: 	Delete_UD (upAR_new, EUD_TYPE_AR); 
// 		Check upAR_new, upAR_new->cpAR, upAR_new->cpAR->spPkt deleted correctly. We can manually delete.
//-----------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <iostream>

#include "UD_Bus.h"


int GetID(UPUD upThis, EUDType eType) {

	switch (eType) {
		case EUD_TYPE_AR: 
			return (upThis->cpAR->GetID());
		case EUD_TYPE_R: 
			return (upThis->cpR->GetID());
		case EUD_TYPE_AW: 
			return (upThis->cpAW->GetID());
		case EUD_TYPE_W: 
			return (upThis->cpW->GetID());
		case EUD_TYPE_B: 
			return (upThis->cpB->GetID());
		case EUD_TYPE_UNDEFINED: 
			return (-1);
		default: 
			break;
	};
	assert(0);
	return (-1);
};


// Maintain UD
EResultType Delete_UD(UPUD upThis, EUDType eType) {

	switch (eType) {
		case EUD_TYPE_AR: 
			delete (upThis->cpAR);	// spPkt deleted in cpAR destructor
			upThis->cpAR = NULL; 
			delete (upThis);	// Check upThis itself deleted
			upThis = NULL; 
			break;
		case EUD_TYPE_AW: 
			delete (upThis->cpAW);
			upThis->cpAW = NULL;
			delete (upThis);
			upThis = NULL;
			break;
		case EUD_TYPE_W: 
			delete (upThis->cpW);
			upThis->cpW  = NULL;
			delete (upThis); 
			upThis = NULL; 
			break;
		case EUD_TYPE_R: 
			delete (upThis->cpR);  
			upThis->cpR  = NULL; 
			delete (upThis); 
			upThis = NULL; 
			break;
		case EUD_TYPE_B: 
			delete (upThis->cpB);  
			upThis->cpB  = NULL; 
			delete (upThis); 
			upThis = NULL; 
			break;
		case EUD_TYPE_UNDEFINED: 
			assert(0);
			return(ERESULT_TYPE_FAIL);
		default: 
			break;
	};

	return (ERESULT_TYPE_SUCCESS);
};


UPUD Copy_UD(UPUD upThis, EUDType eType) {

	// Generate and initialize
	UPUD upNew = new UUD;
	upNew->cpAR = NULL;
	upNew->cpR  = NULL;
	upNew->cpAW = NULL;
	upNew->cpW  = NULL;
	upNew->cpB  = NULL;
	
	switch (eType) {
		case EUD_TYPE_AR:
			// upThis->cpAR->CheckPkt();
			upNew->cpAR = Copy_CAxPkt(upThis->cpAR);
			break;
		case EUD_TYPE_R:
			// upThis->cpR->CheckPkt();
			upNew->cpR  = Copy_CRPkt(upThis->cpR);
			break;
		case EUD_TYPE_AW: 
			// upThis->cpAW->CheckPkt();
			upNew->cpAW = Copy_CAxPkt(upThis->cpAW);       
			break;
		case EUD_TYPE_W:
			// upThis->cpW->CheckPkt();
			upNew->cpW  = Copy_CWPkt(upThis->cpW);
			break;
		case EUD_TYPE_B:
			// upThis->cpB->CheckPkt();
			upNew->cpB  = Copy_CBPkt(upThis->cpB);
			break;
		case EUD_TYPE_UNDEFINED:
			assert(0);
			return (NULL);
		default:
			break;
	};

	return (upNew);
};


// Generate new pointer
// Copy all member values one-by-one
CPAxPkt Copy_CAxPkt(CPAxPkt cpThis) {

	ETransDirType eDir = cpThis->GetDir();

	CPAxPkt cpAx_new = new CAxPkt(eDir);	// New instance. Note: spPkt generated when CAxPkt generated

	// Get all member values 
	string	    cName       = cpThis->GetName();
	ETransType  eTransType  = cpThis->GetTransType();
	string	    cSrcName    = cpThis->GetSrcName();
	EResultType eFinalTrans = cpThis->IsFinalTrans();
	int	    nTransNum   = cpThis->GetTransNum();
	int64_t	    nVA         = cpThis->GetVA();
	int	    nTileNum    = cpThis->GetTileNum();

	int     nID   = cpThis->GetID();
	int64_t nAddr = cpThis->GetAddr();
	int     nLen  = cpThis->GetLen();

	// Set all member values
	cpAx_new->SetName(cName);
	cpAx_new->SetTransDirType(eDir);
	cpAx_new->SetTransType(eTransType);
	cpAx_new->SetSrcName(cSrcName);
	cpAx_new->SetFinalTrans(eFinalTrans);
	cpAx_new->SetTransNum(nTransNum);
	cpAx_new->SetVA(nVA);
	cpAx_new->SetTileNum(nTileNum);

	cpAx_new->SetPkt(nID, nAddr, nLen);
	return (cpAx_new);
};


// Generate new pointer
// Copy member values
CPRPkt Copy_CRPkt(CPRPkt cpThis) {

	CPRPkt cpR_new = new CRPkt;	// New instance

	// Get all members 
	string	     cName       = cpThis->GetName(); 
	EResultType  eFinalTrans = cpThis->IsFinalTrans();

	int nID   = cpThis->GetID();
	int nData = cpThis->GetData();
	int nLast = -1;
	if (cpThis->IsLast() == ERESULT_TYPE_YES) { 
	     nLast = 1;
	} 
	else if (cpThis->IsLast() == ERESULT_TYPE_NO) {
	     nLast = 0;
	} 
	else {
	     assert(0);
	};

	// Set members
	cpR_new->SetName(cName);
	cpR_new->SetFinalTrans(eFinalTrans);

	cpR_new->SetPkt(nID, nData, nLast);

	return (cpR_new);
};


// Generate new pointer
// Copy member values
CPWPkt Copy_CWPkt(CPWPkt cpThis) {
	
	CPWPkt cpW_new = new CWPkt;	// New instance

	// Get all members 
	string     cName      = cpThis->GetName(); 
	ETransType eTransType = cpThis->GetTransType();
	string	   cSrcName   = cpThis->GetSrcName();
	
	int nID   = cpThis->GetID();
	int nData = cpThis->GetData();
	int nLast = -1;
	if (cpThis->IsLast() == ERESULT_TYPE_YES) {
		nLast = 1;
	}
	else if (cpThis->IsLast() == ERESULT_TYPE_NO) {
		nLast = 0;
	}
	else {
		assert(0);
	};

	// Set members
	cpW_new->SetName(cName);
	cpW_new->SetTransType(eTransType);
	cpW_new->SetSrcName(cSrcName);

	cpW_new->SetPkt(nID, nData, nLast);

	return (cpW_new);
};


// Generate new pointer
// Copy member values
CPBPkt Copy_CBPkt(CPBPkt cpThis) {
	
	CPBPkt cpB_new = new CBPkt;	// New instance

	// Get all members 
	string      cName       = cpThis->GetName(); 
	int         nID         = cpThis->GetID();
	EResultType eFinalTrans = cpThis->IsFinalTrans();

	// Set members
	cpB_new->SetName(cName);
	cpB_new->SetPkt(nID);
	cpB_new->SetFinalTrans(eFinalTrans);

	return (cpB_new);
};


// Debug
EResultType Display_UD(UPUD upThis, EUDType eType) {

	switch (eType) {
		case EUD_TYPE_AR:
			upThis->cpAR->Display();
			break;
		case EUD_TYPE_R:
			upThis->cpR->Display();
			break;
		case EUD_TYPE_AW: 
			upThis->cpAW->Display(); 
			break;
		case EUD_TYPE_W:
			upThis->cpW->Display();
			break;
		case EUD_TYPE_B:
			upThis->cpB->Display();
			break;
		case EUD_TYPE_UNDEFINED:
			assert(0);
		default:
			break;
	};
	
	return (ERESULT_TYPE_SUCCESS);
};

