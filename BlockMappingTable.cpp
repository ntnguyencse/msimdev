#include "BlockMappingTable.h"

Block_Mapping_Table::Block_Mapping_Table()
{
    _head = nullptr;
    _tail = nullptr;
}

void Block_Mapping_Table::add(unsigned int startVPN, unsigned int startPPN, unsigned int belongToFreeList, SPPTE* PageTable)
{

    Block_Mapping_Node *node = new Block_Mapping_Node;

    node->_startVPN = startVPN;
    node->_startPPN = startPPN;
    node->_belongToFreeList = belongToFreeList;
    node->_PageTable = PageTable;
    node->next = nullptr;

    if (_head == nullptr)
    {
        _head = node;
        _tail = node;
    }
    else
    {
        _tail->next = node;
        _tail = node;
    }
}

Block_Mapping_Node *Block_Mapping_Table::getHead()
{
    return _head;
}

Block_Mapping_Node *Block_Mapping_Table::getTail()
{
    return _tail;
}

Block_Mapping_Table::~Block_Mapping_Table()
{
    Block_Mapping_Node *node;
    while (_head != nullptr)
    {
        node = _head->next;
        delete (_head);
        _head = node;
    }
}

void Block_Mapping_Table::dump()
{
    std::cout.width(12);
    std::cout << "Start_VPN";

    std::cout.width(12);
    std::cout << "Start_PPN";

    std::cout.width(17);
    std::cout << "BelongToFreeList";

    std::cout << std::endl;

    for (int i = 0; i < 38; i++)
        std::cout << "-";
    std::cout << std::endl;

    Block_Mapping_Node *node = _head;

    while (node != nullptr)
    {
        std::cout.width(12);
        std::cout << node->_startVPN;

        std::cout.width(12);
        std::cout << node->_startPPN;

        std::cout.width(17);
        std::cout << node->_belongToFreeList;

        std::cout << std::endl;

        node = node->next;
    }

    std::cout << std::endl;
}