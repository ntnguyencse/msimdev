//-----------------------------------------------------------
// Filename     : CMST.cpp 
// Version	: 0.79
// Date         : 18 Nov 2019
// Description	: Master definition
//-----------------------------------------------------------
// Note
// (1) int64_t nAddr: Max address is 0x7FFF_FFFF_FFFF_FFFF
//-----------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <iostream>
#include <string.h>

#include "CMST.h"

// Constructor
CMST::CMST(string cName) {

	// Generate ports 
	this->cpTx_AR = new CTRx("MST_Tx_AR", ETRX_TYPE_TX, EPKT_TYPE_AR);
	this->cpTx_AW = new CTRx("MST_Tx_AW", ETRX_TYPE_TX, EPKT_TYPE_AW);
	this->cpRx_R  = new CTRx("MST_Rx_R",  ETRX_TYPE_RX, EPKT_TYPE_R);
	this->cpTx_W  = new CTRx("MST_Tx_W",  ETRX_TYPE_TX, EPKT_TYPE_W);
	this->cpRx_B  = new CTRx("MST_Rx_B",  ETRX_TYPE_RX, EPKT_TYPE_B);

	// Generate FIFO
	this->cpFIFO_AR = new CFIFO("MST_FIFO_AR", EUD_TYPE_AR, 16);		// Jae
	this->cpFIFO_AW = new CFIFO("MST_FIFO_AW", EUD_TYPE_AW, 16);
	this->cpFIFO_W  = new CFIFO("MST_FIFO_W",  EUD_TYPE_W,  64);

	// this->cpFIFO_AR = new CFIFO("MST_FIFO_AR", EUD_TYPE_AR, 10000000);		// Debug
	// this->cpFIFO_AW = new CFIFO("MST_FIFO_AW", EUD_TYPE_AW, 10000000);

	// Addr generator
	this->cpAddrGen_AR = new CAddrGen("AddrGen_AR", ETRANS_DIR_TYPE_READ);
	this->cpAddrGen_AW = new CAddrGen("AddrGen_AW", ETRANS_DIR_TYPE_WRITE);

	// Initialize
        this->cName = cName;

        this->nAllTransFinished = -1;
        this->nARTransFinished  = -1;
        this->nAWTransFinished  = -1;

        this->eAllTransFinished = ERESULT_TYPE_UNDEFINED;
        this->eARTransFinished  = ERESULT_TYPE_UNDEFINED;
        this->eAWTransFinished  = ERESULT_TYPE_UNDEFINED;

        this->nCycle_AR_Finished = -1;
        this->nCycle_AW_Finished = -1;

	this->nARTrans = -1;
	this->nAWTrans = -1;

	this->nMO_AR = -1;
	this->nMO_AW = -1;

	// Config traffic
	this->nAR_GEN_NUM = -1;
	this->nAW_GEN_NUM = -1;

	this->nAR_START_ADDR = 0;
	this->nAW_START_ADDR = 0;

	this->ScalingFactor = -1;  

	this->AR_ISSUE_MIN_INTERVAL = -1;
	this->AW_ISSUE_MIN_INTERVAL = -1;

	// Trace
	this->Trace_rewind = -1;
};


// Destructor
CMST::~CMST() {

	// Debug
	assert (this->cpTx_AR != NULL);
	assert (this->cpTx_AW != NULL);
	assert (this->cpTx_W  != NULL);
	assert (this->cpRx_R  != NULL);
	assert (this->cpRx_B  != NULL);

	assert (this->cpFIFO_AR != NULL);
	assert (this->cpFIFO_AW != NULL);
	assert (this->cpFIFO_W  != NULL);

	assert (this->cpAddrGen_AR != NULL);
	assert (this->cpAddrGen_AW != NULL);

	delete (this->cpTx_AR);
	delete (this->cpRx_R);
	delete (this->cpTx_AW);
	delete (this->cpTx_W);
	delete (this->cpRx_B);

	delete (this->cpFIFO_AR);
	delete (this->cpFIFO_AW);
	delete (this->cpFIFO_W);

	delete (this->cpAddrGen_AR);
	delete (this->cpAddrGen_AW);

	this->cpTx_AR = NULL;
	this->cpTx_AW = NULL;
	this->cpTx_W  = NULL;
	this->cpRx_R  = NULL;
	this->cpRx_B  = NULL;

	this->cpFIFO_AR = NULL;
	this->cpFIFO_AW = NULL;
	this->cpFIFO_W  = NULL;

	this->cpAddrGen_AR = NULL;
	this->cpAddrGen_AW = NULL;
};


// Initialize
EResultType CMST::Reset() {

	this->cpTx_AR->Reset();
	this->cpTx_AW->Reset();
	this->cpTx_W ->Reset();
	this->cpRx_R ->Reset();
	this->cpRx_B ->Reset();

	this->cpFIFO_AR->Reset();	
	this->cpFIFO_AW->Reset();	
	this->cpFIFO_W ->Reset();	

	this->cpAddrGen_AR->Reset();	
	this->cpAddrGen_AW->Reset();	

        this->nAllTransFinished = 0;
        this->nARTransFinished  = 0;
        this->nAWTransFinished  = 0;

        this->eAllTransFinished = ERESULT_TYPE_NO;
        this->eARTransFinished  = ERESULT_TYPE_NO;
        this->eAWTransFinished  = ERESULT_TYPE_NO;

        this->nCycle_AR_Finished = 0;
        this->nCycle_AW_Finished = 0;

	this->nARTrans = 0;
	this->nAWTrans = 0;

	this->nMO_AR = 0;
	this->nMO_AW = 0;

	if (this->nAR_GEN_NUM == 0) {
		this->SetARTransFinished(ERESULT_TYPE_YES);	
	};

	if (this->nAW_GEN_NUM == 0) {
		this->SetAWTransFinished(ERESULT_TYPE_YES);	
	};

	this->ScalingFactor = 1;				// No scaling by default  

	this->AR_ISSUE_MIN_INTERVAL = 4;
	this->AW_ISSUE_MIN_INTERVAL = 4;

	// Trace
	this->Trace_rewind = 0;

	return (ERESULT_TYPE_SUCCESS);
};


//-------------------------------------------------------------------	
// Trace master PIN 
// 	Generate Ax and FIFO push
//-------------------------------------------------------------------	
EResultType CMST::LoadTransfer_Ax_PIN_Trace(int64_t nCycle, FILE *fp) {

	// Check trace finished
	if (feof(fp)) {
		return (ERESULT_TYPE_SUCCESS);
	};

	int     nID   = 1;
	int64_t nAddr = -1;
	int     nLen  = 3;

	// char cLine[BUFSIZ]; // BUFSIZ (8192) defined in stdio.h
	char cLine[25]; 
	char RW;
	// int  nLine  = 0;

	if (this->Trace_rewind == 1) {
		this->Trace_rewind = 0;
		// rewind(fp);			// Re-start from 1st line
		// fseek (fp, -13, SEEK_CUR);	// Offset -13 from current cursor
		fseek (fp, -21, SEEK_CUR);	// Offset -21 from current cursor. PIN_trc
	};

	//---------------------------------------------	
	// Trace format: LineNum  RW  AxAddr
	//---------------------------------------------	
        while (fgets(cLine, sizeof(cLine), fp) != NULL) { // cLine entire line.

		// Check content
		if (cLine[0] == '\n') continue; 
		// printf("%s", cLine);
	
		// Get value	
		// sscanf(cLine, "%d %c %lx",  &nLine, &RW, &nAddr);
		// printf("%d: %c %32lx \n", nLine, RW,  nAddr);
		sscanf(cLine, "%c %lx",  &RW, &nAddr);
		// printf("%c 0x%lx \n", RW, nAddr);

		// Ax
		if (RW == 'R') { // AR

			// Check all trans finished
			if (this->nARTrans == this->nAR_GEN_NUM) {
				return (ERESULT_TYPE_SUCCESS);
			};

			// Check FIFO full 
			if (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_YES) {
				this->Trace_rewind = 1;
				return (ERESULT_TYPE_SUCCESS);
			};

			// Check line
			// if (nLine <= this->nARTrans) continue;

			// Generate AR trans
			CPAxPkt cpAR_new = NULL;
			UPUD    upAR_new = NULL;

			char cARPktName[50];
			sprintf(cARPktName, "AR%d_%s", this->nARTrans+1, this->cName.c_str());
			cpAR_new = new CAxPkt(cARPktName, ETRANS_DIR_TYPE_READ);
			
			//                ID,    Addr,   Len
			cpAR_new->SetPkt(nID,   nAddr,  nLen);
			// printf("[Cycle %3ld: %s.LoadTransfer_AR_PIN_Trace] %s generated.\n", nCycle, this->cName.c_str(), cARPktName);
			cpAR_new->SetSrcName(this->cName);
			cpAR_new->SetTransNum(this->nARTrans+1);
			cpAR_new->SetTransType(ETRANS_TYPE_NORMAL);
			cpAR_new->SetVA(nAddr);
			// cpAR_new->Display();
			
			// Push
			upAR_new = new UUD;
			upAR_new->cpAR = cpAR_new;
			this->cpFIFO_AR->Push(upAR_new, 0);

			#ifdef DEBUG_MST	
			// this->cpFIFO_AR->CheckFIFO();
			// printf("R %d\t 0x%lx \n", this->nARTrans+1, nAddr);
			
			// printf("[Cycle %3ld: %s.LoadTransfer_Ax_PIN_Trace] (%s) push FIFO_AR.\n", nCycle, this->cName.c_str(), cARPktName);
			// this->cpFIFO_AR->Display();
			#endif
		
			// Maintain	
			Delete_UD(upAR_new, EUD_TYPE_AR); // Be careful to check upAR_new, upAR_new->cpAR, upAR_new->cpAR->spPkt deleted correctly. We can manually delete these.

			// Stat
			this->nARTrans++;

			// Finished this cycle
			break;
		}
		else if (RW == 'W') { // AW

			// Check all trans finished
			if (this->nAWTrans == this->nAW_GEN_NUM) {
				return (ERESULT_TYPE_SUCCESS);
			};

			// Check FIFO full 
			if (this->cpFIFO_AW->IsFull() == ERESULT_TYPE_YES) {
				this->Trace_rewind = 1;
				return (ERESULT_TYPE_SUCCESS);
			};

			// Check line
			// if (nLine <= this->nAWTrans) continue;

			// Generate AW trans
			CPAxPkt cpAW_new = NULL;
			UPUD    upAW_new = NULL;

			char cAWPktName[50];
			sprintf(cAWPktName, "AW%d_%s", this->nAWTrans+1, this->cName.c_str());
			cpAW_new = new CAxPkt(cAWPktName, ETRANS_DIR_TYPE_WRITE);
			
			//               ID,    Addr,   Len
			cpAW_new->SetPkt(nID,   nAddr,  nLen);
			// printf("[Cycle %3ld: %s.LoadTransfer_AW_PIN_Trace] %s generated.\n", nCycle, this->cName.c_str(), cAWPktName);
			cpAW_new->SetSrcName(this->cName);
			cpAW_new->SetTransNum(this->nAWTrans+1);
			cpAW_new->SetTransType(ETRANS_TYPE_NORMAL);
			cpAW_new->SetVA(nAddr);
			// cpAW_new->Display();
			
			// Push
			upAW_new = new UUD;
			upAW_new->cpAW = cpAW_new;
			this->cpFIFO_AW->Push(upAW_new, 0);

			#ifdef DEBUG_MST
			// this->cpFIFO_AW->CheckFIFO();
			// printf("W %d\t 0x%lx \n", this->nAWTrans+1, nAddr);
			
			// printf("[Cycle %3ld: %s.LoadTransfer_Ax_PIN_Trace] (%s) push FIFO_AW.\n", nCycle, this->cName.c_str(), cAWPktName);
			// this->cpFIFO_AW->Display();
			#endif
		
			// Maintain	
			Delete_UD(upAW_new, EUD_TYPE_AW);

			// Stat
			this->nAWTrans++;

			// Finished this cycle
			break;
		}
		else {
			assert (0);
		};

		// Trace
		this->Trace_rewind = 0; // read next line
	};
	
	return (ERESULT_TYPE_SUCCESS);
};


//-------------------------------------------------------------	
// Trace master AXI
//	Generate AR and FIFO push
//	FIXME When FIFO size limited, line number lost
//	FIXME fopen, fclose should be global
//-------------------------------------------------------------	
EResultType CMST::LoadTransfer_AR_AXI_Trace(int64_t nCycle) {

	// Check FIFO full 
	if (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	if (this->nARTrans == this->nAR_GEN_NUM) {
		return (ERESULT_TYPE_SUCCESS);
	};
	
	CPAxPkt cpAR_new = NULL;
	UPUD    upAR_new = NULL;
	
	char const *FileName = "AXI_trace.trc";
	FILE *fp = NULL;
	char cLine[BUFSIZ]; // BUFSIZ (8192) is defined in stdio.h
	char nLine = 0;
	
	fp = fopen(FileName, "r");
	if (!fp) {
		printf("Couldn't open file %s for reading.\n", FileName);
		return (ERESULT_TYPE_FAIL);
	}
	// printf("Opened file %s for reading.\n\n", FileName);
	
	int nCycleDelay  = -1;
	int AxID   = -1;
	int AxADDR = -1;
	int AxLEN  = -1;
	
	// printf("--------------------------------\n");
	// printf("Line: Cycle AxID   AxADDR AxLEN \n");
	// printf("--------------------------------\n");

        while (fgets(cLine, sizeof(cLine), fp) != NULL) { // Entire line

		// First six lines are comments
		if (nLine < 6) {
			nLine++;
			continue;
		};
		
		nLine++;
		
		// printf("%4d: %s", nLine, cLine); // Newline is in cLine
		
		sscanf(cLine, "%d %d %d %d", &nCycleDelay, &AxID, &AxADDR, &AxLEN);
		// printf("%4d: %4d %6d %8x   %d\n", nLine-6, nCycleDelay, AxID, ARADDR, ARLEN);
		
		// Generate AR transaction
		char cARPktName[50];
		sprintf(cARPktName, "AR%d_%s", nLine-6, this->cName.c_str());
		cpAR_new = new CAxPkt(cARPktName, ETRANS_DIR_TYPE_READ);
		
		// AxID, AxADDR, AxLEN
		int     nID   = AxID;
		int64_t nAddr = AxADDR;
		int     nLen  = AxLEN;
		
		//               ID,    Addr,   Len
		cpAR_new->SetPkt(nID,   nAddr,  nLen);
		// printf("[Cycle %3ld: %s.LoadTransfer_AR_Trace] %s generated.\n", nCycle, this->cName.c_str(), cARPktName);
		cpAR_new->SetSrcName(this->cName);
		cpAR_new->SetTransNum(nLine-6);
		cpAR_new->SetTransType(ETRANS_TYPE_NORMAL);
		cpAR_new->SetVA(nAddr);
		cpAR_new->Display();
		
		// Push
		upAR_new = new UUD;
		upAR_new->cpAR = cpAR_new;
		this->cpFIFO_AR->Push(upAR_new, nCycleDelay);

		#ifdef DEBUG_MST 
		// this->cpFIFO_AR->CheckFIFO();
		printf("[Cycle %3ld: %s.LoadTransfer_AR_AXI_Trace] (%s) push FIFO_AR.\n", nCycle, this->cName.c_str(), cARPktName);
		this->cpFIFO_AR->Display();
		#endif
	
		// Maintain	
		Delete_UD(upAR_new, EUD_TYPE_AR);
		
		// Stat
		this->nARTrans++;
	};
	
	// AR_NUM
	this->Set_nAR_GEN_NUM(nLine-6);
	
	// Stat
	// printf("\n Number of AR = %d\n", nLine-6);
	
	return (ERESULT_TYPE_SUCCESS);
};


//-------------------------------------------------------------	
// Generate AR. FIFO push
//-------------------------------------------------------------	
EResultType CMST::LoadTransfer_AR_Test(int64_t nCycle) {

	// Check master finished
	if (this->cpAddrGen_AR->IsFinalTrans() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check trans num user defined
	if (this->nARTrans == this->nAR_GEN_NUM) {
		return (ERESULT_TYPE_SUCCESS);
	};
	
	// Check FIFO full 
	if (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	CPAxPkt cpAR_new = NULL;
	UPUD	upAR_new = NULL;

	int64_t nAddr = this->nAR_START_ADDR - AR_ADDR_INC;
	// int64_t nAddr = this->nAR_START_ADDR - 0x20 ;			// Test

	for (int nIndex =0; nIndex <(this->nAR_GEN_NUM); nIndex++) {

		// Generate AR transaction
		char cARPktName[50];
		sprintf(cARPktName, "AR%d_%s", nIndex+1, this->cName.c_str());
		cpAR_new = new CAxPkt(cARPktName, ETRANS_DIR_TYPE_READ);
		
		// AxID
		int nID = ARID;
		
		#ifdef ARID_RANDOM
		nID = (rand() % 16);
		#endif
		
		// AxADDR
		nAddr += AR_ADDR_INC;
		// nAddr -= AR_ADDR_INC;	// Jae
		// nAddr += 0x20;		// Test
		
		// Random generation
		#ifdef AR_ADDR_RANDOM	
		nAddr  = ( ( (rand() % 0x7FFF) >> 6) << 6 ); // Addr signed int type.
		// nAddr  = ( ( (rand() % 0x7FFFFFFF) >> 6) << 6 ); // Addr signed int type.
		// nAddr  = ( ( (rand() % 0xFF) >> 6) << 6 );  // Addr signed int type  // Test
		#endif
		
		//               ID,    Addr,   Len
		cpAR_new->SetPkt(nID,   nAddr,  MAX_BURST_LENGTH-1);
		// printf("[Cycle %3ld: %s.LoadTransfer_AR] %s generated.\n", nCycle, this->cName.c_str(), cARPktName);
		cpAR_new->SetSrcName(this->cName);
		cpAR_new->SetTransNum(nIndex+1);
		cpAR_new->SetTransType(ETRANS_TYPE_NORMAL);
		cpAR_new->SetVA(nAddr);
		// cpAR_new->Display();		
		
		// Push
		upAR_new = new UUD;
		upAR_new->cpAR = cpAR_new;
		this->cpFIFO_AR->Push(upAR_new, MST_FIFO_LATENCY);

		#ifdef DEBUG_MST	
		// this->cpFIFO_AR->CheckFIFO();
		// printf("[Cycle %3ld: %s.LoadTransfer_AR] (%s) push FIFO_AR.\n", nCycle, this->cName.c_str(), cARPktName);
		// this->cpFIFO_AR->Display();
		#endif
	
		// Maintain	
		Delete_UD(upAR_new, EUD_TYPE_AR);
		
		// Stat
		this->nARTrans++;
	};
	return (ERESULT_TYPE_SUCCESS);
};


//-------------------------------------------------------------	
// Generate AW and FIFO push
//-------------------------------------------------------------	
EResultType CMST::LoadTransfer_AW_Test(int64_t nCycle) {

	// Check master finished
	if (this->cpAddrGen_AW->IsFinalTrans() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check trans num user defined
	if (this->nAWTrans == this->nAW_GEN_NUM) {
		return (ERESULT_TYPE_SUCCESS);
	};
	
	// Check FIFO full 
	if (this->cpFIFO_AW->IsFull() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// if (this->cpFIFO_W->IsFull() == ERESULT_TYPE_YES) {
	// 	return (ERESULT_TYPE_SUCCESS);
	// };

	int64_t nAddr = this->nAW_START_ADDR - AW_ADDR_INC;
	// int64_t nAddr = this->nAW_START_ADDR - 0x20;		// Test

	for (int nIndex =0; nIndex <(this->nAW_GEN_NUM); nIndex++) {

		// Generate AW transaction
		char cAWPktName[50];
		sprintf(cAWPktName, "AW%d_%s", nIndex+1, this->cName.c_str());

		#ifdef DEBUG_MST
		// printf("[Cycle %3ld: %s.LoadTransfer_AW] %s generated.\n", nCycle, this->cName.c_str(), cAWPktName);
		#endif
		
		CPAxPkt cpAW_new = new CAxPkt(cAWPktName, ETRANS_DIR_TYPE_WRITE);
		
		// AxID
		int nAWID = AWID;
		
		#ifdef AWID_RANDOM
		nAWID = (rand() % 16);
		#endif
		
		// AxADDR
		nAddr += AW_ADDR_INC;
		// nAddr -= AW_ADDR_INC;	// Jae
		// nAddr += 0x20;		// Test
		
		// Random generation
		#ifdef AW_ADDR_RANDOM	
		nAddr  = ( ( (rand() % 0x7FFFFFFF) >> 6) << 6 );  // Addr signed int type
		//nAddr  = ( ( (rand() % 0xFF) >> 6) << 6 );  // Addr signed int type  // Test
		#endif
		
		//               ID,    Addr,   Len
		cpAW_new->SetPkt(nAWID, nAddr,  MAX_BURST_LENGTH-1);
		cpAW_new->SetSrcName(this->cName);
		cpAW_new->SetTransNum(nIndex+1);
		cpAW_new->SetVA(nAddr);
		cpAW_new->SetTransType(ETRANS_TYPE_NORMAL);
		// cpAW_new->Display();		
		
		// Push
		UPUD upAW_new = new UUD;
		upAW_new->cpAW = cpAW_new;
		this->cpFIFO_AW->Push(upAW_new, MST_FIFO_LATENCY);

		#ifdef DEBUG_MST	
		// printf("[Cycle %3ld: %s.LoadTransfer_AW] (%s) push FIFO_AW.\n", nCycle, this->cName.c_str(), cAWPktName);
		// this->cpFIFO_AW->Display();
		// this->cpFIFO_AW->CheckFIFO();
		#endif
	
		#ifdef USE_W_CHANNEL
		// Get W info
		int nBurstNum = cpAW_new->GetLen();
		int nID       = cpAW_new->GetID();
		#endif

		// Maintain AW
		Delete_UD(upAW_new, EUD_TYPE_AW);
	
		#ifdef USE_W_CHANNEL	
		for (int i=0; i<=nBurstNum; i++) {
		
			char cWPktName[50];
			sprintf(cWPktName, "W%d_%s",this->nAWTrans+1, this->cName.c_str());
			CPWPkt  cpW_new = new CWPkt(cWPktName);
			cpW_new->SetID(nID);
			cpW_new->SetData(i+1);
			cpW_new->SetSrcName(this->cName);
			cpW_new->SetTransType(ETRANS_TYPE_NORMAL);

			if (i == nBurstNum) {
			        cpW_new->SetLast(ERESULT_TYPE_YES);
			} 
			else {
			        cpW_new->SetLast(ERESULT_TYPE_NO);
			};
			
			// Push W
			UPUD upW_new = new UUD;
			upW_new->cpW = cpW_new;
			this->cpFIFO_W->Push(upW_new, MST_FIFO_LATENCY);
			
			#ifdef DEBUG_MST	
			// upW_new->cpW->CheckPkt();
			// this->cpFIFO_W->CheckFIFO();
			// printf("[Cycle %3ld: %s.LoadTransfer_AW] (%s) push FIFO_W.\n", nCycle, this->cName.c_str(), cWPktName);
			// this->cpFIFO_W->Display();
			#endif

			// Maintain     
			Delete_UD(upW_new, EUD_TYPE_W);
		};
		#endif

		// Stat
		this->nAWTrans++;
	};
	return (ERESULT_TYPE_SUCCESS);
};


//-------------------------------------------------------------	
// 1. Generate AR 
// 2. Push FIFO
//-------------------------------------------------------------	
// 	AddrMap		: LIAM, BFAM, TILE
// 	Operation	: RASTER_SCAN, ROTATION, CNN, RANDOM
//-------------------------------------------------------------	
EResultType CMST::LoadTransfer_AR(int64_t nCycle, string cAddrMap, string cOperation) {
	
	// Check master finished
	if (this->cpAddrGen_AR->IsFinalTrans() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check trans num user defined
	if (this->nARTrans == this->nAR_GEN_NUM) {
		return (ERESULT_TYPE_SUCCESS);
	};
	
	// Check FIFO full 
	if (this->cpFIFO_AR->IsFull() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};
	
	// if (this->cpFIFO_W->IsFull() == ERESULT_TYPE_YES) {
	// 	return (ERESULT_TYPE_SUCCESS);
	// };
	
	CPAxPkt cpAR_new = NULL;
	
	// Generate
	char cARPktName[50];
	sprintf(cARPktName, "AR%d_%s",this->nARTrans+1, this->cName.c_str());
	// printf("[Cycle %3ld: %s.LoadTransfer_AR] %s generated.\n", nCycle, this->cName.c_str(), cARPktName);
	cpAR_new = new CAxPkt(cARPktName, ETRANS_DIR_TYPE_READ);

	// Get addr	 
	this->Set_AR_AddrMap(cAddrMap);
	int64_t nAddr = this->cpAddrGen_AR->GetAddr(cAddrMap);

	if (cOperation == "RANDOM") {	
		nAddr  = ( ( (rand() % 0x7FFFFFFF) >> 6) << 6 );  		// Addr signed int type
	};	

	#ifdef DEBUG
	// Check addr 
	if (nAddr == -1) {
		assert (0);							// No addr generated
		return (ERESULT_TYPE_SUCCESS);
	};
	#endif

	// Get tile num
	int nTileNum = this->cpAddrGen_AR->GetTileNum();

	// Get len
	int nNumPixelTrans = this->cpAddrGen_AR->GetNumPixelTrans(); 
	int nTransSizeByte = nNumPixelTrans * BYTE_PER_PIXEL;
	int nLen = -1;
	if      (nTransSizeByte > (BURST_SIZE * 3))	nLen = 3; 
	else if (nTransSizeByte > (BURST_SIZE * 2))	nLen = 2; 
	else if (nTransSizeByte > (BURST_SIZE * 1))	nLen = 1; 
	else if (nTransSizeByte > (BURST_SIZE * 0))	nLen = 0; 
	else    assert (0);

	// Set pkt
	//               ID,    Addr,   Len
	cpAR_new->SetPkt(ARID,  nAddr,  nLen);
	// cpAR_new->SetPkt(ARID,  nAddr,  MAX_BURST_LENGTH-1);
	cpAR_new->SetSrcName(this->cName);
	cpAR_new->SetTransNum(nARTrans + 1);
	cpAR_new->SetVA(nAddr);
	cpAR_new->SetTileNum(nTileNum);
	cpAR_new->SetTransType(ETRANS_TYPE_NORMAL);
	// printf("[Cycle %3ld: %s.LoadTransfer_AR] %s generated.\n", nCycle, this->cName.c_str(), cARPktName);
	// cpAR_new->Display();
	
	// Check final trans application
	EResultType eFinalTrans = this->cpAddrGen_AR->IsFinalTrans();
	if (eFinalTrans == ERESULT_TYPE_YES) {
		cpAR_new->SetFinalTrans(ERESULT_TYPE_YES);
	};
	
	// Push AR
	UPUD upAR_new = new UUD;
	upAR_new->cpAR = cpAR_new;
	this->cpFIFO_AR->Push(upAR_new, MST_FIFO_LATENCY);

	#ifdef DEBUG_MST
	// printf("[Cycle %3ld: %s.LoadTransfer_AR] (%s) push FIFO_AR.\n", nCycle, this->cName.c_str(), cARPktName);
	// this->cpFIFO_AR->Display();
	// this->cpFIFO_AR->CheckFIFO();
	#endif
	
	// Maintain AR
	Delete_UD(upAR_new, EUD_TYPE_AR);  // Check upAR_new correctly delete

	// Stat
	this->nARTrans++;
	
	return (ERESULT_TYPE_SUCCESS);
};


//-------------------------------------------------------------	
// 1. Generate AW
// 2. Push FIFO
//-------------------------------------------------------------	
EResultType CMST::LoadTransfer_AW(int64_t nCycle, string cAddrMap, string cOperation) { // AddrMap (LIAM, BFAM, TILE), Operation (RASTER_SCAN, ROTATION, RANDOM)
	
	// Check master finished
	if (this->cpAddrGen_AW->IsFinalTrans() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};

	if (this->nAWTrans == this->nAW_GEN_NUM) {
		return (ERESULT_TYPE_SUCCESS);
	};
	
	// Check FIFO full 
	if (this->cpFIFO_AW->IsFull() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_SUCCESS);
	};
	
	// if (this->cpFIFO_W->IsFull() == ERESULT_TYPE_YES) {
	// 	return (ERESULT_TYPE_SUCCESS);
	// };
	
	CPAxPkt cpAW_new = NULL;
	
	// Generate
	char cAWPktName[50];
	sprintf(cAWPktName, "AW%d_%s",this->nAWTrans+1, this->cName.c_str());
	// printf("[Cycle %3ld: %s.LoadTransfer_AW] %s generated.\n", nCycle, this->cName.c_str(), cAWPktName);
	cpAW_new = new CAxPkt(cAWPktName, ETRANS_DIR_TYPE_WRITE);
	
	// Get addr
	this->Set_AW_AddrMap(cAddrMap);
	int64_t nAddr = this->cpAddrGen_AW->GetAddr(cAddrMap);
	
	if (cOperation == "RANDOM") {	
		nAddr  = ( ( (rand() % 0x7FFFFFFF) >> 6) << 6 );  			// Addr signed int type
	};	

	#ifdef DEBUG
	// Check addr 
	if (nAddr == -1) {								// No addr generated
		assert (0);
		return (ERESULT_TYPE_SUCCESS);
	};
	#endif

	// Get tile num
	int nTileNum = this->cpAddrGen_AW->GetTileNum();

	// Get len
	int nNumPixelTrans = this->cpAddrGen_AW->GetNumPixelTrans(); 
	int nTransSizeByte = nNumPixelTrans * BYTE_PER_PIXEL;
	int nLen = -1;
	if      (nTransSizeByte > (BURST_SIZE * 3))	nLen = 3; 
	else if (nTransSizeByte > (BURST_SIZE * 2))	nLen = 2; 
	else if (nTransSizeByte > (BURST_SIZE * 1))	nLen = 1; 
	else if (nTransSizeByte > (BURST_SIZE * 0))	nLen = 0; 
	else    assert (0);

	//               ID,    Addr,  Len
	cpAW_new->SetPkt(AWID,  nAddr, nLen);
	// cpAW_new->SetPkt(AWID,  nAddr,  MAX_BURST_LENGTH-1);
	cpAW_new->SetSrcName(this->cName);
	cpAW_new->SetTransNum(nAWTrans + 1);
	cpAW_new->SetVA(nAddr);
	cpAW_new->SetTileNum(nTileNum);
	cpAW_new->SetTransType(ETRANS_TYPE_NORMAL);

	#ifdef DEBUG_MST
	// printf("[Cycle %3ld: %s.LoadTransfer_AW] %s generated.\n", nCycle, this->cName.c_str(), cAWPktName);
	// cpAW_new->Display();
	#endif
	
	// Check final trans application
	EResultType eFinalTrans = this->cpAddrGen_AW->IsFinalTrans();

	if (eFinalTrans == ERESULT_TYPE_YES) {
		cpAW_new->SetFinalTrans(ERESULT_TYPE_YES);
	};
	
	// Push AW
	UPUD upAW_new = new UUD;
	upAW_new->cpAW = cpAW_new;
	this->cpFIFO_AW->Push(upAW_new, MST_FIFO_LATENCY);

	#ifdef DEBUG_MST
	// printf("[Cycle %3ld: %s.LoadTransfer_AW] (%s) push FIFO_AW.\n", nCycle, this->cName.c_str(), cAWPktName);
	// this->cpFIFO_AW->Display();
	// this->cpFIFO_AW->CheckFIFO();
	#endif
	
	#ifdef USE_W_CHANNEL
	// Get info for W
	int nBurstNum = cpAW_new->GetLen();
	int nID       = cpAW_new->GetID();
	#endif
	
	// Maintain AW
	Delete_UD(upAW_new, EUD_TYPE_AW);

	#ifdef USE_W_CHANNEL    
	for (int i=0; i<=nBurstNum; i++) {
		
		char cWPktName[50];
		sprintf(cWPktName, "W%d_%s",this->nAWTrans+1, this->cName.c_str());

		CPWPkt  cpW_new = new CWPkt(cWPktName);
		cpW_new->SetID(nID);
		cpW_new->SetData(i+1);
		cpW_new->SetSrcName(this->cName);
		cpW_new->SetTransType(ETRANS_TYPE_NORMAL);

		if (i == nBurstNum) {
		        cpW_new->SetLast(ERESULT_TYPE_YES);
		}
		else {
		        cpW_new->SetLast(ERESULT_TYPE_NO);
		};
		
		// Push W
		UPUD upW_new = new UUD;
		upW_new->cpW = cpW_new;
		this->cpFIFO_W->Push(upW_new, MST_FIFO_LATENCY);
	
		#ifdef DEBUG_MST	
		// upW_new->cpW->CheckPkt();
		// this->cpFIFO_W->CheckFIFO();
		// printf("[Cycle %3ld: %s.LoadTransfer_AW] (%s) push FIFO_W.\n", nCycle, this->cName.c_str(), cWPktName);
		// this->cpFIFO_W->Display();
		// this->cpFIFO_W->CheckFIFO();
		#endif
		
		// Maintain     
		Delete_UD(upW_new, EUD_TYPE_W);
	};
	#endif
	
	// Stat
	this->nAWTrans++;
	
	return (ERESULT_TYPE_SUCCESS);
};


//------------------------------------
// AR valid
//------------------------------------
EResultType CMST::Do_AR_fwd(int64_t nCycle) {

	// Check issue interval
	if (nCycle % this->AR_ISSUE_MIN_INTERVAL != 0) {
	      return (ERESULT_TYPE_SUCCESS);  
	};

	// Check Tx valid 
	if (this->cpTx_AR->IsBusy() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_FAIL);
	};

	// Check FIFO
	if (this->cpFIFO_AR->IsEmpty() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_FAIL);
	};
	// this->cpFIFO_AR->CheckFIFO();

	// Check MO
	if (this->GetMO_AR() >= MAX_MO_COUNT) {
		return (ERESULT_TYPE_FAIL);
	};

	// Pop
	UPUD upAR_new = this->cpFIFO_AR->Pop();

	#ifdef DEBUG
	if (upAR_new == NULL) {
		assert (0);
		return (ERESULT_TYPE_SUCCESS);
	};
	#endif

	#ifdef DEBUG_MST
	assert (upAR_new != NULL);
	// this->cpFIFO_AR->CheckFIFO();
	// printf("[Cycle %3ld: %s.Do_AR_fwd] (%s) popped FIFO_AR.\n", nCycle, this->cName.c_str(), upAR_new->cpAR->GetName().c_str());
	// this->cpFIFO_AR->Display();
	#endif

	// Put Tx
	CPAxPkt cpAR_new = upAR_new->cpAR;
	this->cpTx_AR->PutAx(cpAR_new);

	#ifdef DEBUG_MST
	// printf("[Cycle %3ld: %s.Do_AR_fwd] (%s) put Tx_AR.\n", nCycle, this->cName.c_str(), upAR_new->cpAR->GetName().c_str());
	// upAR_new->cpAR->Display();
	// Debug
	// cpAR_new->CheckPkt();
	#endif

	// Maintain
	Delete_UD(upAR_new, EUD_TYPE_AR); // Check: upAR_new itself deleted

	return (ERESULT_TYPE_SUCCESS);
};


//-----------------------------------------
// AW valid
//-----------------------------------------
EResultType CMST::Do_AW_fwd(int64_t nCycle) {

	// Check issue interval
	// if ((nCycle+1) % MST_AW_ISSUE_MIN_INTERVAL != 0) { // // Every interval cycles. 5,9,13,...
	if (nCycle % AW_ISSUE_MIN_INTERVAL != 0) { // Every interval cycles. 4,8,12,... 
	      return (ERESULT_TYPE_SUCCESS);  
	};

	// Check Tx valid 
	if (this->cpTx_AW->IsBusy() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_FAIL);
	};

	// Check FIFO
	if (this->cpFIFO_AW->IsEmpty() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_FAIL);
	};

	// Check MO
	if (this->GetMO_AW() >= MAX_MO_COUNT) {
		return (ERESULT_TYPE_FAIL);
	};
	// this->cpFIFO_AW->CheckFIFO();

	// Pop
	UPUD upAW_new = this->cpFIFO_AW->Pop();
	if (upAW_new == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	#ifdef DEBUG_MST
	assert (upAW_new != NULL);
	// this->cpFIFO_AW->CheckFIFO();
	// printf("[Cycle %3ld: %s.Do_AW_fwd] (%s) popped FIFO_AW.\n", nCycle, this->cName.c_str(), upAW_new->cpAW->GetName().c_str());
	// this->cpFIFO_AW->Display();
	#endif

	// Put Tx
	CPAxPkt cpAW_new = upAW_new->cpAW;
	this->cpTx_AW->PutAx(cpAW_new);

	#ifdef DEBUG_MST
	printf("[Cycle %3ld: %s.Do_AW_fwd] (%s) put Tx_AW.\n", nCycle, this->cName.c_str(), upAW_new->cpAW->GetName().c_str());
	// upAW_new->cpAW->Display();
	// cpAW_new->CheckPkt();
	#endif

	// Maintain
	Delete_UD(upAW_new, EUD_TYPE_AW);

	return (ERESULT_TYPE_SUCCESS);
};


//------------------------------------------
// W valid
//------------------------------------------
EResultType CMST::Do_W_fwd(int64_t nCycle) {

	// Check Tx valid 
	if (this->cpTx_W->IsBusy() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_FAIL);
	};

	// Check FIFO
	if (this->cpFIFO_W->IsEmpty() == ERESULT_TYPE_YES) {
		return (ERESULT_TYPE_FAIL);
	};
	// this->cpFIFO_W->CheckFIFO();

	// Pop
	UPUD upW_new = this->cpFIFO_W->Pop();
	if (upW_new == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	#ifdef DEBUG_MST
	assert (upW_new != NULL);
	// this->cpFIFO_W->CheckFIFO();
	// printf("[Cycle %3ld: %s.Do_W_fwd] (%s) popped FIFO_W.\n", nCycle, this->cName.c_str(), upW_new->cpW->GetName().c_str());
	// this->cpFIFO_W->Display();
	#endif

	// Put Tx
	CPWPkt cpW_new = upW_new->cpW;
	this->cpTx_W->PutW(cpW_new);

	#ifdef DEBUG_MST
	printf("[Cycle %3ld: %s.Do_W_fwd] (%s) put Tx_W.\n", nCycle, this->cName.c_str(), upW_new->cpW->GetName().c_str());
	// upW_new->cpW->Display()
	// cpW_new->CheckPkt();
	#endif

	// Maintain
	Delete_UD(upW_new, EUD_TYPE_W);

	return (ERESULT_TYPE_SUCCESS);
};


//-----------------------------------------
// AR ready
//-----------------------------------------
EResultType CMST::Do_AR_bwd(int64_t nCycle) {

	// Check Tx valid 
	CPAxPkt cpAR = this->cpTx_AR->GetAx();
	if (cpAR == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check Tx ready
	EResultType eAcceptResult = this->cpTx_AR->GetAcceptResult();

	if (eAcceptResult == ERESULT_TYPE_ACCEPT) {

		#ifdef DEBUG_MST
		string cARPktName = cpAR->GetName();
		printf("[Cycle %3ld: %s.Do_AR_bwd] %s handshake Tx_AR.\n", nCycle, this->cName.c_str(), cARPktName.c_str());
		// cpAR->Display();
		#endif

		// Stat 
		this->Increase_MO_AR();

	};
	return (ERESULT_TYPE_SUCCESS);
};


//-------------------------------------------
// AW ready
//-------------------------------------------
EResultType CMST::Do_AW_bwd(int64_t nCycle) {

	// Check Tx valid 
	CPAxPkt cpAW = this->cpTx_AW->GetAx();
	if (cpAW == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check Tx ready
	EResultType eAcceptResult = this->cpTx_AW->GetAcceptResult();

	if (eAcceptResult == ERESULT_TYPE_ACCEPT) {

		#ifdef DEBUG_MST
		string cAWPktName = cpAW->GetName();
		printf("[Cycle %3ld: %s.Do_AW_bwd] %s handshake Tx_AW.\n", nCycle, this->cName.c_str(), cAWPktName.c_str());
		// cpAW->Display();
		#endif

		// Stat 
		this->Increase_MO_AW();
	};
	return (ERESULT_TYPE_SUCCESS);
};


//-----------------------------------------
// W ready
//-----------------------------------------
EResultType CMST::Do_W_bwd(int64_t nCycle) {

	// Check Tx valid 
	CPWPkt cpW = this->cpTx_W->GetW();
	if (cpW == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};

	// Check Tx ready
	EResultType eAcceptResult = this->cpTx_W->GetAcceptResult();

	if (eAcceptResult == ERESULT_TYPE_ACCEPT) {
		string cWPktName = cpW->GetName();

		#ifdef DEBUG_MST
		if (cpW->IsLast() == ERESULT_TYPE_YES) {
			printf("[Cycle %3ld: %s.Do_W_bwd] (%s) WLAST handshake Tx_W.\n", nCycle, this->cName.c_str(), cWPktName.c_str());
		} 
		else {
			printf("[Cycle %3ld: %s.Do_W_bwd] (%s) handshake Tx_W.\n", nCycle, this->cName.c_str(), cWPktName.c_str());
		};
		// cpW->Display();
		#endif
	};
	return (ERESULT_TYPE_SUCCESS);
};


//------------------------------------------
// R Valid
//------------------------------------------
EResultType CMST::Do_R_fwd(int64_t nCycle) {

        // Check Rx valid 
        // if (this->cpRx_R->IsBusy() == ERESULT_TYPE_YES) {
	//	return (ERESULT_TYPE_SUCCESS);
        // };

        // Check remote-Tx valid
        CPRPkt cpR = this->cpRx_R->GetPair()->GetR();
        if (cpR == NULL) {
                return (ERESULT_TYPE_SUCCESS);
        };

	// Debug
	// cpR->CheckPkt();
	string cPktName = cpR->GetName();

	// Put Rx
	this->cpRx_R->PutR(cpR);

	#ifdef DEBUG_MST
	printf("[Cycle %3ld: %s.Do_R_fwd] (%s) put Rx_R.\n", nCycle, this->cName.c_str(), cPktName.c_str());
	// cpR->Display();
	#endif
	
        return (ERESULT_TYPE_SUCCESS);
};


//-------------------------------------------
// B Valid
//-------------------------------------------
EResultType CMST::Do_B_fwd(int64_t nCycle) {

        // Check Rx valid 
        // if (this->cpRx_B->IsBusy() == ERESULT_TYPE_YES) {
	//	return (ERESULT_TYPE_SUCCESS);
        // };

        // Check remote-Tx valid
        CPBPkt cpB = this->cpRx_B->GetPair()->GetB();
        if (cpB == NULL) {
                return (ERESULT_TYPE_SUCCESS);
        };

	// Debug
	// cpB->CheckPkt();
	string cPktName = cpB->GetName();

	// Put Rx 
	this->cpRx_B->PutB(cpB);

	#ifdef DEBUG_MST
	printf("[Cycle %3ld: %s.Do_B_fwd] (%s) Put Rx_B.\n", nCycle, this->cName.c_str(), cPktName.c_str());
	// cpB->Display();
	#endif
		
        return (ERESULT_TYPE_SUCCESS);
};


//------------------------------------------
// R ready
//------------------------------------------
EResultType CMST::Do_R_bwd(int64_t nCycle) {

        // Check remote-Tx valid
        // CPRPkt cpR = this->cpRx_R->GetPair()->GetR();
        // if (cpR == NULL) {
	//	return (ERESULT_TYPE_SUCCESS);
        //};

	// Check Rx valid
	CPRPkt cpR = this->cpRx_R->GetR();
        if (cpR == NULL) {
                return (ERESULT_TYPE_SUCCESS);
	};

        // Set ready
	this->cpRx_R->SetAcceptResult(ERESULT_TYPE_ACCEPT);

	string cRPktName = cpR->GetName();
	// Debug 
	// cpR->CheckPkt();
	// cpR->Display();

	
	if (cpR->IsLast() == ERESULT_TYPE_YES) {

		#ifdef DEBUG_MST
		printf("[Cycle %3ld: %s.Do_R_bwd] (%s) RLAST handshake Rx_R.\n", nCycle, this->cName.c_str(), cRPktName.c_str());
		#endif

		// Stat  
		this->Decrease_MO_AR();	

		this->nAllTransFinished++;
		this->nARTransFinished++;
		
		// Check sim finish	
		// if (this->nARTransFinished == this->nAR_GEN_NUM) {	// Jae
		if (cpR->IsFinalTrans() == ERESULT_TYPE_YES) {
		        this->SetARTransFinished(ERESULT_TYPE_YES);
			this->Set_nCycle_AR_Finished(nCycle);
		        printf("[Cycle %3ld: %s.Do_R_bwd] All read transactions finished Rx_R.\n", nCycle, this->cName.c_str());
		};
		
		// if (this->nAllTransFinished >= (this->nAR_GEN_NUM + this->nAW_GEN_NUM)) {
		if (this->IsARTransFinished() == ERESULT_TYPE_YES and this->IsAWTransFinished() == ERESULT_TYPE_YES) {
		        this->SetAllTransFinished(ERESULT_TYPE_YES);
		        printf("[Cycle %3ld: %s.Do_R_bwd] All transactions finished Rx_R.\n", nCycle, this->cName.c_str());
		};
	} 
	else {
		#ifdef DEBUG_MST
		printf("[Cycle %3ld: %s.Do_R_bwd] (%s) handshake Rx_R.\n", nCycle, this->cName.c_str(), cRPktName.c_str());
		#endif
	};

        return (ERESULT_TYPE_SUCCESS);
};


//------------------------------------------
// B ready
//------------------------------------------
EResultType CMST::Do_B_bwd(int64_t nCycle) {

	// Check remote-Tx valid
	// CPBPkt cpB = this->cpRx_B->GetPair()->GetB();
	// if (cpB == NULL) {
	//	return (ERESULT_TYPE_SUCCESS);
	// };
	
	// Check Rx valid 
	CPBPkt cpB = this->cpRx_B->GetB();
	if (cpB == NULL) {
		return (ERESULT_TYPE_SUCCESS);
	};
	
	// Set ready
	this->cpRx_B->SetAcceptResult(ERESULT_TYPE_ACCEPT);

	string cBPktName = cpB->GetName();

	#ifdef DEBUG_MST
	printf("[Cycle %3ld: %s.Do_B_bwd] (%s) handshake Rx_B.\n", nCycle, this->cName.c_str(), cBPktName.c_str());
	// cpB->CheckPkt();
	// cpB->Display();
	#endif
	
	// Stat 
	this->Decrease_MO_AW();	
	
	// Count finished transactions	
	this->nAllTransFinished++;
	this->nAWTransFinished++;

	// Check sim finish	
	// if (this->nAWTransFinished == this->nAW_GEN_NUM) { // Jae
	if (cpB->IsFinalTrans() == ERESULT_TYPE_YES) {
		this->SetAWTransFinished(ERESULT_TYPE_YES);	
		this->Set_nCycle_AW_Finished(nCycle);	
		printf("[Cycle %3ld: %s.Do_B_bwd] All write transactions finished Rx_B.\n", nCycle, this->cName.c_str());
	};
	
	// if (this->nAllTransFinished >= (this->nAR_GEN_NUM + this->nAW_GEN_NUM)) {
	if (this->IsARTransFinished() == ERESULT_TYPE_YES and this->IsAWTransFinished() == ERESULT_TYPE_YES) {
		this->SetAllTransFinished(ERESULT_TYPE_YES);	
		printf("[Cycle %3ld: %s.Do_B_bwd] All transactions finished Rx_B.\n", nCycle, this->cName.c_str());
	};

	return (ERESULT_TYPE_SUCCESS);
};


// Debug
EResultType CMST::CheckLink() {

	assert (this->cpTx_AR != NULL);
	assert (this->cpTx_AW != NULL);
	assert (this->cpTx_W  != NULL);
	assert (this->cpRx_R  != NULL);
	assert (this->cpRx_B  != NULL);
	assert (this->cpTx_AR->GetPair() != NULL);
	assert (this->cpTx_AW->GetPair() != NULL);
	assert (this->cpTx_W->GetPair()  != NULL);
	assert (this->cpRx_R ->GetPair() != NULL);
	assert (this->cpRx_B ->GetPair() != NULL);
	assert (this->cpTx_AR->GetTRxType() != this->cpTx_AR->GetPair()->GetTRxType());
	assert (this->cpTx_AW->GetTRxType() != this->cpTx_AW->GetPair()->GetTRxType());
	assert (this->cpTx_W ->GetTRxType() != this->cpTx_W ->GetPair()->GetTRxType());
	assert (this->cpRx_R ->GetTRxType() != this->cpRx_R ->GetPair()->GetTRxType());
	assert (this->cpRx_B ->GetTRxType() != this->cpRx_B ->GetPair()->GetTRxType());
	assert (this->cpTx_AR->GetPktType() == this->cpTx_AR->GetPair()->GetPktType());
	assert (this->cpTx_AW->GetPktType() == this->cpTx_AW->GetPair()->GetPktType());
	assert (this->cpTx_W ->GetPktType() == this->cpTx_W ->GetPair()->GetPktType());
	assert (this->cpRx_R ->GetPktType() == this->cpRx_R ->GetPair()->GetPktType());
	assert (this->cpRx_B ->GetPktType() == this->cpRx_B ->GetPair()->GetPktType());
	assert (this->cpTx_AR->GetPair()->GetPair()== this->cpTx_AR);
	assert (this->cpTx_AW->GetPair()->GetPair()== this->cpTx_AW);
	assert (this->cpTx_W ->GetPair()->GetPair()== this->cpTx_W);
	assert (this->cpRx_R ->GetPair()->GetPair()== this->cpRx_R);
	assert (this->cpRx_B ->GetPair()->GetPair()== this->cpRx_B);

        return (ERESULT_TYPE_SUCCESS);
};


// Set all transactions finished
EResultType CMST::SetAllTransFinished(EResultType eResult) {  
	this->eAllTransFinished = eResult;
        return (ERESULT_TYPE_SUCCESS);
};


// Set AR transactions finished
EResultType CMST::SetARTransFinished(EResultType eResult) {  
	this->eARTransFinished = eResult;
        return (ERESULT_TYPE_SUCCESS);
};


// Set AW transactions finished
EResultType CMST::SetAWTransFinished(EResultType eResult) {  

	this->eAWTransFinished = eResult;
        return (ERESULT_TYPE_SUCCESS);
};


// Check all transactions finished
EResultType CMST::IsAllTransFinished() {  

	if (this->eAllTransFinished == ERESULT_TYPE_YES) {
        	return (ERESULT_TYPE_YES);
	};
        return (ERESULT_TYPE_NO);
};


// Check AR transactions finished
EResultType CMST::IsARTransFinished() {  

	if (this->eARTransFinished == ERESULT_TYPE_YES) {
        	return (ERESULT_TYPE_YES);
	};
        return (ERESULT_TYPE_NO);
};


// Check AW transactions finished
EResultType CMST::IsAWTransFinished() {  

	if (this->eAWTransFinished == ERESULT_TYPE_YES) {
        	return (ERESULT_TYPE_YES);
	};
        return (ERESULT_TYPE_NO);
};


// Set cycle AR finished 
EResultType CMST::Set_nCycle_AR_Finished(int64_t nCycle) {  

	this->nCycle_AR_Finished = nCycle;
       	return (ERESULT_TYPE_YES);
};


// Set cycle AW finished 
EResultType CMST::Set_nCycle_AW_Finished(int64_t nCycle) {  

	this->nCycle_AW_Finished = nCycle;
       	return (ERESULT_TYPE_YES);
};


// Set number total AR 
EResultType CMST::Set_nAR_GEN_NUM(int nNum) {  

	this->nAR_GEN_NUM = nNum;
	this->cpAddrGen_AR->SetNumTotalTrans(nNum);
       	return (ERESULT_TYPE_YES);
};


// Set number total AW 
EResultType CMST::Set_nAW_GEN_NUM(int nNum) {  

	this->nAW_GEN_NUM = nNum;
	this->cpAddrGen_AW->SetNumTotalTrans(nNum);
       	return (ERESULT_TYPE_YES);
};


// Set image horizontal size scaling factor 
EResultType CMST::Set_ScalingFactor(float Num) {

	this->ScalingFactor = Num;
	this->cpAddrGen_AR->Set_ScalingFactor(Num);
       	return (ERESULT_TYPE_YES);
};


// Set issue rate 
EResultType CMST::Set_AR_ISSUE_MIN_INTERVAL(int nNum) {

	this->AR_ISSUE_MIN_INTERVAL = nNum;
       	return (ERESULT_TYPE_YES);
};


// Set issue rate 
EResultType CMST::Set_AW_ISSUE_MIN_INTERVAL(int nNum) {

	this->AW_ISSUE_MIN_INTERVAL = nNum;
       	return (ERESULT_TYPE_YES);
};


// Set start address AR 
EResultType CMST::Set_nAR_START_ADDR(int64_t nAddr) {  

	this->nAR_START_ADDR = nAddr;  // LoadTransfer_AR_Test()
	this->cpAddrGen_AR->SetStartAddr(nAddr);
       	return (ERESULT_TYPE_YES);
};


// Set start address AW 
EResultType CMST::Set_nAW_START_ADDR(int64_t nAddr) {  

	this->nAW_START_ADDR = nAddr;  // LoadTransfer_AW_Test()
	this->cpAddrGen_AW->SetStartAddr(nAddr);
       	return (ERESULT_TYPE_YES);
};


// Set address map AR 
EResultType CMST::Set_AR_AddrMap(string cAddrMap) {

	this->cpAddrGen_AR->SetAddrMap(cAddrMap);
       	return (ERESULT_TYPE_YES);
};


// Set address map AW 
EResultType CMST::Set_AW_AddrMap(string cAddrMap) {

	this->cpAddrGen_AW->SetAddrMap(cAddrMap);
       	return (ERESULT_TYPE_YES);
};


// Set operation AR 
EResultType CMST::Set_AR_Operation(string cOperation) {

	this->cpAddrGen_AR->SetOperation(cOperation);
       	return (ERESULT_TYPE_YES);
};


// Set operation AW 
EResultType CMST::Set_AW_Operation(string cOperation) {

	this->cpAddrGen_AW->SetOperation(cOperation);
       	return (ERESULT_TYPE_YES);
};


// Get cycle AR finished 
int64_t CMST::Get_nCycle_AR_Finished() {  

	return (this->nCycle_AR_Finished);
};


// Get cycle AW finished 
int64_t CMST::Get_nCycle_AW_Finished() {  

	return (this->nCycle_AW_Finished);
};


// Increase AR MO count 
EResultType CMST::Increase_MO_AR() {  

	this->nMO_AR++;
        return (ERESULT_TYPE_SUCCESS);
};


// Decrease AR MO count 
EResultType CMST::Decrease_MO_AR() {  

	this->nMO_AR--;

	#ifdef DEBUG
	assert (this->nMO_AR >= 0);
	#endif

        return (ERESULT_TYPE_SUCCESS);
};


// Increase AW MO count 
EResultType CMST::Increase_MO_AW() {  

	this->nMO_AW++;
        return (ERESULT_TYPE_SUCCESS);
};


// Decrease AW MO count 
EResultType CMST::Decrease_MO_AW() {  

	this->nMO_AW--;

	#ifdef DEBUG
	assert (this->nMO_AW >= 0);
	#endif

        return (ERESULT_TYPE_SUCCESS);
};


// Get AR MO count
int CMST::GetMO_AR() {

	#ifdef DEBUG
	assert (this->nMO_AR >= 0);
	#endif

	return (this->nMO_AR);
};


// Get AW MO count
int CMST::GetMO_AW() {

	#ifdef DEBUG
	assert (this->nMO_AW >= 0);
	#endif

	return (this->nMO_AW);
};


// Update state
EResultType CMST::UpdateState(int64_t nCycle) {  

        this->cpTx_AR   ->UpdateState();
	this->cpTx_AW   ->UpdateState();
	// this->cpTx_W ->UpdateState();
        this->cpRx_R    ->UpdateState();
        this->cpRx_B    ->UpdateState();

	this->cpAddrGen_AR->UpdateState();
	this->cpAddrGen_AW->UpdateState();
	
	// this->cpFIFO_AR->UpdateState(); // nLatency 0
	// this->cpFIFO_AW->UpdateState();
	// this->cpFIFO_W ->UpdateState();

        return (ERESULT_TYPE_SUCCESS);
}; 


// Stat 
EResultType CMST::PrintStat(int64_t nCycle, FILE *fp) {
	
	printf("--------------------------------------------------------\n");
	printf("\t Name: %s\n",	this->cName.c_str());
	printf("--------------------------------------------------------\n");
	printf("\t Number of AR                   : %d\n",	this->nARTrans);
	printf("\t Number of AW                   : %d\n",	this->nAWTrans);

	// printf("\t Max allowed AR MO           : %d\n",	MAX_MO_COUNT);
	// printf("\t Max allowed AW MO           : %d\n\n",	MAX_MO_COUNT);

	printf("\t Cycle to finish read           : %ld\n",	this->nCycle_AR_Finished);
	printf("\t Cycle to finish write          : %ld\n\n",	this->nCycle_AW_Finished);

	#ifdef METRIC_ANALYSIS_ENABLE
	printf("\t (%s) Avg tile bank metric AR   : %1.2f\n",	this->cName.c_str(), (float)(this->cpAddrGen_AR->GetTileBankMetric())   / (IMG_HORIZONTAL_SIZE * IMG_VERTICAL_SIZE / (TILEH * TILEV) ));
	printf("\t (%s) Avg tile bank metric AW   : %1.2f\n\n",	this->cName.c_str(), (float)(this->cpAddrGen_AW->GetTileBankMetric())   / (IMG_HORIZONTAL_SIZE * IMG_VERTICAL_SIZE / (TILEH * TILEV) ));

	printf("\t (%s) Avg linear bank metric AR : %1.2f\n",	this->cName.c_str(), (float)(this->cpAddrGen_AR->GetLinearBankMetric()) / (IMG_HORIZONTAL_SIZE * IMG_VERTICAL_SIZE / PIXELS_PER_TRANS ));
	printf("\t (%s) Avg linear bank metric AW : %1.2f\n\n",	this->cName.c_str(), (float)(this->cpAddrGen_AW->GetLinearBankMetric()) / (IMG_HORIZONTAL_SIZE * IMG_VERTICAL_SIZE / PIXELS_PER_TRANS ));
	#endif

	// printf("\t Address map AR              : %s\n",	this->cpAddrGen_AR->GetAddrMap().c_str());
	// printf("\t Address map AW              : %s\n\n",	this->cpAddrGen_AW->GetAddrMap().c_str());

	printf("\t Operation AR                   : %s\n",	this->cpAddrGen_AR->GetOperation().c_str());
	printf("\t Operation AW                   : %s\n\n",	this->cpAddrGen_AW->GetOperation().c_str());
	
	// printf("\t Utilization of R channel    : %1.2f\n",	(float)(this->nR)/nCycle);
	// printf("\t Utilization of W channel    : %1.2f\n\n", (float)(this->nW)/nCycle);
	
	// printf("--------------------------------------------------------\n");

	// Debug
	assert (this->nMO_AR == 0);
	assert (this->nMO_AW == 0);
	assert (this->nARTransFinished  == this->nARTrans);
	assert (this->nAWTransFinished  == this->nAWTrans);
	assert (this->nAllTransFinished == this->nARTransFinished + this->nAWTransFinished);
	assert (this->nAllTransFinished == this->nARTrans + this->nAWTrans);

	if (this->nAR_GEN_NUM > 0) {
		assert (this->IsARTransFinished()  == ERESULT_TYPE_YES);
		// assert (this->IsAllTransFinished() == ERESULT_TYPE_YES);
	};
	if (this->nAW_GEN_NUM > 0) {
		assert (this->IsAWTransFinished()  == ERESULT_TYPE_YES);
		// assert (this->IsAllTransFinished() == ERESULT_TYPE_YES);
	};

	assert (this->cpFIFO_AR->GetCurNum() == 0);
	assert (this->cpFIFO_AW->GetCurNum() == 0);
	assert (this->cpFIFO_W->GetCurNum()  == 0);


	//--------------------------------------------------------------	
	// FILE out
	//--------------------------------------------------------------	
	#ifdef FILE_OUT
	fprintf(fp, "--------------------------------------------------------\n");
	fprintf(fp, "\t Name : %s\n",				 this->cName.c_str());
	fprintf(fp, "--------------------------------------------------------\n");
	fprintf(fp, "\t Number of AR                : %d\n",	 this->nARTrans);
	fprintf(fp, "\t Number of AW                : %d\n",	 this->nAWTrans);

	fprintf(fp, "\t Max allowed AR MO           : %d\n",	 MAX_MO_COUNT);
	fprintf(fp, "\t Max allowed AW MO           : %d\n\n",	 MAX_MO_COUNT);

	fprintf(fp, "\t Cycle to finish read        : %ld\n",	 this->nCycle_AR_Finished);
	fprintf(fp, "\t Cycle to finish write       : %ld\n",	 this->nCycle_AW_Finished);
	
	// fprintf(fp, "\t Utilization of R channel : %1.2f\n",	 (float)(this->nR)/nCycle);
	// fprintf(fp, "\t Utilization of W channel : %1.2f\n\n",(float)(this->nW)/nCycle);
	
	// fprintf(fp, "--------------------------------------------------------\n");
	#endif

	return (ERESULT_TYPE_SUCCESS);
};

